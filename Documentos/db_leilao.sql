-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Tempo de geração: 07/10/2018 às 00:39
-- Versão do servidor: 5.7.23
-- Versão do PHP: 7.2.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `db_leilao`
--

-- --------------------------------------------------------

--
-- Estrutura para tabela `cad_avaliacoes`
--

CREATE TABLE `cad_avaliacoes` (
  `id_avalicao` int(11) NOT NULL,
  `fk_cliente` int(11) NOT NULL,
  `fk_leiloeiro` int(11) NOT NULL,
  `fk_leilao` int(11) NOT NULL,
  `avaliacao` int(11) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura para tabela `cad_categorias`
--

CREATE TABLE `cad_categorias` (
  `id_categoria` int(11) NOT NULL,
  `fk_categoria_pai` int(11) DEFAULT NULL,
  `nome_categoria` varchar(255) NOT NULL,
  `ativa` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Fazendo dump de dados para tabela `cad_categorias`
--

INSERT INTO `cad_categorias` (`id_categoria`, `fk_categoria_pai`, `nome_categoria`, `ativa`) VALUES
(1, NULL, 'Eletrônicos', 1),
(2, 1, 'Games', 1),
(3, 1, 'Computadores', 1),
(4, 1, 'Eletrodomésticos', 1),
(5, 1, 'Celular', 1),
(6, 1, 'Tablet', 1),
(7, NULL, 'Livros', 1),
(8, NULL, 'Carro', 1),
(9, 8, 'Acessórios', 1),
(10, NULL, 'Roupas', 1),
(11, NULL, 'Casa', 1),
(12, NULL, 'Perfumes', 1);

-- --------------------------------------------------------

--
-- Estrutura para tabela `cad_compra_pacotes`
--

CREATE TABLE `cad_compra_pacotes` (
  `id` int(11) NOT NULL,
  `title` text,
  `quantity` int(11) DEFAULT NULL,
  `currency_id` varchar(3) DEFAULT NULL,
  `unit_price` double DEFAULT NULL,
  `date_buy` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fk_pacote` int(11) DEFAULT NULL,
  `fk_usuario` int(11) DEFAULT NULL,
  `quantidade_ingresso` int(11) DEFAULT NULL,
  `link_compra` text,
  `id_consulta_preference` text,
  `topic` text,
  `finalizado` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura para tabela `cad_compra_recompensa`
--

CREATE TABLE `cad_compra_recompensa` (
  `id_compra_recompensa` int(11) NOT NULL,
  `fk_produto` int(11) DEFAULT NULL,
  `fk_usuario` int(11) DEFAULT NULL,
  `data_compra` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `entregue` tinyint(1) DEFAULT '0',
  `data_entrega` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura para tabela `cad_creditos`
--

CREATE TABLE `cad_creditos` (
  `id_creditos` int(11) NOT NULL,
  `fk_usuario` int(11) DEFAULT NULL,
  `creditos` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura para tabela `cad_detalhes_views`
--

CREATE TABLE `cad_detalhes_views` (
  `id_detalhes_views` int(11) NOT NULL,
  `nome_view` text,
  `nome_campo` text,
  `tipo_campo` text,
  `descricao_campo` text,
  `visivel` tinyint(1) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura para tabela `cad_favoritos`
--

CREATE TABLE `cad_favoritos` (
  `fk_leilao` int(11) NOT NULL,
  `fk_usuario` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura para tabela `cad_grupos`
--

CREATE TABLE `cad_grupos` (
  `id_grupo` int(11) NOT NULL,
  `nome_grupo` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Fazendo dump de dados para tabela `cad_grupos`
--

INSERT INTO `cad_grupos` (`id_grupo`, `nome_grupo`) VALUES
(1, 'Estados'),
(2, 'Generos');

-- --------------------------------------------------------

--
-- Estrutura para tabela `cad_hist_notificacao`
--

CREATE TABLE `cad_hist_notificacao` (
  `id_hist_notificacao` int(11) NOT NULL,
  `fk_usuario_destino` int(11) DEFAULT NULL,
  `fk_notificacao` int(11) DEFAULT NULL,
  `data_envio_notificacao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `data_leitura` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `notificacao_enviada` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura para tabela `cad_ingressos`
--

CREATE TABLE `cad_ingressos` (
  `id_ingresso` int(11) NOT NULL,
  `fk_usuario` int(11) NOT NULL,
  `quantidade` int(11) NOT NULL DEFAULT '0',
  `ativo` tinyint(4) DEFAULT '0',
  `prioridade` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura para tabela `cad_item_grupo`
--

CREATE TABLE `cad_item_grupo` (
  `id_item_grupo` int(11) NOT NULL,
  `fk_grupo` int(11) DEFAULT NULL,
  `nome_item_grupo` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Fazendo dump de dados para tabela `cad_item_grupo`
--

INSERT INTO `cad_item_grupo` (`id_item_grupo`, `fk_grupo`, `nome_item_grupo`) VALUES
(1, 1, 'AC'),
(2, 1, 'AL'),
(3, 1, 'AP'),
(4, 1, 'AM'),
(5, 1, 'BA'),
(6, 1, 'CE'),
(7, 1, 'DF'),
(8, 1, 'ES'),
(9, 1, 'GO'),
(10, 1, 'MA'),
(11, 1, 'MT'),
(12, 1, 'MS'),
(13, 1, 'MG'),
(14, 1, 'PA'),
(15, 1, 'PB'),
(16, 1, 'PR'),
(17, 1, 'PE'),
(18, 1, 'PI'),
(19, 1, 'RJ'),
(20, 1, 'RN'),
(21, 1, 'RS'),
(22, 1, 'RO'),
(23, 1, 'RR'),
(24, 1, 'SC'),
(25, 1, 'SP'),
(26, 1, 'SE'),
(27, 1, 'TO'),
(28, 2, 'Masculino'),
(29, 2, 'Feminino');

-- --------------------------------------------------------

--
-- Estrutura para tabela `cad_lances`
--

CREATE TABLE `cad_lances` (
  `id_lances` int(11) NOT NULL,
  `valor` double NOT NULL,
  `data_lance` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fk_sala_leilao` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura para tabela `cad_leilao`
--

CREATE TABLE `cad_leilao` (
  `id_leilao` int(11) NOT NULL,
  `fk_produto` int(11) NOT NULL,
  `valor_minimo` double DEFAULT NULL,
  `lance_minimo` double NOT NULL DEFAULT '0.01',
  `ingressos` int(11) NOT NULL DEFAULT '1',
  `data_inicio` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `data_fim_previsto` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `status_leilao` int(11) NOT NULL,
  `fk_usuario_arrematou` int(11) DEFAULT NULL,
  `quantidade_minima_usuarios` int(11) DEFAULT NULL,
  `tempo_iniciar` int(11) DEFAULT NULL,
  `data_fim_efetivo` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `notificado` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura para tabela `cad_notificacao`
--

CREATE TABLE `cad_notificacao` (
  `id_notificacao` int(11) NOT NULL,
  `fk_usuario` int(11) DEFAULT NULL,
  `data_notificacao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `titulo_notificacao` varchar(20) DEFAULT NULL,
  `notificacao` text,
  `data_limite_notificacao` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura para tabela `cad_produtos`
--

CREATE TABLE `cad_produtos` (
  `id_produto` int(11) NOT NULL,
  `fk_categoria` int(11) NOT NULL,
  `nome_produto` varchar(255) NOT NULL,
  `produto_usado` tinyint(1) DEFAULT NULL,
  `descricao_produto` text,
  `fk_usuario` int(11) NOT NULL,
  `produto_recompensa` tinyint(1) DEFAULT '0',
  `produto_recompensa_custo` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura para tabela `cad_tokens`
--

CREATE TABLE `cad_tokens` (
  `id_token` int(11) NOT NULL,
  `token` varchar(300) DEFAULT NULL,
  `fk_usuario` int(11) DEFAULT NULL,
  `aparelho` int(11) DEFAULT NULL,
  `data_registro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura para tabela `compra_ingresso`
--

CREATE TABLE `compra_ingresso` (
  `id_compra_ingresso` int(11) NOT NULL,
  `descricao` varchar(60) DEFAULT NULL,
  `fk_ingresso` int(11) NOT NULL,
  `data_compra` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `valor` double NOT NULL,
  `code_pagamento` text,
  `fk_pacote` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura para tabela `hist_compra_pacotes`
--

CREATE TABLE `hist_compra_pacotes` (
  `id_hist_compra_pacotes` int(11) NOT NULL,
  `fk_compra_pacotes` int(11) DEFAULT NULL,
  `id` text,
  `transaction_amount` double DEFAULT NULL,
  `total_paid_amount` double DEFAULT NULL,
  `shipping_cost` double DEFAULT NULL,
  `currency_id` varchar(3) DEFAULT NULL,
  `status` text,
  `status_detail` text,
  `operation_type` text,
  `date_approved` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_created` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `amount_refunded` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura para tabela `pacote_ingresso`
--

CREATE TABLE `pacote_ingresso` (
  `id_pacote_ingresso` int(11) NOT NULL,
  `descricao_pacote` varchar(45) NOT NULL,
  `quantidade_ingresso` int(11) NOT NULL,
  `valor_pacote` double NOT NULL,
  `gratis` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura para tabela `sala_leilao`
--

CREATE TABLE `sala_leilao` (
  `id_sala_leilao` int(11) NOT NULL,
  `fk_leilao` int(11) DEFAULT NULL,
  `fk_usuario` int(11) DEFAULT NULL,
  `custo_entrada` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura para tabela `seg_aplicacao`
--

CREATE TABLE `seg_aplicacao` (
  `id_aplicacao` int(11) NOT NULL,
  `link_aplicacao` varchar(100) NOT NULL,
  `titulo_aplicacao` varchar(100) NOT NULL,
  `descricao_aplicacao` text NOT NULL,
  `fk_controller` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Fazendo dump de dados para tabela `seg_aplicacao`
--

INSERT INTO `seg_aplicacao` (`id_aplicacao`, `link_aplicacao`, `titulo_aplicacao`, `descricao_aplicacao`, `fk_controller`) VALUES
(1, 'seguranca/view_editar_perfil', 'Editar Perfil', 'Editar Perfil', 2),
(2, 'seguranca/view_usuarios', 'Listar Usuários ADM', 'Listar Usuários ADM', 2),
(3, 'seguranca/view_grupos', 'Listar Grupos', 'Listar Grupos', 3),
(4, 'seguranca/view_editar_grupo', 'Editar Grupo', 'Editar Grupo', 3),
(5, 'seguranca/view_editar_usuario', 'Editar Usuário', 'Editar Usuários', 2),
(6, 'seguranca/view_novo_grupo', 'Novo Grupo', 'Novo Grupo', 3),
(7, 'seguranca/view_novo_usuario', 'Novo Usuário', 'Novo Usuários', 2),
(8, 'notificacoes/view_notificacoes', 'Notificações', 'Notificações', 4),
(9, 'notificacoes/view_hist_notificacoes', 'Minhas Notificações', 'Minhas Notificações', 4),
(10, 'notificacoes/view_notificar', 'Nova Notificação', 'Nova Notificação', 4),
(11, 'notificacoes/view_editar_notificacao', 'Editar Notificação', 'Editar Notificação', 4),
(12, 'relatorios/view_relatorio_acesso', 'Acessos', 'Relatório dos Acessos', 5),
(13, 'relatorios/view_relatorio_navegacao', 'Navegação', 'Relatório de Navegação', 5),
(14, 'relatorios/view_relatorio_edicoes', 'Edições', 'Relatório de Edições', 5),
(15, 'categorias/view_categorias', 'Categorias', 'Categorias', 6),
(16, 'leilao/view_lista_leilao_pendente', 'Leilões Pendentes', 'View para listar leilões pendentes de aprovação', 7),
(17, 'leilao/view_lista_leilao_ativo', 'Leilões Ativos', 'Leilões Ativos', 7),
(18, 'leilao/view_lista_leilao_finalizado', 'Leilões Finalizados', 'Leilões Finalizados', 7),
(19, 'leilao/view_lista_leiloes', 'Todos Leilões', 'Listar todos leilões', 7),
(20, 'leilao/view_novo_leiloes', 'Criar Leilões', 'Criar leilões', 7),
(21, 'produto/view_lista_produtos', 'Todos produtos', 'Todos produtos', 8),
(22, 'produto/view_novo_produto', 'Criar produto', 'Criar produto', 8),
(23, 'produto/view_editar_produto', 'Editar produto', 'Editar produto', 8),
(24, 'pacote/view_lista_pacotes', 'Todos pacotes', 'Todos pacotes', 9),
(25, 'pacote/view_novo_pacote', 'Criar pacote', 'Criar pacote', 9),
(26, 'pacote/view_editar_pacote', 'Editar pacote', 'Editar pacote', 9),
(27, 'seguranca/view_usuarios_geral', 'Listar Geral Usuários', 'Listar Usuários ADM', 2),
(28, 'leilao/view_editar_leilao', 'Editar Leilões', 'Editar leilões', 7),
(29, 'produto/view_listar_recompensas', 'Recompensas', 'Recompensas', 8),
(30, 'pacote/view_vendas_pacote', 'Vendas pacote', 'Vendas pacote', 9);

-- --------------------------------------------------------

--
-- Estrutura para tabela `seg_aplicacoes_grupos`
--

CREATE TABLE `seg_aplicacoes_grupos` (
  `id_aplicacoes_grupos` int(11) NOT NULL,
  `fk_grupo` int(11) NOT NULL,
  `fk_aplicacao` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Fazendo dump de dados para tabela `seg_aplicacoes_grupos`
--

INSERT INTO `seg_aplicacoes_grupos` (`id_aplicacoes_grupos`, `fk_grupo`, `fk_aplicacao`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 1, 5),
(4, 1, 6),
(5, 1, 7),
(6, 1, 10),
(7, 1, 15),
(8, 1, 16),
(9, 1, 17),
(10, 1, 18),
(11, 1, 19),
(12, 1, 20),
(13, 1, 21),
(14, 1, 22),
(15, 1, 23),
(16, 1, 24),
(17, 1, 25),
(18, 1, 26),
(19, 1, 27),
(20, 1, 28),
(21, 1, 29),
(22, 1, 30);

-- --------------------------------------------------------

--
-- Estrutura para tabela `seg_aplicacoes_menu`
--

CREATE TABLE `seg_aplicacoes_menu` (
  `id_aplicacoes_menu` int(11) NOT NULL,
  `fk_aplicacao` int(11) NOT NULL,
  `fk_menu` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Fazendo dump de dados para tabela `seg_aplicacoes_menu`
--

INSERT INTO `seg_aplicacoes_menu` (`id_aplicacoes_menu`, `fk_aplicacao`, `fk_menu`) VALUES
(1, 1, 2),
(2, 2, 3),
(3, 3, 4),
(4, 8, 5),
(5, 9, 6),
(6, 10, 6),
(7, 12, 8),
(8, 13, 9),
(9, 14, 10),
(10, 15, 11),
(11, 16, 13),
(12, 17, 14),
(13, 18, 15),
(14, 19, 16),
(15, 20, 17),
(16, 21, 19),
(17, 22, 20),
(18, 24, 22),
(19, 25, 23),
(20, 27, 24),
(21, 29, 25),
(22, 30, 27);

-- --------------------------------------------------------

--
-- Estrutura para tabela `seg_controllers`
--

CREATE TABLE `seg_controllers` (
  `id_controller` int(11) NOT NULL,
  `link_controller` varchar(100) NOT NULL,
  `descricao_controller` text NOT NULL,
  `fk_model` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Fazendo dump de dados para tabela `seg_controllers`
--

INSERT INTO `seg_controllers` (`id_controller`, `link_controller`, `descricao_controller`, `fk_model`) VALUES
(1, 'Main', 'Responsável pelo gerênciamento do acesso ao sistema.', 2),
(2, 'Controller_usuarios', 'Responsável pelo gerênciamento dos usuários.', 3),
(3, 'Controller_grupos', 'Responsável pelo gerênciamento dos grupos.', 4),
(4, 'Controller_notificacoes', 'Controller Notificações', 5),
(5, 'Controller_relatorios', 'Relatórios', 6),
(6, 'Controller_categorias', 'Controller categorias', 7),
(7, 'Controller_leiloes', 'Controller leilões', 8),
(8, 'Controller_produtos', 'Controller produtos', 9),
(9, 'Controller_pacotes', 'Controller pacotes', 10);

-- --------------------------------------------------------

--
-- Estrutura para tabela `seg_grupos`
--

CREATE TABLE `seg_grupos` (
  `id_grupo` int(11) NOT NULL COMMENT 'ID Grupo',
  `nome_grupo` varchar(100) NOT NULL COMMENT 'Nome Grupo',
  `descricao_grupo` text NOT NULL COMMENT 'Descrição Grupo',
  `usuario_criou_grupo` int(11) DEFAULT NULL COMMENT 'Usuário que criou',
  `ativo_grupo` tinyint(1) NOT NULL COMMENT 'Status Grupo',
  `criacao_grupo` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Data Criação'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Fazendo dump de dados para tabela `seg_grupos`
--

INSERT INTO `seg_grupos` (`id_grupo`, `nome_grupo`, `descricao_grupo`, `usuario_criou_grupo`, `ativo_grupo`, `criacao_grupo`) VALUES
(1, 'Administradores', 'Administradores do sistema', NULL, 1, '2018-07-31 17:04:02'),
(2, 'Gerenciamento', 'Gerenciamento de painel', NULL, 1, '2018-07-31 17:04:02'),
(3, 'Clientes', 'Clientes', NULL, 1, '2018-07-31 17:04:02');

-- --------------------------------------------------------

--
-- Estrutura para tabela `seg_log_acesso`
--

CREATE TABLE `seg_log_acesso` (
  `id_log_acesso` int(11) NOT NULL,
  `fk_usuario` int(11) DEFAULT NULL,
  `data_log_acesso` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ip_usuario_acesso` varchar(16) DEFAULT NULL,
  `acesso` tinyint(1) DEFAULT NULL,
  `maquina_usuario_acesso` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura para tabela `seg_log_edicao`
--

CREATE TABLE `seg_log_edicao` (
  `id_log_edicao` int(11) NOT NULL,
  `fk_usuario` int(11) DEFAULT NULL,
  `data_log_edicao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `original_edicao` text,
  `novo_edicao` text,
  `campo_edicao` text,
  `tabela_edicao` text,
  `fk_aplicacao` int(11) DEFAULT NULL,
  `id_edicao` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura para tabela `seg_log_erro`
--

CREATE TABLE `seg_log_erro` (
  `id_log_erro` int(11) NOT NULL,
  `fk_usuario` int(11) DEFAULT NULL,
  `data_log_erro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `cod` text,
  `erro` text,
  `query` text,
  `erro_feedback` text,
  `funcao` text,
  `maquina_usuario_erro` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura para tabela `seg_log_navegacao`
--

CREATE TABLE `seg_log_navegacao` (
  `id_log_navegacao` int(11) NOT NULL,
  `fk_usuario` int(11) DEFAULT NULL,
  `data_log_navegacao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `permissao` tinyint(1) DEFAULT NULL,
  `fk_aplicacao` int(11) DEFAULT NULL,
  `parametros` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura para tabela `seg_menu`
--

CREATE TABLE `seg_menu` (
  `id_menu` int(11) NOT NULL,
  `titulo_menu` varchar(100) NOT NULL,
  `descricao_menu` text NOT NULL,
  `menu_acima` int(11) DEFAULT NULL,
  `posicao_menu` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Fazendo dump de dados para tabela `seg_menu`
--

INSERT INTO `seg_menu` (`id_menu`, `titulo_menu`, `descricao_menu`, `menu_acima`, `posicao_menu`) VALUES
(1, 'Segurança', 'Segurança, Perfils e Grupos', NULL, 1000),
(2, 'Editar Perfil', 'Editar Perfil', 1, NULL),
(3, 'Usuários', 'Lista de usuários ADM', 1, NULL),
(4, 'Grupos', 'Lista de grupos', 1, NULL),
(5, 'Notificações', 'Listas de Notificações', NULL, 999),
(6, 'Enviar Notificação', 'Lista de suas notificações podendo enviar uma nova.', 1, NULL),
(7, 'Relatórios', 'Relatórios', NULL, 998),
(8, 'Acessos', 'Relatório de Acessos', 7, NULL),
(9, 'Navegação', 'Relatório de Navegação', 7, NULL),
(10, 'Edições', 'Relatório de Edições', 7, NULL),
(11, 'Categorias', 'Categorias', NULL, 10),
(12, 'Leilão', 'Menus de leilões', NULL, 1),
(13, 'Pendentes', 'Leilões pendentes de aprovação', 12, NULL),
(14, 'Leilões Aprovados', 'Leilões Aprovados', 12, NULL),
(15, 'Leilões Finalizados', 'Leilões Finalizados', 12, NULL),
(16, 'Todos Leilões', 'Todos Leilões', 12, NULL),
(17, 'Criar Leilão', 'Criar Leilão', 12, NULL),
(18, 'Produtos', 'Produtos', NULL, 2),
(19, 'Criar produto', 'Criar produto', 18, NULL),
(20, 'Ver Produtos', 'Ver Produtos', 18, NULL),
(21, 'Pacotes', 'Pacotes', NULL, 3),
(22, 'Criar pacote', 'Criar pacote', 21, NULL),
(23, 'Ver Pacotes', 'Ver Pacotes', 21, NULL),
(24, 'Usuários', 'Lista Geral Usuários', 1, NULL),
(25, 'Ver Recompensas', 'Ver Recompensas', 18, NULL),
(26, 'Pagamentos', 'Pagamentos', NULL, NULL),
(27, 'Criar pacote', 'Criar pacote', 26, NULL);

-- --------------------------------------------------------

--
-- Estrutura para tabela `seg_models`
--

CREATE TABLE `seg_models` (
  `id_model` int(11) NOT NULL,
  `link_model` varchar(100) NOT NULL,
  `descricao_model` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Fazendo dump de dados para tabela `seg_models`
--

INSERT INTO `seg_models` (`id_model`, `link_model`, `descricao_model`) VALUES
(1, 'Model_menu', 'Responsável pelos menus.'),
(2, 'Model_seguranca', 'Responsável pelo acesso as sistema e controle de perfils.'),
(3, 'Model_usuarios', 'Responsável pelo gerênciamento dos usuários.'),
(4, 'Model_grupos', 'Responsável pelo gerênciamento dos grupos.'),
(5, 'Model_notificacoes', 'Model Notificações'),
(6, 'Model_relatorios', 'Model Relatórios'),
(7, 'Model_categorias', 'Model categorias'),
(8, 'Model_leiloes', 'Model leilões'),
(9, 'Model_produtos', 'Model produtos'),
(10, 'Model_pacotes', 'Model pacotes');

-- --------------------------------------------------------

--
-- Estrutura para tabela `seg_usuarios`
--

CREATE TABLE `seg_usuarios` (
  `id_usuario` int(11) NOT NULL COMMENT 'ID Usuário',
  `nome_usuario` varchar(100) NOT NULL COMMENT 'Nome Usuário',
  `sexo_usuario` int(11) DEFAULT NULL COMMENT 'Sexo do usuário',
  `rg_usuario` varchar(11) DEFAULT NULL COMMENT 'RG do usuário',
  `cpf_usuario` varchar(11) DEFAULT NULL COMMENT 'cpf do usuario',
  `email_usuario` varchar(40) NOT NULL COMMENT 'E-mail Usuário',
  `telefone_usuario` varchar(11) DEFAULT NULL COMMENT 'Telefone Usuário',
  `celular_usuario` varchar(11) NOT NULL COMMENT 'Celular usuário',
  `cep_usuario` varchar(8) DEFAULT NULL COMMENT 'cep usuario',
  `logradouro_usuario` text COMMENT 'logradouro do usuario',
  `bairro_usuario` text COMMENT 'bairro do usuário',
  `cidade_usuario` varchar(255) DEFAULT NULL COMMENT 'cidade do usuario',
  `estado_usuario` int(11) DEFAULT NULL COMMENT 'estado do usuario',
  `numero_usuario` varchar(8) DEFAULT NULL COMMENT 'numero do usuario',
  `complemento_usuario` text COMMENT 'Complemento endereço',
  `login_usuario` varchar(255) NOT NULL COMMENT 'Login Usuário',
  `senha_usuario` varchar(40) NOT NULL COMMENT 'Senha Usuário',
  `ativo_usuario` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Status Usuário',
  `ativado_sms` tinyint(1) DEFAULT '0' COMMENT 'Ativado por SMS',
  `fk_grupo_usuario` int(11) NOT NULL COMMENT 'Grupo Usuário',
  `usuario_criou_usuario` int(11) DEFAULT NULL COMMENT 'Usuário que criou',
  `criacao_usuario` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Data criação',
  `fbid` text COMMENT 'id do facebook',
  `token_acesso` varchar(255) DEFAULT NULL COMMENT 'token de acesso para login'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Fazendo dump de dados para tabela `seg_usuarios`
--

INSERT INTO `seg_usuarios` (`id_usuario`,`nome_usuario`,`email_usuario`,`celular_usuario`,`login_usuario`,`senha_usuario`,`ativo_usuario`,`fk_grupo_usuario`) VALUES 
(1,'Usuário Administrador','megamil3d@gmail.com','11962782329','admin','40bd001563085fc35165329ea1ff5c5ecbdbbeef',TRUE,1),
(2,'Gerente','gerente@leilao24h.com','11987654321','gerente','40bd001563085fc35165329ea1ff5c5ecbdbbeef',TRUE,2),
(3,'Cliente','cliente@leilao24h.com','11976543218','cliente','40bd001563085fc35165329ea1ff5c5ecbdbbeef',TRUE,3);

-- --------------------------------------------------------

--
-- Estrutura para tabela `status_leilao`
--

CREATE TABLE `status_leilao` (
  `id_status_leilao` int(11) NOT NULL,
  `status` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Fazendo dump de dados para tabela `status_leilao`
--

INSERT INTO `status_leilao` (`id_status_leilao`, `status`) VALUES
(1, 'Pendente'),
(2, 'Aprovado'),
(3, 'Reprovado'),
(4, 'Finalizado');

-- --------------------------------------------------------

--
-- Estrutura stand-in para view `view_completar_perfil`
-- (Veja abaixo para a visão atual)
--
CREATE TABLE `view_completar_perfil` (
`id_usuario` int(11)
,`bloquear` int(1)
);

-- --------------------------------------------------------

--
-- Estrutura stand-in para view `view_ingresso`
-- (Veja abaixo para a visão atual)
--
CREATE TABLE `view_ingresso` (
`id_ingresso` int(11)
,`fk_usuario` int(11)
,`quantidade` int(11)
,`ativo` tinyint(4)
,`prioridade` int(11)
,`id_compra_ingresso` int(11)
,`descricao` varchar(60)
,`data_compra` timestamp
,`valor` double(19,2)
,`code_pagamento` text
,`id_pacote_ingresso` int(11)
,`descricao_pacote` varchar(45)
,`quantidade_ingresso` int(11)
,`valor_pacote` double(19,2)
,`gratis` tinyint(4)
);

-- --------------------------------------------------------

--
-- Estrutura stand-in para view `view_leilao`
-- (Veja abaixo para a visão atual)
--
CREATE TABLE `view_leilao` (
`id_leilao` int(11)
,`valor_minimo` double
,`lance_minimo` double
,`ingressos` int(11)
,`data_inicio` timestamp
,`data_fim_previsto` timestamp
,`data_fim_efetivo` timestamp
,`status_leilao` int(11)
,`status` varchar(45)
,`id_produto` int(11)
,`fk_categoria` int(11)
,`fk_usuario` int(11)
,`nome_produto` varchar(255)
,`descricao_produto` text
,`produto_usado` tinyint(1)
,`nome_usuario` varchar(100)
,`fk_usuario_arrematou` int(11)
,`nome_usuario_arrematou` varchar(100)
,`nome_usuario_leiloando` varchar(100)
,`avaliacao` decimal(13,2)
,`lance_formatado` varchar(62)
,`nome_categoria` varchar(255)
);

-- --------------------------------------------------------

--
-- Estrutura stand-in para view `view_lista_grupos`
-- (Veja abaixo para a visão atual)
--
CREATE TABLE `view_lista_grupos` (
`id` int(11)
,`nome_grupo` varchar(100)
,`descricao_grupo` text
,`usuario` int(11)
,`nome_usuario` varchar(100)
,`ativo_grupo` tinyint(1)
,`ativo` varchar(7)
,`criacao_grupo` timestamp
,`criacao_grupo_formatado` varchar(28)
,`usuarios_ativos` bigint(21)
);

-- --------------------------------------------------------

--
-- Estrutura stand-in para view `view_lista_usuarios`
-- (Veja abaixo para a visão atual)
--
CREATE TABLE `view_lista_usuarios` (
`id` int(11)
,`nome_usuario` varchar(100)
,`email_usuario` varchar(40)
,`telefone_usuario` varchar(11)
,`login_usuario` varchar(255)
,`ativo_usuario` tinyint(1)
,`ativo` varchar(7)
,`fk_grupo_usuario` int(11)
,`nome_grupo` varchar(100)
,`usuario` int(11)
,`usuario_criou` varchar(100)
,`criacao_usuario` timestamp
,`criacao_usuario_formatado` varchar(28)
);

-- --------------------------------------------------------

--
-- Estrutura stand-in para view `view_notificar_leiloes`
-- (Veja abaixo para a visão atual)
--
CREATE TABLE `view_notificar_leiloes` (
`tipo` bigint(20)
,`id` int(11)
,`titulo` varchar(270)
,`descricao` varchar(297)
,`token_usuario` varchar(300)
,`inicio` bigint(21)
,`fim` bigint(21)
,`notificado` bigint(20)
);

-- --------------------------------------------------------

--
-- Estrutura stand-in para view `view_relatorio_acessos`
-- (Veja abaixo para a visão atual)
--
CREATE TABLE `view_relatorio_acessos` (
`id` int(11)
,`id_usuario` int(11)
,`usuario` varchar(100)
,`data_acesso` timestamp
,`data_acesso_formatado` varchar(28)
,`ip` varchar(16)
,`maquina` text
,`acesso` tinyint(1)
,`acessou` varchar(6)
);

-- --------------------------------------------------------

--
-- Estrutura stand-in para view `view_relatorio_edicoes`
-- (Veja abaixo para a visão atual)
--
CREATE TABLE `view_relatorio_edicoes` (
`id` int(11)
,`fk_usuario` int(11)
,`nome_usuario` varchar(100)
,`data_log` timestamp
,`data_log_formatado` varchar(28)
,`original_edicao` text
,`novo_edicao` text
,`fk_aplicacao` int(11)
,`descricao_aplicacao` text
,`id_edicao` int(11)
,`campo` text
);

-- --------------------------------------------------------

--
-- Estrutura stand-in para view `view_relatorio_navegacao`
-- (Veja abaixo para a visão atual)
--
CREATE TABLE `view_relatorio_navegacao` (
`id` int(11)
,`fk_usuario` int(11)
,`nome_usuario` varchar(100)
,`data_log` timestamp
,`data_log_formatado` varchar(28)
,`permissao` tinyint(1)
,`com_permissao` varchar(3)
,`fk_aplicacao` int(11)
,`descricao_aplicacao` text
,`parametros` text
);

-- --------------------------------------------------------

--
-- Estrutura stand-in para view `view_usuario_leilao`
-- (Veja abaixo para a visão atual)
--
CREATE TABLE `view_usuario_leilao` (
`id_sala_leilao` int(11)
,`id_usuario` int(11)
,`nome_usuario` varchar(100)
,`id_leilao` int(11)
,`valor_minimo` double
,`valor_atual` double(19,2)
,`lance_minimo` double
,`data_inicio` timestamp
,`data_fim_previsto` timestamp
,`token_acesso` varchar(255)
,`inicio` bigint(21)
,`quantidade_minima_usuarios` int(11)
,`tempo_iniciar` int(11)
,`fim` bigint(21)
);

-- --------------------------------------------------------

--
-- Estrutura para view `view_completar_perfil`
--
DROP TABLE IF EXISTS `view_completar_perfil`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_completar_perfil`  AS  select `seg_usuarios`.`id_usuario` AS `id_usuario`,(((`seg_usuarios`.`nome_usuario` = '') or isnull(`seg_usuarios`.`nome_usuario`) or (`seg_usuarios`.`sexo_usuario` = '') or isnull(`seg_usuarios`.`sexo_usuario`) or (`seg_usuarios`.`rg_usuario` = '') or isnull(`seg_usuarios`.`rg_usuario`) or (`seg_usuarios`.`cpf_usuario` = '') or isnull(`seg_usuarios`.`cpf_usuario`) or (`seg_usuarios`.`email_usuario` = '') or isnull(`seg_usuarios`.`email_usuario`) or (`seg_usuarios`.`celular_usuario` = '') or isnull(`seg_usuarios`.`celular_usuario`) or (`seg_usuarios`.`cep_usuario` = '') or isnull(`seg_usuarios`.`cep_usuario`) or (`seg_usuarios`.`logradouro_usuario` = '') or isnull(`seg_usuarios`.`logradouro_usuario`) or (`seg_usuarios`.`bairro_usuario` = '') or isnull(`seg_usuarios`.`bairro_usuario`) or (`seg_usuarios`.`cidade_usuario` = '') or isnull(`seg_usuarios`.`cidade_usuario`) or (`seg_usuarios`.`estado_usuario` = '') or isnull(`seg_usuarios`.`estado_usuario`) or (`seg_usuarios`.`numero_usuario` = '') or isnull(`seg_usuarios`.`numero_usuario`)) and (select (count(0) > 0) from `cad_leilao` where (`cad_leilao`.`fk_usuario_arrematou` = `seg_usuarios`.`id_usuario`))) AS `bloquear` from `seg_usuarios` ;

-- --------------------------------------------------------

--
-- Estrutura para view `view_ingresso`
--
DROP TABLE IF EXISTS `view_ingresso`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_ingresso`  AS  select `cdi`.`id_ingresso` AS `id_ingresso`,`cdi`.`fk_usuario` AS `fk_usuario`,`cdi`.`quantidade` AS `quantidade`,`cdi`.`ativo` AS `ativo`,`cdi`.`prioridade` AS `prioridade`,`cmi`.`id_compra_ingresso` AS `id_compra_ingresso`,`cmi`.`descricao` AS `descricao`,`cmi`.`data_compra` AS `data_compra`,round(`cmi`.`valor`,2) AS `valor`,`cmi`.`code_pagamento` AS `code_pagamento`,`pci`.`id_pacote_ingresso` AS `id_pacote_ingresso`,`pci`.`descricao_pacote` AS `descricao_pacote`,`pci`.`quantidade_ingresso` AS `quantidade_ingresso`,round(`pci`.`valor_pacote`,2) AS `valor_pacote`,`pci`.`gratis` AS `gratis` from ((`cad_ingressos` `cdi` join `compra_ingresso` `cmi` on((`cdi`.`id_ingresso` = `cmi`.`fk_ingresso`))) join `pacote_ingresso` `pci` on((`cmi`.`fk_pacote` = `pci`.`id_pacote_ingresso`))) ;

-- --------------------------------------------------------

--
-- Estrutura para view `view_leilao`
--
DROP TABLE IF EXISTS `view_leilao`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_leilao`  AS  select `cl`.`id_leilao` AS `id_leilao`,  `cl`.`data_fim_efetivo` AS `data_fim_efetivo`  ,`cl`.`valor_minimo` AS `valor_minimo`,`cl`.`lance_minimo` AS `lance_minimo`,`cl`.`ingressos` AS `ingressos`,`cl`.`data_inicio` AS `data_inicio`,`cl`.`data_fim_previsto` AS `data_fim_previsto`,`cl`.`status_leilao` AS `status_leilao`,`sl`.`status` AS `status`,`cp`.`id_produto` AS `id_produto`,`cp`.`fk_categoria` AS `fk_categoria`,`cp`.`fk_usuario` AS `fk_usuario`,`cp`.`nome_produto` AS `nome_produto`,`cp`.`descricao_produto` AS `descricao_produto`,`cp`.`produto_usado` AS `produto_usado`,`ss`.`nome_usuario` AS `nome_usuario`,`cl`.`fk_usuario_arrematou` AS `fk_usuario_arrematou`,ifnull((select `ss_`.`nome_usuario` from `seg_usuarios` `ss_` where (`ss_`.`id_usuario` = `cl`.`fk_usuario_arrematou`)),'-') AS `nome_usuario_arrematou`,(select `ss_`.`nome_usuario` from `seg_usuarios` `ss_` where (`ss_`.`id_usuario` = `cp`.`fk_usuario`)) AS `nome_usuario_leiloando`,round((select avg(`ca`.`avaliacao`) AS `avaliacao` from `cad_avaliacoes` `ca` where (`ca`.`fk_leiloeiro` = `cp`.`fk_usuario`)),2) AS `avaliacao`,(select format(`cad_lances`.`valor`,2,'de_DE') from (`cad_lances` join `sala_leilao` on((`cad_lances`.`fk_sala_leilao` = `sala_leilao`.`id_sala_leilao`))) where (`sala_leilao`.`fk_leilao` = `cl`.`id_leilao`) order by `cad_lances`.`valor` desc limit 1) AS `lance_formatado`,`cc`.`nome_categoria` AS `nome_categoria` from ((((`cad_leilao` `cl` join `cad_produtos` `cp` on((`cl`.`fk_produto` = `cp`.`id_produto`))) join `seg_usuarios` `ss` on((`ss`.`id_usuario` = `cp`.`fk_usuario`))) join `status_leilao` `sl` on((`sl`.`id_status_leilao` = `cl`.`status_leilao`))) join `cad_categorias` `cc` on((`cc`.`id_categoria` = `cp`.`fk_categoria`))) group by `cl`.`id_leilao` ;

-- --------------------------------------------------------

--
-- Estrutura para view `view_lista_grupos`
--
DROP TABLE IF EXISTS `view_lista_grupos`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_lista_grupos`  AS  select `seg_grupos`.`id_grupo` AS `id`,`seg_grupos`.`nome_grupo` AS `nome_grupo`,`seg_grupos`.`descricao_grupo` AS `descricao_grupo`,`seg_grupos`.`usuario_criou_grupo` AS `usuario`,`seg_usuarios`.`nome_usuario` AS `nome_usuario`,`seg_grupos`.`ativo_grupo` AS `ativo_grupo`,(case `seg_grupos`.`ativo_grupo` when 1 then 'Ativo' when 0 then 'Inativo' end) AS `ativo`,`seg_grupos`.`criacao_grupo` AS `criacao_grupo`,date_format(`seg_grupos`.`criacao_grupo`,'%d/%m/%Y às %H:%i:%s') AS `criacao_grupo_formatado`,(select count(0) from `seg_usuarios` where ((`seg_usuarios`.`fk_grupo_usuario` = `seg_grupos`.`id_grupo`) and (`seg_usuarios`.`ativo_usuario` = TRUE))) AS `usuarios_ativos` from (`seg_grupos` left join `seg_usuarios` on((`seg_usuarios`.`id_usuario` = `seg_grupos`.`usuario_criou_grupo`))) ;

-- --------------------------------------------------------

--
-- Estrutura para view `view_lista_usuarios`
--
DROP TABLE IF EXISTS `view_lista_usuarios`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_lista_usuarios`  AS  select `su1`.`id_usuario` AS `id`,`su1`.`nome_usuario` AS `nome_usuario`,`su1`.`email_usuario` AS `email_usuario`,`su1`.`telefone_usuario` AS `telefone_usuario`,`su1`.`login_usuario` AS `login_usuario`,`su1`.`ativo_usuario` AS `ativo_usuario`,(case `su1`.`ativo_usuario` when 1 then 'Ativo' when 0 then 'Inativo' end) AS `ativo`,`su1`.`fk_grupo_usuario` AS `fk_grupo_usuario`,`seg_grupos`.`nome_grupo` AS `nome_grupo`,`su1`.`usuario_criou_usuario` AS `usuario`,(select `su0`.`nome_usuario` from `seg_usuarios` `su0` where (`su0`.`id_usuario` = `su1`.`usuario_criou_usuario`)) AS `usuario_criou`,`su1`.`criacao_usuario` AS `criacao_usuario`,date_format(`su1`.`criacao_usuario`,'%d/%m/%Y às %H:%i:%s') AS `criacao_usuario_formatado` from (`seg_usuarios` `su1` join `seg_grupos` on((`seg_grupos`.`id_grupo` = `su1`.`fk_grupo_usuario`))) ;

-- --------------------------------------------------------

--
-- Estrutura para view `view_notificar_leiloes`
--
DROP TABLE IF EXISTS `view_notificar_leiloes`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_notificar_leiloes`  AS  select 1 AS `tipo`,`cl`.`id_leilao` AS `id`,concat('Não perca o(a) ',`cp`.`nome_produto`) AS `titulo`,concat('O leilão do produto: \'',`cp`.`nome_produto`,'\' já vai começar!') AS `descricao`,`ct`.`token` AS `token_usuario`,timestampdiff(SECOND,now(),`cl`.`data_inicio`) AS `inicio`,timestampdiff(SECOND,now(),`cl`.`data_fim_previsto`) AS `fim`,ifnull(`cl`.`notificado`,0) AS `notificado` from (((`sala_leilao` `sl` join `cad_leilao` `cl` on((`cl`.`id_leilao` = `sl`.`fk_leilao`))) join `cad_tokens` `ct` on((`ct`.`fk_usuario` = `sl`.`fk_usuario`))) join `cad_produtos` `cp` on((`cp`.`id_produto` = `cl`.`fk_produto`))) where ((`cl`.`notificado` = 0) and (timestampdiff(SECOND,now(),`cl`.`data_inicio`) <= 200)) union select 2 AS `tipo`,`cl`.`id_leilao` AS `id`,concat(`cp`.`nome_produto`,' Finalizando') AS `titulo`,concat('Ainda dá tempo de arrematar o(a): ',`cp`.`nome_produto`) AS `descricao`,`ct`.`token` AS `token_usuario`,timestampdiff(SECOND,now(),`cl`.`data_inicio`) AS `inicio`,timestampdiff(SECOND,now(),`cl`.`data_fim_previsto`) AS `fim`,ifnull(`cl`.`notificado`,0) AS `notificado` from (((`sala_leilao` `sl` join `cad_leilao` `cl` on((`cl`.`id_leilao` = `sl`.`fk_leilao`))) join `cad_tokens` `ct` on((`ct`.`fk_usuario` = `sl`.`fk_usuario`))) join `cad_produtos` `cp` on((`cp`.`id_produto` = `cl`.`fk_produto`))) where ((`cl`.`notificado` = 1) and (timestampdiff(SECOND,now(),`cl`.`data_fim_previsto`) <= (`cl`.`tempo_iniciar` * 60))) union select 3 AS `tipo`,`cl`.`id_leilao` AS `id`,concat(`cp`.`nome_produto`,' Finalizado') AS `titulo`,concat('Leilão \'',`cp`.`nome_produto`,'\' Finalizado') AS `descricao`,`ct`.`token` AS `token_usuario`,timestampdiff(SECOND,now(),`cl`.`data_inicio`) AS `inicio`,timestampdiff(SECOND,now(),`cl`.`data_fim_previsto`) AS `fim`,ifnull(`cl`.`notificado`,0) AS `notificado` from (((`sala_leilao` `sl` join `cad_leilao` `cl` on((`cl`.`id_leilao` = `sl`.`fk_leilao`))) join `cad_tokens` `ct` on((`ct`.`fk_usuario` = `sl`.`fk_usuario`))) join `cad_produtos` `cp` on((`cp`.`id_produto` = `cl`.`fk_produto`))) where ((`cl`.`notificado` = 2) and (timestampdiff(SECOND,now(),`cl`.`data_fim_previsto`) <= 0)) union select 4 AS `tipo`,`cl`.`id_leilao` AS `id`,concat(`cp`.`nome_produto`,' Finalizado') AS `titulo`,concat('O seu leilão do produto: \'',`cp`.`nome_produto`,'\' foi finalizado') AS `descricao`,`ct`.`token` AS `token_usuario`,timestampdiff(SECOND,now(),`cl`.`data_inicio`) AS `inicio`,timestampdiff(SECOND,now(),`cl`.`data_fim_previsto`) AS `fim`,ifnull(`cl`.`notificado`,0) AS `notificado` from ((`cad_leilao` `cl` join `cad_produtos` `cp` on((`cp`.`id_produto` = `cl`.`fk_produto`))) join `cad_tokens` `ct` on((`ct`.`fk_usuario` = `cp`.`fk_usuario`))) where ((`cl`.`notificado` = 2) and (timestampdiff(SECOND,now(),`cl`.`data_fim_previsto`) <= 0)) order by `tipo`,`id` ;

-- --------------------------------------------------------

--
-- Estrutura para view `view_relatorio_acessos`
--
DROP TABLE IF EXISTS `view_relatorio_acessos`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_relatorio_acessos`  AS  select `seg_log_acesso`.`id_log_acesso` AS `id`,`seg_usuarios`.`id_usuario` AS `id_usuario`,`seg_usuarios`.`nome_usuario` AS `usuario`,`seg_log_acesso`.`data_log_acesso` AS `data_acesso`,date_format(`seg_log_acesso`.`data_log_acesso`,'%d/%m/%Y às %H:%i:%s') AS `data_acesso_formatado`,`seg_log_acesso`.`ip_usuario_acesso` AS `ip`,`seg_log_acesso`.`maquina_usuario_acesso` AS `maquina`,`seg_log_acesso`.`acesso` AS `acesso`,(case `seg_log_acesso`.`acesso` when 1 then 'Entrou' when 0 then 'Saiu' end) AS `acessou` from (`seg_log_acesso` join `seg_usuarios` on((`seg_usuarios`.`id_usuario` = `seg_log_acesso`.`fk_usuario`))) ;

-- --------------------------------------------------------

--
-- Estrutura para view `view_relatorio_edicoes`
--
DROP TABLE IF EXISTS `view_relatorio_edicoes`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_relatorio_edicoes`  AS  select `seg_log_edicao`.`id_log_edicao` AS `id`,`seg_log_edicao`.`fk_usuario` AS `fk_usuario`,`seg_usuarios`.`nome_usuario` AS `nome_usuario`,`seg_log_edicao`.`data_log_edicao` AS `data_log`,date_format(`seg_log_edicao`.`data_log_edicao`,'%d/%m/%Y às %H:%i:%s') AS `data_log_formatado`,`seg_log_edicao`.`original_edicao` AS `original_edicao`,`seg_log_edicao`.`novo_edicao` AS `novo_edicao`,`seg_log_edicao`.`fk_aplicacao` AS `fk_aplicacao`,`seg_aplicacao`.`descricao_aplicacao` AS `descricao_aplicacao`,`seg_log_edicao`.`id_edicao` AS `id_edicao`,(select `information_schema`.`columns`.`COLUMN_COMMENT` AS `comentario` from `information_schema`.`columns` where ((`information_schema`.`columns`.`TABLE_SCHEMA` = 'db_leilao') and (`information_schema`.`columns`.`TABLE_NAME` = `seg_log_edicao`.`tabela_edicao`) and (`information_schema`.`columns`.`COLUMN_NAME` = `seg_log_edicao`.`campo_edicao`))) AS `campo` from ((`seg_log_edicao` join `seg_usuarios` on((`seg_log_edicao`.`fk_usuario` = `seg_usuarios`.`id_usuario`))) join `seg_aplicacao` on((`seg_aplicacao`.`id_aplicacao` = `seg_log_edicao`.`fk_aplicacao`))) ;

-- --------------------------------------------------------

--
-- Estrutura para view `view_relatorio_navegacao`
--
DROP TABLE IF EXISTS `view_relatorio_navegacao`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_relatorio_navegacao`  AS  select `seg_log_navegacao`.`id_log_navegacao` AS `id`,`seg_log_navegacao`.`fk_usuario` AS `fk_usuario`,`seg_usuarios`.`nome_usuario` AS `nome_usuario`,`seg_log_navegacao`.`data_log_navegacao` AS `data_log`,date_format(`seg_log_navegacao`.`data_log_navegacao`,'%d/%m/%Y às %H:%i:%s') AS `data_log_formatado`,`seg_log_navegacao`.`permissao` AS `permissao`,(case `seg_log_navegacao`.`permissao` when 1 then 'Sim' when 0 then 'Não' end) AS `com_permissao`,`seg_log_navegacao`.`fk_aplicacao` AS `fk_aplicacao`,`seg_aplicacao`.`descricao_aplicacao` AS `descricao_aplicacao`,`seg_log_navegacao`.`parametros` AS `parametros` from ((`seg_log_navegacao` join `seg_usuarios` on((`seg_log_navegacao`.`fk_usuario` = `seg_usuarios`.`id_usuario`))) join `seg_aplicacao` on((`seg_aplicacao`.`id_aplicacao` = `seg_log_navegacao`.`fk_aplicacao`))) ;

-- --------------------------------------------------------

--
-- Estrutura para view `view_usuario_leilao`
--
DROP TABLE IF EXISTS `view_usuario_leilao`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_usuario_leilao`  AS  select `sala_leilao`.`id_sala_leilao` AS `id_sala_leilao`,`seg_usuarios`.`id_usuario` AS `id_usuario`,`seg_usuarios`.`nome_usuario` AS `nome_usuario`,`cad_leilao`.`id_leilao` AS `id_leilao`,`cad_leilao`.`valor_minimo` AS `valor_minimo`,round(ifnull((select max(`cad_lances`.`valor`) AS `valor` from (`cad_lances` join `sala_leilao` on((`cad_lances`.`fk_sala_leilao` = `sala_leilao`.`id_sala_leilao`))) where (`sala_leilao`.`fk_leilao` = `cad_leilao`.`id_leilao`) limit 1),`cad_leilao`.`valor_minimo`),2) AS `valor_atual`,`cad_leilao`.`lance_minimo` AS `lance_minimo`,`cad_leilao`.`data_inicio` AS `data_inicio`,`cad_leilao`.`data_fim_previsto` AS `data_fim_previsto`,`seg_usuarios`.`token_acesso` AS `token_acesso`,timestampdiff(SECOND,now(),`cad_leilao`.`data_inicio`) AS `inicio`,`cad_leilao`.`quantidade_minima_usuarios` AS `quantidade_minima_usuarios`,`cad_leilao`.`tempo_iniciar` AS `tempo_iniciar`,if((timestampdiff(SECOND,now(),`cad_leilao`.`data_fim_previsto`) < 0),0,timestampdiff(SECOND,now(),`cad_leilao`.`data_fim_previsto`)) AS `fim` from ((`cad_leilao` join `sala_leilao` on((`sala_leilao`.`fk_leilao` = `cad_leilao`.`id_leilao`))) join `seg_usuarios` on((`seg_usuarios`.`id_usuario` = `sala_leilao`.`fk_usuario`))) ;

--
-- Índices de tabelas apagadas
--

--
-- Índices de tabela `cad_avaliacoes`
--
ALTER TABLE `cad_avaliacoes`
  ADD PRIMARY KEY (`id_avalicao`),
  ADD UNIQUE KEY `fk_leilao` (`fk_leilao`),
  ADD KEY `fk_leiloeiro` (`fk_leiloeiro`),
  ADD KEY `fk_cliente` (`fk_cliente`);

--
-- Índices de tabela `cad_categorias`
--
ALTER TABLE `cad_categorias`
  ADD PRIMARY KEY (`id_categoria`),
  ADD KEY `fk_categoria_pai` (`fk_categoria_pai`);

--
-- Índices de tabela `cad_compra_pacotes`
--
ALTER TABLE `cad_compra_pacotes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_usuario` (`fk_usuario`),
  ADD KEY `fk_pacote` (`fk_pacote`);

--
-- Índices de tabela `cad_compra_recompensa`
--
ALTER TABLE `cad_compra_recompensa`
  ADD PRIMARY KEY (`id_compra_recompensa`),
  ADD KEY `fk_produto` (`fk_produto`),
  ADD KEY `fk_usuario` (`fk_usuario`);

--
-- Índices de tabela `cad_creditos`
--
ALTER TABLE `cad_creditos`
  ADD PRIMARY KEY (`id_creditos`),
  ADD KEY `fk_usuario` (`fk_usuario`);

--
-- Índices de tabela `cad_detalhes_views`
--
ALTER TABLE `cad_detalhes_views`
  ADD PRIMARY KEY (`id_detalhes_views`);

--
-- Índices de tabela `cad_favoritos`
--
ALTER TABLE `cad_favoritos`
  ADD PRIMARY KEY (`fk_leilao`,`fk_usuario`),
  ADD KEY `fk_usuario` (`fk_usuario`);

--
-- Índices de tabela `cad_grupos`
--
ALTER TABLE `cad_grupos`
  ADD PRIMARY KEY (`id_grupo`);

--
-- Índices de tabela `cad_hist_notificacao`
--
ALTER TABLE `cad_hist_notificacao`
  ADD PRIMARY KEY (`id_hist_notificacao`),
  ADD KEY `fk_notificacao` (`fk_notificacao`),
  ADD KEY `fk_usuario_destino` (`fk_usuario_destino`);

--
-- Índices de tabela `cad_ingressos`
--
ALTER TABLE `cad_ingressos`
  ADD PRIMARY KEY (`id_ingresso`),
  ADD KEY `fk_usuario` (`fk_usuario`);

--
-- Índices de tabela `cad_item_grupo`
--
ALTER TABLE `cad_item_grupo`
  ADD PRIMARY KEY (`id_item_grupo`),
  ADD KEY `fk_grupo` (`fk_grupo`);

--
-- Índices de tabela `cad_lances`
--
ALTER TABLE `cad_lances`
  ADD PRIMARY KEY (`id_lances`),
  ADD KEY `fk_sala_leilao` (`fk_sala_leilao`);

--
-- Índices de tabela `cad_leilao`
--
ALTER TABLE `cad_leilao`
  ADD PRIMARY KEY (`id_leilao`),
  ADD KEY `fk_produto` (`fk_produto`),
  ADD KEY `status_leilao` (`status_leilao`),
  ADD KEY `fk_usuario_arrematou` (`fk_usuario_arrematou`);

--
-- Índices de tabela `cad_notificacao`
--
ALTER TABLE `cad_notificacao`
  ADD PRIMARY KEY (`id_notificacao`),
  ADD KEY `fk_usuario` (`fk_usuario`);

--
-- Índices de tabela `cad_produtos`
--
ALTER TABLE `cad_produtos`
  ADD PRIMARY KEY (`id_produto`),
  ADD KEY `fk_categoria` (`fk_categoria`),
  ADD KEY `fk_usuario` (`fk_usuario`);

--
-- Índices de tabela `cad_tokens`
--
ALTER TABLE `cad_tokens`
  ADD PRIMARY KEY (`id_token`),
  ADD KEY `fk_usuario` (`fk_usuario`);

--
-- Índices de tabela `compra_ingresso`
--
ALTER TABLE `compra_ingresso`
  ADD PRIMARY KEY (`id_compra_ingresso`),
  ADD KEY `fk_ingresso` (`fk_ingresso`),
  ADD KEY `fk_pacote` (`fk_pacote`);

--
-- Índices de tabela `hist_compra_pacotes`
--
ALTER TABLE `hist_compra_pacotes`
  ADD PRIMARY KEY (`id_hist_compra_pacotes`),
  ADD KEY `fk_compra_pacotes` (`fk_compra_pacotes`);

--
-- Índices de tabela `pacote_ingresso`
--
ALTER TABLE `pacote_ingresso`
  ADD PRIMARY KEY (`id_pacote_ingresso`);

--
-- Índices de tabela `sala_leilao`
--
ALTER TABLE `sala_leilao`
  ADD PRIMARY KEY (`id_sala_leilao`),
  ADD UNIQUE KEY `fk_leilao` (`fk_leilao`,`fk_usuario`),
  ADD KEY `fk_usuario` (`fk_usuario`);

--
-- Índices de tabela `seg_aplicacao`
--
ALTER TABLE `seg_aplicacao`
  ADD PRIMARY KEY (`id_aplicacao`),
  ADD KEY `fk_controller` (`fk_controller`);

--
-- Índices de tabela `seg_aplicacoes_grupos`
--
ALTER TABLE `seg_aplicacoes_grupos`
  ADD PRIMARY KEY (`id_aplicacoes_grupos`),
  ADD KEY `fk_aplicacao` (`fk_aplicacao`),
  ADD KEY `fk_grupo` (`fk_grupo`);

--
-- Índices de tabela `seg_aplicacoes_menu`
--
ALTER TABLE `seg_aplicacoes_menu`
  ADD PRIMARY KEY (`id_aplicacoes_menu`),
  ADD KEY `fk_aplicacao` (`fk_aplicacao`),
  ADD KEY `fk_menu` (`fk_menu`);

--
-- Índices de tabela `seg_controllers`
--
ALTER TABLE `seg_controllers`
  ADD PRIMARY KEY (`id_controller`),
  ADD KEY `fk_model` (`fk_model`);

--
-- Índices de tabela `seg_grupos`
--
ALTER TABLE `seg_grupos`
  ADD PRIMARY KEY (`id_grupo`),
  ADD UNIQUE KEY `unique_nome_grupo` (`nome_grupo`),
  ADD KEY `usuario_criou_grupo` (`usuario_criou_grupo`);

--
-- Índices de tabela `seg_log_acesso`
--
ALTER TABLE `seg_log_acesso`
  ADD PRIMARY KEY (`id_log_acesso`),
  ADD KEY `fk_usuario` (`fk_usuario`);

--
-- Índices de tabela `seg_log_edicao`
--
ALTER TABLE `seg_log_edicao`
  ADD PRIMARY KEY (`id_log_edicao`),
  ADD KEY `fk_usuario` (`fk_usuario`),
  ADD KEY `fk_aplicacao` (`fk_aplicacao`);

--
-- Índices de tabela `seg_log_erro`
--
ALTER TABLE `seg_log_erro`
  ADD PRIMARY KEY (`id_log_erro`),
  ADD KEY `fk_usuario` (`fk_usuario`);

--
-- Índices de tabela `seg_log_navegacao`
--
ALTER TABLE `seg_log_navegacao`
  ADD PRIMARY KEY (`id_log_navegacao`),
  ADD KEY `fk_usuario` (`fk_usuario`),
  ADD KEY `fk_aplicacao` (`fk_aplicacao`);

--
-- Índices de tabela `seg_menu`
--
ALTER TABLE `seg_menu`
  ADD PRIMARY KEY (`id_menu`),
  ADD KEY `menu_acima` (`menu_acima`);

--
-- Índices de tabela `seg_models`
--
ALTER TABLE `seg_models`
  ADD PRIMARY KEY (`id_model`);

--
-- Índices de tabela `seg_usuarios`
--
ALTER TABLE `seg_usuarios`
  ADD PRIMARY KEY (`id_usuario`),
  ADD UNIQUE KEY `celular_usuario` (`celular_usuario`),
  ADD UNIQUE KEY `email_usuario` (`email_usuario`),
  ADD UNIQUE KEY `login_usuario` (`login_usuario`),
  ADD UNIQUE KEY `cpf_usuario` (`cpf_usuario`),
  ADD UNIQUE KEY `token_acesso` (`token_acesso`),
  ADD KEY `usuario_criou_usuario` (`usuario_criou_usuario`),
  ADD KEY `estado_usuario` (`estado_usuario`),
  ADD KEY `fk_grupo_usuario` (`fk_grupo_usuario`);

--
-- Índices de tabela `status_leilao`
--
ALTER TABLE `status_leilao`
  ADD PRIMARY KEY (`id_status_leilao`);

--
-- AUTO_INCREMENT de tabelas apagadas
--

--
-- AUTO_INCREMENT de tabela `cad_avaliacoes`
--
ALTER TABLE `cad_avaliacoes`
  MODIFY `id_avalicao` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `cad_categorias`
--
ALTER TABLE `cad_categorias`
  MODIFY `id_categoria` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT de tabela `cad_compra_pacotes`
--
ALTER TABLE `cad_compra_pacotes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `cad_compra_recompensa`
--
ALTER TABLE `cad_compra_recompensa`
  MODIFY `id_compra_recompensa` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `cad_creditos`
--
ALTER TABLE `cad_creditos`
  MODIFY `id_creditos` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `cad_detalhes_views`
--
ALTER TABLE `cad_detalhes_views`
  MODIFY `id_detalhes_views` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `cad_grupos`
--
ALTER TABLE `cad_grupos`
  MODIFY `id_grupo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de tabela `cad_hist_notificacao`
--
ALTER TABLE `cad_hist_notificacao`
  MODIFY `id_hist_notificacao` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `cad_ingressos`
--
ALTER TABLE `cad_ingressos`
  MODIFY `id_ingresso` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `cad_item_grupo`
--
ALTER TABLE `cad_item_grupo`
  MODIFY `id_item_grupo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT de tabela `cad_lances`
--
ALTER TABLE `cad_lances`
  MODIFY `id_lances` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `cad_leilao`
--
ALTER TABLE `cad_leilao`
  MODIFY `id_leilao` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `cad_notificacao`
--
ALTER TABLE `cad_notificacao`
  MODIFY `id_notificacao` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `cad_produtos`
--
ALTER TABLE `cad_produtos`
  MODIFY `id_produto` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `cad_tokens`
--
ALTER TABLE `cad_tokens`
  MODIFY `id_token` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `compra_ingresso`
--
ALTER TABLE `compra_ingresso`
  MODIFY `id_compra_ingresso` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `hist_compra_pacotes`
--
ALTER TABLE `hist_compra_pacotes`
  MODIFY `id_hist_compra_pacotes` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `pacote_ingresso`
--
ALTER TABLE `pacote_ingresso`
  MODIFY `id_pacote_ingresso` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `sala_leilao`
--
ALTER TABLE `sala_leilao`
  MODIFY `id_sala_leilao` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `seg_aplicacao`
--
ALTER TABLE `seg_aplicacao`
  MODIFY `id_aplicacao` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT de tabela `seg_aplicacoes_grupos`
--
ALTER TABLE `seg_aplicacoes_grupos`
  MODIFY `id_aplicacoes_grupos` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT de tabela `seg_aplicacoes_menu`
--
ALTER TABLE `seg_aplicacoes_menu`
  MODIFY `id_aplicacoes_menu` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT de tabela `seg_controllers`
--
ALTER TABLE `seg_controllers`
  MODIFY `id_controller` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de tabela `seg_grupos`
--
ALTER TABLE `seg_grupos`
  MODIFY `id_grupo` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID Grupo', AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de tabela `seg_log_acesso`
--
ALTER TABLE `seg_log_acesso`
  MODIFY `id_log_acesso` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `seg_log_edicao`
--
ALTER TABLE `seg_log_edicao`
  MODIFY `id_log_edicao` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `seg_log_erro`
--
ALTER TABLE `seg_log_erro`
  MODIFY `id_log_erro` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `seg_log_navegacao`
--
ALTER TABLE `seg_log_navegacao`
  MODIFY `id_log_navegacao` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `seg_menu`
--
ALTER TABLE `seg_menu`
  MODIFY `id_menu` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT de tabela `seg_models`
--
ALTER TABLE `seg_models`
  MODIFY `id_model` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de tabela `seg_usuarios`
--
ALTER TABLE `seg_usuarios`
  MODIFY `id_usuario` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID Usuário';

--
-- AUTO_INCREMENT de tabela `status_leilao`
--
ALTER TABLE `status_leilao`
  MODIFY `id_status_leilao` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Restrições para dumps de tabelas
--

--
-- Restrições para tabelas `cad_avaliacoes`
--
ALTER TABLE `cad_avaliacoes`
  ADD CONSTRAINT `cad_avaliacoes_ibfk_1` FOREIGN KEY (`fk_leiloeiro`) REFERENCES `seg_usuarios` (`id_usuario`),
  ADD CONSTRAINT `cad_avaliacoes_ibfk_2` FOREIGN KEY (`fk_leilao`) REFERENCES `cad_leilao` (`id_leilao`),
  ADD CONSTRAINT `cad_avaliacoes_ibfk_3` FOREIGN KEY (`fk_cliente`) REFERENCES `seg_usuarios` (`id_usuario`);

--
-- Restrições para tabelas `cad_categorias`
--
ALTER TABLE `cad_categorias`
  ADD CONSTRAINT `cad_categorias_ibfk_1` FOREIGN KEY (`fk_categoria_pai`) REFERENCES `cad_categorias` (`id_categoria`);

--
-- Restrições para tabelas `cad_compra_pacotes`
--
ALTER TABLE `cad_compra_pacotes`
  ADD CONSTRAINT `cad_compra_pacotes_ibfk_1` FOREIGN KEY (`fk_usuario`) REFERENCES `seg_usuarios` (`id_usuario`),
  ADD CONSTRAINT `cad_compra_pacotes_ibfk_2` FOREIGN KEY (`fk_pacote`) REFERENCES `pacote_ingresso` (`id_pacote_ingresso`);

--
-- Restrições para tabelas `cad_compra_recompensa`
--
ALTER TABLE `cad_compra_recompensa`
  ADD CONSTRAINT `cad_compra_recompensa_ibfk_1` FOREIGN KEY (`fk_produto`) REFERENCES `cad_produtos` (`id_produto`),
  ADD CONSTRAINT `cad_compra_recompensa_ibfk_2` FOREIGN KEY (`fk_usuario`) REFERENCES `seg_usuarios` (`id_usuario`);

--
-- Restrições para tabelas `cad_creditos`
--
ALTER TABLE `cad_creditos`
  ADD CONSTRAINT `cad_creditos_ibfk_1` FOREIGN KEY (`fk_usuario`) REFERENCES `seg_usuarios` (`id_usuario`);

--
-- Restrições para tabelas `cad_favoritos`
--
ALTER TABLE `cad_favoritos`
  ADD CONSTRAINT `cad_favoritos_ibfk_1` FOREIGN KEY (`fk_leilao`) REFERENCES `cad_leilao` (`id_leilao`),
  ADD CONSTRAINT `cad_favoritos_ibfk_2` FOREIGN KEY (`fk_usuario`) REFERENCES `seg_usuarios` (`id_usuario`);

--
-- Restrições para tabelas `cad_hist_notificacao`
--
ALTER TABLE `cad_hist_notificacao`
  ADD CONSTRAINT `cad_hist_notificacao_ibfk_1` FOREIGN KEY (`fk_notificacao`) REFERENCES `cad_notificacao` (`id_notificacao`),
  ADD CONSTRAINT `cad_hist_notificacao_ibfk_2` FOREIGN KEY (`fk_usuario_destino`) REFERENCES `seg_usuarios` (`id_usuario`);

--
-- Restrições para tabelas `cad_ingressos`
--
ALTER TABLE `cad_ingressos`
  ADD CONSTRAINT `cad_ingressos_ibfk_1` FOREIGN KEY (`fk_usuario`) REFERENCES `seg_usuarios` (`id_usuario`);

--
-- Restrições para tabelas `cad_item_grupo`
--
ALTER TABLE `cad_item_grupo`
  ADD CONSTRAINT `cad_item_grupo_ibfk_1` FOREIGN KEY (`fk_grupo`) REFERENCES `cad_grupos` (`id_grupo`);

--
-- Restrições para tabelas `cad_lances`
--
ALTER TABLE `cad_lances`
  ADD CONSTRAINT `cad_lances_ibfk_1` FOREIGN KEY (`fk_sala_leilao`) REFERENCES `sala_leilao` (`id_sala_leilao`);

--
-- Restrições para tabelas `cad_leilao`
--
ALTER TABLE `cad_leilao`
  ADD CONSTRAINT `cad_leilao_ibfk_1` FOREIGN KEY (`fk_produto`) REFERENCES `cad_produtos` (`id_produto`),
  ADD CONSTRAINT `cad_leilao_ibfk_2` FOREIGN KEY (`status_leilao`) REFERENCES `status_leilao` (`id_status_leilao`),
  ADD CONSTRAINT `cad_leilao_ibfk_3` FOREIGN KEY (`fk_usuario_arrematou`) REFERENCES `seg_usuarios` (`id_usuario`);

--
-- Restrições para tabelas `cad_notificacao`
--
ALTER TABLE `cad_notificacao`
  ADD CONSTRAINT `cad_notificacao_ibfk_1` FOREIGN KEY (`fk_usuario`) REFERENCES `seg_usuarios` (`id_usuario`);

--
-- Restrições para tabelas `cad_produtos`
--
ALTER TABLE `cad_produtos`
  ADD CONSTRAINT `cad_produtos_ibfk_1` FOREIGN KEY (`fk_categoria`) REFERENCES `cad_categorias` (`id_categoria`),
  ADD CONSTRAINT `cad_produtos_ibfk_2` FOREIGN KEY (`fk_usuario`) REFERENCES `seg_usuarios` (`id_usuario`);

--
-- Restrições para tabelas `cad_tokens`
--
ALTER TABLE `cad_tokens`
  ADD CONSTRAINT `cad_tokens_ibfk_1` FOREIGN KEY (`fk_usuario`) REFERENCES `seg_usuarios` (`id_usuario`);

--
-- Restrições para tabelas `compra_ingresso`
--
ALTER TABLE `compra_ingresso`
  ADD CONSTRAINT `compra_ingresso_ibfk_1` FOREIGN KEY (`fk_ingresso`) REFERENCES `cad_ingressos` (`id_ingresso`),
  ADD CONSTRAINT `compra_ingresso_ibfk_2` FOREIGN KEY (`fk_pacote`) REFERENCES `pacote_ingresso` (`id_pacote_ingresso`);

--
-- Restrições para tabelas `hist_compra_pacotes`
--
ALTER TABLE `hist_compra_pacotes`
  ADD CONSTRAINT `hist_compra_pacotes_ibfk_1` FOREIGN KEY (`fk_compra_pacotes`) REFERENCES `cad_compra_pacotes` (`id`);

--
-- Restrições para tabelas `sala_leilao`
--
ALTER TABLE `sala_leilao`
  ADD CONSTRAINT `sala_leilao_ibfk_1` FOREIGN KEY (`fk_leilao`) REFERENCES `cad_leilao` (`id_leilao`),
  ADD CONSTRAINT `sala_leilao_ibfk_2` FOREIGN KEY (`fk_usuario`) REFERENCES `seg_usuarios` (`id_usuario`);

--
-- Restrições para tabelas `seg_aplicacao`
--
ALTER TABLE `seg_aplicacao`
  ADD CONSTRAINT `seg_aplicacao_ibfk_1` FOREIGN KEY (`fk_controller`) REFERENCES `seg_controllers` (`id_controller`);

--
-- Restrições para tabelas `seg_aplicacoes_grupos`
--
ALTER TABLE `seg_aplicacoes_grupos`
  ADD CONSTRAINT `seg_aplicacoes_grupos_ibfk_1` FOREIGN KEY (`fk_aplicacao`) REFERENCES `seg_aplicacao` (`id_aplicacao`),
  ADD CONSTRAINT `seg_aplicacoes_grupos_ibfk_2` FOREIGN KEY (`fk_grupo`) REFERENCES `seg_grupos` (`id_grupo`);

--
-- Restrições para tabelas `seg_aplicacoes_menu`
--
ALTER TABLE `seg_aplicacoes_menu`
  ADD CONSTRAINT `seg_aplicacoes_menu_ibfk_1` FOREIGN KEY (`fk_aplicacao`) REFERENCES `seg_aplicacao` (`id_aplicacao`),
  ADD CONSTRAINT `seg_aplicacoes_menu_ibfk_2` FOREIGN KEY (`fk_menu`) REFERENCES `seg_menu` (`id_menu`);

--
-- Restrições para tabelas `seg_controllers`
--
ALTER TABLE `seg_controllers`
  ADD CONSTRAINT `seg_controllers_ibfk_1` FOREIGN KEY (`fk_model`) REFERENCES `seg_models` (`id_model`);

--
-- Restrições para tabelas `seg_grupos`
--
ALTER TABLE `seg_grupos`
  ADD CONSTRAINT `seg_grupos_ibfk_1` FOREIGN KEY (`usuario_criou_grupo`) REFERENCES `seg_usuarios` (`id_usuario`);

--
-- Restrições para tabelas `seg_log_acesso`
--
ALTER TABLE `seg_log_acesso`
  ADD CONSTRAINT `seg_log_acesso_ibfk_1` FOREIGN KEY (`fk_usuario`) REFERENCES `seg_usuarios` (`id_usuario`);

--
-- Restrições para tabelas `seg_log_edicao`
--
ALTER TABLE `seg_log_edicao`
  ADD CONSTRAINT `seg_log_edicao_ibfk_1` FOREIGN KEY (`fk_usuario`) REFERENCES `seg_usuarios` (`id_usuario`),
  ADD CONSTRAINT `seg_log_edicao_ibfk_2` FOREIGN KEY (`fk_aplicacao`) REFERENCES `seg_aplicacao` (`id_aplicacao`);

--
-- Restrições para tabelas `seg_log_erro`
--
ALTER TABLE `seg_log_erro`
  ADD CONSTRAINT `seg_log_erro_ibfk_1` FOREIGN KEY (`fk_usuario`) REFERENCES `seg_usuarios` (`id_usuario`);

--
-- Restrições para tabelas `seg_log_navegacao`
--
ALTER TABLE `seg_log_navegacao`
  ADD CONSTRAINT `seg_log_navegacao_ibfk_1` FOREIGN KEY (`fk_usuario`) REFERENCES `seg_usuarios` (`id_usuario`),
  ADD CONSTRAINT `seg_log_navegacao_ibfk_2` FOREIGN KEY (`fk_aplicacao`) REFERENCES `seg_aplicacao` (`id_aplicacao`);

--
-- Restrições para tabelas `seg_menu`
--
ALTER TABLE `seg_menu`
  ADD CONSTRAINT `seg_menu_ibfk_1` FOREIGN KEY (`menu_acima`) REFERENCES `seg_menu` (`id_menu`);

--
-- Restrições para tabelas `seg_usuarios`
--
ALTER TABLE `seg_usuarios`
  ADD CONSTRAINT `seg_usuarios_ibfk_1` FOREIGN KEY (`usuario_criou_usuario`) REFERENCES `seg_usuarios` (`id_usuario`),
  ADD CONSTRAINT `seg_usuarios_ibfk_2` FOREIGN KEY (`estado_usuario`) REFERENCES `cad_item_grupo` (`id_item_grupo`),
  ADD CONSTRAINT `seg_usuarios_ibfk_3` FOREIGN KEY (`fk_grupo_usuario`) REFERENCES `seg_grupos` (`id_grupo`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

/*Novos campos na cad_leilao para o frete e venda de produtos*/
ALTER TABLE `cad_leilao` ADD `link_compra` TEXT NOT NULL AFTER `notificado`, ADD `id_consulta_preference` TEXT NOT NULL AFTER `link_compra`, ADD `topic` TEXT NOT NULL AFTER `id_consulta_preference`, ADD `finalizado` TINYINT NOT NULL AFTER `topic`, ADD `peso` FLOAT NOT NULL AFTER `finalizado`, ADD `altura` FLOAT NOT NULL AFTER `peso`, ADD `largura` FLOAT NOT NULL AFTER `altura`, ADD `comprimento` FLOAT NOT NULL AFTER `largura`;

/*Novo relatório de vendas*/
INSERT INTO `seg_aplicacao` (`id_aplicacao`, `link_aplicacao`, `titulo_aplicacao`, `descricao_aplicacao`, `fk_controller`) VALUES
(31, 'produto/view_vendas_produtos', 'Vendas produtos', 'Vendas produtos', 8);

INSERT INTO `seg_aplicacoes_grupos` (`id_aplicacoes_grupos`, `fk_grupo`, `fk_aplicacao`) VALUES
(23, 1, 31);

INSERT INTO `seg_aplicacoes_menu` (`id_aplicacoes_menu`, `fk_aplicacao`, `fk_menu`) VALUES
(23, 31, 27);

--
--  Lance Mínimo Arrematar
--
ALTER TABLE `cad_leilao`
  ADD COLUMN `valor_inicial_lance` double DEFAULT 0;

USE `db_leilao`;
CREATE 
     OR REPLACE ALGORITHM = UNDEFINED 
    DEFINER = `root`@`localhost` 
    SQL SECURITY DEFINER
VIEW `view_leilao` AS
    SELECT 
        `cl`.`id_leilao` AS `id_leilao`,
		    `cl`.`data_fim_efetivo` AS `data_fim_efetivo`, 

        `cl`.`link_compra` AS `link_compra`, 
        `cl`.`id_consulta_preference` AS `id_consulta_preference`, 
        `cl`.`finalizado` AS `finalizado`, 
        `cl`.`topic` AS `topic`,   
        `cl`.`valor_inicial_lance` AS `valor_inicial_lance`,   

        `cl`.`valor_minimo` AS `valor_minimo`,
        `cl`.`lance_minimo` AS `lance_minimo`,
        `cl`.`ingressos` AS `ingressos`,
        `cl`.`data_inicio` AS `data_inicio`,
        `cl`.`data_fim_previsto` AS `data_fim_previsto`,
        `cl`.`status_leilao` AS `status_leilao`,
        `sl`.`status` AS `status`,
        `cp`.`id_produto` AS `id_produto`,
        `cp`.`fk_categoria` AS `fk_categoria`,
        `cp`.`fk_usuario` AS `fk_usuario`,
        `cp`.`nome_produto` AS `nome_produto`,
        `cp`.`descricao_produto` AS `descricao_produto`,
        `cp`.`produto_usado` AS `produto_usado`,
        `ss`.`nome_usuario` AS `nome_usuario`,
        `cl`.`fk_usuario_arrematou` AS `fk_usuario_arrematou`,
        IFNULL((SELECT 
                        `ss_`.`nome_usuario`
                    FROM
                        `seg_usuarios` `ss_`
                    WHERE
                        (`ss_`.`id_usuario` = `cl`.`fk_usuario_arrematou`)),
                '-') AS `nome_usuario_arrematou`,
        (SELECT 
                `ss_`.`nome_usuario`
            FROM
                `seg_usuarios` `ss_`
            WHERE
                (`ss_`.`id_usuario` = `cp`.`fk_usuario`)) AS `nome_usuario_leiloando`,
        ROUND((SELECT 
                        AVG(`ca`.`avaliacao`) AS `avaliacao`
                    FROM
                        `cad_avaliacoes` `ca`
                    WHERE
                        (`ca`.`fk_leiloeiro` = `cp`.`fk_usuario`)),
                2) AS `avaliacao`,
        (SELECT 
                FORMAT(`cad_lances`.`valor`, 2, 'de_DE')
            FROM
                (`cad_lances`
                JOIN `sala_leilao` ON ((`cad_lances`.`fk_sala_leilao` = `sala_leilao`.`id_sala_leilao`)))
            WHERE
                (`sala_leilao`.`fk_leilao` = `cl`.`id_leilao`)
            ORDER BY `cad_lances`.`valor` DESC
            LIMIT 1) AS `lance_formatado`,
        `cc`.`nome_categoria` AS `nome_categoria`
    FROM
        ((((`cad_leilao` `cl`
        JOIN `cad_produtos` `cp` ON ((`cl`.`fk_produto` = `cp`.`id_produto`)))
        JOIN `seg_usuarios` `ss` ON ((`ss`.`id_usuario` = `cp`.`fk_usuario`)))
        JOIN `status_leilao` `sl` ON ((`sl`.`id_status_leilao` = `cl`.`status_leilao`)))
        JOIN `cad_categorias` `cc` ON ((`cc`.`id_categoria` = `cp`.`fk_categoria`)))
    GROUP BY `cl`.`id_leilao`;

/*
  Validando cadastro de produto também.
*/
CREATE OR REPLACE VIEW view_completar_perfil as
select `db_leilao`.`seg_usuarios`.`id_usuario` AS `id_usuario`,
(((`db_leilao`.`seg_usuarios`.`nome_usuario` = '') or isnull(`db_leilao`.`seg_usuarios`.`nome_usuario`) or (`db_leilao`.`seg_usuarios`.`sexo_usuario` = '') or isnull(`db_leilao`.`seg_usuarios`.`sexo_usuario`) or (`db_leilao`.`seg_usuarios`.`rg_usuario` = '') or isnull(`db_leilao`.`seg_usuarios`.`rg_usuario`) or (`db_leilao`.`seg_usuarios`.`cpf_usuario` = '') or isnull(`db_leilao`.`seg_usuarios`.`cpf_usuario`) or (`db_leilao`.`seg_usuarios`.`email_usuario` = '') or isnull(`db_leilao`.`seg_usuarios`.`email_usuario`) or (`db_leilao`.`seg_usuarios`.`celular_usuario` = '') or isnull(`db_leilao`.`seg_usuarios`.`celular_usuario`) or (`db_leilao`.`seg_usuarios`.`cep_usuario` = '') or isnull(`db_leilao`.`seg_usuarios`.`cep_usuario`) or (`db_leilao`.`seg_usuarios`.`logradouro_usuario` = '') or isnull(`db_leilao`.`seg_usuarios`.`logradouro_usuario`) or (`db_leilao`.`seg_usuarios`.`bairro_usuario` = '') or isnull(`db_leilao`.`seg_usuarios`.`bairro_usuario`) or (`db_leilao`.`seg_usuarios`.`cidade_usuario` = '') or isnull(`db_leilao`.`seg_usuarios`.`cidade_usuario`) or (`db_leilao`.`seg_usuarios`.`estado_usuario` = '') or isnull(`db_leilao`.`seg_usuarios`.`estado_usuario`) or (`db_leilao`.`seg_usuarios`.`numero_usuario` = '') or isnull(`db_leilao`.`seg_usuarios`.`numero_usuario`)) 
  and 
  (
	  (select (count(0) > 0) from `db_leilao`.`cad_leilao` where (`db_leilao`.`cad_leilao`.`fk_usuario_arrematou` = `db_leilao`.`seg_usuarios`.`id_usuario`))
	  or
	  (select (count(0) > 0) from `db_leilao`.`cad_produtos` where (`db_leilao`.`cad_produtos`.`fk_usuario` = `db_leilao`.`seg_usuarios`.`id_usuario`))
  )
)
 AS `bloquear` from `db_leilao`.`seg_usuarios`;

CREATE OR REPLACE VIEW view_usuario_leilao as
SELECT `db_leilao`.`sala_leilao`.`id_sala_leilao` AS `id_sala_leilao`,
       `db_leilao`.`seg_usuarios`.`id_usuario` AS `id_usuario`,
       `db_leilao`.`seg_usuarios`.`nome_usuario` AS `nome_usuario`,
       `db_leilao`.`cad_leilao`.`id_leilao` AS `id_leilao`,
       `db_leilao`.`cad_leilao`.`valor_minimo` AS `valor_minimo`,
       `db_leilao`.`cad_leilao`.`valor_inicial_lance` AS `valor_inicial_lance`,
       round(ifnull(
                      (SELECT max(`db_leilao`.`cad_lances`.`valor`) AS `valor`
                       FROM (`db_leilao`.`cad_lances`
                             JOIN `db_leilao`.`sala_leilao` on((`db_leilao`.`cad_lances`.`fk_sala_leilao` = `db_leilao`.`sala_leilao`.`id_sala_leilao`)))
                       WHERE (`db_leilao`.`sala_leilao`.`fk_leilao` = `db_leilao`.`cad_leilao`.`id_leilao`)
                       LIMIT 1),`db_leilao`.`cad_leilao`.`valor_inicial_lance`), 2) AS `valor_atual`,
       `db_leilao`.`cad_leilao`.`lance_minimo` AS `lance_minimo`,
       `db_leilao`.`cad_leilao`.`data_inicio` AS `data_inicio`,
       `db_leilao`.`cad_leilao`.`data_fim_previsto` AS `data_fim_previsto`,
       `db_leilao`.`seg_usuarios`.`token_acesso` AS `token_acesso`,
       timestampdiff(SECOND, now(), `db_leilao`.`cad_leilao`.`data_inicio`) AS `inicio`,
       `db_leilao`.`cad_leilao`.`quantidade_minima_usuarios` AS `quantidade_minima_usuarios`,
       `db_leilao`.`cad_leilao`.`tempo_iniciar` AS `tempo_iniciar`,
       if((timestampdiff(SECOND, now(), `db_leilao`.`cad_leilao`.`data_fim_previsto`) < 0),0, timestampdiff(SECOND, now(), `db_leilao`.`cad_leilao`.`data_fim_previsto`)) AS `fim`
FROM ((`db_leilao`.`cad_leilao`
       JOIN `db_leilao`.`sala_leilao` on((`db_leilao`.`sala_leilao`.`fk_leilao` = `db_leilao`.`cad_leilao`.`id_leilao`)))
      JOIN `db_leilao`.`seg_usuarios` on((`db_leilao`.`seg_usuarios`.`id_usuario` = `db_leilao`.`sala_leilao`.`fk_usuario`)));


/*Produtos Banners*/
INSERT INTO seg_menu (menu_acima, posicao_menu, titulo_menu, descricao_menu, id_menu) VALUES (null, 4,    'Banners',  'Banners',  '28');
INSERT INTO seg_aplicacao (link_aplicacao, titulo_aplicacao, descricao_aplicacao, fk_controller) VALUES ('produto/view_banners', 'Banner APP',  'Banner APP',   '8');
insert into seg_aplicacoes_menu(fk_aplicacao,fk_menu) values ( 32, 28);
insert into seg_aplicacoes_grupos(fk_aplicacao,fk_grupo) values( 32,1);
/*Produtos*/