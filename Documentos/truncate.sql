/*Usado para limpar o banco, menos as tabelas com dados fixos*/
use db_leilao;
SET FOREIGN_KEY_CHECKS = 0; 

Truncate table cad_avaliacoes;
ALTER TABLE cad_avaliacoes AUTO_INCREMENT = 1;

Truncate table hist_compra_pacotes;
/*ALTER TABLE hist_compra_pacotes AUTO_INCREMENT = 1;*/

Truncate table cad_compra_pacotes;
ALTER TABLE cad_compra_pacotes AUTO_INCREMENT = 1;

Truncate table cad_compra_recompensa;
/*ALTER TABLE cad_compra_recompensa AUTO_INCREMENT = 1;*/

Truncate table cad_creditos;
ALTER TABLE cad_creditos AUTO_INCREMENT = 1;

Truncate table cad_detalhes_views;
ALTER TABLE cad_detalhes_views AUTO_INCREMENT = 1;

Truncate table cad_favoritos;
ALTER TABLE cad_favoritos AUTO_INCREMENT = 1;

Truncate table cad_hist_notificacao;
/*ALTER TABLE cad_hist_notificacao AUTO_INCREMENT = 1;*/

Truncate table cad_ingressos;
ALTER TABLE cad_ingressos AUTO_INCREMENT = 1;

Truncate table cad_lances;
ALTER TABLE cad_lances AUTO_INCREMENT = 1;

Truncate table cad_leilao;
/*ALTER TABLE cad_leilao AUTO_INCREMENT = 1;*/

Truncate table cad_notificacao;
ALTER TABLE cad_notificacao AUTO_INCREMENT = 1;

Truncate table cad_produtos;
ALTER TABLE cad_produtos AUTO_INCREMENT = 1;

Truncate table cad_tokens;
ALTER TABLE cad_tokens AUTO_INCREMENT = 1;

Truncate table compra_ingresso;
ALTER TABLE compra_ingresso AUTO_INCREMENT = 1;

Truncate table pacote_ingresso;
ALTER TABLE pacote_ingresso AUTO_INCREMENT = 1;

Truncate table sala_leilao;
ALTER TABLE sala_leilao AUTO_INCREMENT = 1;

Truncate table seg_log_acesso;
ALTER TABLE seg_log_acesso AUTO_INCREMENT = 1;

Truncate table seg_log_edicao;
ALTER TABLE seg_log_edicao AUTO_INCREMENT = 1;

Truncate table seg_log_erro;
ALTER TABLE seg_log_erro AUTO_INCREMENT = 1;

Truncate table seg_log_navegacao;
ALTER TABLE seg_log_navegacao AUTO_INCREMENT = 1;

Truncate table seg_usuarios;
ALTER TABLE seg_usuarios AUTO_INCREMENT = 1;

SET FOREIGN_KEY_CHECKS = 1;

INSERT INTO `seg_usuarios` (`id_usuario`,`nome_usuario`,`email_usuario`,`celular_usuario`,`login_usuario`,`senha_usuario`,`ativo_usuario`,`ativado_sms`,`fk_grupo_usuario`) VALUES 
(1,'Usuário Administrador','megamil3d@gmail.com','11962782222','admin','7c4a8d09ca3762af61e59520943dc26494f8941b',TRUE,TRUE,1),
(2,'Gerente','gerente@leilao24h.com','11987654321','gerente','7c4a8d09ca3762af61e59520943dc26494f8941b',TRUE,TRUE,2),
(3,'Cliente','cliente@leilao24h.com','11976543218','cliente','7c4a8d09ca3762af61e59520943dc26494f8941b',TRUE,TRUE,3),
(4,'Teste','teste@leilao24h.com','11976543211','Teste','7c4a8d09ca3762af61e59520943dc26494f8941b',TRUE,TRUE,3);

/* CEP Default
    use db_leilao;
    update seg_usuarios set cep_usuario = '03147100' where fk_grupo_usuario = 1 and id_usuario > 0
*/