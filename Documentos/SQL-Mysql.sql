/*
Observações.
	Comentários: Os campos em tabelas nas quais os usuários podem alterar seu valor devem ser comentados, 
	pois o comentário será usado no relatório de edições realizadas, para melhor identificar o campo que recebeu 
	a alteração.

	Alterar:
		Em Drop, e Create o nome do schema.
		Em view_relatorio_edicoes alterar o nome do schema*/


DROP DATABASE db_leilao;

CREATE SCHEMA db_leilao CHARACTER SET utf8 COLLATE utf8_general_ci; /*ALTERAR NO VIEW_ALIAS ANTES DE USAR.*/

use db_leilao;

/*GRUPO DE ITENS*/
CREATE TABLE IF NOT EXISTS cad_grupos(

	id_grupo int not null AUTO_INCREMENT,
	nome_grupo text,

	PRIMARY KEY (id_grupo)

);

insert into cad_grupos (id_grupo,nome_grupo)
		values 
			(1,'Estados'),
			(2,'Generos');

CREATE TABLE IF NOT EXISTS cad_item_grupo(

	id_item_grupo int not null AUTO_INCREMENT,
	fk_grupo int,
	nome_item_grupo text,

	PRIMARY KEY (id_item_grupo),
	FOREIGN KEY (fk_grupo) REFERENCES cad_grupos (id_grupo)

);

insert into cad_item_grupo (id_item_grupo,fk_grupo,nome_item_grupo) 
		values 
			
			(1,1,'AC'), /*Estados*/
			(2,1,'AL'),
			(3,1,'AP'),
			(4,1,'AM'),
			(5,1,'BA'),
			(6,1,'CE'),
			(7,1,'DF'),
			(8,1,'ES'),
			(9,1,'GO'),
			(10,1,'MA'),
			(11,1,'MT'),
			(12,1,'MS'),
			(13,1,'MG'),
			(14,1,'PA'),
			(15,1,'PB'),
			(16,1,'PR'),
			(17,1,'PE'),	
			(18,1,'PI'),
			(19,1,'RJ'),
			(20,1,'RN'),
			(21,1,'RS'),
			(22,1,'RO'),
			(23,1,'RR'),
			(24,1,'SC'),
			(25,1,'SP'),
			(26,1,'SE'),
			(27,1,'TO'),

			(28,2,'Masculino'), /*Genero*/
			(29,2,'Feminino'); 

-- -----------------------------------------------------
-- Table seg_usuarios
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS seg_usuarios (
  id_usuario INT(11) NOT NULL AUTO_INCREMENT COMMENT 'ID Usuário',
  nome_usuario VARCHAR(100) NOT NULL COMMENT 'Nome Usuário',
  sexo_usuario int NULL COMMENT 'Sexo do usuário',
  rg_usuario VARCHAR(11) NULL COMMENT 'RG do usuário',
  cpf_usuario VARCHAR(11) NULL COMMENT 'cpf do usuario',
  email_usuario VARCHAR(40) NOT NULL COMMENT 'E-mail Usuário',
  telefone_usuario VARCHAR(11) NULL COMMENT 'Telefone Usuário',
  celular_usuario VARCHAR(11) NOT NULL COMMENT 'Celular usuário',
  
  cep_usuario VARCHAR(8) NULL COMMENT 'cep usuario',
  logradouro_usuario TEXT NULL COMMENT 'logradouro do usuario',
  bairro_usuario TEXT NULL COMMENT 'bairro do usuário',
  cidade_usuario VARCHAR(255) NULL COMMENT 'cidade do usuario',
  estado_usuario INT NULL COMMENT 'estado do usuario',
  numero_usuario VARCHAR(8) NULL COMMENT 'numero do usuario',
  complemento_usuario TEXT NULL COMMENT 'Complemento endereço',

  login_usuario VARCHAR(255) NOT NULL COMMENT 'Login Usuário',
  senha_usuario VARCHAR(40) NOT NULL COMMENT 'Senha Usuário',
  ativo_usuario TINYINT(1) NOT NULL DEFAULT 1 COMMENT 'Status Usuário',
  ativado_sms TINYINT(1) DEFAULT 0 comment "Ativado por SMS",
  fk_grupo_usuario INT(11) NOT NULL COMMENT 'Grupo Usuário',
  usuario_criou_usuario INT(11) NULL DEFAULT NULL COMMENT 'Usuário que criou',
  criacao_usuario TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Data criação',
  fbid TEXT NULL COMMENT 'id do facebook',

  token_acesso VARCHAR(255) NULL COMMENT 'token de acesso para login',
  
  PRIMARY KEY (id_usuario),
  UNIQUE (celular_usuario),
  UNIQUE (cpf_usuario),
  UNIQUE (email_usuario),
  UNIQUE (login_usuario),
  UNIQUE (token_acesso),
  FOREIGN KEY (usuario_criou_usuario) REFERENCES seg_usuarios (id_usuario),
  FOREIGN KEY (estado_usuario)    REFERENCES cad_item_grupo (id_item_grupo) /*Grupo 1*/
);

/*Segurança Grupos*/
CREATE TABLE IF NOT EXISTS seg_grupos(

	id_grupo int not null AUTO_INCREMENT comment "ID Grupo",
	nome_grupo character varying(100) NOT NULL comment "Nome Grupo",
	descricao_grupo text NOT NULL comment "Descrição Grupo",
	usuario_criou_grupo int comment "Usuário que criou",
	ativo_grupo boolean NOT NULL comment "Status Grupo",
	criacao_grupo timestamp DEFAULT CURRENT_TIMESTAMP comment "Data Criação",

	PRIMARY KEY (id_grupo),
	CONSTRAINT unique_nome_grupo UNIQUE (nome_grupo),
	FOREIGN KEY (usuario_criou_grupo) REFERENCES seg_usuarios (id_usuario)

);

ALTER TABLE seg_usuarios
ADD FOREIGN KEY (fk_grupo_usuario) REFERENCES seg_grupos(id_grupo);

/*Segurança, cadastro dos models*/
CREATE TABLE IF NOT EXISTS seg_models(

	id_model int not null AUTO_INCREMENT,
	link_model character varying(100) not null,
	descricao_model text not null,

	CONSTRAINT pk_model PRIMARY KEY (id_model)

);

CREATE TABLE IF NOT EXISTS seg_log_acesso(

	id_log_acesso int NOT NULL AUTO_INCREMENT,
	fk_usuario int,
	data_log_acesso timestamp DEFAULT CURRENT_TIMESTAMP,
	ip_usuario_acesso character varying(16),
	acesso boolean,
	maquina_usuario_acesso text,

	PRIMARY KEY (id_log_acesso),
	FOREIGN KEY (fk_usuario) REFERENCES seg_usuarios (id_usuario)

);

CREATE TABLE IF NOT EXISTS seg_log_erro(

	id_log_erro int NOT NULL AUTO_INCREMENT,
	fk_usuario int,
	data_log_erro timestamp DEFAULT CURRENT_TIMESTAMP,
	cod text, /*Text para casos onde o código seja texto ao invés de número*/
	erro text,
	query text,
	erro_feedback text, /*Relatado pelo usuário*/
	funcao text,
	maquina_usuario_erro text,

	PRIMARY KEY (id_log_erro),
	FOREIGN KEY (fk_usuario) REFERENCES seg_usuarios (id_usuario)

);

/*Segurança, cadastro dos controllers*/
CREATE TABLE IF NOT EXISTS seg_controllers(

	id_controller int not null AUTO_INCREMENT,
	link_controller character varying(100) not null,
	descricao_controller text not null,
	fk_model int,

	PRIMARY KEY (id_controller),
	FOREIGN KEY (fk_model) REFERENCES seg_models (id_model)

);

/*Segurança menu, gerá um menu no aplicativo, as telas devem ser vinculadas*/
CREATE TABLE IF NOT EXISTS seg_menu(

	id_menu int not null AUTO_INCREMENT,
	titulo_menu character varying(100) NOT NULL,
	descricao_menu text NOT NULL,
	menu_acima int, 
	posicao_menu int,

	PRIMARY KEY (id_menu),
	FOREIGN KEY (menu_acima) REFERENCES seg_menu (id_menu)

);

/*Segurança, aplicações do menu.*/
CREATE TABLE IF NOT EXISTS seg_aplicacao(

	id_aplicacao int not null AUTO_INCREMENT,
	link_aplicacao character varying(100) NOT NULL,
	titulo_aplicacao character varying(100) NOT NULL,
	descricao_aplicacao text NOT NULL,
	fk_controller int,

	PRIMARY KEY (id_aplicacao),
    FOREIGN KEY (fk_controller) REFERENCES seg_controllers (id_controller)

);

/*Gera um Log dos acessos do usuário.*/
CREATE TABLE IF NOT EXISTS seg_log_navegacao(

	id_log_navegacao int NOT NULL AUTO_INCREMENT,
	fk_usuario int,
	data_log_navegacao timestamp DEFAULT CURRENT_TIMESTAMP,
	permissao boolean,
	fk_aplicacao int,
	parametros text,

	PRIMARY KEY (id_log_navegacao),
	FOREIGN KEY (fk_usuario) REFERENCES seg_usuarios (id_usuario),
	FOREIGN KEY (fk_aplicacao) REFERENCES seg_aplicacao (id_aplicacao)

);

CREATE TABLE IF NOT EXISTS seg_log_edicao(

	id_log_edicao int NOT NULL AUTO_INCREMENT,
	fk_usuario int,
	data_log_edicao timestamp DEFAULT CURRENT_TIMESTAMP,
	original_edicao text,
	novo_edicao text,
	campo_edicao text,
	tabela_edicao text,
	fk_aplicacao int,
	id_edicao int,
	

	PRIMARY KEY (id_log_edicao),
	FOREIGN KEY (fk_usuario) REFERENCES seg_usuarios (id_usuario),
	FOREIGN KEY (fk_aplicacao) REFERENCES seg_aplicacao (id_aplicacao)

);

/*Vincula as aplicações ao menu*/
CREATE TABLE IF NOT EXISTS seg_aplicacoes_menu(

	id_aplicacoes_menu int not null AUTO_INCREMENT,
	fk_aplicacao int NOT NULL,
	fk_menu int NOT NULL,

	PRIMARY KEY (id_aplicacoes_menu),
	FOREIGN KEY (fk_aplicacao) REFERENCES seg_aplicacao (id_aplicacao),
    FOREIGN KEY (fk_menu) REFERENCES seg_menu (id_menu)

);

/*Segurança, Grupo aplicação*/
CREATE TABLE IF NOT EXISTS seg_aplicacoes_grupos(

	id_aplicacoes_grupos int not null AUTO_INCREMENT,
	fk_grupo int NOT NULL,
	fk_aplicacao int NOT NULL,

	PRIMARY KEY (id_aplicacoes_grupos),
	FOREIGN KEY (fk_aplicacao) REFERENCES seg_aplicacao (id_aplicacao),
    FOREIGN KEY (fk_grupo) REFERENCES seg_grupos (id_grupo)

);

/*Cadastro dos controllers e models*/
insert into seg_models (id_model,link_model,descricao_model) values 
	(1,'Model_menu','Responsável pelos menus.');
insert into seg_models (id_model,link_model,descricao_model) values 
	(2,'Model_seguranca','Responsável pelo acesso as sistema e controle de perfils.');
insert into seg_models (id_model,link_model,descricao_model) values 
	(3,'Model_usuarios','Responsável pelo gerênciamento dos usuários.');
insert into seg_models (id_model,link_model,descricao_model) values 
	(4,'Model_grupos','Responsável pelo gerênciamento dos grupos.');


insert into seg_controllers (id_controller,link_controller,descricao_controller,fk_model) values 
	(1,'Main','Responsável pelo gerênciamento do acesso ao sistema.',2);
insert into seg_controllers (id_controller,link_controller,descricao_controller,fk_model) values 
	(2,'Controller_usuarios','Responsável pelo gerênciamento dos usuários.',3);
insert into seg_controllers (id_controller,link_controller,descricao_controller,fk_model) values 
	(3,'Controller_grupos','Responsável pelo gerênciamento dos grupos.',4);


/*INSERTS PARA USUÁRIO DE TESTES*/
insert into seg_grupos(id_grupo,nome_grupo,descricao_grupo,ativo_grupo) values (1,'Administradores','Administradores do sistema',true);
insert into seg_grupos(id_grupo,nome_grupo,descricao_grupo,ativo_grupo) values (2,'Gernciamento','Gernciamento de painel',true);
insert into seg_grupos(id_grupo,nome_grupo,descricao_grupo,ativo_grupo) values (3,'Clientes','Clientes',true);

insert into seg_usuarios (id_usuario,nome_usuario,email_usuario,celular_usuario,login_usuario,senha_usuario,ativo_usuario,fk_grupo_usuario)
values (1,'Usuário Administrador','megamil3d@gmail.com','11962782329','admin','40bd001563085fc35165329ea1ff5c5ecbdbbeef',TRUE,1),
(2,'Gerente','gerente@leilao24h.com','11987654321','gerente','40bd001563085fc35165329ea1ff5c5ecbdbbeef',TRUE,2),
(3,'Cliente','cliente@leilao24h.com','11976543218','cliente','40bd001563085fc35165329ea1ff5c5ecbdbbeef',TRUE,3);

/*Definindo os menus.*/
insert into seg_menu (id_menu,titulo_menu,descricao_menu,menu_acima,posicao_menu) 
	values (1,'Segurança','Segurança, Perfils e Grupos',null,1000);	 /*1000 para Garantir que será o útima menu*/
insert into seg_menu (id_menu,titulo_menu,descricao_menu,menu_acima,posicao_menu) 
	values (2,'Editar Perfil','Editar Perfil',1,null);
insert into seg_menu (id_menu,titulo_menu,descricao_menu,menu_acima,posicao_menu) 
	values (3,'Usuários','Lista de usuários ADM',1,null);
insert into seg_menu (id_menu,titulo_menu,descricao_menu,menu_acima,posicao_menu) 
	values (4,'Grupos','Lista de grupos',1,null);

/*Difinindo aplicações e links*/
insert into seg_aplicacao (id_aplicacao,link_aplicacao,titulo_aplicacao,descricao_aplicacao,fk_controller) 
	values (1,'seguranca/view_editar_perfil','Editar Perfil','Editar Perfil',2);
insert into seg_aplicacao (id_aplicacao,link_aplicacao,titulo_aplicacao,descricao_aplicacao,fk_controller) 
	values (2,'seguranca/view_usuarios','Listar Usuários ADM','Listar Usuários ADM',2);
insert into seg_aplicacao (id_aplicacao,link_aplicacao,titulo_aplicacao,descricao_aplicacao,fk_controller) 
	values (3,'seguranca/view_grupos','Listar Grupos','Listar Grupos',3);

/*Aplicações sem menu*/
insert into seg_aplicacao (id_aplicacao,link_aplicacao,titulo_aplicacao,descricao_aplicacao,fk_controller) 
	values (4,'seguranca/view_editar_grupo','Editar Grupo','Editar Grupo',3);
insert into seg_aplicacao (id_aplicacao,link_aplicacao,titulo_aplicacao,descricao_aplicacao,fk_controller) 
	values (5,'seguranca/view_editar_usuario','Editar Usuário','Editar Usuários',2);
insert into seg_aplicacao (id_aplicacao,link_aplicacao,titulo_aplicacao,descricao_aplicacao,fk_controller) 
	values (6,'seguranca/view_novo_grupo','Novo Grupo','Novo Grupo',3);
insert into seg_aplicacao (id_aplicacao,link_aplicacao,titulo_aplicacao,descricao_aplicacao,fk_controller) 
	values (7,'seguranca/view_novo_usuario','Novo Usuário','Novo Usuários',2);

/*Indicando quais aplicações estão ligadas a quais menus.*/
insert into seg_aplicacoes_menu(fk_aplicacao,fk_menu)
	values (1,2);
insert into seg_aplicacoes_menu(fk_aplicacao,fk_menu)
	values (2,3);
insert into seg_aplicacoes_menu(fk_aplicacao,fk_menu)
	values (3,4);

/*Dando permissões para o grupo administrador.*/
insert into seg_aplicacoes_grupos(fk_aplicacao,fk_grupo)
	values(1,1); 
insert into seg_aplicacoes_grupos(fk_aplicacao,fk_grupo)
	values(2,1);
/*insert into seg_aplicacoes_grupos(fk_aplicacao,fk_grupo)
	values(3,1);
insert into seg_aplicacoes_grupos(fk_aplicacao,fk_grupo)
	values(4,1);*/
insert into seg_aplicacoes_grupos(fk_aplicacao,fk_grupo)
	values(5,1); 
insert into seg_aplicacoes_grupos(fk_aplicacao,fk_grupo)
	values(6,1);
insert into seg_aplicacoes_grupos(fk_aplicacao,fk_grupo)
	values(7,1);

/*Notificações*/
INSERT INTO seg_models (id_model,link_model, descricao_model) VALUES (5,'Model_notificacoes', 'Model Notificações');
INSERT INTO seg_controllers (id_controller,link_controller, descricao_controller, fk_model) VALUES (4,'Controller_notificacoes', 'Controller Notificações', 5);

INSERT INTO seg_menu (menu_acima, posicao_menu, titulo_menu, descricao_menu, id_menu) VALUES (NULL, 999, 'Notificações', 'Listas de Notificações', 5);
INSERT INTO seg_menu (menu_acima, posicao_menu, titulo_menu, descricao_menu, id_menu) VALUES (1, NULL, 'Enviar Notificação', 'Lista de suas notificações podendo enviar uma nova.', 6);

INSERT INTO seg_aplicacao (id_aplicacao,link_aplicacao, titulo_aplicacao, descricao_aplicacao, fk_controller) VALUES (8,'notificacoes/view_notificacoes', 'Notificações', 'Notificações', 4);
insert into seg_aplicacoes_menu(fk_aplicacao,fk_menu) values (8,5);
/*insert into seg_aplicacoes_grupos(fk_aplicacao,fk_grupo) values(8,1);*/

INSERT INTO seg_aplicacao (id_aplicacao,link_aplicacao, titulo_aplicacao, descricao_aplicacao, fk_controller) VALUES (9,'notificacoes/view_hist_notificacoes', 'Minhas Notificações', 'Minhas Notificações', 4);
insert into seg_aplicacoes_menu(fk_aplicacao,fk_menu) values (9,6);
/*insert into seg_aplicacoes_grupos(fk_aplicacao,fk_grupo) values(9,1);*/

INSERT INTO seg_aplicacao (id_aplicacao,link_aplicacao, titulo_aplicacao, descricao_aplicacao, fk_controller) VALUES (10,'notificacoes/view_notificar', 'Nova Notificação', 'Nova Notificação', 4);
/*insert into seg_aplicacoes_grupos(fk_aplicacao,fk_grupo) values(10,1);*/

INSERT INTO seg_aplicacao (id_aplicacao,link_aplicacao, titulo_aplicacao, descricao_aplicacao, fk_controller) VALUES (11,'notificacoes/view_editar_notificacao', 'Editar Notificação', 'Editar Notificação', 4);
/*insert into seg_aplicacoes_grupos(fk_aplicacao,fk_grupo) values(11,1);*/

/*Relatórios*/
INSERT INTO seg_models (id_model,link_model, descricao_model) VALUES (6,'Model_relatorios', 'Model Relatórios');
INSERT INTO seg_controllers (id_controller,link_controller, descricao_controller, fk_model) VALUES (5,'Controller_relatorios', 'Relatórios', 6);

INSERT INTO seg_menu (menu_acima, titulo_menu, descricao_menu, id_menu, posicao_menu) VALUES (NULL, 'Relatórios', 'Relatórios', 7, 998);
INSERT INTO seg_menu (menu_acima, titulo_menu, descricao_menu, id_menu, posicao_menu) VALUES (7, 'Acessos', 'Relatório de Acessos', 8, null);
INSERT INTO seg_menu (menu_acima, titulo_menu, descricao_menu, id_menu, posicao_menu) VALUES (7, 'Navegação', 'Relatório de Navegação', 9, null);
INSERT INTO seg_menu (menu_acima, titulo_menu, descricao_menu, id_menu, posicao_menu) VALUES (7, 'Edições', 'Relatório de Edições', 10, null);

INSERT INTO seg_aplicacao (id_aplicacao,link_aplicacao, titulo_aplicacao, descricao_aplicacao, fk_controller) VALUES (12,'relatorios/view_relatorio_acesso', 'Acessos', 'Relatório dos Acessos', 5);
insert into seg_aplicacoes_menu(fk_aplicacao,fk_menu) values (12,8);
/*insert into seg_aplicacoes_grupos(fk_aplicacao,fk_grupo) values(12,1);*/

INSERT INTO seg_aplicacao (id_aplicacao,link_aplicacao, titulo_aplicacao, descricao_aplicacao, fk_controller) VALUES (13,'relatorios/view_relatorio_navegacao', 'Navegação', 'Relatório de Navegação', 5);
insert into seg_aplicacoes_menu(fk_aplicacao,fk_menu) values (13,9);
/*insert into seg_aplicacoes_grupos(fk_aplicacao,fk_grupo) values(13,1);*/

INSERT INTO seg_aplicacao (id_aplicacao,link_aplicacao, titulo_aplicacao, descricao_aplicacao, fk_controller) VALUES (14,'relatorios/view_relatorio_edicoes', 'Edições', 'Relatório de Edições', 5);
insert into seg_aplicacoes_menu(fk_aplicacao,fk_menu) values (14,10);
/*insert into seg_aplicacoes_grupos(fk_aplicacao,fk_grupo) values(14,1);*/

CREATE TABLE IF NOT EXISTS cad_notificacao (

	id_notificacao int not null AUTO_INCREMENT,
	fk_usuario int,
	data_notificacao timestamp DEFAULT CURRENT_TIMESTAMP,
	titulo_notificacao character varying(20),
	notificacao text,
	data_limite_notificacao date,

	PRIMARY KEY (id_notificacao),
	FOREIGN KEY (fk_usuario) REFERENCES seg_usuarios (id_usuario)

);

CREATE TABLE IF NOT EXISTS cad_hist_notificacao (

	id_hist_notificacao int not null AUTO_INCREMENT,
	fk_usuario_destino int,
	fk_notificacao int,
	data_envio_notificacao timestamp DEFAULT CURRENT_TIMESTAMP,
	data_leitura timestamp DEFAULT 0,
	notificacao_enviada boolean DEFAULT false, /*Quando o navegador/mobile receber já marca para não enviar novamente.*/

	PRIMARY KEY (id_hist_notificacao),
	FOREIGN KEY (fk_notificacao) REFERENCES cad_notificacao (id_notificacao),
	FOREIGN KEY (fk_usuario_destino) REFERENCES seg_usuarios (id_usuario)

);


/*Views*/
-- Por não trazer os comentários das views não pode ajudar.
-- create or replace view view_alias as
-- 	SELECT TABLE_NAME as tabela,
-- 		   COLUMN_KEY as chave,
-- 		   COLUMN_NAME as coluna,
-- 		   DATA_TYPE as tipo,
-- 		   CHARACTER_MAXIMUM_LENGTH as tamanho,
-- 		   COLUMN_TYPE as coluna_detalhes,
-- 		   COLUMN_COMMENT as comentario
-- 	FROM INFORMATION_SCHEMA.COLUMNS
-- 	WHERE TABLE_SCHEMA = 'db_leilao';

/*Tabela que irá dar detalhes dos campos da view*/
CREATE TABLE IF NOT EXISTS cad_detalhes_views (

	id_detalhes_views  int not null AUTO_INCREMENT,
	nome_view text,
	nome_campo text,
	tipo_campo text,
	descricao_campo text,
	visivel boolean DEFAULT true, /*Existem campos que não devem ser listados, mas existem para serem usados de filtro*/
	
	PRIMARY KEY (id_detalhes_views)

);
/***************************************************************************************************/
create or replace view view_relatorio_acessos as
	SELECT  
		id_log_acesso as id,
		id_usuario,
		nome_usuario as usuario,
		data_log_acesso as data_acesso,
		date_format(data_log_acesso,'%d/%m/%Y às %H:%i:%s') as data_acesso_formatado,
		ip_usuario_acesso as ip,
		maquina_usuario_acesso as maquina,
		acesso, 

		case acesso 
			when 1 then 'Entrou' 
            when 0 then 'Saiu' end as acessou
            
		FROM seg_log_acesso
		inner join seg_usuarios on id_usuario = fk_usuario;

insert into cad_detalhes_views (nome_view,nome_campo,tipo_campo,descricao_campo,visivel) values 
	("view_relatorio_acessos","id","int","ID",true),
	("view_relatorio_acessos","usuario","text","Nome Usuário",true),
	("view_relatorio_acessos","data_acesso_formatado","timestamp","Data Acesso",true),
	("view_relatorio_acessos","ip","text","IP",true),
	("view_relatorio_acessos","maquina","text","Maquina / Navegador",true),
	("view_relatorio_acessos","acessou","text","Login / Logoff",true),
	-- Campos usados somente para o where.
	("view_relatorio_acessos","id_usuario","int","Nome Usuário",false),
	("view_relatorio_acessos","data_acesso","timestamp","Data Acesso",false),
	("view_relatorio_acessos","acesso","int","Login / Logoff",false);

/***************************************************************************************************/

create or replace view view_relatorio_navegacao as
	select 
    id_log_navegacao as id,
    fk_usuario,
    nome_usuario,
    data_log_navegacao as data_log,
    date_format(data_log_navegacao,'%d/%m/%Y às %H:%i:%s') as data_log_formatado,
    permissao,
    case permissao 
			when 1 then 'Sim' 
            when 0 then 'Não' end as com_permissao,
	fk_aplicacao,
    descricao_aplicacao,
    parametros
    
    from seg_log_navegacao
    inner join seg_usuarios on fk_usuario = id_usuario
    inner join seg_aplicacao on id_aplicacao = fk_aplicacao;

insert into cad_detalhes_views (nome_view,nome_campo,tipo_campo,descricao_campo,visivel) values 
	("view_relatorio_navegacao","id","int","ID",true),
	("view_relatorio_navegacao","nome_usuario","text","Nome Usuário",true),
	("view_relatorio_navegacao","data_log_formatado","timestamp","Data Acesso",true),
	("view_relatorio_navegacao","com_permissao","timestamp","Tem acesso?",true),
	("view_relatorio_navegacao","descricao_aplicacao","text","Aplicação",true),
	("view_relatorio_navegacao","parametros","text","Parametros Usados",true),

	("view_relatorio_navegacao","fk_usuario","int","Nome Usuário",false),
	("view_relatorio_navegacao","data_log","timestamp","Data Acesso",false),
	("view_relatorio_navegacao","permissao","tinyint","Tem acesso?",false),
	("view_relatorio_navegacao","fk_aplicacao","int","Aplicação",false);
/***************************************************************************************************/
create or replace view view_relatorio_edicoes as
	select 
    id_log_edicao as id,
    fk_usuario,
    nome_usuario,
    data_log_edicao as data_log,
    date_format(data_log_edicao,'%d/%m/%Y às %H:%i:%s') as data_log_formatado,
    original_edicao,
    novo_edicao,
    fk_aplicacao,
    descricao_aplicacao,
    id_edicao,
    (SELECT  COLUMN_COMMENT as comentario
	FROM INFORMATION_SCHEMA.COLUMNS
	WHERE TABLE_SCHEMA = 'db_leilao' AND TABLE_NAME = tabela_edicao AND COLUMN_NAME = campo_edicao) as campo
    
    from seg_log_edicao
    inner join seg_usuarios on fk_usuario = id_usuario
    inner join seg_aplicacao on id_aplicacao = fk_aplicacao;

insert into cad_detalhes_views (nome_view,nome_campo,tipo_campo,descricao_campo,visivel) values 
	("view_relatorio_edicoes","id","int","ID",true),
	("view_relatorio_edicoes","nome_usuario","text","Nome Usuário",true),
	("view_relatorio_edicoes","data_log_formatado","timestamp","Data Edição",true),
	("view_relatorio_edicoes","original_edicao","text","Valor Original",true),
	("view_relatorio_edicoes","novo_edicao","text","Novo Valor",true),
	("view_relatorio_edicoes","descricao_aplicacao","text","Titulo Aplicação",true),
	("view_relatorio_edicoes","id_edicao","int","Filtro Edição",true),
	("view_relatorio_edicoes","campo","text","Campo",true),

	("view_relatorio_edicoes","fk_usuario","int","Nome Usuário",false),
	("view_relatorio_edicoes","data_log","timestamp","Data Edição",false),
	("view_relatorio_edicoes","fk_aplicacao","int","Titulo Aplicação",false);
/***************************************************************************************************/
/*View de listas*/
create or replace view view_lista_grupos as
	select 
    id_grupo as id,
    nome_grupo,
    descricao_grupo,
    usuario_criou_grupo as usuario,
    nome_usuario,
    ativo_grupo,
    case ativo_grupo 
			when 1 then 'Ativo' 
            when 0 then 'Inativo' end as ativo,
    criacao_grupo,
    date_format(criacao_grupo,'%d/%m/%Y às %H:%i:%s') as criacao_grupo_formatado,
    (select count(*) from seg_usuarios where fk_grupo_usuario = id_grupo and ativo_usuario = true) as usuarios_ativos
    
    from seg_grupos
    left join seg_usuarios on id_usuario = usuario_criou_grupo;

insert into cad_detalhes_views (nome_view,nome_campo,tipo_campo,descricao_campo,visivel) values 
	("view_lista_grupos","id","int","ID",true),
	("view_lista_grupos","nome_grupo","text","Nome Grupo",true),
	("view_lista_grupos","descricao_grupo","text","Descrição Grupo",true),
	("view_lista_grupos","nome_usuario","text","Criado por",true),
	("view_lista_grupos","criacao_grupo_formatado","timestamp","Criado Em",true),
	("view_lista_grupos","usuarios_ativos","int","Usuários Ativos",true),
	("view_lista_grupos","ativo","tinyint","Status Grupo",true),

	("view_lista_grupos","usuario","int","Criado por",false),
	("view_lista_grupos","ativo_grupo","tinyint","Status Grupo",false),
	("view_lista_grupos","criacao_grupo","timestamp","Criado Em",false);
/***************************************************************************************************/
create or replace view view_lista_usuarios as
	select 
    id_usuario as id,
    nome_usuario,
    email_usuario,
    telefone_usuario,
    login_usuario,
    ativo_usuario,
    case ativo_usuario 
			when 1 then 'Ativo' 
            when 0 then 'Inativo' end as ativo,
    fk_grupo_usuario,
    nome_grupo,
    usuario_criou_usuario usuario,
    (select nome_usuario from seg_usuarios su0 where su0.id_usuario = su1.usuario_criou_usuario) as usuario_criou,
    criacao_usuario,
    date_format(criacao_usuario,'%d/%m/%Y às %H:%i:%s') as criacao_usuario_formatado
    
    from seg_usuarios su1
    inner join seg_grupos on id_grupo = fk_grupo_usuario;

insert into cad_detalhes_views (nome_view,nome_campo,tipo_campo,descricao_campo,visivel) values 
	("view_lista_usuarios","id","int","ID",true),
	("view_lista_usuarios","nome_usuario","text","Usuário",true),
	("view_lista_usuarios","email_usuario","text","E-mail",true),
	("view_lista_usuarios","telefone_usuario","text","Telefone",true),
	("view_lista_usuarios","login_usuario","text","Login",true),
	("view_lista_usuarios","ativo","text","Status Usuário",true),
	("view_lista_usuarios","nome_grupo","text","Grupo",true),
	("view_lista_usuarios","usuario_criou","text","Criado por",true),
	("view_lista_usuarios","criacao_usuario_formatado","timestamp","Criado em",true),
	
	("view_lista_usuarios","ativo_usuario","tinyint","Status Usuário",false),
	("view_lista_usuarios","fk_grupo_usuario","int","Grupo",false),
	("view_lista_usuarios","usuario","int","Criado por",false),
	("view_lista_usuarios","criacao_usuario","timestamp","Criado em",false);
/***************************************************************************************************/
/*Notificações PUSH, Tokens.*/
CREATE TABLE IF NOT EXISTS cad_tokens(

	id_token int not null auto_increment,
	token character varying(300),
	fk_usuario int,
	aparelho int, /*1 iOS, 2 Android, 3 WEB*/
	data_registro timestamp DEFAULT CURRENT_TIMESTAMP,

	PRIMARY KEY (id_token),
	FOREIGN KEY (fk_usuario) REFERENCES seg_usuarios(id_usuario)

);

/*********************************************************************************************************/

-- -----------------------------------------------------
-- Table cad_categorias
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS cad_categorias (
  id_categoria INT NOT NULL AUTO_INCREMENT,
  fk_categoria_pai INT NULL,
  nome_categoria VARCHAR(255) NOT NULL,
  ativa BOOLEAN NOT NULL DEFAULT true,
  PRIMARY KEY (id_categoria),
  FOREIGN KEY (fk_categoria_pai) REFERENCES cad_categorias (id_categoria)
);


-- -----------------------------------------------------
-- Table cad_produtos
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS cad_produtos (
  id_produto INT NOT NULL AUTO_INCREMENT,
  fk_categoria INT NOT NULL,
  nome_produto VARCHAR(255) NOT NULL,
  descricao_produto TEXT NULL,
  fk_usuario INT NOT NULL,
  PRIMARY KEY (id_produto),
  FOREIGN KEY (fk_categoria) REFERENCES cad_categorias (id_categoria),
  FOREIGN KEY (fk_usuario) REFERENCES seg_usuarios (id_usuario)
);

-- -----------------------------------------------------
-- Table status_leilao
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS status_leilao (
  id_status_leilao INT NOT NULL AUTO_INCREMENT,
  status VARCHAR(45) NOT NULL,
  PRIMARY KEY (id_status_leilao)
);

INSERT INTO status_leilao (status) VALUES ('Pendente'), ('Aprovado'), ('Reprovado'),('Finalizado');


-- -----------------------------------------------------
-- Table cad_leilao
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS cad_leilao (
  id_leilao INT(11) NOT NULL AUTO_INCREMENT,
  fk_produto INT(11) NOT NULL,
  valor_minimo DOUBLE NULL DEFAULT NULL,
  lance_minimo REAL NOT NULL DEFAULT 0.01,
  ingressos INT NOT NULL DEFAULT 1,
  data_inicio TIMESTAMP  DEFAULT 0,
  data_fim_previsto TIMESTAMP  DEFAULT 0,
  status_leilao INT NOT NULL,
  fk_usuario_arrematou int,
  data_fim_efetivo TIMESTAMP DEFAULT 0,
  notificado int DEFAULT 0,
  PRIMARY KEY (id_leilao),
  FOREIGN KEY (fk_produto) 				REFERENCES cad_produtos (id_produto),
  FOREIGN KEY (status_leilao) 			REFERENCES status_leilao (id_status_leilao),
  FOREIGN KEY (fk_usuario_arrematou) 	REFERENCES seg_usuarios (id_usuario)
);

-- -----------------------------------------------------
-- Table sala_leilao
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS sala_leilao (
  id_sala_leilao INT NOT NULL AUTO_INCREMENT,
  fk_leilao INT NULL,
  fk_usuario INT NULL,
  custo_entrada INT NULL,
  PRIMARY KEY (id_sala_leilao),
  UNIQUE(fk_leilao, fk_usuario),
  FOREIGN KEY (fk_leilao) REFERENCES cad_leilao (id_leilao),
  FOREIGN KEY (fk_usuario) REFERENCES seg_usuarios (id_usuario)
  );


-- -----------------------------------------------------
-- Table cad_lances
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS cad_lances (
  id_lances INT(11) NOT NULL AUTO_INCREMENT,
  valor DOUBLE NOT NULL,
  data_lance TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  fk_sala_leilao INT NULL,
  PRIMARY KEY (id_lances),
  FOREIGN KEY (fk_sala_leilao) REFERENCES sala_leilao (id_sala_leilao)
);


-- -----------------------------------------------------
-- Table pacote_ingresso
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS pacote_ingresso (
  id_pacote_ingresso INT NOT NULL AUTO_INCREMENT,
  descricao_pacote VARCHAR(45) NOT NULL,
  quantidade_ingresso INT NOT NULL,
  valor_pacote REAL NOT NULL,
  gratis TINYINT NOT NULL DEFAULT 0,
  PRIMARY KEY (id_pacote_ingresso));

-- -----------------------------------------------------
-- Table cad_ingressos
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS cad_ingressos (
  id_ingresso INT(11) NOT NULL AUTO_INCREMENT,
  fk_usuario INT(11) NOT NULL,
  quantidade INT(11) NOT NULL DEFAULT 0,
  ativo TINYINT NULL DEFAULT 0,
  prioridade INT NOT NULL,
  PRIMARY KEY (id_ingresso),
  FOREIGN KEY (fk_usuario) REFERENCES seg_usuarios (id_usuario));

-- -----------------------------------------------------
-- Table compra_ingresso
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS compra_ingresso (
  id_compra_ingresso INT(11) NOT NULL AUTO_INCREMENT,
  descricao VARCHAR(60) NULL,
  fk_ingresso INT(11) NOT NULL,
  data_compra TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  valor REAL NOT NULL,
  code_pagamento TEXT NULL,
  fk_pacote INT NOT NULL,
  PRIMARY KEY (id_compra_ingresso),
  FOREIGN KEY (fk_ingresso) REFERENCES cad_ingressos (id_ingresso),
  FOREIGN KEY (fk_pacote) REFERENCES pacote_ingresso (id_pacote_ingresso));

-- -----------------------------------------------------
-- VIEW view_ingresso
-- -----------------------------------------------------
create or replace view view_ingresso as
	select
		cdi.id_ingresso,
		cdi.fk_usuario,
		cdi.quantidade,
		cdi.ativo,
		cdi.prioridade,
		cmi.id_compra_ingresso,
		cmi.descricao,
		-- cmi.fk_ingresso,
		cmi.data_compra,
		round(cmi.valor, 2) valor,
		cmi.code_pagamento,
		-- cmi.fk_pacote,
		pci.id_pacote_ingresso,
		pci.descricao_pacote,
		pci.quantidade_ingresso,
		round(pci.valor_pacote, 2) valor_pacote,
		pci.gratis
	from cad_ingressos cdi
	inner join compra_ingresso cmi on (cdi.id_ingresso = cmi.fk_ingresso)
	inner join pacote_ingresso pci on (cmi.fk_pacote = pci.id_pacote_ingresso)
	-- where cdi.ativo is true
	-- and cdi.fk_usuario = 3
	-- and cdi.quantidade > 0
	-- order by pci.gratis desc, cdi.prioridade
;

-- -----------------------------------------------------
-- Table cad_avaliacoes
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS cad_avaliacoes (
  id_avalicao 	int(11) NOT NULL AUTO_INCREMENT,
  fk_cliente 	int NOT NULL,
  fk_leiloeiro 	int NOT NULL,
  fk_leilao 	int NOT NULL,
  avaliacao 	int DEFAULT 1,
  	PRIMARY KEY (id_avalicao),
  	UNIQUE(fk_leilao),
  	FOREIGN KEY (fk_leiloeiro) 
  		REFERENCES seg_usuarios (id_usuario),
  	FOREIGN KEY (fk_leilao) 
  		REFERENCES cad_leilao (id_leilao),
  	FOREIGN KEY (fk_cliente) 
  		REFERENCES seg_usuarios (id_usuario)
);

create or replace view view_leilao as
select
		cl.id_leilao,
		cl.valor_minimo,
		cl.lance_minimo,
		cl.ingressos,
		cl.data_inicio,
		cl.data_fim_previsto,
		cl.status_leilao,
		sl.status,
		cp.id_produto,
		cp.fk_categoria,
		cp.fk_usuario,
		cp.nome_produto,
		cp.descricao_produto,
        ss.nome_usuario,
        cl.fk_usuario_arrematou,
        ifnull((select nome_usuario from seg_usuarios ss_ where ss_.id_usuario = cl.fk_usuario_arrematou), "-") as nome_usuario_arrematou,
        (select nome_usuario from seg_usuarios ss_ where ss_.id_usuario = cp.fk_usuario) as nome_usuario_leiloando,
        round((select avg(avaliacao) avaliacao from cad_avaliacoes ca where ca.fk_leiloeiro = cp.fk_usuario),2) as avaliacao,

        (select format(valor,2,'de_DE') from cad_lances inner join sala_leilao on fk_sala_leilao = id_sala_leilao where sala_leilao.fk_leilao = cl.id_leilao order by valor desc limit 1) as lance_formatado,
        
        cc.nome_categoria

	from cad_leilao cl
	inner join cad_produtos 	cp	on cl.fk_produto = cp.id_produto
    inner join seg_usuarios 	ss	on ss.id_usuario = cp.fk_usuario
    inner join status_leilao 	sl	on sl.id_status_leilao = cl.status_leilao
    inner join cad_categorias 	cc	on cc.id_categoria = cp.fk_categoria
    group by id_leilao

-- -----------------------------------------------------
-- Table cad_favorito
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS cad_favoritos (
  fk_leilao INT NOT NULL,
  fk_usuario INT NOT NULL,
  PRIMARY KEY (fk_leilao, fk_usuario),
  FOREIGN KEY (fk_leilao) REFERENCES cad_leilao (id_leilao),
  FOREIGN KEY (fk_usuario) REFERENCES seg_usuarios (id_usuario)
);

/**********************************************************************************************/
/**********************************Menu dinamico e acesso**************************************/
/**********************************************************************************************/
INSERT INTO seg_models (link_model, descricao_model) VALUES ('Model_categorias', 'Model categorias');
INSERT INTO seg_controllers (link_controller, descricao_controller, fk_model) VALUES ('Controller_categorias', 'Controller categorias', 7);

INSERT INTO seg_menu (menu_acima, posicao_menu, titulo_menu, descricao_menu, id_menu) VALUES (NULL, '10', 'Categorias', 'Categorias', '11');

INSERT INTO seg_aplicacao (link_aplicacao, titulo_aplicacao, descricao_aplicacao, fk_controller) VALUES ('categorias/view_categorias', 'Categorias', 'Categorias', '6');
insert into seg_aplicacoes_menu(fk_aplicacao,fk_menu) values ( 15, 11);
insert into seg_aplicacoes_grupos(fk_aplicacao,fk_grupo) values( 15,1);


/********23/04/2018********/
INSERT INTO seg_models (link_model, descricao_model) VALUES ('Model_leiloes', 'Model leilões');
INSERT INTO seg_controllers (link_controller, descricao_controller, fk_model) VALUES ('Controller_leiloes', 'Controller leilões', 8);

INSERT INTO seg_menu (menu_acima, posicao_menu, titulo_menu, descricao_menu, id_menu) VALUES (NULL, '1', 'Leilão', 'Menus de leilões', '12');
INSERT INTO seg_menu (menu_acima, posicao_menu, titulo_menu, descricao_menu, id_menu) VALUES ('12', NULL, 'Pendentes', 'Leilões pendentes de aprovação', '13');

INSERT INTO seg_aplicacao (link_aplicacao, titulo_aplicacao, descricao_aplicacao, fk_controller) VALUES ('leilao/view_lista_leilao_pendente', 'Leilões Pendentes', 'View para listar leilões pendentes de aprovação', '7');
insert into seg_aplicacoes_menu(fk_aplicacao,fk_menu) values ( 16, 13);
insert into seg_aplicacoes_grupos(fk_aplicacao,fk_grupo) values( 16,1);


/********11/05/2018********/
INSERT INTO seg_menu (menu_acima, posicao_menu, titulo_menu, descricao_menu, id_menu) VALUES ('12', NULL, 'Leilões Aprovados', 'Leilões Aprovados', '14');

INSERT INTO seg_aplicacao (link_aplicacao, titulo_aplicacao, descricao_aplicacao, fk_controller) VALUES ('leilao/view_lista_leilao_ativo', 'Leilões Ativos', 'Leilões Ativos', '7');
insert into seg_aplicacoes_menu(fk_aplicacao,fk_menu) values ( 17, 14);
insert into seg_aplicacoes_grupos(fk_aplicacao,fk_grupo) values( 17,1);

INSERT INTO seg_menu (menu_acima, posicao_menu, titulo_menu, descricao_menu, id_menu) VALUES ('12', NULL, 'Leilões Finalizados', 'Leilões Finalizados', '15');

INSERT INTO seg_aplicacao (link_aplicacao, titulo_aplicacao, descricao_aplicacao, fk_controller) VALUES ('leilao/view_lista_leilao_finalizado', 'Leilões Finalizados', 'Leilões Finalizados', '7');
insert into seg_aplicacoes_menu(fk_aplicacao,fk_menu) values ( 18, 15);
insert into seg_aplicacoes_grupos(fk_aplicacao,fk_grupo) values( 18,1);

/********20/06/2018********/
/*Leilao*/
INSERT INTO seg_menu (menu_acima, posicao_menu, titulo_menu, descricao_menu, id_menu) VALUES ('12', NULL, 'Todos Leilões', 	'Todos Leilões', 	'16');
INSERT INTO seg_menu (menu_acima, posicao_menu, titulo_menu, descricao_menu, id_menu) VALUES ('12', NULL, 'Criar Leilão', 	'Criar Leilão', 	'17');

INSERT INTO seg_aplicacao (link_aplicacao, titulo_aplicacao, descricao_aplicacao, fk_controller) VALUES ('leilao/view_lista_leiloes', 'Todos Leilões', 'Listar todos leilões', '7');
INSERT INTO seg_aplicacao (link_aplicacao, titulo_aplicacao, descricao_aplicacao, fk_controller) VALUES ('leilao/view_novo_leiloes', 'Criar Leilões', 'Criar leilões', '7');

insert into seg_aplicacoes_menu(fk_aplicacao,fk_menu) values ( 19, 16);
insert into seg_aplicacoes_grupos(fk_aplicacao,fk_grupo) values( 19,1);

insert into seg_aplicacoes_menu(fk_aplicacao,fk_menu) values ( 20, 17);
insert into seg_aplicacoes_grupos(fk_aplicacao,fk_grupo) values( 20,1);

/*Produtos*/
INSERT INTO seg_models (link_model, descricao_model) VALUES ('Model_produtos', 'Model produtos');
INSERT INTO seg_controllers (link_controller, descricao_controller, fk_model) VALUES ('Controller_produtos', 'Controller produtos', 9);

INSERT INTO seg_menu (menu_acima, posicao_menu, titulo_menu, descricao_menu, id_menu) VALUES (null, 2, 	  'Produtos', 	'Produtos', 	'18');
INSERT INTO seg_menu (menu_acima, posicao_menu, titulo_menu, descricao_menu, id_menu) VALUES ('18', NULL, 'Criar produto', 	'Criar produto', 	'19');
INSERT INTO seg_menu (menu_acima, posicao_menu, titulo_menu, descricao_menu, id_menu) VALUES ('18', NULL, 'Ver Produtos', 	'Ver Produtos', 	'20');

INSERT INTO seg_aplicacao (link_aplicacao, titulo_aplicacao, descricao_aplicacao, fk_controller) VALUES ('produto/view_lista_produtos', 'Todos produtos', 	'Todos produtos', 	'8');
INSERT INTO seg_aplicacao (link_aplicacao, titulo_aplicacao, descricao_aplicacao, fk_controller) VALUES ('produto/view_novo_produto', 	'Criar produto', 	'Criar produto', 	'8');
INSERT INTO seg_aplicacao (link_aplicacao, titulo_aplicacao, descricao_aplicacao, fk_controller) VALUES ('produto/view_editar_produto', 'Editar produto', 	'Editar produto', 	'8');

insert into seg_aplicacoes_menu(fk_aplicacao,fk_menu) values ( 21, 19);
insert into seg_aplicacoes_grupos(fk_aplicacao,fk_grupo) values( 21,1);

insert into seg_aplicacoes_menu(fk_aplicacao,fk_menu) values ( 22, 20);
insert into seg_aplicacoes_grupos(fk_aplicacao,fk_grupo) values( 22,1);

insert into seg_aplicacoes_grupos(fk_aplicacao,fk_grupo) values( 23,1);

/*Pacotes*/
INSERT INTO seg_models (link_model, descricao_model) VALUES ('Model_pacotes', 'Model pacotes');
INSERT INTO seg_controllers (link_controller, descricao_controller, fk_model) VALUES ('Controller_pacotes', 'Controller pacotes', 10);

INSERT INTO seg_menu (menu_acima, posicao_menu, titulo_menu, descricao_menu, id_menu) VALUES (null, 3, 	  'Pacotes', 		'Pacotes', 		'21');
INSERT INTO seg_menu (menu_acima, posicao_menu, titulo_menu, descricao_menu, id_menu) VALUES ('21', NULL, 'Criar pacote', 	'Criar pacote', '22');
INSERT INTO seg_menu (menu_acima, posicao_menu, titulo_menu, descricao_menu, id_menu) VALUES ('21', NULL, 'Ver Pacotes', 	'Ver Pacotes', 	'23');

INSERT INTO seg_aplicacao (link_aplicacao, titulo_aplicacao, descricao_aplicacao, fk_controller) VALUES ('pacote/view_lista_pacotes', 	'Todos pacotes', 	'Todos pacotes', 	'9');
INSERT INTO seg_aplicacao (link_aplicacao, titulo_aplicacao, descricao_aplicacao, fk_controller) VALUES ('pacote/view_novo_pacote', 	'Criar pacote', 	'Criar pacote', 	'9');
INSERT INTO seg_aplicacao (link_aplicacao, titulo_aplicacao, descricao_aplicacao, fk_controller) VALUES ('pacote/view_editar_pacote', 	'Editar pacote', 	'Editar pacote', 	'9');

insert into seg_aplicacoes_menu(fk_aplicacao,fk_menu) values ( 24, 22);
insert into seg_aplicacoes_grupos(fk_aplicacao,fk_grupo) values( 24,1);

insert into seg_aplicacoes_menu(fk_aplicacao,fk_menu) values ( 25, 23);
insert into seg_aplicacoes_grupos(fk_aplicacao,fk_grupo) values( 25,1);

insert into seg_aplicacoes_grupos(fk_aplicacao,fk_grupo) values( 26,1);


insert into seg_menu (id_menu,titulo_menu,descricao_menu,menu_acima,posicao_menu) 
	values (24,'Usuários','Lista Geral Usuários',1,null);
insert into seg_aplicacao (link_aplicacao,titulo_aplicacao,descricao_aplicacao,fk_controller) 
	values ('seguranca/view_usuarios_geral','Listar Geral Usuários','Listar Usuários ADM',2);

insert into seg_aplicacoes_menu(fk_aplicacao,fk_menu) values ( 27, 24);
insert into seg_aplicacoes_grupos(fk_aplicacao,fk_grupo) values( 27,1);

/*Produtos Banners*/
INSERT INTO seg_menu (menu_acima, posicao_menu, titulo_menu, descricao_menu, id_menu) VALUES (null, 4, 	  'Banners', 	'Banners', 	'28');
INSERT INTO seg_aplicacao (link_aplicacao, titulo_aplicacao, descricao_aplicacao, fk_controller) VALUES ('produto/view_banners', 'Banner APP', 	'Banner APP', 	'8');
insert into seg_aplicacoes_menu(fk_aplicacao,fk_menu) values ( 32, 28);
insert into seg_aplicacoes_grupos(fk_aplicacao,fk_grupo) values( 32,1);
/*Produtos*/

create or replace view view_usuario_leilao as 
SELECT 
	id_sala_leilao, 
	id_usuario,
    nome_usuario, 
    id_leilao, 
    valor_minimo, 
    round(ifnull((select max(valor) as valor from cad_lances inner join sala_leilao on fk_sala_leilao = id_sala_leilao where fk_leilao = id_leilao limit 1),valor_minimo),2) as valor_atual, 
    lance_minimo,
    data_inicio, 
    data_fim_previsto, 
    token_acesso, 
    timestampdiff(SECOND, current_timestamp, data_inicio) as inicio, 
     
    if(timestampdiff(SECOND, current_timestamp, data_fim_previsto) < 0 ,0,timestampdiff(SECOND, current_timestamp, data_fim_previsto)) as fim

    	FROM cad_leilao
            inner join sala_leilao on fk_leilao = id_leilao
            inner join seg_usuarios on id_usuario = fk_usuario;



create or replace view view_notificar_leiloes as 

	SELECT
		1 as tipo,
		id_leilao as id,
	    concat('Não perca o(a) ',nome_produto) as titulo,
	    concat('O leilão do produto: \'',nome_produto,'\' já vai começar!') as descricao,
	    token as token_usuario,
	    timestampdiff(SECOND, current_timestamp, data_inicio) as inicio,
	    timestampdiff(SECOND, current_timestamp, data_fim_previsto) as fim,
	    ifnull(notificado,0) as notificado			   
		   from sala_leilao	as sl
		    inner join cad_leilao 	cl on id_leilao	 	= fk_leilao
		    inner join cad_tokens 	ct on ct.fk_usuario = sl.fk_usuario
		    inner join cad_produtos cp on id_produto 	= fk_produto
	        	where notificado = 0 and timestampdiff(SECOND, current_timestamp, data_inicio) <= 60

UNION

	SELECT
		2 as tipo,
		id_leilao as id,
	    concat(nome_produto,' Finalizando') as titulo,
        concat('Ainda dá tempo de arrematar o(a): ',nome_produto) as descricao,
	    token as token_usuario,
	    timestampdiff(SECOND, current_timestamp, data_inicio) as inicio,
	    timestampdiff(SECOND, current_timestamp, data_fim_previsto) as fim,
	    ifnull(notificado,0) as notificado			   
		   from sala_leilao	as sl
		    inner join cad_leilao 	cl on id_leilao	 	= fk_leilao
		    inner join cad_tokens 	ct on ct.fk_usuario = sl.fk_usuario
		    inner join cad_produtos cp on id_produto 	= fk_produto
            	where notificado = 1 and timestampdiff(SECOND, current_timestamp, data_fim_previsto) <= 60

UNION

	SELECT
		3 as tipo,
		id_leilao as id,
	    concat(nome_produto,' Finalizado') as titulo,
                       
        concat('Leilão \'',nome_produto,'\' Finalizado') as descricao,                
                        
	    token as token_usuario,
	    timestampdiff(SECOND, current_timestamp, data_inicio) as inicio,
	    timestampdiff(SECOND, current_timestamp, data_fim_previsto) as fim,
	    ifnull(notificado,0) as notificado			   
		   from sala_leilao	as sl
		    inner join cad_leilao 	cl on id_leilao	 	= fk_leilao
		    inner join cad_tokens 	ct on ct.fk_usuario = sl.fk_usuario
		    inner join cad_produtos cp on id_produto 	= fk_produto
            	where notificado = 2 and timestampdiff(SECOND, current_timestamp, data_fim_previsto) <= 0

UNION

	SELECT
		4 as tipo,
		id_leilao as id,
	    concat(nome_produto,' Finalizado') as titulo,
                       
        concat('O seu leilão do produto: \'',nome_produto,'\' foi finalizado') as descricao,                
                        
	    token as token_usuario,
	    timestampdiff(SECOND, current_timestamp, data_inicio) as inicio,
	    timestampdiff(SECOND, current_timestamp, data_fim_previsto) as fim,
	    ifnull(notificado,0) as notificado			   
		   from cad_leilao 	cl 
		    inner join cad_produtos cp on id_produto 	= fk_produto
		    inner join cad_tokens 	ct on ct.fk_usuario = cp.fk_usuario
            	where notificado = 2 and timestampdiff(SECOND, current_timestamp, data_fim_previsto) <= 0
            		order by tipo,id;


CREATE TABLE IF NOT EXISTS cad_creditos (

	id_creditos int NOT NULL AUTO_INCREMENT,
	fk_usuario int,
	creditos int,

	PRIMARY KEY (id_creditos),
	FOREIGN KEY (fk_usuario) REFERENCES seg_usuarios (id_usuario)

);

insert into cad_creditos (fk_usuario,creditos) values (1,0) , (2,0) , (3,0);