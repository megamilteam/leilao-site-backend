-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: 04-Fev-2019 às 16:41
-- Versão do servidor: 5.7.24-0ubuntu0.18.04.1
-- PHP Version: 7.2.10-0ubuntu0.18.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_leilao`
--
CREATE DATABASE IF NOT EXISTS `db_leilao` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `db_leilao`;

-- --------------------------------------------------------

--
-- Estrutura da tabela `cad_avaliacoes`
--

DROP TABLE IF EXISTS `cad_avaliacoes`;
CREATE TABLE IF NOT EXISTS `cad_avaliacoes` (
  `id_avalicao` int(11) NOT NULL AUTO_INCREMENT,
  `fk_cliente` int(11) NOT NULL,
  `fk_leiloeiro` int(11) NOT NULL,
  `fk_leilao` int(11) NOT NULL,
  `avaliacao` int(11) DEFAULT '1',
  PRIMARY KEY (`id_avalicao`),
  UNIQUE KEY `fk_leilao` (`fk_leilao`),
  KEY `fk_leiloeiro` (`fk_leiloeiro`),
  KEY `fk_cliente` (`fk_cliente`)
) ENGINE=InnoDB AUTO_INCREMENT=90 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `cad_categorias`
--

DROP TABLE IF EXISTS `cad_categorias`;
CREATE TABLE IF NOT EXISTS `cad_categorias` (
  `id_categoria` int(11) NOT NULL AUTO_INCREMENT,
  `fk_categoria_pai` int(11) DEFAULT NULL,
  `nome_categoria` varchar(255) NOT NULL,
  `ativa` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_categoria`),
  KEY `fk_categoria_pai` (`fk_categoria_pai`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `cad_compra_pacotes`
--

DROP TABLE IF EXISTS `cad_compra_pacotes`;
CREATE TABLE IF NOT EXISTS `cad_compra_pacotes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` text,
  `quantity` int(11) DEFAULT NULL,
  `currency_id` varchar(3) DEFAULT NULL,
  `unit_price` double DEFAULT NULL,
  `date_buy` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fk_pacote` int(11) DEFAULT NULL,
  `fk_usuario` int(11) DEFAULT NULL,
  `quantidade_ingresso` int(11) DEFAULT NULL,
  `link_compra` text,
  `id_consulta_preference` text,
  `topic` text,
  `finalizado` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fk_usuario` (`fk_usuario`),
  KEY `fk_pacote` (`fk_pacote`)
) ENGINE=InnoDB AUTO_INCREMENT=1062 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `cad_compra_recompensa`
--

DROP TABLE IF EXISTS `cad_compra_recompensa`;
CREATE TABLE IF NOT EXISTS `cad_compra_recompensa` (
  `id_compra_recompensa` int(11) NOT NULL AUTO_INCREMENT,
  `fk_produto` int(11) DEFAULT NULL,
  `fk_usuario` int(11) DEFAULT NULL,
  `data_compra` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `entregue` tinyint(1) DEFAULT '0',
  `data_entrega` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id_compra_recompensa`),
  KEY `fk_produto` (`fk_produto`),
  KEY `fk_usuario` (`fk_usuario`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `cad_creditos`
--

DROP TABLE IF EXISTS `cad_creditos`;
CREATE TABLE IF NOT EXISTS `cad_creditos` (
  `id_creditos` int(11) NOT NULL AUTO_INCREMENT,
  `fk_usuario` int(11) DEFAULT NULL,
  `creditos` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_creditos`),
  KEY `fk_usuario` (`fk_usuario`)
) ENGINE=InnoDB AUTO_INCREMENT=4420 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `cad_detalhes_views`
--

DROP TABLE IF EXISTS `cad_detalhes_views`;
CREATE TABLE IF NOT EXISTS `cad_detalhes_views` (
  `id_detalhes_views` int(11) NOT NULL AUTO_INCREMENT,
  `nome_view` text,
  `nome_campo` text,
  `tipo_campo` text,
  `descricao_campo` text,
  `visivel` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id_detalhes_views`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `cad_favoritos`
--

DROP TABLE IF EXISTS `cad_favoritos`;
CREATE TABLE IF NOT EXISTS `cad_favoritos` (
  `fk_leilao` int(11) NOT NULL,
  `fk_usuario` int(11) NOT NULL,
  PRIMARY KEY (`fk_leilao`,`fk_usuario`),
  KEY `fk_usuario` (`fk_usuario`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `cad_grupos`
--

DROP TABLE IF EXISTS `cad_grupos`;
CREATE TABLE IF NOT EXISTS `cad_grupos` (
  `id_grupo` int(11) NOT NULL AUTO_INCREMENT,
  `nome_grupo` text,
  PRIMARY KEY (`id_grupo`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `cad_hist_notificacao`
--

DROP TABLE IF EXISTS `cad_hist_notificacao`;
CREATE TABLE IF NOT EXISTS `cad_hist_notificacao` (
  `id_hist_notificacao` int(11) NOT NULL AUTO_INCREMENT,
  `fk_usuario_destino` int(11) DEFAULT NULL,
  `fk_notificacao` int(11) DEFAULT NULL,
  `data_envio_notificacao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `data_leitura` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `notificacao_enviada` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id_hist_notificacao`),
  KEY `fk_notificacao` (`fk_notificacao`),
  KEY `fk_usuario_destino` (`fk_usuario_destino`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `cad_ingressos`
--

DROP TABLE IF EXISTS `cad_ingressos`;
CREATE TABLE IF NOT EXISTS `cad_ingressos` (
  `id_ingresso` int(11) NOT NULL AUTO_INCREMENT,
  `fk_usuario` int(11) NOT NULL,
  `quantidade` int(11) NOT NULL DEFAULT '0',
  `ativo` tinyint(4) DEFAULT '0',
  `prioridade` int(11) NOT NULL,
  PRIMARY KEY (`id_ingresso`),
  KEY `fk_usuario` (`fk_usuario`)
) ENGINE=InnoDB AUTO_INCREMENT=153 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `cad_item_grupo`
--

DROP TABLE IF EXISTS `cad_item_grupo`;
CREATE TABLE IF NOT EXISTS `cad_item_grupo` (
  `id_item_grupo` int(11) NOT NULL AUTO_INCREMENT,
  `fk_grupo` int(11) DEFAULT NULL,
  `nome_item_grupo` text,
  PRIMARY KEY (`id_item_grupo`),
  KEY `fk_grupo` (`fk_grupo`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `cad_lances`
--

DROP TABLE IF EXISTS `cad_lances`;
CREATE TABLE IF NOT EXISTS `cad_lances` (
  `id_lances` int(11) NOT NULL AUTO_INCREMENT,
  `valor` double NOT NULL,
  `data_lance` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fk_sala_leilao` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_lances`),
  KEY `fk_sala_leilao` (`fk_sala_leilao`)
) ENGINE=InnoDB AUTO_INCREMENT=2273 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `cad_leilao`
--

DROP TABLE IF EXISTS `cad_leilao`;
CREATE TABLE IF NOT EXISTS `cad_leilao` (
  `id_leilao` int(11) NOT NULL AUTO_INCREMENT,
  `fk_produto` int(11) NOT NULL,
  `valor_minimo` double DEFAULT NULL,
  `lance_minimo` double NOT NULL DEFAULT '0.01',
  `ingressos` int(11) NOT NULL DEFAULT '1',
  `data_inicio` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `data_fim_previsto` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `status_leilao` int(11) NOT NULL,
  `fk_usuario_arrematou` int(11) DEFAULT NULL,
  `quantidade_minima_usuarios` int(11) DEFAULT NULL,
  `tempo_iniciar` int(11) DEFAULT NULL,
  `data_fim_efetivo` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `notificado` int(11) DEFAULT '0',
  `link_compra` text NOT NULL,
  `id_consulta_preference` text NOT NULL,
  `topic` text NOT NULL,
  `finalizado` tinyint(4) NOT NULL,
  `peso` float NOT NULL,
  `altura` float NOT NULL,
  `largura` float NOT NULL,
  `comprimento` float NOT NULL,
  PRIMARY KEY (`id_leilao`),
  KEY `fk_produto` (`fk_produto`),
  KEY `status_leilao` (`status_leilao`),
  KEY `fk_usuario_arrematou` (`fk_usuario_arrematou`)
) ENGINE=InnoDB AUTO_INCREMENT=638 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `cad_notificacao`
--

DROP TABLE IF EXISTS `cad_notificacao`;
CREATE TABLE IF NOT EXISTS `cad_notificacao` (
  `id_notificacao` int(11) NOT NULL AUTO_INCREMENT,
  `fk_usuario` int(11) DEFAULT NULL,
  `data_notificacao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `titulo_notificacao` varchar(20) DEFAULT NULL,
  `notificacao` text,
  `data_limite_notificacao` date DEFAULT NULL,
  PRIMARY KEY (`id_notificacao`),
  KEY `fk_usuario` (`fk_usuario`)
) ENGINE=InnoDB AUTO_INCREMENT=94 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `cad_produtos`
--

DROP TABLE IF EXISTS `cad_produtos`;
CREATE TABLE IF NOT EXISTS `cad_produtos` (
  `id_produto` int(11) NOT NULL AUTO_INCREMENT,
  `fk_categoria` int(11) NOT NULL,
  `nome_produto` varchar(255) NOT NULL,
  `produto_usado` tinyint(1) DEFAULT NULL,
  `descricao_produto` text,
  `fk_usuario` int(11) NOT NULL,
  `produto_recompensa` tinyint(1) DEFAULT '0',
  `produto_recompensa_custo` double DEFAULT NULL,
  PRIMARY KEY (`id_produto`),
  KEY `fk_categoria` (`fk_categoria`),
  KEY `fk_usuario` (`fk_usuario`)
) ENGINE=InnoDB AUTO_INCREMENT=256 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `cad_tokens`
--

DROP TABLE IF EXISTS `cad_tokens`;
CREATE TABLE IF NOT EXISTS `cad_tokens` (
  `id_token` int(11) NOT NULL AUTO_INCREMENT,
  `token` varchar(300) DEFAULT NULL,
  `fk_usuario` int(11) DEFAULT NULL,
  `aparelho` int(11) DEFAULT NULL,
  `data_registro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_token`),
  KEY `fk_usuario` (`fk_usuario`)
) ENGINE=InnoDB AUTO_INCREMENT=3471 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `compra_ingresso`
--

DROP TABLE IF EXISTS `compra_ingresso`;
CREATE TABLE IF NOT EXISTS `compra_ingresso` (
  `id_compra_ingresso` int(11) NOT NULL AUTO_INCREMENT,
  `descricao` varchar(60) DEFAULT NULL,
  `fk_ingresso` int(11) NOT NULL,
  `data_compra` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `valor` double NOT NULL,
  `code_pagamento` text,
  `fk_pacote` int(11) NOT NULL,
  PRIMARY KEY (`id_compra_ingresso`),
  KEY `fk_ingresso` (`fk_ingresso`),
  KEY `fk_pacote` (`fk_pacote`)
) ENGINE=InnoDB AUTO_INCREMENT=153 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `hist_compra_pacotes`
--

DROP TABLE IF EXISTS `hist_compra_pacotes`;
CREATE TABLE IF NOT EXISTS `hist_compra_pacotes` (
  `id_hist_compra_pacotes` int(11) NOT NULL AUTO_INCREMENT,
  `fk_compra_pacotes` int(11) DEFAULT NULL,
  `id` text,
  `transaction_amount` double DEFAULT NULL,
  `total_paid_amount` double DEFAULT NULL,
  `shipping_cost` double DEFAULT NULL,
  `currency_id` varchar(3) DEFAULT NULL,
  `status` text,
  `status_detail` text,
  `operation_type` text,
  `date_approved` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_created` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `amount_refunded` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_hist_compra_pacotes`),
  KEY `fk_compra_pacotes` (`fk_compra_pacotes`)
) ENGINE=InnoDB AUTO_INCREMENT=6197 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `pacote_ingresso`
--

DROP TABLE IF EXISTS `pacote_ingresso`;
CREATE TABLE IF NOT EXISTS `pacote_ingresso` (
  `id_pacote_ingresso` int(11) NOT NULL AUTO_INCREMENT,
  `descricao_pacote` varchar(45) NOT NULL,
  `quantidade_ingresso` int(11) NOT NULL,
  `valor_pacote` double NOT NULL,
  `gratis` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_pacote_ingresso`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `sala_leilao`
--

DROP TABLE IF EXISTS `sala_leilao`;
CREATE TABLE IF NOT EXISTS `sala_leilao` (
  `id_sala_leilao` int(11) NOT NULL AUTO_INCREMENT,
  `fk_leilao` int(11) DEFAULT NULL,
  `fk_usuario` int(11) DEFAULT NULL,
  `custo_entrada` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_sala_leilao`),
  UNIQUE KEY `fk_leilao` (`fk_leilao`,`fk_usuario`),
  KEY `fk_usuario` (`fk_usuario`)
) ENGINE=InnoDB AUTO_INCREMENT=22048 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `seg_aplicacao`
--

DROP TABLE IF EXISTS `seg_aplicacao`;
CREATE TABLE IF NOT EXISTS `seg_aplicacao` (
  `id_aplicacao` int(11) NOT NULL AUTO_INCREMENT,
  `link_aplicacao` varchar(100) NOT NULL,
  `titulo_aplicacao` varchar(100) NOT NULL,
  `descricao_aplicacao` text NOT NULL,
  `fk_controller` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_aplicacao`),
  KEY `fk_controller` (`fk_controller`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `seg_aplicacoes_grupos`
--

DROP TABLE IF EXISTS `seg_aplicacoes_grupos`;
CREATE TABLE IF NOT EXISTS `seg_aplicacoes_grupos` (
  `id_aplicacoes_grupos` int(11) NOT NULL AUTO_INCREMENT,
  `fk_grupo` int(11) NOT NULL,
  `fk_aplicacao` int(11) NOT NULL,
  PRIMARY KEY (`id_aplicacoes_grupos`),
  KEY `fk_aplicacao` (`fk_aplicacao`),
  KEY `fk_grupo` (`fk_grupo`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `seg_aplicacoes_menu`
--

DROP TABLE IF EXISTS `seg_aplicacoes_menu`;
CREATE TABLE IF NOT EXISTS `seg_aplicacoes_menu` (
  `id_aplicacoes_menu` int(11) NOT NULL AUTO_INCREMENT,
  `fk_aplicacao` int(11) NOT NULL,
  `fk_menu` int(11) NOT NULL,
  PRIMARY KEY (`id_aplicacoes_menu`),
  KEY `fk_aplicacao` (`fk_aplicacao`),
  KEY `fk_menu` (`fk_menu`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `seg_controllers`
--

DROP TABLE IF EXISTS `seg_controllers`;
CREATE TABLE IF NOT EXISTS `seg_controllers` (
  `id_controller` int(11) NOT NULL AUTO_INCREMENT,
  `link_controller` varchar(100) NOT NULL,
  `descricao_controller` text NOT NULL,
  `fk_model` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_controller`),
  KEY `fk_model` (`fk_model`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `seg_grupos`
--

DROP TABLE IF EXISTS `seg_grupos`;
CREATE TABLE IF NOT EXISTS `seg_grupos` (
  `id_grupo` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID Grupo',
  `nome_grupo` varchar(100) NOT NULL COMMENT 'Nome Grupo',
  `descricao_grupo` text NOT NULL COMMENT 'Descrição Grupo',
  `usuario_criou_grupo` int(11) DEFAULT NULL COMMENT 'Usuário que criou',
  `ativo_grupo` tinyint(1) NOT NULL COMMENT 'Status Grupo',
  `criacao_grupo` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Data Criação',
  PRIMARY KEY (`id_grupo`),
  UNIQUE KEY `unique_nome_grupo` (`nome_grupo`),
  KEY `usuario_criou_grupo` (`usuario_criou_grupo`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `seg_log_acesso`
--

DROP TABLE IF EXISTS `seg_log_acesso`;
CREATE TABLE IF NOT EXISTS `seg_log_acesso` (
  `id_log_acesso` int(11) NOT NULL AUTO_INCREMENT,
  `fk_usuario` int(11) DEFAULT NULL,
  `data_log_acesso` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ip_usuario_acesso` varchar(16) DEFAULT NULL,
  `acesso` tinyint(1) DEFAULT NULL,
  `maquina_usuario_acesso` text,
  PRIMARY KEY (`id_log_acesso`),
  KEY `fk_usuario` (`fk_usuario`)
) ENGINE=InnoDB AUTO_INCREMENT=732 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `seg_log_edicao`
--

DROP TABLE IF EXISTS `seg_log_edicao`;
CREATE TABLE IF NOT EXISTS `seg_log_edicao` (
  `id_log_edicao` int(11) NOT NULL AUTO_INCREMENT,
  `fk_usuario` int(11) DEFAULT NULL,
  `data_log_edicao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `original_edicao` text,
  `novo_edicao` text,
  `campo_edicao` text,
  `tabela_edicao` text,
  `fk_aplicacao` int(11) DEFAULT NULL,
  `id_edicao` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_log_edicao`),
  KEY `fk_usuario` (`fk_usuario`),
  KEY `fk_aplicacao` (`fk_aplicacao`)
) ENGINE=InnoDB AUTO_INCREMENT=1948 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `seg_log_erro`
--

DROP TABLE IF EXISTS `seg_log_erro`;
CREATE TABLE IF NOT EXISTS `seg_log_erro` (
  `id_log_erro` int(11) NOT NULL AUTO_INCREMENT,
  `fk_usuario` int(11) DEFAULT NULL,
  `data_log_erro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `cod` text,
  `erro` text,
  `query` text,
  `erro_feedback` text,
  `funcao` text,
  `maquina_usuario_erro` text,
  PRIMARY KEY (`id_log_erro`),
  KEY `fk_usuario` (`fk_usuario`)
) ENGINE=InnoDB AUTO_INCREMENT=422 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `seg_log_navegacao`
--

DROP TABLE IF EXISTS `seg_log_navegacao`;
CREATE TABLE IF NOT EXISTS `seg_log_navegacao` (
  `id_log_navegacao` int(11) NOT NULL AUTO_INCREMENT,
  `fk_usuario` int(11) DEFAULT NULL,
  `data_log_navegacao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `permissao` tinyint(1) DEFAULT NULL,
  `fk_aplicacao` int(11) DEFAULT NULL,
  `parametros` text,
  PRIMARY KEY (`id_log_navegacao`),
  KEY `fk_usuario` (`fk_usuario`),
  KEY `fk_aplicacao` (`fk_aplicacao`)
) ENGINE=InnoDB AUTO_INCREMENT=5636 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `seg_menu`
--

DROP TABLE IF EXISTS `seg_menu`;
CREATE TABLE IF NOT EXISTS `seg_menu` (
  `id_menu` int(11) NOT NULL AUTO_INCREMENT,
  `titulo_menu` varchar(100) NOT NULL,
  `descricao_menu` text NOT NULL,
  `menu_acima` int(11) DEFAULT NULL,
  `posicao_menu` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_menu`),
  KEY `menu_acima` (`menu_acima`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `seg_models`
--

DROP TABLE IF EXISTS `seg_models`;
CREATE TABLE IF NOT EXISTS `seg_models` (
  `id_model` int(11) NOT NULL AUTO_INCREMENT,
  `link_model` varchar(100) NOT NULL,
  `descricao_model` text NOT NULL,
  PRIMARY KEY (`id_model`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `seg_usuarios`
--

DROP TABLE IF EXISTS `seg_usuarios`;
CREATE TABLE IF NOT EXISTS `seg_usuarios` (
  `id_usuario` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID Usuário',
  `nome_usuario` varchar(100) NOT NULL COMMENT 'Nome Usuário',
  `sexo_usuario` int(11) DEFAULT NULL COMMENT 'Sexo do usuário',
  `rg_usuario` varchar(11) DEFAULT NULL COMMENT 'RG do usuário',
  `cpf_usuario` varchar(11) DEFAULT NULL COMMENT 'cpf do usuario',
  `email_usuario` varchar(40) NOT NULL COMMENT 'E-mail Usuário',
  `telefone_usuario` varchar(11) DEFAULT NULL COMMENT 'Telefone Usuário',
  `celular_usuario` varchar(11) NOT NULL COMMENT 'Celular usuário',
  `cep_usuario` varchar(8) DEFAULT NULL COMMENT 'cep usuario',
  `logradouro_usuario` text COMMENT 'logradouro do usuario',
  `bairro_usuario` text COMMENT 'bairro do usuário',
  `cidade_usuario` varchar(255) DEFAULT NULL COMMENT 'cidade do usuario',
  `estado_usuario` int(11) DEFAULT NULL COMMENT 'estado do usuario',
  `numero_usuario` varchar(8) DEFAULT NULL COMMENT 'numero do usuario',
  `complemento_usuario` text COMMENT 'Complemento endereço',
  `login_usuario` varchar(255) NOT NULL COMMENT 'Login Usuário',
  `senha_usuario` varchar(40) NOT NULL COMMENT 'Senha Usuário',
  `ativo_usuario` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Status Usuário',
  `ativado_sms` tinyint(1) DEFAULT '0' COMMENT 'Ativado por SMS',
  `fk_grupo_usuario` int(11) NOT NULL COMMENT 'Grupo Usuário',
  `usuario_criou_usuario` int(11) DEFAULT NULL COMMENT 'Usuário que criou',
  `criacao_usuario` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Data criação',
  `fbid` text COMMENT 'id do facebook',
  `token_acesso` varchar(255) DEFAULT NULL COMMENT 'token de acesso para login',
  PRIMARY KEY (`id_usuario`),
  UNIQUE KEY `celular_usuario` (`celular_usuario`),
  UNIQUE KEY `email_usuario` (`email_usuario`),
  UNIQUE KEY `login_usuario` (`login_usuario`),
  UNIQUE KEY `cpf_usuario` (`cpf_usuario`),
  UNIQUE KEY `token_acesso` (`token_acesso`),
  KEY `usuario_criou_usuario` (`usuario_criou_usuario`),
  KEY `estado_usuario` (`estado_usuario`),
  KEY `fk_grupo_usuario` (`fk_grupo_usuario`)
) ENGINE=InnoDB AUTO_INCREMENT=4455 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `status_leilao`
--

DROP TABLE IF EXISTS `status_leilao`;
CREATE TABLE IF NOT EXISTS `status_leilao` (
  `id_status_leilao` int(11) NOT NULL AUTO_INCREMENT,
  `status` varchar(45) NOT NULL,
  PRIMARY KEY (`id_status_leilao`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_completar_perfil`
-- (See below for the actual view)
--
DROP VIEW IF EXISTS `view_completar_perfil`;
CREATE TABLE IF NOT EXISTS `view_completar_perfil` (
`id_usuario` int(11)
,`bloquear` int(1)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_ingresso`
-- (See below for the actual view)
--
DROP VIEW IF EXISTS `view_ingresso`;
CREATE TABLE IF NOT EXISTS `view_ingresso` (
`id_ingresso` int(11)
,`fk_usuario` int(11)
,`quantidade` int(11)
,`ativo` tinyint(4)
,`prioridade` int(11)
,`id_compra_ingresso` int(11)
,`descricao` varchar(60)
,`data_compra` timestamp
,`valor` double(19,2)
,`code_pagamento` text
,`id_pacote_ingresso` int(11)
,`descricao_pacote` varchar(45)
,`quantidade_ingresso` int(11)
,`valor_pacote` double(19,2)
,`gratis` tinyint(4)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_leilao`
-- (See below for the actual view)
--
DROP VIEW IF EXISTS `view_leilao`;
CREATE TABLE IF NOT EXISTS `view_leilao` (
`id_leilao` int(11)
,`data_fim_efetivo` timestamp
,`link_compra` text
,`id_consulta_preference` text
,`finalizado` tinyint(4)
,`topic` text
,`valor_minimo` double
,`lance_minimo` double
,`ingressos` int(11)
,`data_inicio` timestamp
,`data_fim_previsto` timestamp
,`status_leilao` int(11)
,`status` varchar(45)
,`id_produto` int(11)
,`fk_categoria` int(11)
,`fk_usuario` int(11)
,`nome_produto` varchar(255)
,`descricao_produto` text
,`produto_usado` tinyint(1)
,`nome_usuario` varchar(100)
,`fk_usuario_arrematou` int(11)
,`nome_usuario_arrematou` varchar(100)
,`nome_usuario_leiloando` varchar(100)
,`avaliacao` decimal(13,2)
,`lance_formatado` varchar(62)
,`nome_categoria` varchar(255)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_lista_grupos`
-- (See below for the actual view)
--
DROP VIEW IF EXISTS `view_lista_grupos`;
CREATE TABLE IF NOT EXISTS `view_lista_grupos` (
`id` int(11)
,`nome_grupo` varchar(100)
,`descricao_grupo` text
,`usuario` int(11)
,`nome_usuario` varchar(100)
,`ativo_grupo` tinyint(1)
,`ativo` varchar(7)
,`criacao_grupo` timestamp
,`criacao_grupo_formatado` varchar(28)
,`usuarios_ativos` bigint(21)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_lista_usuarios`
-- (See below for the actual view)
--
DROP VIEW IF EXISTS `view_lista_usuarios`;
CREATE TABLE IF NOT EXISTS `view_lista_usuarios` (
`id` int(11)
,`nome_usuario` varchar(100)
,`email_usuario` varchar(40)
,`telefone_usuario` varchar(11)
,`login_usuario` varchar(255)
,`ativo_usuario` tinyint(1)
,`ativo` varchar(7)
,`fk_grupo_usuario` int(11)
,`nome_grupo` varchar(100)
,`usuario` int(11)
,`usuario_criou` varchar(100)
,`criacao_usuario` timestamp
,`criacao_usuario_formatado` varchar(28)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_notificar_leiloes`
-- (See below for the actual view)
--
DROP VIEW IF EXISTS `view_notificar_leiloes`;
CREATE TABLE IF NOT EXISTS `view_notificar_leiloes` (
`tipo` bigint(20)
,`id` int(11)
,`titulo` varchar(270)
,`descricao` varchar(297)
,`token_usuario` varchar(300)
,`inicio` bigint(21)
,`fim` bigint(21)
,`notificado` bigint(20)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_relatorio_acessos`
-- (See below for the actual view)
--
DROP VIEW IF EXISTS `view_relatorio_acessos`;
CREATE TABLE IF NOT EXISTS `view_relatorio_acessos` (
`id` int(11)
,`id_usuario` int(11)
,`usuario` varchar(100)
,`data_acesso` timestamp
,`data_acesso_formatado` varchar(28)
,`ip` varchar(16)
,`maquina` text
,`acesso` tinyint(1)
,`acessou` varchar(6)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_relatorio_edicoes`
-- (See below for the actual view)
--
DROP VIEW IF EXISTS `view_relatorio_edicoes`;
CREATE TABLE IF NOT EXISTS `view_relatorio_edicoes` (
`id` int(11)
,`fk_usuario` int(11)
,`nome_usuario` varchar(100)
,`data_log` timestamp
,`data_log_formatado` varchar(28)
,`original_edicao` text
,`novo_edicao` text
,`fk_aplicacao` int(11)
,`descricao_aplicacao` text
,`id_edicao` int(11)
,`campo` text
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_relatorio_navegacao`
-- (See below for the actual view)
--
DROP VIEW IF EXISTS `view_relatorio_navegacao`;
CREATE TABLE IF NOT EXISTS `view_relatorio_navegacao` (
`id` int(11)
,`fk_usuario` int(11)
,`nome_usuario` varchar(100)
,`data_log` timestamp
,`data_log_formatado` varchar(28)
,`permissao` tinyint(1)
,`com_permissao` varchar(3)
,`fk_aplicacao` int(11)
,`descricao_aplicacao` text
,`parametros` text
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_usuario_leilao`
-- (See below for the actual view)
--
DROP VIEW IF EXISTS `view_usuario_leilao`;
CREATE TABLE IF NOT EXISTS `view_usuario_leilao` (
`id_sala_leilao` int(11)
,`id_usuario` int(11)
,`nome_usuario` varchar(100)
,`id_leilao` int(11)
,`valor_minimo` double
,`valor_atual` double(19,2)
,`lance_minimo` double
,`data_inicio` timestamp
,`data_fim_previsto` timestamp
,`token_acesso` varchar(255)
,`inicio` bigint(21)
,`quantidade_minima_usuarios` int(11)
,`tempo_iniciar` int(11)
,`fim` bigint(21)
);

-- --------------------------------------------------------

--
-- Structure for view `view_completar_perfil`
--
DROP TABLE IF EXISTS `view_completar_perfil`;

CREATE ALGORITHM=UNDEFINED DEFINER=`leilao24h`@`localhost` SQL SECURITY DEFINER VIEW `view_completar_perfil`  AS  select `seg_usuarios`.`id_usuario` AS `id_usuario`,(((`seg_usuarios`.`nome_usuario` = '') or isnull(`seg_usuarios`.`nome_usuario`) or (`seg_usuarios`.`sexo_usuario` = '') or isnull(`seg_usuarios`.`sexo_usuario`) or (`seg_usuarios`.`rg_usuario` = '') or isnull(`seg_usuarios`.`rg_usuario`) or (`seg_usuarios`.`cpf_usuario` = '') or isnull(`seg_usuarios`.`cpf_usuario`) or (`seg_usuarios`.`email_usuario` = '') or isnull(`seg_usuarios`.`email_usuario`) or (`seg_usuarios`.`celular_usuario` = '') or isnull(`seg_usuarios`.`celular_usuario`) or (`seg_usuarios`.`cep_usuario` = '') or isnull(`seg_usuarios`.`cep_usuario`) or (`seg_usuarios`.`logradouro_usuario` = '') or isnull(`seg_usuarios`.`logradouro_usuario`) or (`seg_usuarios`.`bairro_usuario` = '') or isnull(`seg_usuarios`.`bairro_usuario`) or (`seg_usuarios`.`cidade_usuario` = '') or isnull(`seg_usuarios`.`cidade_usuario`) or (`seg_usuarios`.`estado_usuario` = '') or isnull(`seg_usuarios`.`estado_usuario`) or (`seg_usuarios`.`numero_usuario` = '') or isnull(`seg_usuarios`.`numero_usuario`)) and ((select (count(0) > 0) from `cad_leilao` where (`cad_leilao`.`fk_usuario_arrematou` = `seg_usuarios`.`id_usuario`)) or (select (count(0) > 0) from `cad_produtos` where (`cad_produtos`.`fk_usuario` = `seg_usuarios`.`id_usuario`)))) AS `bloquear` from `seg_usuarios` ;

-- --------------------------------------------------------

--
-- Structure for view `view_ingresso`
--
DROP TABLE IF EXISTS `view_ingresso`;

CREATE ALGORITHM=UNDEFINED DEFINER=`leilao24h`@`localhost` SQL SECURITY DEFINER VIEW `view_ingresso`  AS  select `cdi`.`id_ingresso` AS `id_ingresso`,`cdi`.`fk_usuario` AS `fk_usuario`,`cdi`.`quantidade` AS `quantidade`,`cdi`.`ativo` AS `ativo`,`cdi`.`prioridade` AS `prioridade`,`cmi`.`id_compra_ingresso` AS `id_compra_ingresso`,`cmi`.`descricao` AS `descricao`,`cmi`.`data_compra` AS `data_compra`,round(`cmi`.`valor`,2) AS `valor`,`cmi`.`code_pagamento` AS `code_pagamento`,`pci`.`id_pacote_ingresso` AS `id_pacote_ingresso`,`pci`.`descricao_pacote` AS `descricao_pacote`,`pci`.`quantidade_ingresso` AS `quantidade_ingresso`,round(`pci`.`valor_pacote`,2) AS `valor_pacote`,`pci`.`gratis` AS `gratis` from ((`cad_ingressos` `cdi` join `compra_ingresso` `cmi` on((`cdi`.`id_ingresso` = `cmi`.`fk_ingresso`))) join `pacote_ingresso` `pci` on((`cmi`.`fk_pacote` = `pci`.`id_pacote_ingresso`))) ;

-- --------------------------------------------------------

--
-- Structure for view `view_leilao`
--
DROP TABLE IF EXISTS `view_leilao`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_leilao`  AS  select `cl`.`id_leilao` AS `id_leilao`,`cl`.`data_fim_efetivo` AS `data_fim_efetivo`,`cl`.`link_compra` AS `link_compra`,`cl`.`id_consulta_preference` AS `id_consulta_preference`,`cl`.`finalizado` AS `finalizado`,`cl`.`topic` AS `topic`,`cl`.`valor_minimo` AS `valor_minimo`,`cl`.`lance_minimo` AS `lance_minimo`,`cl`.`ingressos` AS `ingressos`,`cl`.`data_inicio` AS `data_inicio`,`cl`.`data_fim_previsto` AS `data_fim_previsto`,`cl`.`status_leilao` AS `status_leilao`,`sl`.`status` AS `status`,`cp`.`id_produto` AS `id_produto`,`cp`.`fk_categoria` AS `fk_categoria`,`cp`.`fk_usuario` AS `fk_usuario`,`cp`.`nome_produto` AS `nome_produto`,`cp`.`descricao_produto` AS `descricao_produto`,`cp`.`produto_usado` AS `produto_usado`,`ss`.`nome_usuario` AS `nome_usuario`,`cl`.`fk_usuario_arrematou` AS `fk_usuario_arrematou`,ifnull((select `ss_`.`nome_usuario` from `seg_usuarios` `ss_` where (`ss_`.`id_usuario` = `cl`.`fk_usuario_arrematou`)),'-') AS `nome_usuario_arrematou`,(select `ss_`.`nome_usuario` from `seg_usuarios` `ss_` where (`ss_`.`id_usuario` = `cp`.`fk_usuario`)) AS `nome_usuario_leiloando`,round((select avg(`ca`.`avaliacao`) AS `avaliacao` from `cad_avaliacoes` `ca` where (`ca`.`fk_leiloeiro` = `cp`.`fk_usuario`)),2) AS `avaliacao`,(select format(`cad_lances`.`valor`,2,'de_DE') from (`cad_lances` join `sala_leilao` on((`cad_lances`.`fk_sala_leilao` = `sala_leilao`.`id_sala_leilao`))) where (`sala_leilao`.`fk_leilao` = `cl`.`id_leilao`) order by `cad_lances`.`valor` desc limit 1) AS `lance_formatado`,`cc`.`nome_categoria` AS `nome_categoria` from ((((`cad_leilao` `cl` join `cad_produtos` `cp` on((`cl`.`fk_produto` = `cp`.`id_produto`))) join `seg_usuarios` `ss` on((`ss`.`id_usuario` = `cp`.`fk_usuario`))) join `status_leilao` `sl` on((`sl`.`id_status_leilao` = `cl`.`status_leilao`))) join `cad_categorias` `cc` on((`cc`.`id_categoria` = `cp`.`fk_categoria`))) group by `cl`.`id_leilao` ;

-- --------------------------------------------------------

--
-- Structure for view `view_lista_grupos`
--
DROP TABLE IF EXISTS `view_lista_grupos`;

CREATE ALGORITHM=UNDEFINED DEFINER=`leilao24h`@`localhost` SQL SECURITY DEFINER VIEW `view_lista_grupos`  AS  select `seg_grupos`.`id_grupo` AS `id`,`seg_grupos`.`nome_grupo` AS `nome_grupo`,`seg_grupos`.`descricao_grupo` AS `descricao_grupo`,`seg_grupos`.`usuario_criou_grupo` AS `usuario`,`seg_usuarios`.`nome_usuario` AS `nome_usuario`,`seg_grupos`.`ativo_grupo` AS `ativo_grupo`,(case `seg_grupos`.`ativo_grupo` when 1 then 'Ativo' when 0 then 'Inativo' end) AS `ativo`,`seg_grupos`.`criacao_grupo` AS `criacao_grupo`,date_format(`seg_grupos`.`criacao_grupo`,'%d/%m/%Y às %H:%i:%s') AS `criacao_grupo_formatado`,(select count(0) from `seg_usuarios` where ((`seg_usuarios`.`fk_grupo_usuario` = `seg_grupos`.`id_grupo`) and (`seg_usuarios`.`ativo_usuario` = TRUE))) AS `usuarios_ativos` from (`seg_grupos` left join `seg_usuarios` on((`seg_usuarios`.`id_usuario` = `seg_grupos`.`usuario_criou_grupo`))) ;

-- --------------------------------------------------------

--
-- Structure for view `view_lista_usuarios`
--
DROP TABLE IF EXISTS `view_lista_usuarios`;

CREATE ALGORITHM=UNDEFINED DEFINER=`leilao24h`@`localhost` SQL SECURITY DEFINER VIEW `view_lista_usuarios`  AS  select `su1`.`id_usuario` AS `id`,`su1`.`nome_usuario` AS `nome_usuario`,`su1`.`email_usuario` AS `email_usuario`,`su1`.`telefone_usuario` AS `telefone_usuario`,`su1`.`login_usuario` AS `login_usuario`,`su1`.`ativo_usuario` AS `ativo_usuario`,(case `su1`.`ativo_usuario` when 1 then 'Ativo' when 0 then 'Inativo' end) AS `ativo`,`su1`.`fk_grupo_usuario` AS `fk_grupo_usuario`,`seg_grupos`.`nome_grupo` AS `nome_grupo`,`su1`.`usuario_criou_usuario` AS `usuario`,(select `su0`.`nome_usuario` from `seg_usuarios` `su0` where (`su0`.`id_usuario` = `su1`.`usuario_criou_usuario`)) AS `usuario_criou`,`su1`.`criacao_usuario` AS `criacao_usuario`,date_format(`su1`.`criacao_usuario`,'%d/%m/%Y às %H:%i:%s') AS `criacao_usuario_formatado` from (`seg_usuarios` `su1` join `seg_grupos` on((`seg_grupos`.`id_grupo` = `su1`.`fk_grupo_usuario`))) ;

-- --------------------------------------------------------

--
-- Structure for view `view_notificar_leiloes`
--
DROP TABLE IF EXISTS `view_notificar_leiloes`;

CREATE ALGORITHM=UNDEFINED DEFINER=`leilao24h`@`localhost` SQL SECURITY DEFINER VIEW `view_notificar_leiloes`  AS  select 1 AS `tipo`,`cl`.`id_leilao` AS `id`,concat('Não perca o(a) ',`cp`.`nome_produto`) AS `titulo`,concat('O leilão do produto: \'',`cp`.`nome_produto`,'\' já vai começar!') AS `descricao`,`ct`.`token` AS `token_usuario`,timestampdiff(SECOND,now(),`cl`.`data_inicio`) AS `inicio`,timestampdiff(SECOND,now(),`cl`.`data_fim_previsto`) AS `fim`,ifnull(`cl`.`notificado`,0) AS `notificado` from (((`sala_leilao` `sl` join `cad_leilao` `cl` on((`cl`.`id_leilao` = `sl`.`fk_leilao`))) join `cad_tokens` `ct` on((`ct`.`fk_usuario` = `sl`.`fk_usuario`))) join `cad_produtos` `cp` on((`cp`.`id_produto` = `cl`.`fk_produto`))) where ((`cl`.`notificado` = 0) and (timestampdiff(SECOND,now(),`cl`.`data_inicio`) <= 200)) union select 2 AS `tipo`,`cl`.`id_leilao` AS `id`,concat(`cp`.`nome_produto`,' Finalizando') AS `titulo`,concat('Ainda dá tempo de arrematar o(a): ',`cp`.`nome_produto`) AS `descricao`,`ct`.`token` AS `token_usuario`,timestampdiff(SECOND,now(),`cl`.`data_inicio`) AS `inicio`,timestampdiff(SECOND,now(),`cl`.`data_fim_previsto`) AS `fim`,ifnull(`cl`.`notificado`,0) AS `notificado` from (((`sala_leilao` `sl` join `cad_leilao` `cl` on((`cl`.`id_leilao` = `sl`.`fk_leilao`))) join `cad_tokens` `ct` on((`ct`.`fk_usuario` = `sl`.`fk_usuario`))) join `cad_produtos` `cp` on((`cp`.`id_produto` = `cl`.`fk_produto`))) where ((`cl`.`notificado` = 1) and (timestampdiff(SECOND,now(),`cl`.`data_fim_previsto`) <= (`cl`.`tempo_iniciar` * 60))) union select 3 AS `tipo`,`cl`.`id_leilao` AS `id`,concat(`cp`.`nome_produto`,' Finalizado') AS `titulo`,concat('Leilão \'',`cp`.`nome_produto`,'\' Finalizado') AS `descricao`,`ct`.`token` AS `token_usuario`,timestampdiff(SECOND,now(),`cl`.`data_inicio`) AS `inicio`,timestampdiff(SECOND,now(),`cl`.`data_fim_previsto`) AS `fim`,ifnull(`cl`.`notificado`,0) AS `notificado` from (((`sala_leilao` `sl` join `cad_leilao` `cl` on((`cl`.`id_leilao` = `sl`.`fk_leilao`))) join `cad_tokens` `ct` on((`ct`.`fk_usuario` = `sl`.`fk_usuario`))) join `cad_produtos` `cp` on((`cp`.`id_produto` = `cl`.`fk_produto`))) where ((`cl`.`notificado` = 2) and (timestampdiff(SECOND,now(),`cl`.`data_fim_previsto`) <= 0)) union select 4 AS `tipo`,`cl`.`id_leilao` AS `id`,concat(`cp`.`nome_produto`,' Finalizado') AS `titulo`,concat('O seu leilão do produto: \'',`cp`.`nome_produto`,'\' foi finalizado') AS `descricao`,`ct`.`token` AS `token_usuario`,timestampdiff(SECOND,now(),`cl`.`data_inicio`) AS `inicio`,timestampdiff(SECOND,now(),`cl`.`data_fim_previsto`) AS `fim`,ifnull(`cl`.`notificado`,0) AS `notificado` from ((`cad_leilao` `cl` join `cad_produtos` `cp` on((`cp`.`id_produto` = `cl`.`fk_produto`))) join `cad_tokens` `ct` on((`ct`.`fk_usuario` = `cp`.`fk_usuario`))) where ((`cl`.`notificado` = 2) and (timestampdiff(SECOND,now(),`cl`.`data_fim_previsto`) <= 0)) order by `tipo`,`id` ;

-- --------------------------------------------------------

--
-- Structure for view `view_relatorio_acessos`
--
DROP TABLE IF EXISTS `view_relatorio_acessos`;

CREATE ALGORITHM=UNDEFINED DEFINER=`leilao24h`@`localhost` SQL SECURITY DEFINER VIEW `view_relatorio_acessos`  AS  select `seg_log_acesso`.`id_log_acesso` AS `id`,`seg_usuarios`.`id_usuario` AS `id_usuario`,`seg_usuarios`.`nome_usuario` AS `usuario`,`seg_log_acesso`.`data_log_acesso` AS `data_acesso`,date_format(`seg_log_acesso`.`data_log_acesso`,'%d/%m/%Y às %H:%i:%s') AS `data_acesso_formatado`,`seg_log_acesso`.`ip_usuario_acesso` AS `ip`,`seg_log_acesso`.`maquina_usuario_acesso` AS `maquina`,`seg_log_acesso`.`acesso` AS `acesso`,(case `seg_log_acesso`.`acesso` when 1 then 'Entrou' when 0 then 'Saiu' end) AS `acessou` from (`seg_log_acesso` join `seg_usuarios` on((`seg_usuarios`.`id_usuario` = `seg_log_acesso`.`fk_usuario`))) ;

-- --------------------------------------------------------

--
-- Structure for view `view_relatorio_edicoes`
--
DROP TABLE IF EXISTS `view_relatorio_edicoes`;

CREATE ALGORITHM=UNDEFINED DEFINER=`leilao24h`@`localhost` SQL SECURITY DEFINER VIEW `view_relatorio_edicoes`  AS  select `seg_log_edicao`.`id_log_edicao` AS `id`,`seg_log_edicao`.`fk_usuario` AS `fk_usuario`,`seg_usuarios`.`nome_usuario` AS `nome_usuario`,`seg_log_edicao`.`data_log_edicao` AS `data_log`,date_format(`seg_log_edicao`.`data_log_edicao`,'%d/%m/%Y às %H:%i:%s') AS `data_log_formatado`,`seg_log_edicao`.`original_edicao` AS `original_edicao`,`seg_log_edicao`.`novo_edicao` AS `novo_edicao`,`seg_log_edicao`.`fk_aplicacao` AS `fk_aplicacao`,`seg_aplicacao`.`descricao_aplicacao` AS `descricao_aplicacao`,`seg_log_edicao`.`id_edicao` AS `id_edicao`,(select `information_schema`.`columns`.`COLUMN_COMMENT` AS `comentario` from `information_schema`.`columns` where ((`information_schema`.`columns`.`TABLE_SCHEMA` = 'db_leilao') and (`information_schema`.`columns`.`TABLE_NAME` = `seg_log_edicao`.`tabela_edicao`) and (`information_schema`.`columns`.`COLUMN_NAME` = `seg_log_edicao`.`campo_edicao`))) AS `campo` from ((`seg_log_edicao` join `seg_usuarios` on((`seg_log_edicao`.`fk_usuario` = `seg_usuarios`.`id_usuario`))) join `seg_aplicacao` on((`seg_aplicacao`.`id_aplicacao` = `seg_log_edicao`.`fk_aplicacao`))) ;

-- --------------------------------------------------------

--
-- Structure for view `view_relatorio_navegacao`
--
DROP TABLE IF EXISTS `view_relatorio_navegacao`;

CREATE ALGORITHM=UNDEFINED DEFINER=`leilao24h`@`localhost` SQL SECURITY DEFINER VIEW `view_relatorio_navegacao`  AS  select `seg_log_navegacao`.`id_log_navegacao` AS `id`,`seg_log_navegacao`.`fk_usuario` AS `fk_usuario`,`seg_usuarios`.`nome_usuario` AS `nome_usuario`,`seg_log_navegacao`.`data_log_navegacao` AS `data_log`,date_format(`seg_log_navegacao`.`data_log_navegacao`,'%d/%m/%Y às %H:%i:%s') AS `data_log_formatado`,`seg_log_navegacao`.`permissao` AS `permissao`,(case `seg_log_navegacao`.`permissao` when 1 then 'Sim' when 0 then 'Não' end) AS `com_permissao`,`seg_log_navegacao`.`fk_aplicacao` AS `fk_aplicacao`,`seg_aplicacao`.`descricao_aplicacao` AS `descricao_aplicacao`,`seg_log_navegacao`.`parametros` AS `parametros` from ((`seg_log_navegacao` join `seg_usuarios` on((`seg_log_navegacao`.`fk_usuario` = `seg_usuarios`.`id_usuario`))) join `seg_aplicacao` on((`seg_aplicacao`.`id_aplicacao` = `seg_log_navegacao`.`fk_aplicacao`))) ;

-- --------------------------------------------------------

--
-- Structure for view `view_usuario_leilao`
--
DROP TABLE IF EXISTS `view_usuario_leilao`;

CREATE ALGORITHM=UNDEFINED DEFINER=`leilao24h`@`localhost` SQL SECURITY DEFINER VIEW `view_usuario_leilao`  AS  select `sala_leilao`.`id_sala_leilao` AS `id_sala_leilao`,`seg_usuarios`.`id_usuario` AS `id_usuario`,`seg_usuarios`.`nome_usuario` AS `nome_usuario`,`cad_leilao`.`id_leilao` AS `id_leilao`,`cad_leilao`.`valor_minimo` AS `valor_minimo`,round(ifnull((select max(`cad_lances`.`valor`) AS `valor` from (`cad_lances` join `sala_leilao` on((`cad_lances`.`fk_sala_leilao` = `sala_leilao`.`id_sala_leilao`))) where (`sala_leilao`.`fk_leilao` = `cad_leilao`.`id_leilao`) limit 1),`cad_leilao`.`valor_minimo`),2) AS `valor_atual`,`cad_leilao`.`lance_minimo` AS `lance_minimo`,`cad_leilao`.`data_inicio` AS `data_inicio`,`cad_leilao`.`data_fim_previsto` AS `data_fim_previsto`,`seg_usuarios`.`token_acesso` AS `token_acesso`,timestampdiff(SECOND,now(),`cad_leilao`.`data_inicio`) AS `inicio`,`cad_leilao`.`quantidade_minima_usuarios` AS `quantidade_minima_usuarios`,`cad_leilao`.`tempo_iniciar` AS `tempo_iniciar`,if((timestampdiff(SECOND,now(),`cad_leilao`.`data_fim_previsto`) < 0),0,timestampdiff(SECOND,now(),`cad_leilao`.`data_fim_previsto`)) AS `fim` from ((`cad_leilao` join `sala_leilao` on((`sala_leilao`.`fk_leilao` = `cad_leilao`.`id_leilao`))) join `seg_usuarios` on((`seg_usuarios`.`id_usuario` = `sala_leilao`.`fk_usuario`))) ;

--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `cad_avaliacoes`
--
ALTER TABLE `cad_avaliacoes`
  ADD CONSTRAINT `cad_avaliacoes_ibfk_1` FOREIGN KEY (`fk_leiloeiro`) REFERENCES `seg_usuarios` (`id_usuario`),
  ADD CONSTRAINT `cad_avaliacoes_ibfk_2` FOREIGN KEY (`fk_leilao`) REFERENCES `cad_leilao` (`id_leilao`),
  ADD CONSTRAINT `cad_avaliacoes_ibfk_3` FOREIGN KEY (`fk_cliente`) REFERENCES `seg_usuarios` (`id_usuario`);

--
-- Limitadores para a tabela `cad_categorias`
--
ALTER TABLE `cad_categorias`
  ADD CONSTRAINT `cad_categorias_ibfk_1` FOREIGN KEY (`fk_categoria_pai`) REFERENCES `cad_categorias` (`id_categoria`);

--
-- Limitadores para a tabela `cad_compra_pacotes`
--
ALTER TABLE `cad_compra_pacotes`
  ADD CONSTRAINT `cad_compra_pacotes_ibfk_1` FOREIGN KEY (`fk_usuario`) REFERENCES `seg_usuarios` (`id_usuario`),
  ADD CONSTRAINT `cad_compra_pacotes_ibfk_2` FOREIGN KEY (`fk_pacote`) REFERENCES `pacote_ingresso` (`id_pacote_ingresso`);

--
-- Limitadores para a tabela `cad_compra_recompensa`
--
ALTER TABLE `cad_compra_recompensa`
  ADD CONSTRAINT `cad_compra_recompensa_ibfk_1` FOREIGN KEY (`fk_produto`) REFERENCES `cad_produtos` (`id_produto`),
  ADD CONSTRAINT `cad_compra_recompensa_ibfk_2` FOREIGN KEY (`fk_usuario`) REFERENCES `seg_usuarios` (`id_usuario`);

--
-- Limitadores para a tabela `cad_creditos`
--
ALTER TABLE `cad_creditos`
  ADD CONSTRAINT `cad_creditos_ibfk_1` FOREIGN KEY (`fk_usuario`) REFERENCES `seg_usuarios` (`id_usuario`);

--
-- Limitadores para a tabela `cad_favoritos`
--
ALTER TABLE `cad_favoritos`
  ADD CONSTRAINT `cad_favoritos_ibfk_1` FOREIGN KEY (`fk_leilao`) REFERENCES `cad_leilao` (`id_leilao`),
  ADD CONSTRAINT `cad_favoritos_ibfk_2` FOREIGN KEY (`fk_usuario`) REFERENCES `seg_usuarios` (`id_usuario`);

--
-- Limitadores para a tabela `cad_hist_notificacao`
--
ALTER TABLE `cad_hist_notificacao`
  ADD CONSTRAINT `cad_hist_notificacao_ibfk_1` FOREIGN KEY (`fk_notificacao`) REFERENCES `cad_notificacao` (`id_notificacao`),
  ADD CONSTRAINT `cad_hist_notificacao_ibfk_2` FOREIGN KEY (`fk_usuario_destino`) REFERENCES `seg_usuarios` (`id_usuario`);

--
-- Limitadores para a tabela `cad_ingressos`
--
ALTER TABLE `cad_ingressos`
  ADD CONSTRAINT `cad_ingressos_ibfk_1` FOREIGN KEY (`fk_usuario`) REFERENCES `seg_usuarios` (`id_usuario`);

--
-- Limitadores para a tabela `cad_item_grupo`
--
ALTER TABLE `cad_item_grupo`
  ADD CONSTRAINT `cad_item_grupo_ibfk_1` FOREIGN KEY (`fk_grupo`) REFERENCES `cad_grupos` (`id_grupo`);

--
-- Limitadores para a tabela `cad_lances`
--
ALTER TABLE `cad_lances`
  ADD CONSTRAINT `cad_lances_ibfk_1` FOREIGN KEY (`fk_sala_leilao`) REFERENCES `sala_leilao` (`id_sala_leilao`);

--
-- Limitadores para a tabela `cad_leilao`
--
ALTER TABLE `cad_leilao`
  ADD CONSTRAINT `cad_leilao_ibfk_1` FOREIGN KEY (`fk_produto`) REFERENCES `cad_produtos` (`id_produto`),
  ADD CONSTRAINT `cad_leilao_ibfk_2` FOREIGN KEY (`status_leilao`) REFERENCES `status_leilao` (`id_status_leilao`),
  ADD CONSTRAINT `cad_leilao_ibfk_3` FOREIGN KEY (`fk_usuario_arrematou`) REFERENCES `seg_usuarios` (`id_usuario`);

--
-- Limitadores para a tabela `cad_notificacao`
--
ALTER TABLE `cad_notificacao`
  ADD CONSTRAINT `cad_notificacao_ibfk_1` FOREIGN KEY (`fk_usuario`) REFERENCES `seg_usuarios` (`id_usuario`);

--
-- Limitadores para a tabela `cad_produtos`
--
ALTER TABLE `cad_produtos`
  ADD CONSTRAINT `cad_produtos_ibfk_1` FOREIGN KEY (`fk_categoria`) REFERENCES `cad_categorias` (`id_categoria`),
  ADD CONSTRAINT `cad_produtos_ibfk_2` FOREIGN KEY (`fk_usuario`) REFERENCES `seg_usuarios` (`id_usuario`);

--
-- Limitadores para a tabela `cad_tokens`
--
ALTER TABLE `cad_tokens`
  ADD CONSTRAINT `cad_tokens_ibfk_1` FOREIGN KEY (`fk_usuario`) REFERENCES `seg_usuarios` (`id_usuario`);

--
-- Limitadores para a tabela `compra_ingresso`
--
ALTER TABLE `compra_ingresso`
  ADD CONSTRAINT `compra_ingresso_ibfk_1` FOREIGN KEY (`fk_ingresso`) REFERENCES `cad_ingressos` (`id_ingresso`),
  ADD CONSTRAINT `compra_ingresso_ibfk_2` FOREIGN KEY (`fk_pacote`) REFERENCES `pacote_ingresso` (`id_pacote_ingresso`);

--
-- Limitadores para a tabela `hist_compra_pacotes`
--
ALTER TABLE `hist_compra_pacotes`
  ADD CONSTRAINT `hist_compra_pacotes_ibfk_1` FOREIGN KEY (`fk_compra_pacotes`) REFERENCES `cad_compra_pacotes` (`id`);

--
-- Limitadores para a tabela `sala_leilao`
--
ALTER TABLE `sala_leilao`
  ADD CONSTRAINT `sala_leilao_ibfk_1` FOREIGN KEY (`fk_leilao`) REFERENCES `cad_leilao` (`id_leilao`),
  ADD CONSTRAINT `sala_leilao_ibfk_2` FOREIGN KEY (`fk_usuario`) REFERENCES `seg_usuarios` (`id_usuario`);

--
-- Limitadores para a tabela `seg_aplicacao`
--
ALTER TABLE `seg_aplicacao`
  ADD CONSTRAINT `seg_aplicacao_ibfk_1` FOREIGN KEY (`fk_controller`) REFERENCES `seg_controllers` (`id_controller`);

--
-- Limitadores para a tabela `seg_aplicacoes_grupos`
--
ALTER TABLE `seg_aplicacoes_grupos`
  ADD CONSTRAINT `seg_aplicacoes_grupos_ibfk_1` FOREIGN KEY (`fk_aplicacao`) REFERENCES `seg_aplicacao` (`id_aplicacao`),
  ADD CONSTRAINT `seg_aplicacoes_grupos_ibfk_2` FOREIGN KEY (`fk_grupo`) REFERENCES `seg_grupos` (`id_grupo`);

--
-- Limitadores para a tabela `seg_aplicacoes_menu`
--
ALTER TABLE `seg_aplicacoes_menu`
  ADD CONSTRAINT `seg_aplicacoes_menu_ibfk_1` FOREIGN KEY (`fk_aplicacao`) REFERENCES `seg_aplicacao` (`id_aplicacao`),
  ADD CONSTRAINT `seg_aplicacoes_menu_ibfk_2` FOREIGN KEY (`fk_menu`) REFERENCES `seg_menu` (`id_menu`);

--
-- Limitadores para a tabela `seg_controllers`
--
ALTER TABLE `seg_controllers`
  ADD CONSTRAINT `seg_controllers_ibfk_1` FOREIGN KEY (`fk_model`) REFERENCES `seg_models` (`id_model`);

--
-- Limitadores para a tabela `seg_grupos`
--
ALTER TABLE `seg_grupos`
  ADD CONSTRAINT `seg_grupos_ibfk_1` FOREIGN KEY (`usuario_criou_grupo`) REFERENCES `seg_usuarios` (`id_usuario`);

--
-- Limitadores para a tabela `seg_log_acesso`
--
ALTER TABLE `seg_log_acesso`
  ADD CONSTRAINT `seg_log_acesso_ibfk_1` FOREIGN KEY (`fk_usuario`) REFERENCES `seg_usuarios` (`id_usuario`);

--
-- Limitadores para a tabela `seg_log_edicao`
--
ALTER TABLE `seg_log_edicao`
  ADD CONSTRAINT `seg_log_edicao_ibfk_1` FOREIGN KEY (`fk_usuario`) REFERENCES `seg_usuarios` (`id_usuario`),
  ADD CONSTRAINT `seg_log_edicao_ibfk_2` FOREIGN KEY (`fk_aplicacao`) REFERENCES `seg_aplicacao` (`id_aplicacao`);

--
-- Limitadores para a tabela `seg_log_erro`
--
ALTER TABLE `seg_log_erro`
  ADD CONSTRAINT `seg_log_erro_ibfk_1` FOREIGN KEY (`fk_usuario`) REFERENCES `seg_usuarios` (`id_usuario`);

--
-- Limitadores para a tabela `seg_log_navegacao`
--
ALTER TABLE `seg_log_navegacao`
  ADD CONSTRAINT `seg_log_navegacao_ibfk_1` FOREIGN KEY (`fk_usuario`) REFERENCES `seg_usuarios` (`id_usuario`),
  ADD CONSTRAINT `seg_log_navegacao_ibfk_2` FOREIGN KEY (`fk_aplicacao`) REFERENCES `seg_aplicacao` (`id_aplicacao`);

--
-- Limitadores para a tabela `seg_menu`
--
ALTER TABLE `seg_menu`
  ADD CONSTRAINT `seg_menu_ibfk_1` FOREIGN KEY (`menu_acima`) REFERENCES `seg_menu` (`id_menu`);

--
-- Limitadores para a tabela `seg_usuarios`
--
ALTER TABLE `seg_usuarios`
  ADD CONSTRAINT `seg_usuarios_ibfk_1` FOREIGN KEY (`usuario_criou_usuario`) REFERENCES `seg_usuarios` (`id_usuario`),
  ADD CONSTRAINT `seg_usuarios_ibfk_2` FOREIGN KEY (`estado_usuario`) REFERENCES `cad_item_grupo` (`id_item_grupo`),
  ADD CONSTRAINT `seg_usuarios_ibfk_3` FOREIGN KEY (`fk_grupo_usuario`) REFERENCES `seg_grupos` (`id_grupo`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

/*Produtos Banners*/
INSERT INTO seg_menu (menu_acima, posicao_menu, titulo_menu, descricao_menu, id_menu) VALUES (null, 4,    'Banners',  'Banners',  '28');
INSERT INTO seg_aplicacao (link_aplicacao, titulo_aplicacao, descricao_aplicacao, fk_controller) VALUES ('produto/view_banners', 'Banner APP',  'Banner APP',   '8');
insert into seg_aplicacoes_menu(fk_aplicacao,fk_menu) values ( 32, 28);
insert into seg_aplicacoes_grupos(fk_aplicacao,fk_grupo) values( 32,1);
/*Produtos*/
