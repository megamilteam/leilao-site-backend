</div>

<footer class="main_footer">

    <div class="flex_box_footer_wrap">

        <div class="flex_box_footer_item">
              &nbsp;&nbsp; &nbsp; &nbsp; &nbsp;Desenvolvedor: <b>Eduardo dos santos</b><br>
              &nbsp;&nbsp; &nbsp; &nbsp; &nbsp;<a href="mailto:megamil3d@gmail.com" style="color: #7a0b09">megamil3d@gmail.com</a>
        </div>

        <div class="flex_box_footer_item">
              &nbsp;&nbsp;Tel.: <span> <b>+55 (11) 9 6278-2329</b> </span><br>
        </div>


    </div>


</footer>


<!-- DataTable -->
<!--<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>style/DataTables/datatables.min.css"/>-->
<!--<script type="text/javascript" src="<?= base_url(); ?>style/DataTables/DataTables/js/jquery.dataTables.js"></script>-->
<!--<script type="text/javascript" src="<?= base_url(); ?>style/js/datatable.js"></script>-->
<script DEFER="DEFER">
    /*Só é executado após o carregamento total da página*/
    $(document).ready(function(){

        $('.principal').show();

        $('.progress_modal_historico').hide();

        $('#historico_modal').click(function () {

            $('.progress_modal_historico').show();
            var id = $(this).attr('cod');

            $('#load_modal_historico').load('<?= base_url() ?>Controller_relatorios/load_historico_edicoes',{id: id},function(){

                $('.progress_modal_historico').hide();
                dataTableLoad();
            });
        });

      /*Usar notificações no navegador para informações remotas e a qualquer momento, com o navegador aberto*/
      /*setInterval(function () {
        console.log("Buscando notificações...");

        $.ajax({url: '<?= base_url(); ?>Controller_notificacoes/ajax_Notificacoes',
                type: 'json',
                method: 'POST'
        }).done(function(dados){

          $.each(dados,function(key,value){

            var usuario = dados[key].nome_usuario;
            var titulo = dados[key].titulo_notificacao;

            if (Notification.permission=='granted') {

                notificar = new Notification('Nova Notificação de: '+ usuario,{
                  'body' : titulo,
                  'icon' : '<?= base_url() ?>style/img/favicon.ico',
                  'tag' : '1'
                });

                setTimeout(notificar.close.bind(notificar), 5000);
                console.log(Notification.permission);

              } else {

                $.toast({
                      icon: 'info',
                      position: 'top-right',
                      hideAfter: false,
                      heading: 'Nova Notificação de: '+usuario,
                      text: titulo
                  });

                console.log('Por favor, Habilite as Notificações '+Notification.permission);
                console.log(Notification.permission);
              }

          });

        });

      }, 60000);*/

      <?php if ($this->session->flashdata('tipo_alerta') != "") {
        echo "$.toast({
                icon: '".$this->session->flashdata('tipo_alerta')."',
                heading: '".$this->session->flashdata('titulo_alerta')."',
                text: '".$this->session->flashdata('mensagem_alerta')."',";
                if ($this->session->flashdata('mensagem_fixa')) {
                  echo "hideAfter: false,";
                } else {
                  echo "showHideTransition: 'fade',";
                }
                echo "position: 'top-right',
            });";

        $this->session->set_flashdata('tipo_alerta','');
        $this->session->set_flashdata('titulo_alerta','');
        $this->session->set_flashdata('mensagem_alerta','');
        $this->session->set_flashdata('mensagem_fixa','');

      }

    ?>

    $('#erro_feedback').click(function(){

      var cod = $(this).attr('cod');

      swal({
          title: "Nos Ajude",
          text: "Por favor, tente descrever o que estava fazendo para ajudar a corrigir este erro.",
          type: "input",
          showCancelButton: true,
          closeOnConfirm: false,
          animation: "slide-from-top",
          inputPlaceholder: "Descreva o que estava fazendo."
        },
        function(inputValue){

          if (inputValue != "") {

            $.ajax({
              type: "post",
              url: "<?= base_url(); ?>Main/reportar_Erro",
              data:{
                  id_log_erro: cod,
                  erro_feedback: inputValue
              },
              error: function(returnval) {
                 mensagem('Error',returnval,'error');
              },
              success: function (returnval) {
                if(returnval == 'sucesso') {

                  swal("Obrigado!", "Obrigado pelo Feedback!");

                } else {

                  swal("Falha =/", "Falha ao reportar erro! "+returnval);

                }

              }
            });

          }

        });
      });

    });
  </script>

  <!-- Usado quando o usuário desabilitou o JavaScript -->
  <noscript>
    <meta http-equiv="refresh" content=1;url="http://enable-javascript.com/pt/">
    <div style="background-color: red; height: 100%; width: 100%; position: absolute; z-index: 200000;" align="center">
      <h1 style="color: white;">HABILITE O JAVASCRIPT PARA USAR O SISTEMA!.</h1>
      <iframe src="http://enable-javascript.com/pt/" width="100%" height="100%"></iframe>
    </div>
  </noscript>
</body>

</html>
