
<div class="row animated fadeInDown blocosMain">
  <div class="col-sm-4 bloco">
    <div class="thumbnail">
      <img src="<?php echo base_url(); ?>style/img/perfil.jpg" alt="Editar seu perfil" style="width: 100%;">
      <div class="caption" align="center">
        <h3>Atualizar seu perfil</h3>
        <p>Altere sua senha, nome, email e demais dados de seu perfil</p>
        <p>
        	<a href="<?php echo base_url(); ?>main/redirecionar/1" class="btn btn-success" role="button">Atualizar seu perfil</a>
        </p>
      </div>
    </div>
  </div>

  <div class="col-sm-4 bloco">
    <div class="thumbnail">
      <img src="<?php echo base_url(); ?>style/img/produtos.png" alt="Produtos" style="width: 100%;">
      <div class="caption" align="center">
        <h3>Produtos</h3>
        <p>Crie um novo produto ou veja os existentes</p>
        <p>
        	<a href="<?php echo base_url(); ?>main/redirecionar/22" class="btn btn-success" role="button">Novo Produto</a>
        	<a href="<?php echo base_url(); ?>main/redirecionar/21" class="btn btn-primary" role="button">Ver Produtos</a>
        </p>
      </div>
    </div>
  </div>

  <div class="col-sm-4 bloco">
    <div class="thumbnail">
      <img src="<?php echo base_url(); ?>style/img/pacote.jpg" alt="Categorias" style="width: 100%;">
      <div class="caption" align="center">
        <h3>Pacotes</h3>
        <p>Crie um novo pacote ou veja os existentes</p>
        <p>
        	<a href="<?php echo base_url(); ?>main/redirecionar/25" class="btn btn-success" role="button">Novo Pacote</a>
        	<a href="<?php echo base_url(); ?>main/redirecionar/24" class="btn btn-primary" role="button">Ver Pacotes</a>
        </p>
      </div>
    </div>
  </div>
</div>
