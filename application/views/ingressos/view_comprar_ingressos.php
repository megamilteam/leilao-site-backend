<?php
//data user
$nome_usuario = $usuario['nome_usuario'];
$email_usuario = $usuario['email_usuario'];
//data pacote
$descricao_pacote = $pacote['descricao_pacote'];
$quantidade_pacote = $pacote['quantidade_ingresso'];
$valor_pacote = $pacote['valor_pacote'];

$array = array('id_usuario' => $usuario['id_usuario'], 'id_pacote' => $pacote['id_pacote_ingresso']); 
?>
 <!DOCTYPE html>
 <html lang="pt-br">
   <head>
     <meta charset="utf-8">
     <meta name="viewport" content="width=device-width, initial-scale=1">
     <title>Comprar Ingressos</title>

     <link rel="shortcut icon" href="<?= base_url('style/img/favicon.ico'); ?>" />
     <link rel="stylesheet" href="<?= base_url('style/css/boot.css'); ?>" />
     <link rel="stylesheet" href="<?= base_url('style/css/bootstrap.min.css'); ?>" />
     <link rel="stylesheet" href="<?= base_url('style/animate/animate.css'); ?>" />
     <link rel="stylesheet" href="<?= base_url('style/css/estilo.css'); ?>" />
   </head>

   <body class="body_webview">

    <header class="main_header_webview">
      <h1 class="animated wobble">Comprar Ingressos</h1>
    </header>

   <main class="main_webview animated slideInUp">

      <div class="main_webview_item">
        <h2>Usuário</h2>
      </div>

      <?= form_open('pagar_ingressos', '', $array); ?>

      <div class="main_webview_item">
        <p class="dados_user"><b>Nome:</b> <?= $nome_usuario; ?></p>
        <span class="dados_user"><b>E-mail:</b> <?= $email_usuario; ?></span>
        <hr>
      </div>

      <div class="main_webview_item">
        <h3>Pacote</h3>
      </div>

      <div class="main_webview_item">
        <div class="form-group has-feedback">
            <label class="control-label"><small style="color: red;">*</small><b>Descrição</b></label>
            <input type="text" class="form-control" id="" value="<?= $descricao_pacote ?>" name="" placeholder="Descrição do pacote">
        </div>
      </div>

      <div class="main_webview_item">
        <div class="form-group has-feedback">
            <label class="control-label"><small style="color: red;">*</small><b>Quantidade de ingressos</b></label>
            <input type="text" class="form-control" id="" value="<?= $quantidade_pacote ?>" name="" placeholder="Quantidade do pacote">
        </div>
      </div>

      <div class="main_webview_item">
        <div class="form-group has-feedback">
            <label class="control-label"><small style="color: red;">*</small><b>Valor do Pacote</b></label>
            <input type="text" class="form-control" id="" value="<?= $valor_pacote ?>" name="" placeholder="Valor do pacote">
        </div>
      </div>

      <div class="main_webview_item">
        <a href="">
          <input type="submit" class="btn btn-success" id="" name="comprar" value="Comprar Ingresso" title="Comprar Ingresso" />
        </a>
      </div>

      <?= form_close(); ?>

   </main>

   </body>
 </html>
