<div class="row">
	<div class="col-md-8">
		<h1> <i class="glyphicon glyphicon-list-alt"></i> Notificações</h1>
	</div>
</div>

<hr>

<table class="table table-bordered table-hover" align="center">
	<thead align="center">
		<th style="width: 60px;" align="center" class="no-filter">Ler</th>
		<th style="width: 10px;" align="center" class="no-filter">Lido</th>
		<th>Título</th>
		<th>Enviada Em:</th>
		<th>Enviada Por:</th>
	</thead>
	<tbody align="center">	
	<?php 

		foreach ($dados_iniciais as $notificacao) {
			echo '<tr>';

			echo '<td style="width: 60px;">
					<button class="btn btn-info detalhes_modal" data-toggle="modal" data-target="#detalhes_modal" cod="'.$notificacao->id_notificacao.'"> <i class="glyphicon glyphicon-eye-open"> </i> Ler
					</button>
				</td>';

			if ($notificacao->data_leitura > 0) {
				echo '<td style="width: 10px;"><span class="glyphicon glyphicon-check" id="lido_'.$notificacao->id_notificacao.'"></span></td>';
			} else {
				echo '<td style="width: 10px;"><span class="glyphicon glyphicon-unchecked" id="lido_'.$notificacao->id_notificacao.'"></span></td>';
			}

			echo '<td>'.$notificacao->titulo_notificacao.'</td>';
			echo '<td>'.$notificacao->data_envio_notificacao.'</td>';
			echo '<td>'.$notificacao->nome_usuario.'</td>';
			echo '</tr>';
		}

	?>
	</tbody>
</table>

<!-- Modal -->
<div class="modal fade" id="detalhes_modal" tabindex="-1" role="dialog" aria-labelledby="detalhes_modalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="detalhes_modalLabel">Notificação</h4>
      </div>
      <div class="modal-body">

      	<div class="progress">
		  <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width: 50%">
		    <span class="sr-only">50%</span>
		  </div>
		</div>

        <div id="load"></div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-info" data-dismiss="modal">Fechar</button>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
	$(document).ready(function(){

		$('.progress').hide();

		$('.detalhes_modal').click(function () {
		  	
		  	$('.progress').show();

		  	var id = $(this).attr('cod');

		  	$('#load').load('<?php echo base_url() ?>Controller_notificacoes/load_notificacao',{id: id},function(){
		  		$('.progress').hide();

		  		$('#lido_'+id).removeClass('glyphicon-unchecked');
		  		$('#lido_'+id).addClass('glyphicon-check');

		  	});

		});

	});
</script>