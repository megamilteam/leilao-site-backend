<div class="row" >
	<div class="col-md-8">
		<h3> <i class="glyphicon glyphicon-gift"></i> Relatório Pagamento de pacotes efetuados</h3>
	</div>
</div>

<table class="table table-bordered table-hover" align="center">

    <thead>
        <tr>
            <th>Tentativas</th>
            <th>ID</th>
            <th>Pacote</th>
            <th>Valor</th>
            <th>Quantidade</th>
            <th>Usuário</th>
            <th>Data Compra</th>
            <th>Status</th>
        </tr>
    </thead>
    <tbody>

	<?php

    	foreach($dados_iniciais['vendas'] as $venda){

    		echo "<tr>";
	    		echo '<td>
	    				<button class="btn btn-info modalClass" cod="'.$venda->id.'" data-toggle="modal" data-target="#Modal"> 
	    					<i class="glyphicon glyphicon-edit"> </i> Ver
	    				</button>
	    			  </td>';
	    		echo "<td>{$venda->preference}</td>";
	    		echo "<td>{$venda->descricao_pacote}</td>";
	    		echo "<td class=\"mascara_monetaria\">{$venda->unit_price}</td>";
	    		echo "<td>{$venda->quantidade_ingresso}</td>";
	    		echo "<td>{$venda->nome_usuario}</td>";
	    		echo "<td>{$venda->date_buy}</td>";
	    		if($venda->finalizado) {
	    			echo "<td>Finalizado</td>";
	    		} else {
	    			echo "<td>Aguardando</td>";
	    		}
    		echo "</tr>";

    	}

	?>

    </tbody>
</table>

<!-- Modal -->
<div class="modal fade" id="Modal" tabindex="-1" role="dialog" aria-labelledby="ModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="ModalLabel">Histórico de tentativas</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                <div id="tentativas" style="width: 100%;"></div>
            </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-info" data-dismiss="modal">Fechar</button>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
	$(document).ready(function(){
		$('.modalClass').click( function () {

			var id = $(this).attr('cod');
			console.log(id);
            $("#tentativas").load("<?php echo base_url(); ?>/Controller_pacotes/detalhesVendasPacote",{id : id},function(){
                $("#detalhesVendasPacote").DataTable();
            });

        })
	});
</script>