<div class="row" >
	<div class="col-md-8">
		<h3> <i class="glyphicon glyphicon-gift"></i> Lista de pacotes</h3>
	</div>
	<div class="col-md-4" align="right">
		<a class="btn btn-success" href="<?php echo base_url(); ?>main/redirecionar/25">
			<i class="glyphicon glyphicon-plus-sign"></i> Novo Pacote
		</a>
	</div>
</div>
<table class="table table-bordered table-hover" align="center">

    <thead>
        <tr>
            <th>ID</th>
            <th>Descrição</th>
            <th>Quantidade</th>
            <th>Valor</th>
            <th>Grátis?</th>
            <th>Editar</th>
        </tr>
    </thead>
    <tbody>
<?php

    foreach($dados_iniciais['pacotes'] as $pacote){
        echo "<tr>";
        echo "<td>{$pacote->id_pacote_ingresso}</td>";
        echo "<td>{$pacote->descricao_pacote}</td>";
        echo "<td>{$pacote->quantidade_ingresso}</td>";
        echo "<td class=\"mascara_monetaria\">{$pacote->valor_pacote}</td>";

        if($pacote->gratis){
            echo "<td>Grátis</td>";
        } else {
            echo "<td>Pago</td>";
        }
        echo '<td><a href="'.base_url().'main/redirecionar/26/'.$pacote->id_pacote_ingresso.'"><button class="btn btn-info"> <i class="glyphicon glyphicon-edit"> </i> Editar</button></a></td>';
        echo "</tr>";
    }


?>
    </tbody>
</table>