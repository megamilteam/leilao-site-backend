<style type="text/css">
	.btn {
		color: white;
		text-shadow: 1px 1px 1px black;
	}
</style>

<div class="row">
	<div class="col-md-8">
		<h1> <i class="glyphicon glyphicon-file"></i> Editar Pacote</h1>
	</div>
	<div class="col-md-4" align="right">
		<button type="button" class="btn btn-info" id="voltar"> <i class="glyphicon glyphicon-backward"></i> Voltar</button>
		<button type="button" class="btn btn-info" id="recarregar" url="<?php echo $_SERVER ['REQUEST_URI'] ?>"> <i class="glyphicon glyphicon-refresh"></i> Recarregar</button>
	</div>
</div>
<hr>

<?php echo form_open('Controller_pacotes/editar_pacote'); ?>

<div class="row">
	<input type="hidden" class="form-control obrigatorio" id="id_pacote_ingresso" name="id_pacote_ingresso" placeholder="Id pacote ingresso" aviso="Id pacote ingresso" value="<?php echo $this->session->flashdata('id_pacote_ingresso_edicao'); ?>" maxlength="100">
    
	<div class="col-md-3">
		<div class="form-group has-feedback">
			<label class="control-label" for="nome_grupo">Descrição do pacote</label> 
			<i class="glyphicon glyphicon-pencil form-control-feedback"></i>
			<input type="text" class="form-control obrigatorio" id="descricao_pacote" name="descricao_pacote" placeholder="Decrição do pacote" aviso="Decrição do pacote" value="<?php echo $this->session->flashdata('descricao_pacote_edicao'); ?>" maxlength="100">
		</div>
	</div>

	<div class="col-md-3">
		<div class="form-group has-feedback">
			<label class="control-label" for="descricao_grupo">Quantidade de ingresso</label> 
			<i class="glyphicon glyphicon-pencil form-control-feedback"></i>
			<input type="text" class="form-control obrigatorio validar_numeros" id="quantidade_ingresso" name="quantidade_ingresso" placeholder="Quantidade de ingresso" aviso="Quantidade de ingresso" value="<?php echo $this->session->flashdata('quantidade_ingresso_edicao'); ?>">
		</div>
    </div>

    <div class="col-md-3">
		<div class="form-group has-feedback">
			<label class="control-label" for="descricao_grupo">Valor do pacote</label> 
			<i class="glyphicon glyphicon-pencil form-control-feedback"></i>
			<input type="text" class="form-control obrigatorio  mascara_monetaria" id="valor_pacote" name="valor_pacote" placeholder="Valor pacote" aviso="Valor pacote" value="<?php echo $this->session->flashdata('valor_pacote_edicao'); ?>">
		</div>
	</div>

	<div class="col-md-2">
		<div class="form-group has-feedback">
			<label class="control-label" for="ativo_grupo">Grátis</label> 
			<i class="glyphicon glyphicon-pencil form-control-feedback"></i>
			<select class="form-control obrigatorio" id="gratis" name="gratis" aviso="Grátis">
				<option value="1">Gratuito</option>
				<option value="0">Pago</option>
			</select>
		</div>
	</div>
</div>

<hr>

<div class="row finalizar_formulario">
	<div class="col-md-9"></div>
	<div class="col-md-3" align="right">
		<button type="button" class="btn btn-danger" title="Limpar os dados dos campos" id="apagar"> <i class="glyphicon glyphicon-trash"></i> Limpar</button>
		<button type="submit" class="btn btn-success" id="validar_Enviar" title="Editar"> <i class="glyphicon glyphicon-floppy-disk"></i> Editar </button>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function(){
        $('#gratis').val(<?php echo $this->session->flashdata('gratis_edicao'); ?>).trigger('change');
	});
</script>

<?php echo form_close(); ?>

