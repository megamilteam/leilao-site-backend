<script type="text/javascript">
	history.replaceState({pagina: "lista_notificacoes"}, "Lista de notificações", "<?php echo base_url() ?>main/redirecionar/34");
</script>

<style type="text/css">
	.btn {
		color: white;
		text-shadow: 1px 1px 1px black;
	}
</style>

<div class="row">
	<div class="col-md-8">
		<h1> <i class="glyphicon glyphicon-file"></i> Nova Recompensa</h1>
	</div>
	<div class="col-md-4" align="right">
		<button type="button" class="btn btn-info" id="voltar"> <i class="glyphicon glyphicon-backward"></i> Voltar</button>
		<button type="button" class="btn btn-info" id="recarregar" url="<?php echo $_SERVER ['REQUEST_URI'] ?>"> <i class="glyphicon glyphicon-refresh"></i> Recarregar</button>
	</div>
</div>
<hr>

<script type="text/javascript">
	$(document).ready(function(){
		$("#fk_usuario_destino").select2();
	});
</script>

<?php echo form_open('controller_usuarios/comprar_recompensas'); ?>

<div class="row">
	
	<div class="col-md-12">
		<div class="form-group has-feedback">
			<label class="control-label" for="fk_usuario_destino">
				Enviar Para o(s) Usuário(s)
			</label> 
			<i class="glyphicon glyphicon-pencil form-control-feedback"></i>
			<select class="form-control" id="fk_usuario_destino" name="fk_usuario_destino[]" multiple="multiple" style="width: 100%;">
				<option value="-1" selected>Mandar Para Todos Usuários</option>
			</select>
		</div>
	</div>

</div>

<div class="row">

	<div class="col-md-12">
		<div class="form-group has-feedback">
			<label class="control-label" for="quantidade_ingresso">Valor</label>
			<i class="glyphicon glyphicon-pencil form-control-feedback"></i>
			<input type="text" class="form-control obrigatorio validar_numeros" id="quantidade_ingresso" name="quantidade_ingresso" placeholder="Quantidade ingresso" aviso="quantidade ingresso" value="" maxlength="20">
		</div>
	</div>

</div>
<hr>

<div class="row finalizar_formulario">
	<div class="col-md-9"></div>
	<div class="col-md-3" align="right">
		<button type="button" class="btn btn-danger" title="Limpar os dados dos campos" id="apagar"> <i class="glyphicon glyphicon-trash"></i> Limpar</button>
		<button type="submit" class="btn btn-success" id="validar_Enviar" title="Criar"> <i class="glyphicon glyphicon-send"></i> Recompensar </button>
	</div>
</div>

<?php echo form_close(); ?>

<script type="text/javascript">
	$(document).ready(function(){

		$('#fk_usuario_destino').select2({
		  	ajax: {
             	url: "<?php echo base_url() ?>controller_notificacoes/usuarios_filtro",
             	dataType: 'json',
             	delay: 1000,
             	data: function (params) {
                 	return {
                    	filtro: params.term // search term
                	};
            	},
            	processResults: function (data) {
					// parse the results into the format expected by Select2.
					// since we are using custom formatting functions we do not need to
					// alter the remote JSON data
                 	return {
                    	results: data
                 	};
            	},
            	cache: true
         	},
         	minimumInputLength: 1
		});
	});
</script>