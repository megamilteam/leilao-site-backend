<div class="row">
	<div class="col-md-8">
		<h1> <i class="glyphicon glyphicon-pencil"></i> TELA PARA TESTES GERAIS.</h1>
	</div>
	<div class="col-md-4" align="right">
		<button type="button" class="btn btn-info" id="voltar"> <i class="glyphicon glyphicon-backward"></i> Voltar</button>
		<button type="button" class="btn btn-info" id="recarregar" url="<?php echo $_SERVER ['REQUEST_URI'] ?>"> <i class="glyphicon glyphicon-refresh"></i> Recarregar</button>
	</div>
</div>
<hr>

<div class="row">
	<div class="col-md-3">
		<div class="form-group has-feedback">
			<label class="control-label" for="data">Data</label> 
			<i class="glyphicon glyphicon-pencil form-control-feedback"></i>
			<input class="form-control mascara_data" id="data" name="data" placeholder="Data" aviso="Data">
		</div>
	</div>

	<div class="col-md-3">
		<div class="form-group has-feedback">
			<label class="control-label" for="tel">Tel</label> 
			<i class="glyphicon glyphicon-pencil form-control-feedback"></i>
			<input class="form-control mascara_tel" id="tel" name="tel" placeholder="Tel" aviso="Tel">
		</div>
	</div>

	<div class="col-md-3">
		<div class="form-group has-feedback">
			<label class="control-label" for="cel">Cel</label> 
			<i class="glyphicon glyphicon-pencil form-control-feedback"></i>
			<input class="form-control mascara_cel" id="cel" name="cel" placeholder="Cel" aviso="Cel">
		</div>
	</div>

	<div class="col-md-3">
		<div class="form-group has-feedback">
			<label class="control-label" for="cep">Cep</label> 
			<i class="glyphicon glyphicon-pencil form-control-feedback"></i>
			<input class="form-control mascara_cep" id="cep" name="cep" placeholder="Cep" aviso="Cep">
		</div>
	</div>
</div>

<div class="row">
	<div class="col-md-3">
		<div class="form-group has-feedback">
			<label class="control-label" for="cs2">Com Select</label> 
			<i class="glyphicon glyphicon-pencil form-control-feedback"></i>
			<select class="form-control" id="cs2" name="cs2">
				<option>1</option>
				<option>2</option>
				<option>3</option>
			</select>
		</div>
	</div>

	<div class="col-md-3">
		<div class="form-group has-feedback">
			<label class="control-label" for="ss2">Sem Select</label> 
			<i class="glyphicon glyphicon-pencil form-control-feedback"></i>
			<select class="form-control semSelect2" id="ss2" name="ss2">
				<option>1</option>
				<option>2</option>
				<option>3</option>
			</select>
		</div>
	</div>

	<div class="col-md-3">
		<div class="form-group has-feedback">
			<label class="control-label" for="cnpj">CNPJ</label> 
			<i class="glyphicon glyphicon-pencil form-control-feedback"></i>
			<input class="form-control mascara_cnpj validar_cnpj" id="cnpj" name="cnpj" placeholder="CNPJ" aviso="CNPJ">
		</div>
	</div>

	<div class="col-md-3">
		<div class="form-group has-feedback">
			<label class="control-label" for="cpf">CPF</label> 
			<i class="glyphicon glyphicon-pencil form-control-feedback"></i>
			<input class="form-control mascara_cpf validar_cpf" id="cpf" name="cpf" placeholder="CPF" aviso="CPF">
		</div>
	</div>
</div>



<div class="row">
	<div class="col-md-3">
		<div class="form-group has-feedback">
			<label class="control-label" for="rg">RG</label> 
			<i class="glyphicon glyphicon-pencil form-control-feedback"></i>
			<input class="form-control mascara_rg" id="rg" name="rg" placeholder="RG" aviso="RG">
		</div>
	</div>

	<div class="col-md-3">
		<div class="form-group has-feedback">
			<label class="control-label" for="placa">Placa</label> 
			<i class="glyphicon glyphicon-pencil form-control-feedback"></i>
			<input class="form-control mascara_placa" id="placa" name="placa" placeholder="Placa" aviso="Placa">
		</div>
	</div>

	<div class="col-md-3">
		<div class="form-group has-feedback">
			<label class="control-label" for="email">E-mail</label> 
			<i class="glyphicon glyphicon-pencil form-control-feedback"></i>
			<input class="form-control validar_email" id="email" name="email" placeholder="E-mail" aviso="E-mail">
		</div>
	</div>

	<div class="col-md-3">
		<div class="form-group has-feedback">
			<label class="control-label" for="numerico">Numérico</label> 
			<i class="glyphicon glyphicon-pencil form-control-feedback"></i>
			<input class="form-control validar_numeros" id="numerico" name="numerico" placeholder="Numérico" aviso="Numérico">
		</div>
	</div>
</div>

<div class="row">
	<div class="col-md-3">
		<div class="form-group has-feedback">
			<label class="control-label" for="decimais">Decimais</label> 
			<i class="glyphicon glyphicon-pencil form-control-feedback"></i>
			<input class="form-control validar_decimais" id="decimais" name="decimais" placeholder="Decimais" aviso="Decimais">
		</div>
	</div>

	<div class="col-md-3">
		<div class="form-group has-feedback">
			<label class="control-label" for="monetaria">Dinheiro R$</label> 
			<i class="glyphicon glyphicon-pencil form-control-feedback"></i>
			<input class="form-control mascara_monetaria" id="monetaria" name="monetaria" placeholder="Dinheiro R$" aviso="Dinheiro R$">
		</div>
	</div>

	<div class="col-md-3">
		<div class="form-group has-feedback">
			<label class="control-label" for="range">Data Range</label> 
			<i class="glyphicon glyphicon-pencil form-control-feedback"></i>
			<input class="form-control mascara_range" id="range" name="range" placeholder="Data Range" aviso="Data Range">
		</div>
	</div>

</div>
<hr>

<div class="row">
	<div class="col-md-12">
		<textarea></textarea>
	</div>
</div>

<hr>

<div class="row">
	
	<table class="table table-bordered table-hover semDataTable_" align="center">
		<thead>
			<tr>
				<th>Coluna 1</th>
				<th>Coluna 2</th>
				<th>Coluna 3</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td> <span class="mascara_tel">111111111</span></td>
				<td>1</td>
				<td>1</td>
			</tr>
			<tr>
				<td>2</td>
				<td>2</td>
				<td>2</td>
			</tr>
		</tbody>
	</table>

</div>

<hr>

<div class="row finalizar_formulario">
	<div class="col-md-9"></div>
	<div class="col-md-3" align="right">
		<button type="button" class="btn btn-danger" title="Limpar os dados dos campos" id="apagar"> <i class="glyphicon glyphicon-trash"></i> Limpar</button>
		<button type="submit" class="btn btn-success" id="validar_Enviar" title="Salvar Edição"> <i class="glyphicon glyphicon-floppy-disk"></i> Atualizar Perfil </button>
	</div>
</div>

<?php echo form_close(); ?>


<script type="text/javascript">
	
$(document).ready(function(){
	$('#range').daterangepicker({
		 "showDropdowns": true,
	    ranges: {
           'Hoje': [moment(), moment()],
           'Últimos 7 Dias': [moment().subtract(6, 'days'), moment()],
           'Últimos 15 Dias': [moment().subtract(14, 'days'), moment()],
           'Últimos 30 Dias': [moment().subtract(29, 'days'), moment()],
           'Este Mês': [moment().startOf('month'), moment().endOf('month')],
           'Último Mês': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
           'Último trimestre': [moment().subtract(89, 'days'), moment()],
           'Último semestre': [moment().subtract(179, 'days'), moment()],
           'Último ano': [moment().subtract(359, 'days'), moment()]
        },
	    "locale": {
	        "format": "DD/MM/YYYY",
	        "separator": " - ",
	        "applyLabel": "Aplicar",
	        "cancelLabel": "Cancelar",
	        "fromLabel": "De",
	        "toLabel": "para",
	        "customRangeLabel": "Customizado",
	        "weekLabel": "S",
	        "daysOfWeek": [
	            "Dom",
	            "Seg",
	            "Ter",
	            "Qua",
	            "Qui",
	            "Sex",
	            "Sab"
	        ],
	        "monthNames": [
	            "Janeiro",
	            "Fevereiro",
	            "Março",
	            "Abril",
	            "Maio",
	            "Junho",
	            "Julho",
	            "Agosto",
	            "Setembro",
	            "Outubro",
	            "Novembro",
	            "Dezembro"
	        ],
	        "firstDay": 1
	    }
	});
});

tinymce.init({
	selector: 'textarea',
	language: 'pt_BR',
	height: 250,
	theme: 'modern',
	upload_action: '<?php echo base_url(); ?>Controller_testes/upload_imagem',
	upload_file_name: 'imagem',
	plugins: [
	'advlist autolink lists link image charmap print preview hr anchor pagebreak',
	'searchreplace wordcount visualblocks visualchars  fullscreen',
	'insertdatetime media nonbreaking save table contextmenu directionality upload',
	'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc '

	],
	toolbar1: 'undo redo |  | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist | link image upload',
	// toolbar2: 'print preview media | forecolor backcolor emoticons | codesample | styleselect ',
	image_advtab: true,
	templates: [
	{ title: 'Test template 1', content: 'Test 1' },
	{ title: 'Test template 2', content: 'Test 2' }
	],
	content_css: [
	'//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
	'//www.tinymce.com/css/codepen.min.css'
	]
});

</script>
