<script type="text/javascript">
	history.replaceState({pagina: "lista_usuarios"}, "Lista dos usuários ", "<?php echo base_url() ?>main/redirecionar/33");
</script>

<div class="row">
	<div class="col-md-6">
		<h1> <i class="glyphicon glyphicon-pencil"></i> Atualizar Whatsapp</h1>
	</div>
</div>
<hr>

<?php echo form_open('controller_usuarios/editar_whatsapp'); ?>

<!-- Campos ocultos -->
<input type="hidden" name="login_inicial" value="<?php echo $this->session->flashdata('login_usuario_edicao'); ?>">
<input type="hidden" name="email_inicial" value="<?php echo $this->session->flashdata('email_usuario_edicao'); ?>">

<div class="row">

	<div class="col-md-3">
		<div class="form-group has-feedback">
			<label class="control-label" for="whatsapp">Whatsapp</label>
			<i class="glyphicon glyphicon-pencil form-control-feedback"></i>
			<input type="text" class="form-control obrigatorio" id="whatsapp" name="whatsapp" placeholder="whatsapp" aviso="whatsapp" value="<?php echo $this->session->flashdata('whatsapp'); ?>">
		</div>
	</div>

</div>

<hr>

<div class="row finalizar_formulario">
	<div class="col-md-8"></div>
	<div class="col-md-4" align="right">
		<button type="button" class="btn btn-danger" title="Limpar os dados dos campos" id="apagar"> <i class="glyphicon glyphicon-trash"></i> Limpar</button>
		<button type="submit" class="btn btn-success" id="validar_Enviar" title="Salvar Edição"> <i class="glyphicon glyphicon-floppy-disk"></i> Atualizar Whatsapp </button>
	</div>
</div>

<?php echo form_close(); ?>
