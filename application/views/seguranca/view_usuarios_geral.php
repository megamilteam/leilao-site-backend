<div class="row" >
	<div class="col-md-8">
		<h3> <i class="glyphicon glyphicon-gift"></i> Lista de Usuarios</h3>
	</div>
</div>
<table class="table table-bordered table-hover" align="center">

    <thead>
        <tr>
            <th class="no-filter">Ver</th>
            <th>ID</th>
            <th>Nome</th>
            <th>E-mail</th>
            <th>Login</th>
            <th>Status</th>
        </tr>
    </thead>
    <tbody>
<?php

    foreach($dados_iniciais['usuarios'] as $usuario){
        echo "<tr>";
        echo '<td><button cod="'.$usuario->id.'" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal"> <i class="glyphicon glyphicon-edit"> </i> Detalhes</button></td>';
        echo "<td>{$usuario->id}</td>";
        echo "<td>{$usuario->nome_usuario}</td>";
        echo "<td>{$usuario->email_usuario}</td>";
        echo "<td>{$usuario->login_usuario}</td>";
        echo "<td>{$usuario->ativo}</td>";
        echo "</tr>";
    }


?>
    </tbody>
</table>

<!-- Modal -->
<div class="modal fade bs-example-modal-sm" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h3 align="center" class="modal-title" id="myModalLabel"><strong class="titulo_modal">Detalhes</strong></h3>
      </div>
      <div class="modal-body">
        <div id="detalhes"></div>
      </div>
    </div>
  </div>
</div>


<script type="text/javascript">
	$('#myModal').on('show.bs.modal', function (event) {

	  	var id = $(event.relatedTarget).attr('cod');
		$('#detalhes').load('<?php echo base_url() ?>Controller_usuarios/detalhes_usuario',{id: id}, function(){});

		$(document).on('click','#inativar_usuario',function(){

			$.post('<?php echo base_url() ?>Controller_usuarios/status_usuario', { id: id, ativar: 0 }, function(){location.reload();});

		});

		$(document).on('click','#ativar_usuario',function(){

			$.post('<?php echo base_url() ?>Controller_usuarios/status_usuario', { id: id, ativar: 1 }, function(){location.reload();});

		});

	})
</script>