<script type="text/javascript">
	history.replaceState({pagina: "lista_usuarios"}, "Lista dos usuários ", "<?php echo base_url() ?>main/redirecionar/2");
</script>

<div class="row">
	<div class="col-md-8">
		<h1> <i class="glyphicon glyphicon-user"></i> Novo Usuário</h1>
	</div>
	<div class="col-md-4" align="right">
		<button type="button" class="btn btn-info" id="voltar"> <i class="glyphicon glyphicon-backward"></i> Voltar</button>
		<button type="button" class="btn btn-info" id="recarregar" url="<?php echo $_SERVER ['REQUEST_URI'] ?>"> <i class="glyphicon glyphicon-refresh"></i> Recarregar</button>
	</div>
</div>
<hr>

<?php echo form_open('controller_usuarios/criar_usuario'); ?>
<div class="row">

	<div class="col-md-5">
		<div class="form-group has-feedback">
			<label class="control-label" for="nome_usuario">Nome do usuário</label>
			<input type="text" class="form-control obrigatorio" id="nome_usuario" name="nome_usuario" placeholder="Nome usuário" aviso="Nome usuário" value="<?php echo $this->session->flashdata('nome_usuario'); ?>" maxlength="100">
		</div>
	</div>

	<div class="col-md-4">
		<div class="form-group has-feedback">
			<label class="control-label" for="email_usuario">E-mail</label>
			<input type="text" class="form-control obrigatorio validar_email_usuario" id="email_usuario" name="email_usuario" placeholder="E-mail" aviso="E-mail" value="<?php echo $this->session->flashdata('email_usuario'); ?>" maxlength="40">
		</div>
	</div>

	<div class="col-md-3">
		<div class="form-group has-feedback">
			<label class="control-label" for="telefone_usuario">Celular</label>
			<input type="text" class="form-control obrigatorio mascara_cel" id="telefone_usuario" name="telefone_usuario" placeholder="Celular" aviso="Celular" value="<?php echo $this->session->flashdata('telefone_usuario'); ?>">
		</div>
	</div>

</div>

<div class="row">

	<div class="col-md-3">
		<div class="form-group has-feedback">
			<label class="control-label" for="login_usuario">Login do usuário</label>
			<input type="text" class="form-control obrigatorio" id="login_usuario" name="login_usuario" placeholder="Login do usuário" aviso="Login do usuário" value="<?php echo $this->session->flashdata('login_usuario'); ?>" maxlength="20">
		</div>
	</div>

	<div class="col-md-2">
		<div class="form-group has-feedback">
			<label class="control-label" for="senha_usuario">Senha</label>
			<input type="password" class="form-control obrigatorio" id="senha_usuario" name="senha_usuario" placeholder="Senha" aviso="Senha">
		</div>
	</div>

	<div class="col-md-2">
		<div class="form-group has-feedback">
			<label class="control-label" for="confirmacaoSenha">Confirme a Senha</label>
			<input type="password" class="form-control obrigatorio" id="confirmacaoSenha" name="confirmacaoSenha" placeholder="Confirme a Senha" aviso="Confirme a Senha">
		</div>
	</div>

	<div class="col-md-3">
		<div class="form-group has-feedback">
			<label class="control-label" for="fk_grupo_usuario">Grupo</label>
			<select class="form-control obrigatorio" id="fk_grupo_usuario" name="fk_grupo_usuario" aviso="Grupo">
			<?php 

				foreach ($dados_iniciais as $grupos) {
					if($grupos->id_grupo == 1){
						echo '<option value="'.$grupos->id_grupo.'">'.$grupos->nome_grupo.'</option>';
					}
				}

			 ?>
			</select>
		</div>
	</div>

	<div class="col-md-2">
		<div class="form-group has-feedback">
			<label class="control-label" for="ativo_usuario">Status</label>
			<select class="form-control obrigatorio" id="ativo_usuario" name="ativo_usuario" aviso="Status">
				<option value="1">Ativo</option>
				<option value="0">Inativo</option>
			</select>
		</div>
	</div>

</div>

<div class="row">

	<div class="col-md-3">
		<div class="form-group has-feedback">
			<label class="control-label" for="cep_usuario">CEP</label> 
			<i class="glyphicon glyphicon-pencil form-control-feedback"></i>
			<input type="text" class="form-control obrigatorio mascara_cep" id="cep_usuario" name="cep_usuario" placeholder="CEP" aviso="CEP" value="<?php echo $this->session->flashdata('cep_usuario'); ?>">
		</div>
	</div>

	<div class="col-md-3">
		<div class="form-group has-feedback">
			<label class="control-label" for="logradouro_usuario">Logradouro</label> 
			<i class="glyphicon glyphicon-pencil form-control-feedback"></i>
			<input type="text" class="form-control obrigatorio" id="logradouro_usuario" name="logradouro_usuario" placeholder="Logradouro" aviso="Logradouro" value="<?php echo $this->session->flashdata('logradouro_usuario'); ?>">
		</div>
	</div>

	<div class="col-md-3">
		<div class="form-group has-feedback">
			<label class="control-label" for="bairro_usuario">Bairro</label> 
			<i class="glyphicon glyphicon-pencil form-control-feedback"></i>
			<input type="text" class="form-control obrigatorio" id="bairro_usuario" name="bairro_usuario" placeholder="Bairro" aviso="Bairro" value="<?php echo $this->session->flashdata('bairro_usuario'); ?>">
		</div>
	</div>

	<div class="col-md-3">
		<div class="form-group has-feedback">
			<label class="control-label" for="cidade_usuario">Cidade</label> 
			<i class="glyphicon glyphicon-pencil form-control-feedback"></i>
			<input type="text" class="form-control obrigatorio" id="cidade_usuario" name="cidade_usuario" placeholder="Cidade" aviso="Cidade" value="<?php echo $this->session->flashdata('cidade_usuario'); ?>">
		</div>
	</div>

</div>

<div class="row">
	
	<div class="col-md-3">
		<div class="form-group has-feedback">
			<label class="control-label" for="estado_usuario">Estado</label> 
			<i class="glyphicon glyphicon-pencil form-control-feedback"></i>
			<select class="form-control obrigatorio" name="estado_usuario" id="estado_usuario">
				<option value="1">AC</option>
				<option value="2">AL</option>
				<option value="3">AP</option>
				<option value="4">AM</option>
				<option value="5">BA</option>
				<option value="6">CE</option>
				<option value="7">DF</option>
				<option value="8">ES</option>
				<option value="9">GO</option>
				<option value="10">MA</option>
				<option value="11">MT</option>
				<option value="12">MS</option>
				<option value="13">MG</option>
				<option value="14">PA</option>
				<option value="15">PB</option>
				<option value="16">PR</option>
				<option value="17">PE</option>
				<option value="18">PI</option>
				<option value="19">RJ</option>
				<option value="20">RN</option>
				<option value="21">RS</option>
				<option value="22">RO</option>
				<option value="23">RR</option>
				<option value="24">SC</option>
				<option value="25">SP</option>
				<option value="26">SE</option>
				<option value="27">TO</option>
			</select>
		</div>
	</div>

	<div class="col-md-3">
		<div class="form-group has-feedback">
			<label class="control-label" for="numero_usuario">Número</label> 
			<i class="glyphicon glyphicon-pencil form-control-feedback"></i>
			<input type="text" class="form-control obrigatorio" id="numero_usuario" name="numero_usuario" placeholder="Número" aviso="Número" value="<?php echo $this->session->flashdata('numero_usuario'); ?>">
		</div>
	</div>

	<div class="col-md-6">
		<div class="form-group has-feedback">
			<label class="control-label" for="complemento_usuario">Complemento</label> 
			<i class="glyphicon glyphicon-pencil form-control-feedback"></i>
			<input type="text" class="form-control" id="complemento_usuario" name="complemento_usuario" placeholder="Complemento" aviso="Complemento" value="<?php echo $this->session->flashdata('complemento_usuario'); ?>">
		</div>
	</div>

</div>

<hr>

<div class="row finalizar_formulario">
	<div class="col-md-9"></div>
	<div class="col-md-3" align="right">
		<button type="button" class="btn btn-danger" title="Limpar os dados dos campos" id="apagar"> <i class="glyphicon glyphicon-trash"></i> Limpar</button>
		<button type="submit" class="btn btn-success" id="validar_Enviar" title="Criar"> <i class="glyphicon glyphicon-floppy-disk"></i> Criar </button>
	</div>
</div>

<?php echo form_close(); ?>

<script type="text/javascript">
	$(document).ready(function(){

		//$('#estado_usuario').val(<?php echo $this->session->flashdata('estado_usuario'); ?>).trigger('change');

		$("#cep_usuario").focusout(function(){

			var settings = {
			"async": true,
			"crossDomain": true,
			"url": "<?php echo base_url(); ?>controller_webservice/buscar_cep?cep="+$("#cep_usuario").val(),
			"method": "GET"
			}

			$.ajax(settings).done(function (response) {
				$("#cep_usuario").val(response.endereco.cep);
				$("#logradouro_usuario").val(response.endereco.logradouro);
				$("#bairro_usuario").val(response.endereco.bairro);
				$("#cidade_usuario").val(response.endereco.localidade);
				$("#complemento_usuario").val(response.endereco.complemento);
				$('#estado_usuario').val(response.endereco.uf_id).trigger('change');
				console.log(response);
			});

		});

	});
</script>
