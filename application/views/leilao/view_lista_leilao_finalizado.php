<!-- View Leilões Pendentes -->
 <div class="main_leiloes_pendentes">

   <div class="container-fluid">

      <div class="leiloes_pendentes">

        <!-- Open Panel -->
        <div class="panel panel-default">

          <!-- Open header panel -->
          <div class="panel-heading">
            <h1 class="">Leilões Finalizados</h1>
          </div>
          <!-- Close header panel -->

          <!-- Corpo do panel -->
          <div class="panel-body">

            <div class="row">

              <!-- Listar Categorias -->
              <div class="col-md-12">
                <p class="j_list al-center">Listar <i class="glyphicon glyphicon-menu-down"></i> </p>
              </div>

              <div class="col-md-12 j_list_leiloes_pendente">

                <div class="row">
                  <div class="col-md-2">
                    <div class="col-md-12 col-sm-4" style="padding: 0; margin-bottom: 30px;">
                      <label class="" for="">Itens por página</label>
                      <input type="hidden" class="base_url_leilao_pendente" name="" value="<?= base_url('main/redirecionar/18/'); ?>">
                      <select class="custom-select form-control" id="j_leilao_pendente">
                        <option value="">Itens</option>
                        <option value="10">10</option>
                        <option value="15">15</option>
                        <option value="20">20</option>
                      </select>
                    </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                    <!-- navigation holder -->
                    <div class="holder"></div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                    <!-- Open Box Produtos -->
                    <div class="flex_box_product" id="box_leiloes_aprovados">

                    <?php

                      function dateFormat($data) {
                          $format = explode(' ', $data);
                          $data = explode('-', $format[0]);
                          if (empty($format[1])):
                              $format[1] = date('H:i:s');
                          endif;
                          $data = $data[2] . '/' . $data[1] . '/' . $data[0] . ' ' . $format[1];
                          return $data;
                      }

                      foreach ($dados_iniciais['leiloes'] as $leilao):
                        $status_leilao = ($leilao->status_leilao == 4 ? 'Finalizado' : '');
                        $desc_leilao = ($leilao->descricao_produto == null ? 'Não existe descrição associada ao produto!' : $leilao->descricao_produto);
                    ?>

                      <!-- Open Box Produto Item -->
                      <div class="flex_box_product_item">

                        <div class="thumbnail boxshadow hoverzoom">
                          <div class="" id="retina"></div>
                          <div class="img" style="background: url('<?= base_url('upload/produtos/'.$leilao->id_produto.'/'.$leilao->id_produto.'_0.png'); ?>') center center no-repeat; background-size: 300px 169px;"></div>
                          <div class="caption">
                            <h3><?= $leilao->nome_produto; ?></h3>
                            <p><b>Cod:</b> <?= $leilao->id_leilao; ?></p>
                            <p><b>Lance Minímo:</b> <?= number_format($leilao->lance_minimo, 2, ',', '.'); ?></p>
                            <p><b>Valor Minímo:</b> <?= number_format($leilao->valor_minimo, 2, ',', '.'); ?></p>
                            <p><b>Data de Inicio:</b> <?= dateFormat($leilao->data_inicio); ?> </p>
                            <p><b>Data de Fim:</b> <?= dateFormat($leilao->data_fim_previsto); ?> </p>
                            <p><b>Status:</b> <?= $status_leilao ?></p>
                            <span style="cursor: pointer;" class="span_desc" data-toggle="collapse" href="#collapse_<?= $leilao->id_leilao; ?>" aria-expanded="false" aria-controls="collapseExample">
                              <b>Descrição</b> <i class="glyphicon glyphicon-plus"></i>
                            </span>
                            <div class="collapse" id="collapse_<?= $leilao->id_leilao; ?>">
                              <div class="well">
                                <?= $desc_leilao; ?>
                              </div>
                            </div>
                          </div>
                          <div class="retina">
                            <!--<a class="product_edit" href="#" data-toggle="tooltip" title="Editar!" alt="[Editar!]" >
                              <i class="glyphicon glyphicon-edit animated"></i>
                            </a>
                            <a class="product_trash" href="#" data-toggle="tooltip" title="Excluir!" alt="[Excluir!]" >
                              <i class="glyphicon glyphicon-trash animated"></i>
                            </a>-->
                          </div>
                        </div>

                      </div>
                      <!-- Close Box Produto Item -->

                      <?php
                        endforeach;
                      ?>

                  </div>
                  <!-- Close Box Produtos -->
                  </div>
                </div>

                </div>

              </div>

            </div>

          </div>
          <!-- Close Corpo do panel -->

        </div>
        <!-- Close Panel -->

      </div>

   </div>

 </div>
<script type="text/javascript">
  $(document).ready(function(){
    $('.j_list').click();
  });
</script>