<!-- View Leilões Pendentes -->
 <div class="main_leiloes_pendentes">

   <div class="container-fluid">

      <div class="leiloes_pendentes">

        <!-- Open Panel -->
        <div class="panel panel-default">

          <!-- Open header panel -->
          <div class="panel-heading">
            <h1 class="">Leilões Pendentes</h1>
          </div>
          <!-- Close header panel -->

          <!-- Corpo do panel -->
          <div class="panel-body">

            <div class="row">

              <!-- Listar Categorias -->
              <div class="col-md-12">
                <p class="j_list al-center">Listar <i class="glyphicon glyphicon-menu-down"></i> </p>
              </div>

              <div class="col-md-12 j_list_leiloes_pendente">

                <div class="row">
                  <div class="col-md-6">
                    <div class="box_form_item">
                      <form>
                          <label>Buscar Leilão: </label>
                          <div class="input-group">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
                            <input type="search" class="form-control input-search" name="search" title="Buscar Leilão Pendente" alt="search-leilao-pendente" placeholder="Buscar">
                          </div>
                      </form>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="col-md-12 col-sm-4" style="padding: 0; margin-bottom: 30px;">
                      <label class="" for="">Itens por página</label>
                      <input type="hidden" class="base_url_leilao_pendente" name="" value="<?= base_url('main/redirecionar/16/'); ?>">
                      <select class="custom-select form-control" id="j_leilao_pendente">
                        <option value="">Itens</option>
                        <option value="10">10</option>
                        <option value="25">25</option>
                        <option value="50">50</option>
                        <option value="100">100</option>
                        <option value="500">500</option>
                      </select>
                    </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                    <!-- navigation holder -->
                    <div class="holder"></div>
                  </div>
                </div>

                <div class="table-responsive">

                    <?php echo form_open('Controller_leilao/form_leiloes'); ?>

                    <input type="hidden" name="qtd_leiloes" value="<?php echo count($dados_iniciais['leiloes']); ?>">

                    <table class="table table-action search-leilao-pendente">
                      <thead>
                        <tr>
                          <th class="t-medium no-filter">Aceitar/Rejeitar</th>
                          <!--<th class="t-medium">Cod</th>-->
                          <th class="t-medium">Nome</th>
                          <th class="t-medium">Valor Minímo</th>
                    <!--  <th class="t-medium">Data Início</th>
                          <th class="t-medium">Data Fim</th>-->
                          <th class="t-medium">Ingressos</th>
                          <th class="t-medium no-filter">Editar</th>
                          <th class="t-medium no-filter">Editar</th>
                          <th class="t-medium no-filter">Cliente</th>
                          <!--<th class="t-medium">Ativar/Inativar</th>-->
                        </tr>
                      </thead>

                      <tbody id="table_pendentes_pagination">                        

                          <?php

                            function dateFormat($data) {
                                $format = explode(' ', $data);
                                $data = explode('-', $format[0]);
                                if (empty($format[1])):
                                    $format[1] = date('H:i:s');
                                endif;
                                $data = $data[2] . '/' . $data[1] . '/' . $data[0] . ' ' . $format[1];
                                return $data;
                            }

                            foreach ($dados_iniciais['leiloes'] as $chave => $dados):
                              $status = ($dados->status_leilao == 1 ? 'Pendente' : '');
                              //var_dump($dados);
                          ?>

                          <tr>
                            <td class="t-medium">
                              <fieldset id="group<?php echo $chave; ?>">
                                  <input type="radio"name="aceitar<?php echo $chave; ?>" value="2-<?= $dados->id_leilao; ?>"> Aceitar <br>
                                  <input type="radio"name="aceitar<?php echo $chave; ?>" value="3-<?= $dados->id_leilao; ?>"> Rejeitar
                              </fieldset>
                            </td>
                            <td class=""><?= $dados->nome_produto; ?></td>
                            <td class=""><?= "R$ " . number_format($dados->valor_minimo, 2, ',', '.'); ?></td>
                      <!--  <td class="">dateFormat($dados->data_inicio); ?></td>
                            <td class="">dateFormat($dados->data_fim_previsto); ?></td> -->
                            <td class=""><?= $dados->ingressos; ?></td>
                            <td><a target="_blank" href="<?php echo base_url(); ?>main/redirecionar/23/<?= $dados->id_produto; ?>"><button type="button" class="btn btn-info" style="width: 100%"> <i class="glyphicon glyphicon-edit"> </i> Produto</button></a></td>
                            <td><a target="_blank" href="<?php echo base_url(); ?>main/redirecionar/28/<?= $dados->id_leilao; ?>"><button type="button" class="btn btn-info" style="width: 100%"> <i class="glyphicon glyphicon-edit"> </i> Leilão</button></a></td>
                             <td><button type="button" cod="<?php echo $dados->fk_usuario; ?>" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal" style="width: 100%"> <i class="glyphicon glyphicon-edit"> </i> Ver</button></td>

                             <!--<td>
                               <a href="" data-toggle="tooltip" title="Excluir!" alt="[Excluir]">
                                 <i class="font-medium color-red glyphicon glyphicon-trash animated"></i>
                               </a>
                             </td>
                          </tr>-->

                          <?php endforeach; ?>

                      </tbody>

                    </table>

                          <button type="submit" name="aceitar" class="btn btn-success"><i class="glyphicon glyphicon-plus"></i> Concluir </button>

                  </form>

                </div>

              </div>

            </div>

          </div>
          <!-- Close Corpo do panel -->

        </div>
        <!-- Close Panel -->

      </div>

   </div>

 </div>

<script type="text/javascript">
  $(document).ready(function(){
    $('.j_list').click();
  });
</script>

<!-- Modal -->
<div class="modal fade bs-example-modal-sm" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h3 align="center" class="modal-title" id="myModalLabel"><strong class="titulo_modal">Detalhes</strong></h3>
      </div>
      <div class="modal-body">
        <div id="detalhes"></div>
      </div>
    </div>
  </div>
</div>


<script type="text/javascript">
    $('#myModal').on('show.bs.modal', function (event) {

        var id = $(event.relatedTarget).attr('cod');
        $('#detalhes').load('<?php echo base_url() ?>Controller_usuarios/detalhes_usuario',{id: id}, function(){});

        $(document).on('click','#inativar_usuario',function(){

            $.post('<?php echo base_url() ?>Controller_usuarios/status_usuario', { id: id, ativar: 0 }, function(){location.reload();});

        });

        $(document).on('click','#ativar_usuario',function(){

            $.post('<?php echo base_url() ?>Controller_usuarios/status_usuario', { id: id, ativar: 1 }, function(){location.reload();});

        });

    })
</script>