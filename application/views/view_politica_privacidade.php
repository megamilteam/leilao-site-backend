<!DOCTYPE html>
<title>Política de Privacidade e Segurança</title>
<style>
div.container {
background-color: #ffffff;
}
div.container p {
font-family: Arial;
font-size: 14px;
font-style: normal;
font-weight: normal;
text-decoration: none;
text-transform: none;
color: #000000;
background-color: #ffffff;
}
</style>

<div class="container">
<p>Política de Privacidade e Segurança</p>
<p></p>
<p>A Política de Privacidade e Segurança abaixo constitui parte integrante dos Termos e Condições de Uso.   </p>
<p></p>
<p>1. Identificação </p>
<p></p>
<p>Este site é de propriedade, mantido, e operado por WLW COMÉRCIO DE PRODUTOS PESSOAIS, com endereço na Rua cinco de outubro, nº 42 Americanópolis São Paulo/SP,  inscrita no CNPJ sob o nº 28.295.767/0001-04.</p>
<p></p>
<p>Política de Privacidade e Segurança</p>
<p></p>
<p>A Política de Privacidade e Segurança abaixo constitui parte integrante dos Termos e Condições de Uso.   </p>
<p></p>
<p>1. Identificação </p>
<p></p>
<p>Este site é de propriedade, mantido, e operado por WLW COMÉRCIO DE PRODUTOS PESSOAIS, com endereço na Rua cinco de outubro, nº 42 Americanópolis São Paulo/SP,  inscrita no CNPJ sob o nº 28.295.767/0001-04..   </p>
<p>dados pessoais do Usuário, pressupõe o consentimento expresso quanto à coleta, uso, armazenamento e tratamento de dados pessoais. O Usuário é responsável, nas esferas civil e criminal, pela veracidade e atualização dos dados fornecidos (inclusive os dados pessoais) e o LEILÃO 24H se exime de qualquer reponsabilidade por danos decorrentes do preenchimento incompleto, impreciso ou inexato do cadastro pelo Usuário, sob qualquer meio ou forma, ou, ainda, pelo uso desse cadastro de forma indevida por qualquer terceiro não autorizado a usar tais dados ou, ainda, por terceiros que tenham, devida ou  indevidamente, obtido os dados do Usuário para acesso no Site, agindo como se ele fosse. </p>
<p>Coleta de dados: Cookies. Visando oferecer a melhor experiência de navegação e Compras ao Usuário, o LEILÃO 24H  utiliza-se de tecnologias para coletar e armazenar informações relacionadas à visita do Usuário no Site e isso pode incluir o envio de um ou mais cookies ou identificadores anônimos que coletam dados relativos às preferências de navegação e às páginas visitadas pelo Usuário. Desta forma, a apresentação do Site fica personalizada e alinhada aos interesses pessoais do Usuário. A utilização destes dados fica restrita ao objetivo indicado e o LEILÃO 24H  se compromete a não utilizar ou permitir a utilização de tais dados com outra finalidade. Ademais, a coleta, guarda e tratamento destes dados é absolutamente automatizada, não havendo nenhuma possibilidade de contato humano com os dados em questão. O Usuário pode e poderá, a qualquer tempo, caso discorde da política de cookies acima, utilizar as ferramentas de seu navegador que impedem a instalação de cookies e ainda apagar quaisquer cookies existentes em seu dispositivo de conexão com a internet. Neste caso, algumas funcionalidades do Site poderão apresentar erros. O LEILÃO 24H poderá ainda utilizar-se de outras tecnologias para a coleta de dados de navegação dos Usuários, comprometendo-se a guardá-los, tratá-los e utilizá-los em conformidade com este Política. </p>
<p>Coleta de dados: Registros de acesso.O LEILÃO 24H manterá em sua base de dados todas as informações relativas aos acessos do Usuário ao Site, incluindo, mas não se limitando ao endereço IP, às páginas acessadas, aos horários e datas de acesso, e o dispositivo de acesso utilizado, nos termos da legislação vigente. Tais registros poderão ser utilizados em investigações internas ou públicas para averiguação de práticas que possam gerar quaisquer prejuízos pelo LEILÃO 24H, inclusive a prática de crimes em ambientes virtuais. Coleta de dados: Outras formas. pelo O LEILÃO 24H  poderá coletar dados pessoais do Usuário, que não os identificados aqui, que sejam inseridos pelo Usuário e sobre o Usuário voluntariamente no processo de navegação do Site ou quando entrar em contato com o  LEILÃO 24H. Finalmente, o LEILÃO 24H poderá acessar bases de dados públicas ou privadas para confirmar a veracidade dos dados pessoais informados pelo Usuário, inclusive dados relacionados ao pagamento da Compra. </p>
<p>Utilização de dados: E-mail. O LEILÃO 24H utilizará o e-mail do Usuário prioritariamente para enviar informações sobre suas Compras (confirmação de Compra e atualizações da situação). O e-mail cadastrado também será utilizado para comunicação relacionada aos cursos em que o Cliente estiver inscrito e para recuperação da senha de acesso do Cliente, em caso de perda ou esquecimento da senha. </p>
<p>Utilização de dados: Publicidade via mala direta, e-mail, MMS ou SMS. No momento do cadastro de seus dados de contato, o Usuário terá a opção de aceitar ou proibir o envio de mensagens publicitárias. Ademais, a qualquer momento, o Usuário poderá alterar sua decisão, por meio de acesso à sua Conta no Site ou de contato com o LEILÃO 24H ou, ainda, no campo de descadastro existente nas newsletters enviadas pelo LEILÃO 24H sendo que nesse caso o descadastramento da base de dados poderá demorar até 5 (cinco) dias para ser efetivada. </p>
<p>Utilização dos dados: Outras formas. Além das formas expostas acima, pelo LEILÃO 24H poderá, a seu exclusivo critério, utilizar os dados pessoais do Usuário nas seguintes formas: (i) atualização de cadastro; (ii) garantia da segurança do Usuário; (iii) resposta a solicitações do próprio Usuário; (iv) informação acerca de alterações nos Termos e Condições de Uso ou das Políticas; (v) elaboração de estatísticas com relação ao uso do Site, garantido o anonimato do Usuário, inclusive para fins de aperfeiçoamento e entendimento do perfil dos Usuários para a melhoria do Site; (vi) aperfeiçoamento de ferramentas de interatividade entre o Site e o Usuário, garantido seu anonimato; (vii) cumprimento de ordens judiciais; e (viii) defesa dos direitos do LEILÃO 24H contra o Usuário em procedimentos judiciais ou administrativos. </p>
<p>Guarda dos dados. O LEILÃO 24H  guardará todos os dados coletados em suas bases de dados protegidas e seguras. Tais dados serão acessados apenas por processos computadorizados automatizados, profissionais autorizados e nos casos listados nesta Política. Caso o Usuário requeira a exclusão de seus dados da base de dados, o LEILÃO 24H se reserva o seu direito de manter os dados em questão em cópias de salvaguarda por até 6 (seis) meses, a fim de cumprir obrigações legais de guarda obrigatória.   </p>
<p></p>
<p>5. Compartilhamento e divulgação dos dados </p>
<p></p>
<p>O  LEILÃO 24H tem a confidencialidade dos dados pessoais do Usuário como prioridade em seus negócios. Assim, assume o compromisso de não divulgar, compartilhar, dar acesso a, facilitar acesso a, alugar, vender, trocar ou de qualquer outra forma disponibilizar tais informações a terceiros, sob nenhum pretexto, exceto nos casos autorizados expressamente pelo Usuário, inclusive nos casos indicados abaixo.   Com o único intuito de permitir a concretização de pagamentos no site, o LEILÃO 24H poderá compartilhar dados pessoais dos Usuários com seus parceiros comerciais, como empresas processadoras de pagamentos, administradoras de cartão de crédito e transportadoras. Neste caso, serão compartilhados apenas os dados pessoais imprescindíveis para que o parceiro comercial do LEILÃO 24H desempenhe sua atividade (cobrança, entrega, etc.). Ademais, tais parceiros comerciais serão obrigados, por meio de contratos de confidencialidade, a não arquivar, manter em arquivo, compilar, copiar, reproduzir ou compartilhar tais dados com quem quer que seja.   A outra hipótese de divulgação de dados pessoais é por meio de uma determinação judicial. Também neste caso, a divulgação ocorrerá apenas na medida necessária para cumprir a determinação judicial, permanecendo sigilosos os dados não requeridos pela autoridade em questão.   </p>
<p></p>
<p>6. Dados transmitidos sem solicitação do LEILÃO 24H </p>
<p>O LEILÃO 24H solicita ao Usuário que não envie ao  LEILÃO 24H  quaisquer informações comerciais, criações pessoais, ideias, fotografias, projetos, conceitos, etc (Conteúdos Não Solicitados). Tais Conteúdos Não Solicitados serão sumariamente descartados, sem qualquer leitura ou incorporação às bases de dados do  LEILÃO 24H. Nos termos da Lei de Direitos Autorais, não são suscetíveis de proteção no Brasil as ideias, concepções abstratas, projetos, planos e esquemas. Desta forma, o eventual uso pelo  LEILÃO 24H  de quaisquer Conteúdos Não Solicitados será decorrente de desenvolvimento interno e independente e poderá ocorrer livremente, não sendo devida ou exigida qualquer autorização ou compensação ao usuário ou consumidor. O  LEILÃO 24H  desenvolve de forma independente todas as suas políticas e atividades, rechaçando desde já qualquer acusação ou alegação de aproveitamento de Conteúdos Não Solicitados. </p>
<p></p>
<p>7. Medidas de segurança   </p>
<p></p>
<p>Recursos tecnológicos. O  LEILÃO 24H  adota recursos tecnológicos avançados para garantir a segurança de todos os dados pessoais coletados e armazenados. Nem mesmo os funcionários do LEILÃO 24H têm livre acesso à base de dados dos Usuários, sendo este limitado apenas àquelas pessoas cujas funções exigem o contato com dados pessoais. Entre as medidas de segurança implementadas, estão a utilização de modernas forma de criptografia e a instalação de barreiras contra o acesso indevido à base de dados (firewalls). Tais medidas podem ser verificadas pelo Usuário acessando o Site pela visualização do “cadeado de segurança” em seu navegador de internet. </p>
<p>Sigilo da senha. O LEILÃO 24H recomenda que o Usuário mantenha sua senha sob total sigilo, evitando a sua divulgação a terceiros. O  LEILÃO 24H nunca solicitará ao Usuário que informe sua senha fora do Site, por telefone, e-mail ou por qualquer outro meio de comunicação. A senha do Usuário deverá ser usada exclusivamente no momento do acesso à Conta do Usuário no Site. Caso o Usuário suspeite que sua senha tenha sido exposta a terceiros,o LEILÃO 24H recomenda a imediata substituição da senha. </p>
<p>E-mails suspeitos. O LEILÃO 24H envia ao Usuário apenas e-mails com mensagens publicitárias, divulgando produtos e serviços ou atualizando informações. O  LEILÃO 24H não envia mensagens (i) solicitando dados pessoais do Usuário; (ii) solicitando a senha ou dados financeiros do Usuário; (iii) com arquivos anexos exceto documentos em PDF; ou (iv) com links para download de arquivos. Caso receba um e-mail com tais características, desconsidere-o e entre em contato com o  LEILÃO 24H ,.</p>
<p>Cartões de crédito. O LEILÃO 24H não armazena em sua base de dados informações financeiras do Usuário, como as informações referentes a cartões de crédito. O procedimento de aprovação do pagamento ocorre entre o Usuário, os bancos e as administradoras de cartões, sem intervenção do LEILÃO 24H. Impossibilidade de responsabilização. Em que pese os maiores esforços do LEILÃO 24H, o atual estágio da tecnologia não permite que se crie uma base de dados absolutamente segura contra ataques. Desta forma, o LEILÃO 24H  exime-se de qualquer responsabilidade por danos eventualmente causados por terceiros, inclusive por invasões no Site ou na base de dados, por vírus ou por vazamento de informações, a menos que fique comprovada exclusiva culpa do  LEILÃO 24H .</p>
<p></p>
<p>8. Direitos das pessoas sobre os dados coletados </p>
<p></p>
<p>O  LEILÃO 24H permite que o Usuário faça diferentes tipos de cadastro, contendo mais ou menos informações de acordo com seu próprio objetivo no Site ( ARREMATAR E/OU LEILOAR). Assim, o Usuário tem a possibilidade de escolher a forma de cadastro, devendo preenchê-lo com informações verídicas e atualizadas. O Usuário declara ser o legítimo titular de seus dados pessoais e poderá, a qualquer momento, utilizar as ferramentas do Site para editá-los, atualizá-los ou removê-los preventivamente de nossa base de dados. O LEILÃO 24H  manterá os dados preventivamente removidos em sigilo pelo prazo de seis meses, para atender obrigações legais de guarda obrigatória, descartando-os definitivamente após tal período. O LEILÃO 24H disponibiliza ainda ferramentas para que o Usuário possa determinar alguns usos de seus dados pessoais, como a autorização para envio de peças de publicidade.   </p>
<p></p>
<p>9. Uso de dados em caso de alteração de controle do LEILÃO 24H </p>
<p>Os dados coletados podem ser eventualmente transferidos a um terceiro em caso de alteração do controle, de uma aquisição, de uma incorporação, de uma fusão ou de uma venda de ativos do LEILÃO 24H] sob qualquer meio ou forma.   </p>
<p></p>
<p>10. Política de dados de menores   </p>
<p></p>
<p>O Site não é direcionado a menores de 16 (dezesseis) anos. No entanto, o acesso ao Site não é proibido aos menores, uma vez que não há qualquer conteúdo restrito por questões etárias. Os formulários e questionários do site do LEILÃO 24H não visam obter dados de menores. Caso tais dados sejam inseridos por menores, seu representante legal poderá contatar com o LEILÃO 24H para retificar, modificar ou remover tais dados.</p>
<p></p>
<p></p>
</div>