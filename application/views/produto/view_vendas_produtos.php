<div class="row" >
	<div class="col-md-8">
		<h3> <i class="glyphicon glyphicon-gift"></i> Lista de arremates</h3>
	</div>
</div>
<table class="table table-bordered table-hover" align="center">

    <thead>
        <tr>
            <th>Data Arremate</th>
            <th>Vendedor</th>
            <th>Produto</th>
            <th>Valor venda</th>
            <th>Comprador</th>
            <th>Link cobrança</th>
            <th>Status Pagamento</th>            
            <th>Já Pagou</th>
        </tr>
    </thead>
    <tbody>
<?php

    foreach($dados_iniciais['leiloes'] as $leilao){
        echo "<tr>";

        echo "<td>{$leilao->data_fim_previsto}</td>";
		echo "<td>{$leilao->nome_usuario_leiloando}</td>";
        echo "<td>{$leilao->nome_produto}</td>";
        echo "<td class=\"mascara_monetaria\">{$leilao->lance_formatado}</td>";
		echo "<td>{$leilao->nome_usuario_arrematou}</td>";
        echo "<td>{$leilao->link_compra}</td>";
        if(isset($leilao->finalizado) && !is_null($leilao->finalizado) && $leilao->finalizado) {
            echo "<td align=\"center\">PAGO</td>";
            echo "<td></td>";
        } else {
            echo "<td>Pendente</td>";
            echo '<td><button cod="'.$leilao->id_leilao.'" class="btn btn-primary btn-lg confirmPay"> <i class="glyphicon glyphicon-edit"> </i> Marcar como pago</button></td>';
        }
        
        echo "</tr>";
    }


?>
    </tbody>
</table>


<script type="text/javascript">
    $('.confirmPay').click(function (event) {

        var id_leilao = $(this).attr('cod');
        swal({
		  title: "CONFIRMAR PAGAMENTO?",
		  text: 'Confirma que o usuário pagou o produto? essa acão NÃO pode ser desfeita',
		  type: "warning",
		  showCancelButton: true,
		  confirmButtonColor: "#DD6B55",
		  cancelButtonText: "CANCELAR",
		  confirmButtonText: "PAGO!",
		  closeOnConfirm: false
		},function(){

            $.ajax({  
                type: "POST",  
                url: "<?php echo base_url(); ?>Controller_leilao/marcarPago",  
                data:{id_leilao: id_leilao},
                dataType:'json',
                complete: function()
                {  
                    location.reload();  
                } 
            }); 
			
		});

    })
</script>