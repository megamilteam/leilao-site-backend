<div class="row" >
	<div class="col-md-8">
		<h3> <i class="glyphicon glyphicon-gift"></i> Lista de produtos</h3>
	</div>
	<div class="col-md-4" align="right">
		<a class="btn btn-success" href="<?php echo base_url(); ?>main/redirecionar/22">
			<i class="glyphicon glyphicon-plus-sign"></i> Novo Produto
		</a>
	</div>
</div>
<table class="table table-bordered table-hover" align="center">

    <thead>
        <tr>
            <th>Editar</th>
            <th>ID</th>
            <th>Categoria</th>
            <th>Título</th>
            <th>Descrição</th>
            <th>Usado</th>
            <th>Recompensa?</th>
            <th>Valor em pontos</th>
        </tr>
    </thead>
    <tbody>

<?php

    foreach($dados_iniciais['produtos'] as $produto){
        echo "<tr>";
        echo '<td><a href="'.base_url().'main/redirecionar/23/'.$produto->id_produto.'"><button class="btn btn-info"> <i class="glyphicon glyphicon-edit"> </i> Editar</button></a></td>';
        echo "<td>{$produto->id_produto}</td>";
        echo "<td>{$produto->nome_categoria}</td>";
        echo "<td>{$produto->nome_produto}</td>";
        echo "<td>{$produto->descricao_produto}</td>";
        if($produto->produto_usado) {
            echo "<td>Usado</td>";
        } else {
            echo "<td>Novo</td>";
        }
        if($produto->produto_recompensa) {
            echo "<td>Recompensa</td>";
            echo "<td>{$produto->produto_recompensa_custo}</td>";
        } else {
            echo "<td>Produto Comum</td>";
            echo "<td>-</td>";
        }
        echo "</tr>";
    }

?>

    </tbody>
</table>