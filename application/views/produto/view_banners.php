<div class="row">
	<div class="col-md-8">
		<h1> <i class="glyphicon glyphicon-file"></i> Banner Aplicativo</h1>
	</div>
	<div class="col-md-4" align="right">
		<button type="button" class="btn btn-info" id="voltar"> <i class="glyphicon glyphicon-backward"></i> Voltar</button>
		<button type="button" class="btn btn-info" id="recarregar" url="<?php echo $_SERVER ['REQUEST_URI'] ?>"> <i class="glyphicon glyphicon-refresh"></i> Recarregar</button>
	</div>
</div>
<hr>

<?php 

	if(is_dir(FCPATH.'/upload/banners/') === false) {
		$banners = array();
	} else {
		$banners = scandir(FCPATH.'/upload/banners/', 1);
	}

?>

<div id="banners" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
  	<?php 
  		$active = true;
  		foreach ($banners as $chave => $banner) {
  			$active = $active === true ? ' active' : '';
  			if($banner != '.' && $banner != '..')
  				echo '<li data-target="#banners" data-slide-to="'.$chave.'" class="'.$active.'"></li>';
  		} 
  	?>
  </ol>

  <!-- Wrapper for slides -->
  <div class="carousel-inner">
  	<?php 

  		$active = true;
  		foreach ($banners as $chave => $banner) {
  			if($banner != '.' && $banner != '..'){
  				$active = $active === true ? ' active' : '';
  				echo '<div class="item'.$active.'">
				      	<img src="'.base_url('/upload/banners/'.$banner).'" title="'.$banner.'">
				      	<div class="carousel-caption">
				        	<h3>Remover?</h3>
				        	<p id="'.$banner.'" style="cursor: pointer" class="remover">Clique aqui para remover banner</p>
				      	</div>
				     </div>';
  			}
  		} 
  	?>

  </div>

<?php if(count($banners) > 2) { ?>
	  <!-- Left and right controls -->
	  <a class="left carousel-control" href="#banners" data-slide="prev">
	    <span class="glyphicon glyphicon-chevron-left"></span>
	    <span class="sr-only">Próximo</span>
	  </a>
	  <a class="right carousel-control" href="#banners" data-slide="next">
	    <span class="glyphicon glyphicon-chevron-right"></span>
	    <span class="sr-only">Anterior</span>
	  </a>

<?php } ?>

</div>

<hr>
<?php echo form_open_multipart('Controller_produtos/novo_banner'); ?>
<div class="col-md-3"></div>
<div class="col-md-6">
	<label for="banner">Nova Imagem</label>
	<div class="input-group">
	   <input type="file" class="form-control" name="imagem" id="banner">
	   <span class="input-group-btn">
	        <button class="btn btn-success" type="submit">Enviar</button>
	   </span>
	</div>
</div>
<?php echo form_close(); ?>

<script type="text/javascript">
	$('.remover').click(function(){

		if (confirm('Confirma que deseja apagar esse banner? ('+$(this).attr('id')+')')) {
        $.ajax({  
                type: "POST",  
                url: "<?php echo base_url(); ?>Controller_produtos/removerBanner",  
                data:{banner: $(this).attr('id')},
                dataType:'json',
                complete: function()
                {  
                    location.reload();  
                } 
            }); 
	    }

	});
</script>
