<style type="text/css">
	.btn {
		color: white;
		text-shadow: 1px 1px 1px black;
	}
</style>

<script type="text/javascript">
	//Exibir imagem ao selecionar.
	function readURL(input, id) {
	   
	   	if (input.files && input.files[0]) {
	       var reader = new FileReader();

	       reader.onload = function (e) {
	           $('#'+id).attr('src', e.target.result);
	       }
	       reader.readAsDataURL(input.files[0]);
	       
	   }

	}
</script>

<div class="row">
	<div class="col-md-8">
		<h1> <i class="glyphicon glyphicon-file"></i> Novo Produto</h1>
	</div>
	<div class="col-md-4" align="right">
		<button type="button" class="btn btn-info" id="voltar"> <i class="glyphicon glyphicon-backward"></i> Voltar</button>
		<button type="button" class="btn btn-info" id="recarregar" url="<?php echo $_SERVER ['REQUEST_URI'] ?>"> <i class="glyphicon glyphicon-refresh"></i> Recarregar</button>
	</div>
</div>
<hr>

<?php echo form_open_multipart('Controller_produtos/novo_produto'); ?>

<div class="row" style="margin-bottom: 40px;">

<?php for ($i=0; $i < 6; $i++) {  ?>

	<div class="col-md-2" align="center">
		<label for='file<?= $i; ?>'>
			Foto <?= $i+1; ?>
			<img id="foto<?= $i; ?>" src="<?php echo base_url() ?>style/img/foto_file.png" width="100px" height="100px">
		</label>  
		<input type="file" id="file<?= $i; ?>" name="imagem[]" multiple="multiple" onchange="readURL(this,'foto<?= $i; ?>');" style="display:none">
	</div>
		
<?php } ?>

</div>

<div class="row">
    
	<div class="col-md-4">
		<div class="form-group has-feedback">
			<label class="control-label" for="fk_categoria">Categoria</label> 

			<select class="form-control obrigatorio" id="fk_categoria" name="fk_categoria" aviso="Categoria">
				<option>Selecione...</option>
			<?php 

				foreach ($dados_iniciais['categorias'] as $categoria) {
					echo '<option value="'.$categoria->id_categoria.'">'.$categoria->nome_categoria.'</option>';
				}

			 ?>
			</select>
		</div>
	</div>

	<div class="col-md-4">
		<div class="form-group has-feedback">
			<label class="control-label" for="nome_produto">Título</label> 
			<i class="glyphicon glyphicon-pencil form-control-feedback"></i>
			<input type="text" class="form-control obrigatorio" id="nome_produto" name="nome_produto" placeholder="Título" aviso="Título" value="" maxlength="100">
		</div>
	</div>

	<div class="col-md-1" style="padding-top: 20px;" align="center">
		<div class="form-group has-feedback">
			<label class="control-label" for="produto_usado">O Produto é usado?</label> 
			<br>
			<input type="checkbox" id="produto_usado" name="produto_usado" placeholder="Usado?" aviso="Usado?">
		</div>
	</div>

	<div class="col-md-1" style="padding-top: 20px;" align="center">
		<div class="form-group has-feedback">
			<label class="control-label" for="produto_recompensa">Ativar Recompensa?</label> 
			<br>
			<input type="checkbox" id="produto_recompensa" name="produto_recompensa" placeholder="Recompensa?" aviso="Recompensa?">
		</div>
	</div>

	<div class="col-md-2">
		<div class="form-group has-feedback">
			<label class="control-label" for="produto_recompensa_custo">Custo em pontos</label> 
			<i class="glyphicon glyphicon-pencil form-control-feedback"></i>
			<input type="text" class="form-control validar_numeros" id="produto_recompensa_custo" name="produto_recompensa_custo" placeholder="Valor em pontos" aviso="Valor em pontos" readonly>
		</div>
	</div>

</div>

<div class="row">
		
	<div class="col-md-12">
		<div class="form-group has-feedback">
			<label class="control-label" for="descricao_produto">Descrição</label> 
			<i class="glyphicon glyphicon-pencil form-control-feedback"></i>
			<textarea class="form-control obrigatorio" id="descricao_produto" name="descricao_produto" aviso="Descrição" placeholder="Descrição" rows="10"></textarea>
		</div>
    </div>

</div>

<hr>

<div class="row finalizar_formulario">
	<div class="col-md-9"></div>
	<div class="col-md-3" align="right">
		<button type="button" class="btn btn-danger" title="Limpar os dados dos campos" id="apagar"> <i class="glyphicon glyphicon-trash"></i> Limpar</button>
		<button type="submit" class="btn btn-success" id="validar_Enviar" title="Criar"> <i class="glyphicon glyphicon-floppy-disk"></i> Criar </button>
	</div>
</div>

<?php echo form_close(); ?>

<script type="text/javascript">
	$(document).ready(function(){
		$("#produto_recompensa").change(function(){
			if(this.checked) {
				$('#produto_recompensa_custo')
					.attr("readonly",false)
					.addClass("obrigatorio");
			} else {
				$('#produto_recompensa_custo')
					.attr("readonly",true)
					.removeClass("obrigatorio");
			}
		});
	});
</script>