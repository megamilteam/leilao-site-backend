<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Controller_produtos extends CI_Controller {

	function __construct() {

        parent::__construct();
		$this->load->model('Model_produtos');

	}

    public function upload($id_produto){

        //upload an image options
        $config = array();
        $endereco                = $_SERVER['DOCUMENT_ROOT'].base_url().'upload/produtos/'.$id_produto.'/';
        $config['upload_path']   = $endereco;
        $config['allowed_types'] = '*';
        $config['overwrite']     = TRUE;

        if (!file_exists($endereco)) {
            mkdir($endereco, 0777, true);
        }

        $this->load->library('upload');
        $files = $_FILES;
        $count = count($_FILES['imagem']['name']);

        for($i = 0; $i < $count; $i++) {   

            if($files['imagem']['name'][$i] != "") {

                $fileName = $id_produto.'_'.$i.'.png';
                echo $fileName.'<br>';

                if(file_exists($endereco.$fileName)) {

                    unlink($endereco.$fileName);

                }

                $_FILES['imagem']['name']     = $fileName;
                $_FILES['imagem']['type']     = $files['imagem']['type'][$i];
                $_FILES['imagem']['tmp_name'] = $files['imagem']['tmp_name'][$i];
                $_FILES['imagem']['error']    = $files['imagem']['error'][$i];
                $_FILES['imagem']['size']     = $files['imagem']['size'][$i];    

                // var_dump($_FILES);
                // echo '<hr>';

                $this->upload->initialize($config);
                if(!$this->upload->do_upload('imagem')){
                    echo "problema: ";
                    echo $this->upload->display_errors();     
                }

            } else {
                echo "falhou".$files['imagem']['name'][$i];
            }

        }

        //echo "count".$count;
        //die();
        
    }

    public function removerBanner() {

        $banner  = $this->input->post('banner');
        unlink($_SERVER['DOCUMENT_ROOT'].base_url().'upload/banners/'.$banner);

    }

    public function novo_banner(){

        $msg = "";
        $title = "";
        $status = '';

        $config = array();
        $endereco                = $_SERVER['DOCUMENT_ROOT'].base_url().'upload/banners/';
        $config['upload_path']   = $endereco;
        $config['allowed_types'] = '*';
        $config['overwrite']     = TRUE;

        if (!file_exists($endereco)) {
            mkdir($endereco, 0777, true);
        }

        $banners = scandir(FCPATH.'/upload/banners/', 1);
        $_FILES['imagem']['name'] = (count($banners) - 1).'.png';

        $this->load->library('upload');
        $this->upload->initialize($config);
        if(!$this->upload->do_upload('imagem')){
            $msg = $this->upload->display_errors();     
        }

        if($msg == "") {
            $msg    = 'Banner adicionado com sucesso!';
            $title  = 'Registro inserido';
            $status = 'success';
        } else {
            $title  = 'Registro não inserido';
            $status = 'error';
        }

        $this->aviso($title,$msg,$status,false);
        redirect('main/redirecionar/32');

    }

	public function novo_produto(){

        $this->form_validation->set_rules('fk_categoria','Categoria','required');
        $this->form_validation->set_rules('nome_produto','Nome Produto','required');
        $this->form_validation->set_rules('descricao_produto','Descrição Produto','required');
        
        $produto_usado      = $this->input->post('produto_usado');
        $produto_recompensa = $this->input->post('produto_recompensa');

        $dados = array (
                    'fk_categoria'              => $this->input->post('fk_categoria'),
                    'nome_produto'              => $this->input->post('nome_produto'),
                    'descricao_produto'         => $this->input->post('descricao_produto'),
                    'produto_usado'             => isset($produto_usado) ? true : false,
                    'produto_recompensa'        => isset($produto_recompensa) ? true : false,
                    'produto_recompensa_custo'  => isset($produto_recompensa) ? $this->input->post('produto_recompensa_custo') : 0,
                    'fk_usuario'                => $this->session->userdata('usuario')
                );

        if ($this->form_validation->run()) {

            $this->Model_produtos->start();
            $id = $this->Model_produtos->create($dados);
            $commit = $this->Model_produtos->commit();
            
            if ($commit['status']) {
                $this->aviso('Registro Criado','Produto criado com sucesso!','success',false);

                $this->upload($id);

                redirect('main/redirecionar/23/'.$id);
            } else {

                $this->aviso('Falha ao criar','Erro(s) ao inserir dados: "'.$commit['message'].'" <br> <a href="#" id="erro_feedback" cod="'.$commit['log_erro'].'">Clique Aqui Para Reportar</a>','error',true);

                $this->session->set_flashdata($dados);
                redirect('main/redirecionar/22');
            }

        } else {

            $this->aviso('Falha ao criar','Erro(s) no formulário: '.validation_errors(),'error',true);

            $this->session->set_flashdata($dados);
            redirect('main/redirecionar/22');

        }

    }

    public function editar_produto(){

        $this->form_validation->set_rules('fk_categoria','Categoria','required');
        $this->form_validation->set_rules('nome_produto','Nome Produto','required');
        $this->form_validation->set_rules('descricao_produto','Descrição Produto','required');

        $produto_usado      = $this->input->post('produto_usado');
        $produto_recompensa = $this->input->post('produto_recompensa');
                    
        $dados = array (
                    'id_produto'      	        => $this->input->post('id_produto'),
                    'fk_categoria'              => $this->input->post('fk_categoria'),
                    'nome_produto'              => $this->input->post('nome_produto'),
                    'descricao_produto'         => $this->input->post('descricao_produto'),
                    'produto_recompensa'        => isset($produto_recompensa) ? true : false,
                    'produto_recompensa_custo'  => isset($produto_recompensa) ? $this->input->post('produto_recompensa_custo') : 0
                );

        if ($this->form_validation->run()) {

            $this->Model_produtos->start();
            $this->Model_produtos->update($dados);
            $commit = $this->Model_produtos->commit();
            
            if ($commit['status']) {
                $this->aviso('Registro Criado','Produto editado com sucesso!','success',false);
                $this->upload($dados['id_produto']);
                redirect('main/redirecionar/23/'.$dados['id_produto']);
            } else {

                $this->aviso('Falha ao editar','Erro(s) ao inserir dados: "'.$commit['message'].'" <br> <a href="#" id="erro_feedback" cod="'.$commit['log_erro'].'">Clique Aqui Para Reportar</a>','error',true);

                $this->session->set_flashdata($dados);
                redirect('main/redirecionar/23/'.$dados['id_produto']);
            }

        } else {

            $this->aviso('Falha ao editar','Erro(s) no formulário: '.validation_errors(),'error',true);

            $this->session->set_flashdata($dados);
            redirect('main/redirecionar/23/'.$dados['id_produto']);

        }

    }

    public function entrega(){

        $cod = $this->input->post('cod');
        $dados = $this->Model_produtos->marcarEntrega($cod);

        //Enviar Push TODO
        $curl = curl_init();

        $titulo     = "Recompensa entregue!";
        $mensagem   = "O Produto '{$dados->nome_produto}' foi entregue.";
        $address = $_SERVER['SERVER_NAME'].base_url();

        curl_setopt_array($curl, array(
          CURLOPT_URL => "http://{$address}controller_notificacoes/notificando?usuarios={$dados->id_usuario}&titulo=".urlencode($titulo)."&mensagem=".urlencode($mensagem)."&id_leilao=0",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
          CURLOPT_HTTPHEADER => array("Cache-Control: no-cache"),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        $this->aviso('Registro Criado','Seu cliente já foi avisado da entrega!','success',false);


    }

    public function aviso($titulo,$aviso,$tipo,$fixo){

        //Toast apresenta erro quando existe uma quebra de linha, que ocorre com o validation_errors().
            $aviso_ = str_replace('
', '', $aviso);

        $aviso = str_replace('\'', '"', $aviso_);

        $this->session->set_flashdata('titulo_alerta',$titulo);
        $this->session->set_flashdata('mensagem_alerta',$aviso);
        $this->session->set_flashdata('tipo_alerta',$tipo);
        $this->session->set_flashdata('mensagem_fixa',$fixo);

    }

}