<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Controller_notificacao_free extends CI_Controller {

	function __construct() {

	    parent::__construct();
		    
	}

    #######################################################################################################################
    //Notificações PUSH
    public function notificando() {

            $mensagem   = $this->input->post('mensagem');
			$titulo     = $this->input->post('titulo');

            $tokens 	= $this->input->post('token');

            var_dump($tokens);
            
            $devicesToken = array ();

            $resultadoHTML = "";

            $prontos = 0; //Garante que passou por todos token

            while ($prontos < count($tokens)) {

                $devicesToken[] = $tokens[$prontos];
                    
                $prontos += 1;

                if(count($devicesToken) == 999) { //Garante que não passará o limite de envio
                    
                    $gcpm = new FCMPushMessage();
                    $gcpm->setDevices($devicesToken);

                    $response = $gcpm->send(array('mensagem' => $mensagem,'titulo' => $titulo));
                    $resultadoHTML .= "<br><hr><br>";

                    $devicesToken = array(); //zera o array, e fica pronto para mais 999 tokens

                }


            }
            
            if(count($devicesToken) > 0) { //Garante que se não entrar no if do while, enviara os tokens existentes no array.

                $gcpm = new FCMPushMessage();
                $gcpm->setDevices($devicesToken);

                $response = $gcpm->send(array('mensagem' => $mensagem,'titulo' => $titulo));

                $resultadoHTML .= "<br><hr><br>";

            }
            //Conferir envio das notificações.
            echo $resultadoHTML;      

    }


}

class FCMPushMessage {

    var $url = 'https://fcm.googleapis.com/fcm/send';
    
    var $serverApiKey = "AAAAQ-XSQSY:APA91bE5ySjbugAVsLkSL1ib8g0IIB6YlPda_PWafYoefUkYW8mHb19qwQpJ-A3wHrpX_k9kBCJpsgnx1zEWVFWPvKYwfweS4xVF62vFRK5WkiUPvpPMb1BRt80j4sEi8CEBNIdkhI02";
    var $devices = array();
    
    function setDevices($deviceIds){
    
        if(is_array($deviceIds)){
            $this->devices = $deviceIds;
        } else {
            $this->devices = array($deviceIds);
        }
    
    }
    function send($data = null){
        
        if(!is_array($this->devices) || count($this->devices) == 0){
            $this->error("No devices set");
        }
        
        if(strlen($this->serverApiKey) < 8){
            $this->error("Server API Key not set");
        }

        $notification = array
        (
            "body"      => $data['mensagem'],
            "title"     => $data['titulo'],
            "badge"     => 1,
            'vibrate'   => true,
            'sound'     => "default",
        );
        
        $fields = array(
            'registration_ids'  => $this->devices,
            'priority'          => 'high',
            'content_available' => true,
            'notification'      => $notification,
            'data'              => $data,
        );
        

        $headers = array( 
            'Authorization: key=' . $this->serverApiKey,
            'Content-Type: application/json'
        );
        $ch = curl_init();
        
        curl_setopt( $ch, CURLOPT_URL, $this->url );
        
        curl_setopt( $ch, CURLOPT_POST, true );
        curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
        
        curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode( $fields ) );
        
        curl_setopt( $ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false);
        
        $result = curl_exec($ch);
        
        curl_close($ch);
        
        return $result;
    }
    
    function error($msg){
        echo "Falha ao enviar:";
        echo "\t" . $msg;
        exit(1);
    }

}