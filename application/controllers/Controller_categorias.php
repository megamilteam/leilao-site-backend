<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Controller_categorias extends CI_Controller {

	public function __construct() {
		
		parent::__construct();
		$this->load->model('model_categorias');
	}

	public function categorias() {

		header("Content-type:application/json");

		$categorias = $this->model_categorias->view_categorias();

		print_r(json_encode($categorias));
	}

	public function criar_categoria() {

		$this->form_validation->set_rules('nome_categoria','Nome da categoria','required|is_unique[cad_categorias.nome_categoria]');

		$dados = array ('nome_categoria' => $this->input->post('nome_categoria'));

		$fk_categoria_pai = $this->input->post('fk_categoria_pai');
		if($fk_categoria_pai > 0) {
			$dados['fk_categoria_pai'] = $fk_categoria_pai;
		}

		if ($this->form_validation->run()) {

			$this->model_categorias->start();
			$id = $this->model_categorias->create($dados);
			$commit = $this->model_categorias->commit();
			
			if ($commit['status']) {
				$this->aviso('Categoria Criada','Categoria criada com sucesso!','success',false);

				redirect('main/redirecionar/15/'.$id);
			} else {

				$this->aviso('Falha ao criar','Erro(s) ao inserir dados: "'.$commit['message'].'" <br> <a href="#" id="erro_feedback" cod="'.$commit['log_erro'].'">Clique Aqui Para Reportar</a>','error',true);

				$this->session->set_flashdata($dados);
				redirect('main/redirecionar/15');
			}

		} else {

			$this->aviso('Falha ao criar','Erro(s) no formulário: '.validation_errors(),'error',true);

			$this->session->set_flashdata($dados);
			redirect('main/redirecionar/15');

		}

	}

	public function uploadImagem() {

		//upload an image options
        $config 				 = array();
        $id 					 = $this->input->post('id');
        $endereco                = $_SERVER['DOCUMENT_ROOT'].base_url().'upload/categorias/';
        $config['upload_path']   = $endereco;
        $config['allowed_types'] = '*';
        $config['overwrite']     = TRUE;

        if (!file_exists($endereco)) {
            mkdir($endereco, 0777, true);
        }

        $this->load->library('upload');
        $files = $_FILES;

        if($files['imagem']['name'] != "") {

            $fileName = 'categoria_'.$id.'.png';
            echo $fileName.'<br>';

            if(file_exists($endereco.$fileName)) {

                unlink($endereco.$fileName);

            }

            $_FILES['imagem']['name']     = $fileName;
            $_FILES['imagem']['type']     = $files['imagem']['type'];
            $_FILES['imagem']['tmp_name'] = $files['imagem']['tmp_name'];
            $_FILES['imagem']['error']    = $files['imagem']['error'];
            $_FILES['imagem']['size']     = $files['imagem']['size'];    

            // var_dump($_FILES);
            // echo '<hr>';

            $this->upload->initialize($config);
            if(!$this->upload->do_upload('imagem')){
                echo "problema: ";
                echo $this->upload->display_errors();     
            }

        } else {
            echo "falhou".$files['imagem']['name'];
        }

	}

    public function aviso($titulo,$aviso,$tipo,$fixo){

        //Toast apresenta erro quando existe uma quebra de linha, que ocorre com o validation_errors().
            $aviso_ = str_replace('
', '', $aviso);

        $aviso = str_replace('\'', '"', $aviso_);

        $this->session->set_flashdata('titulo_alerta',$titulo);
        $this->session->set_flashdata('mensagem_alerta',$aviso);
        $this->session->set_flashdata('tipo_alerta',$tipo);
        $this->session->set_flashdata('mensagem_fixa',$fixo);

    }

}

/* End of file Controller_categorias.php */
/* Location: ./application/controllers/Controller_categorias.php */