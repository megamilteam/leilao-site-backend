<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use mercadopago\sdk\lib\mercadopago;
include APPPATH . 'third_party/Frete.php';

/*

	pending :
		O usuário ainda não completou o processo de pagamento.
	approved :
		O pagamento foi aprovado e acreditado.
	in_process :
		O pagamento estão em revisão.
	in_mediation :
		Os usuários tem começada uma disputa.
	rejected :
		O pagamento foi rejeitado. O usuário pode tentar novamente.
	cancelled :
		O pagamento foi cancelado por uma das parte ou porque o tempo expirou.
	refunded :
		O pagamento foi devolvido ao usuário.
	charged_back :
		Foi feito um chargeback no cartão do comprador.

*/

class Controller_pagamentos extends CI_Controller {

	private $mp = null;

	function __construct() {

	    parent::__construct();
	    try {
	    	$this->mp = new MP ("5099980624105392", "lJw5w2FHK5IW7qzQA4UPFphjNlSHusVH");
	    } catch (Exception $e) {
	    	echo "Falha ao iniciar";
	    	die();
	    }		
		$this->mp->sandbox_mode(FALSE);
		$this->load->model('model_pacotes');
		$this->load->model('model_produtos');
		$this->load->model('model_ingressos');

		    
	}


	/*MERCADO PAGO*/

	/*
		Como resposta, terá um código único chamado access_token. Ele deverá ser utilizado para todas as operacões que realizar com as APIs, enviando como parâmetro na URL: https://api.mercadopago.com/.../?access_token=SEU_ACCESS_TOKEN

		DOC. https://www.mercadopago.com.br/developers/pt/api-docs/basics/authentication/
		Status: Funcional.
	*/
	public function getToken()
	{
		$access_token = $this->mp->get_access_token();
		print_r ($access_token);
	} 

	/*
		Create a Checkout preference
		DOC. https://www.mercadopago.com.br/developers/pt/tools/sdk/server/php/#basic-checkout
		Status: Funcional.
	*/
	public function newPreference()
	{
	    
		$id_usuario = $this->uri->segment(2);
		$id_pacote = $this->uri->segment(3);
		$version = $this->uri->segment(4);

		$pacote = $this->model_pacotes->pacote($id_pacote);
		$address = $_SERVER['SERVER_NAME'].base_url();

		$item = array (
		            "id" => $id_pacote,
		            "title" => $pacote->descricao_pacote, //Nome do pacote
		            "picture_url" => "http://{$address}style/img/pacote.jpg",
		            "description" => "Pacote {$id_pacote} com {$pacote->quantidade_ingresso} ingressos.",
		            "quantity" => 1,
		            "currency_id" => "BRL",
		            "unit_price" => round($pacote->valor_pacote,2) //Preço Pacote
		        );

		$item_banco = $item;
		$item_banco['fk_pacote'] = $id_pacote;
		$item_banco['fk_usuario'] = $id_usuario;
		$item_banco['quantidade_ingresso'] = $pacote->quantidade_ingresso;
		unset($item_banco['id']);
		unset($item_banco['picture_url']);
		unset($item_banco['description']);
		$id = $this->model_pacotes->insertCompra($item_banco);
		
		$item['id'] = $id.'_1'; //_1 = Representa compras de ingressos

		$usuario = $this->model_pacotes->dadosCliente($id_usuario);

		$payer['email'] 		 	 = $usuario->email_usuario;
		$payer['first_name'] 		 = ucwords($usuario->nome_usuario);
		$payer['registration_date']  = $usuario->criacao_usuario;
		$payer['phone']['area_code'] = substr($usuario->celular_usuario, 0, 2);
		$payer['phone']['number'] 	 = substr($usuario->celular_usuario, 2);

	    $preference_data = array ("items" => array ($item), 
	    							"payer" => $payer,
	    							"statement_descriptor" => PROJETO);

		$result = $this->mp->create_preference($preference_data);

		if($result['status'] >= 200 && $result['status'] < 300){
			
			//link_compra
			$iPod = stripos($_SERVER['HTTP_USER_AGENT'], "iPod");
			$iPhone = stripos($_SERVER['HTTP_USER_AGENT'], "iPhone");
			$iPad = stripos($_SERVER['HTTP_USER_AGENT'], "iPad");
	
			if (($iPod || $iPhone || $iPad) && $version == '1.1.2') {
				
				if(true){

					echo '<!DOCTYPE html>
							<html lang="pt-br">
							<head>
							<meta charset="utf-8">
							<meta http-equiv="X-UA-Compatible" content="IE=edge">
							<meta name="viewport" content="width=device-width, initial-scale=1">
							
							Obrigado por usar o leilão 24H, <br>
								No momento os ingressos não tem custo, acesse o Perfil -> Recompensas e escolha um pacote grátis de ingressos.';

				} else {
				
					$this->model_pacotes->link_compra($result['response']['init_point'],$id);
					redirect($result['response']['init_point']);

				}
				
			} else { 

				$this->model_pacotes->link_compra($result['response']['init_point'],$id);
				redirect($result['response']['init_point']);
				
			}

		} else {
			echo "Error on newPreference";
			print_r($result);
			print_r($item);
			print_r($pacote);
		}

	}

	/*
		Get an existent Checkout preference
		DOC. https://www.mercadopago.com.br/developers/pt/tools/sdk/server/php/#basic-checkout
		@param preference_id, String que contem a identificaçao do produto a ser exibido, ele é retornado após o newPreference.


		Status: Funcional.
	*/
	public function getPreference() 
	{
		$preference_id = $this->input->get('preference_id');

		$result = $this->mp->get_preference($preference_id);

		if($result['status'] >= 200 && $result['status'] < 300){
			//echo $result['response']['init_point'];
			print_r($result);
		} else {
			echo "Error on newPreference";
			print_r($result);
		}
	}

	/*
		Update an existent Checkout preference
		DOC. https://www.mercadopago.com.br/developers/pt/tools/sdk/server/php/#basic-checkout
		@param preference_id, String que contem a identificaçao do produto a ser exibido, ele é retornado após o newPreference.
	*/
	public function updatePreference()
	{
		$preference_id = $this->input->get('preference_id');
	   	$preference_data = array (
		    "items" => array (
		        array (
		            "title" => "Test Modified",
		            "quantity" => 1,
		            "currency_id" => "BRL",
		            "unit_price" => 20.4
		        )
		    )
		);

		$result = $this->mp->update_preference($preference_id, $preference_data);

		if($result['status'] >= 200 && $result['status'] < 300){
			echo $result['response']['init_point'];
		} else {
			echo "Error on newPreference";
			print_r($result);
		}
	}

	/*Baseado em:
		https://github.com/mercadopago/sdk-php/blob/master/examples/instant-payment-notifications/receive-ipn.php
	*/
	public function registerPayment($id = null, $topic = null, $catch = null){

		$id_get 	= $this->input->get('id');
		$topic_get 	= $this->input->get('topic');

		if(!isset($id)) 	{$id 	= isset($id_get) ? $id_get : null;}
		if(!isset($topic)) 	{$topic = isset($topic_get) ? $topic_get : null;}

		// Get the payment reported by the IPN. Glossary of attributes response in https://developers.mercadopago.com
		log_message('error',"topic ".$topic);
		//Esse ID serve para buscar detalhes no 
		//https://api.mercadopago.com/merchant_orders/ID_AQUI?access_token=TOKEN_AQUI
		log_message('error',"id ".$id);

		if(!isset($id) || $id == '') {
			log_message('error',"Cancelando registerPayment, dados nulos. {$_SERVER["REMOTE_ADDR"]}");
			exit();
		}

		$params = ["access_token" => $this->mp->get_access_token()];

		if($topic == 'payment') {
			try {
				$payment_info = $this->mp->get("/v1/payments/" . $id, $params, false);
				$merchant_order_info = $this->mp->get("/merchant_orders/" . $payment_info["response"]["order"]["id"], $params, false);
			} catch (Exception $e) {
				log_message('error',"Falha payment");
				if(!isset($catch)){
					log_message('error',"Chamada interna passando id {$id} como merchant_order");
					$this->registerPayment($id,"merchant_order",true);
				}
				http_response_code(201);
				die();
			}
		// Get the merchant_order reported by the IPN. Glossary of attributes response in https://developers.mercadopago.com	
		} else { //} if($topic == 'merchant_order'){
			try {
				$merchant_order_info = $this->mp->get("/merchant_orders/" . $id, $params, false);
			} catch (Exception $e) {
				log_message('error',"Falha merchant_order");
				if(!isset($catch)){
					log_message('error',"Chamada interna passando id {$id} como payment");
					$this->registerPayment($id,"payment",true);
				}
				http_response_code(201);
				die();
			}			
		}

		//If the payment's transaction amount is equal (or bigger) than the merchant order's amount you can release your items 
		if (isset($merchant_order_info) && $merchant_order_info["status"] == 200) {

			$id_ = $merchant_order_info["response"]["items"][0]['id'];
			$id_ = explode('_',$id_);

			if(isset($id_[1]) && $id_[1] == 2){
				$id_leilao = $id_[0];
			} else {
				$id_pacote_ingresso = $id_[0];
			}

			if(isset($id_pacote_ingresso) && !$this->model_pacotes->validaPagamento($id_pacote_ingresso)){
				log_message('error',"ID Pacote pagamento {$id_pacote_ingresso} pendente!");

				$transaction_amount_payments = 0;
				$transaction_amount_order = $merchant_order_info["response"]["total_amount"];
			    $payments = $merchant_order_info["response"]["payments"];

			    log_message('error',"total_amount ".$merchant_order_info["response"]["total_amount"]);

			    $this->model_pacotes->update_Consulta_Preference($id,$topic,$id_pacote_ingresso);

			    foreach ($payments as  $payment) {
			    	log_message('error',"Status ".$payment['status']);
			    	if($payment['status'] == 'approved'){
				    	$transaction_amount_payments += $payment['transaction_amount'];
				    }	
				    $payment['fk_compra_pacotes'] = $id_pacote_ingresso;
				    $this->model_pacotes->start();
				    $this->model_pacotes->insertHistCompra($payment);
				    $this->model_pacotes->commit();

			    }

			    if($transaction_amount_payments >= $transaction_amount_order){
			    	$pacote = $this->model_pacotes->consultarPacote($id_pacote_ingresso);
			    	log_message('error',"OK, Disponibilize seus itens - Usuário {$pacote->fk_usuario} / Pacote {$pacote->fk_pacote}");

			    	if(!$pacote->finalizado && $this->pagar_pacote($pacote->fk_usuario,$pacote->fk_pacote)){
						$this->model_pacotes->finalizarPagamento($id_pacote_ingresso);
						log_message('error',"Finalizado com succeso!");
						http_response_code(200);
						die();
			    	} else {
			    		log_message('error',"O produto seria finalizado, mas houve uma falha.");
			    	}

			    } else {
			    	$pacote = $this->model_pacotes->consultarPacote($id_pacote_ingresso);    	
			    	log_message('error',"Don't release your items - Usuário {$pacote->fk_usuario} / Pacote {$id_pacote_ingresso}");
				}

				error_log(print_r($merchant_order_info,TRUE));

			} else if(isset($id_leilao) && !$this->model_pacotes->validaPagamentoProduto($id_leilao)) {

				log_message('error',"ID leilao pagamento {$id_leilao} pendente!");

				$transaction_amount_payments = 0;
				$transaction_amount_order = $merchant_order_info["response"]["total_amount"];
			    $payments = $merchant_order_info["response"]["payments"];

			    log_message('error',"total_amount ".$merchant_order_info["response"]["total_amount"]);

			    $this->model_pacotes->update_Consulta_Preference_leilao($id,$topic,$id_leilao);

			    foreach ($payments as  $payment) {
			    	log_message('error',"Status ".$payment['status']);
			    	if($payment['status'] == 'approved'){
				    	$transaction_amount_payments += $payment['transaction_amount'];
					}	
					
			    }

			    if($transaction_amount_payments >= 1){
					
					log_message('error',"OK, Disponibilize seus itens - Leilão {$id_leilao}");
					$this->model_pacotes->finalizarLeilao($id_leilao);
					log_message('error',"Finalizado com succeso!");
					http_response_code(200);
					die();

			    } else {
			    	log_message('error',"Don't release your items - Leilão {$id_leilao}");
				}

				error_log(print_r($merchant_order_info,TRUE));

			} else {

				log_message('error',"ID pagamento {$id_[0]}");
				http_response_code(201);
				die();

			}

		} else {
			
			log_message('error',"status falha");
			http_response_code(201);
			die();

		}

		http_response_code(200);
		die();

	}

	//Efetuar a compra do ingresso, Mesma funçao do Samuel
	public function pagar_pacote($id_usuario,$id_pacote_ingresso) {

		$pacote = $this->model_ingressos->getPacote($id_pacote_ingresso);

		$cad_ingressos = array(
			'fk_usuario' => $id_usuario,
			'quantidade' => $pacote['quantidade_ingresso'],
			'gratis' => $pacote['gratis']
		);

		$compra_ingresso = array(
			'valor' => $pacote['valor_pacote'],
			'fk_pacote' => $pacote['id_pacote_ingresso'],
		);

		$this->model_ingressos->start();

		$id_ingresso = $this->model_ingressos->insertIngresso($cad_ingressos);

		$compra_ingresso['fk_ingresso'] = $id_ingresso;

		$id_comprar = $this->model_ingressos->insertCompra($compra_ingresso);

		$update = array(
			'id_ingresso' => $id_ingresso,
			'ativo' => true
		);

		$this->model_ingressos->updateIngresso($update);

		$commit = $this->model_ingressos->commit();

		return $commit['status'];

	
	}

	//Todo Controlar repetiçoes, buscar dos pedidos pendentes
	//Usado por Cronjob, porém também caso o pagseguro erre e mande Paymente será tentado novamente
	//Cron JOB
	public function forcarMultiploEnvio(){

		$id_usuario = $this->input->get('id_usuario');
		$ingressosPendentes = $this->model_pacotes->validarPendentes($id_usuario);
		$produtosPendentes = $this->model_pacotes->validarPendentesProdutos($id_usuario);

		foreach ($ingressosPendentes as $key => $pendente) {
			
			log_message('error', "forcarMultiploEnvio -> Enviando Ingresso ID {$pendente->id} e Topic {$pendente->topic}");
			$this->registerPayment($pendente->id,$pendente->topic);

		}

		foreach ($produtosPendentes as $key => $pendente) {
			
			log_message('error', "forcarMultiploEnvio -> Enviando Produto ID {$pendente->id} e Topic {$pendente->topic}");
			$this->registerPayment($pendente->id,$pendente->topic);

		}


	}

	public function calculoFrete($servico = null, $cepOrigem = null, $cepDestino = null, $produtos = null){

		try {

			$frete = new Frete($servico, $cepOrigem, $cepDestino, $produtos);
			$freteTotal = $frete->calcular();

			return array("status" => 1, "valor" => number_format($freteTotal, 2, '.', ''));

		} catch (Exception $e) {

			return array("status" => 0, "mensagem" =>  $e->getMessage());

		}		
	
	}

	/*
		Usando campo para frete
		https://www.mercadopago.com.br/devsite-legacy/pt/api-docs/basic-checkout/preferences/
	 */
	public function newPreferenceProduct()
	{
	    
		$id_usuario = $this->uri->segment(3);
		$id_leilao = $this->uri->segment(4);

		$produto = $this->model_produtos->getProduto($id_leilao);

		//if($produto->link_compra != '') {
		if(false) { //FIXME, links incorretos reportados.

			redirect($produto->link_compra);

		} else {

			$address = $_SERVER['SERVER_NAME'].base_url();

			$item = array (
				"id" => $id_leilao.'_2', //_2 Representa um leilão
				"title" => $produto->nome_produto, //Nome do produto
				"picture_url" => "http://{$address}upload/produtos/{$produto->id_produto}/{$produto->id_produto}_0.png",
				"description" => $produto->descricao_produto,
				"quantity" => 1,
				"currency_id" => "BRL",
				"unit_price" => round($produto->valor,2) //Preço Produto
			);


			$usuario = $this->model_produtos->dadosCliente($id_usuario);

			$payer['email'] 		 	 = $usuario->email_usuario;
			$payer['first_name'] 		 = ucwords($usuario->nome_usuario);
			$payer['registration_date']  = $usuario->criacao_usuario;
			$payer['phone']['area_code'] = substr($usuario->celular_usuario, 0, 2);
			$payer['phone']['number'] 	 = substr($usuario->celular_usuario, 2);
			//testar
			$servico = "40010";
			$cepOrigem = $produto->cep_usuario;
			$cepDestino = $usuario->cep_usuario;
			$produtos[0]['qtd'] = 1;
			$produtos[0]['peso'] = $produto->peso;
			$produtos[0]['altura'] = $produto->altura;
			$produtos[0]['largura'] = $produto->largura;
			$produtos[0]['comprimento'] = $produto->comprimento;

			if(floatval($produto->peso) > 0){

				$cost = $this->calculoFrete($servico, $cepOrigem, $cepDestino, $produtos);

				if($cost['status']){
					$cost = floatval($cost['valor']);
				} else {
					//interromper fluxo
					echo $cost['mensagem'];
					die();
				}

			} else {
				$cost = 0;
			}

			$shipments = array(
				// "mode" => "me2",
				// "local_pickup" => false,
				// "dimensions" => "30x30x30,500",
				// "default_shipping_method " => 73328,
				// "free_methods" => array(
				// 	"id" => 73328
				// ),
				// "free_shipping" => false,
				// "receiver_address" => array(
				// 	"zip_code" => "07152390",
				// 	"street_number" => 25,
				// 	"street_name" => "Rua maria eliza silva"
				// ) 
				"cost" => $cost
			);

			$preference_data = array ("items" => array ($item), 
										"payer" => $payer,
										"shipments" => $shipments,
										"statement_descriptor" => PROJETO);

			$result = $this->mp->create_preference($preference_data);

			if($result['status'] >= 200 && $result['status'] < 300){
					
				$this->model_produtos->link_compra($result['response']['init_point'],$id_leilao);
				redirect($result['response']['init_point']);

			} else {
				echo "Error on newPreference";
				print_r($result);
				print_r($item);
				print_r($pacote);
			}

		}

		

	}

}