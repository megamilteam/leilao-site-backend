<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Controller_leilao extends CI_Controller {

	function __construct() {

        parent::__construct();
        $this->load->model('model_leiloes');
        $this->load->model('model_webservice');

	}

    public function novo_leilao(){

        $this->form_validation->set_rules('fk_produto','Produto','required');
        $this->form_validation->set_rules('valor_minimo','Valor mínimo','required');
        $this->form_validation->set_rules('lance_minimo','Lance mínimo','required');
        $this->form_validation->set_rules('ingressos','Ingressos','required');
        $this->form_validation->set_rules('tempo_iniciar','Tempo para Início','required');
        $this->form_validation->set_rules('periodo','Período','required');

        $this->form_validation->set_rules('altura','Altura (cm)','required');
        $this->form_validation->set_rules('largura','Largura (cm)','required');
        $this->form_validation->set_rules('comprimento','Comprimento (cm)','required');
        $this->form_validation->set_rules('peso','Peso (kg)','required');

        $datas = explode(' até ',$this->input->post('periodo'));

        if($this->input->post('ingressos') == 1 || $this->input->post('valor_inicial_lance') > $this->input->post('valor_minimo')) {
            $valor_inicial_lance = $this->input->post('valor_minimo');
        } else {
            $valor_inicial_lance = $this->input->post('valor_inicial_lance');
        }

        $dados = array (
                    'fk_produto'                    => $this->input->post('fk_produto'),
                    'valor_minimo'                  => $this->input->post('valor_minimo'),
                    'valor_inicial_lance'        => $valor_inicial_lance,
                    'lance_minimo'                  => $this->input->post('lance_minimo'),
                    'ingressos'                     => $this->input->post('ingressos'),
                    'tempo_iniciar'                 => $this->input->post('tempo_iniciar'),

                    'altura'                        => str_replace(',','.',$this->input->post('altura')),
                    'largura'                       => str_replace(',','.',$this->input->post('largura')),
                    'comprimento'                   => str_replace(',','.',$this->input->post('comprimento')),
                    'peso'                          => str_replace(',','.',$this->input->post('peso')),

                    'quantidade_minima_usuarios'    => $this->input->post('quantidade_minima_usuarios'),
                    'data_inicio'                   => $this->data($datas[0],true),
                    'data_fim_previsto'             => $this->data($datas[1],true),
                    'status_leilao'                 => 1
                );

        $dados['tempo_iniciar'] = $dados['tempo_iniciar']  * 60;

        if ($this->form_validation->run()) {

            $this->model_leiloes->start();
            $id = $this->model_leiloes->insert($dados);
            $commit = $this->model_leiloes->commit();

            if ($commit['status']) {
                $this->aviso('Registro Criado','Leilão criado com sucesso!','success',false);

                redirect('main/redirecionar/20');
            } else {

                $this->aviso('Falha ao criar','Erro(s) ao inserir dados: "'.$commit['message'].'" <br> <a href="#" id="erro_feedback" cod="'.$commit['log_erro'].'">Clique Aqui Para Reportar</a>','error',true);

                $this->session->set_flashdata($dados);
                redirect('main/redirecionar/20');
            }

        } else {

            $this->aviso('Falha ao criar','Erro(s) no formulário: '.validation_errors(),'error',true);

            $this->session->set_flashdata($dados);
            redirect('main/redirecionar/20');

        }

    }

    public function editar_leilao(){

        $this->form_validation->set_rules('fk_produto','Produto','required');
        $this->form_validation->set_rules('valor_minimo','Valor mínimo','required');
        $this->form_validation->set_rules('lance_minimo','Lance mínimo','required');
        $this->form_validation->set_rules('ingressos','Ingressos','required');
        $this->form_validation->set_rules('tempo_iniciar','Tempo para Início','required');
        $this->form_validation->set_rules('periodo','Período','required');

        $this->form_validation->set_rules('altura','Altura (cm)','required');
        $this->form_validation->set_rules('largura','Largura (cm)','required');
        $this->form_validation->set_rules('comprimento','Comprimento (cm)','required');
        $this->form_validation->set_rules('peso','Peso (kg)','required');

        $datas = explode(' até ',$this->input->post('periodo'));

        if($this->input->post('ingressos') == 1 || $this->input->post('valor_inicial_lance') > $this->input->post('valor_minimo')) {
            $valor_inicial_lance = $this->input->post('valor_minimo');
        } else {
            $valor_inicial_lance = $this->input->post('valor_inicial_lance');
        }

        $dados = array (
                    'id_leilao'                     => $this->input->post('id_leilao'),
                    'fk_produto'                    => $this->input->post('fk_produto'),
                    'valor_minimo'                  => $this->input->post('valor_minimo'),
                    'valor_inicial_lance'        => $valor_inicial_lance,
                    'lance_minimo'                  => $this->input->post('lance_minimo'),
                    'tempo_iniciar'                 => $this->input->post('tempo_iniciar'),
                    'ingressos'                     => $this->input->post('ingressos'),
                    'quantidade_minima_usuarios'    => $this->input->post('quantidade_minima_usuarios'),

                    'altura'                        => str_replace(',','.',$this->input->post('altura')),
                    'largura'                       => str_replace(',','.',$this->input->post('largura')),
                    'comprimento'                   => str_replace(',','.',$this->input->post('comprimento')),
                    'peso'                          => str_replace(',','.',$this->input->post('peso')),

                    'data_inicio'                   => $this->data($datas[0],true),
                    'data_fim_previsto'             => $this->data($datas[1],true),
                    'status_leilao'                 => 1
                );

        if ($this->form_validation->run()) {

            $this->model_leiloes->start();
            $this->model_leiloes->update($dados);
            $commit = $this->model_leiloes->commit();

            if ($commit['status']) {
                $this->aviso('Registro Criado','Leilão editado com sucesso!','success',false);

                redirect('main/redirecionar/28/'.$dados['id_leilao']);
            } else {

                $this->aviso('Falha ao Editar','Erro(s) ao editar dados: "'.$commit['message'].'" <br> <a href="#" id="erro_feedback" cod="'.$commit['log_erro'].'">Clique Aqui Para Reportar</a>','error',true);

                $this->session->set_flashdata($dados);
                redirect('main/redirecionar/28/'.$dados['id_leilao']);
            }

        } else {

            $this->aviso('Falha ao editar','Erro(s) no formulário: '.validation_errors(),'error',true);

            $this->session->set_flashdata($dados);
            redirect('main/redirecionar/28/'.$dados['id_leilao']);

        }

    }

    ##################################################################
    /**
     *  Formata a data para o banco de dados
     *  @param $data        recebe a data que deseja converter
     *  @param $timestamp   recebe um booleano, TRUE caso seja timestamp.
     */

    public function data($data = null, $timestamp = null) {

        if (isset($data) && $data != "" && $timestamp) {

            return date("Y-m-d H:i:s", strtotime(str_replace('/', '-', $data)));
        } else if (isset($data) && $data != "" && !$timestamp) {

            return date("Y-m-d", strtotime(str_replace('/', '-', $data)));
        } else {

            return 0;
        }
    }

    public function form_leiloes(){

        $qtd = $this->input->post('qtd_leiloes');

        for ($i=0; $i < $qtd; $i++) {

            $aceite = explode('-', $this->input->post('aceitar'.$i));
            if(count($aceite) == 2) {
                $this->model_leiloes->acetarRejeitar($aceite[0],$aceite[1]);
            }

        }


        if($qtd < 2) {
            $this->aviso('Sucesso',$qtd.' Leilão aprovado / rejeitado com sucesso','success',false);
        } else {
            $this->aviso('Sucesso',$qtd.' Leilões aprovados / rejeitados com sucesso','success',false);
        }

        redirect('main/redirecionar/16');

    }

    public function aviso($titulo,$aviso,$tipo,$fixo){

        //Toast apresenta erro quando existe uma quebra de linha, que ocorre com o validation_errors().
            $aviso_ = str_replace('
', '', $aviso);

        $aviso = str_replace('\'', '"', $aviso_);

        $this->session->set_flashdata('titulo_alerta',$titulo);
        $this->session->set_flashdata('mensagem_alerta',$aviso);
        $this->session->set_flashdata('tipo_alerta',$tipo);
        $this->session->set_flashdata('mensagem_fixa',$fixo);

    }

    public function marcarPago()  {
        $this->model_leiloes->validandoPagamento($this->input->post('id_leilao'));
    }

    ######################################################
    //Avisando quem arrematou
    ######################################################
    public function arrematados() {

        $arrematados = $this->model_leiloes->getArrematados();

        $address = $_SERVER['SERVER_NAME'].base_url();

        foreach($arrematados as $arrematado){

            $titulo = "Parabéns!";
            $mensagem = "Você arrematou o produto '{$arrematado['nome_produto']}' pelo valor de: R$ {$arrematado['valor']}.";

            $curl = curl_init();

            curl_setopt_array($curl,
                array(
                    CURLOPT_URL => "http://{$address}controller_notificacoes/notificando",
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => "",
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 30,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => "POST",
                    CURLOPT_POSTFIELDS => "usuarios={$arrematado['id_usuario']}&titulo={$titulo}&mensagem={$mensagem}",
                    CURLOPT_HTTPHEADER => array(
                      "Content-Type: application/x-www-form-urlencoded",
                      "cache-control: no-cache"
                    ),
                  ));

            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);

            $link = 'http://'.$address.'controller_pagamentos/newPreferenceProduct/'.$arrematado['id_usuario'].'/'.$arrematado['id_leilao'].'"';

            $emailCorpo = '
            <p>'.$titulo.'</p>
            <p>'.$mensagem.'</p>
            <table role="presentation" border="0" cellpadding="0" cellspacing="0" class="btn btn-primary">
              <tbody>
                <tr>
                  <td align="left">
                    <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                      <tbody>
                        <tr>
                          <td> <a href="'.$link.'" target="_blank">Pagar</a> </td>
                        </tr>
                      </tbody>
                    </table>
                  </td>
                </tr>
              </tbody>
            </table>';

            $this->bodyEmail('Produto Arrematado!',$emailCorpo,$arrematado['email_usuario'],$arrematado['nome_usuario']);

        }

    }

    public function bodyEmail($titulo,$corpo,$email,$nomeUsuario) {

        $address = $_SERVER['SERVER_NAME'].base_url();

        $html = '
        <!doctype html>
        <html>
          <head>
            <meta name="viewport" content="width=device-width" />
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
            <title>'.$titulo.'</title>
            <style>
              /* ------------------------------------- GLOBAL RESETS ------------------------------------- */ /*All the styling goes here*/ img{border: none; -ms-interpolation-mode: bicubic; max-width: 100%;}body{background-color: #f6f6f6; font-family: sans-serif; -webkit-font-smoothing: antialiased; font-size: 14px; line-height: 1.4; margin: 0; padding: 15; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;}table{border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;}table td{font-family: sans-serif; font-size: 14px; vertical-align: top;}/* ------------------------------------- BODY & CONTAINER ------------------------------------- */ .body{background-color: #f6f6f6; width: 100%;}/* Set a max-width, and make it display as block so it will automatically stretch to that width, but will also shrink down on a phone or something */ .container{display: block; margin: 0 auto !important; /* makes it centered */ max-width: 580px; padding: 10px; width: 580px;}/* This should also be a block element, so that it will fill 100% of the .container */ .content{box-sizing: border-box; display: block; margin: 0 auto; max-width: 580px; padding: 10px;}/* ------------------------------------- HEADER, FOOTER, MAIN ------------------------------------- */ .main{background: #ffffff; border-radius: 3px; width: 100%;}.wrapper{box-sizing: border-box; padding: 20px;}.content-block{padding-bottom: 10px; padding-top: 10px;}.footer{clear: both; margin-top: 10px; text-align: center; width: 100%;}.footer td, .footer p, .footer span, .footer a{color: #999999; font-size: 12px; text-align: center;}/* ------------------------------------- TYPOGRAPHY ------------------------------------- */ h1, h2, h3, h4{color: #000000; font-family: sans-serif; font-weight: 400; line-height: 1.4; margin: 0; margin-bottom: 30px;}h1{font-size: 35px; font-weight: 300; text-align: center; text-transform: capitalize;}p, ul, ol{font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; margin-bottom: 15px;}p li, ul li, ol li{list-style-position: inside; margin-left: 5px;}a{color: #3498db; text-decoration: underline;}/* ------------------------------------- BUTTONS ------------------------------------- */ .btn{box-sizing: border-box; width: 100%;}.btn > tbody > tr > td{padding-bottom: 15px;}.btn table{width: auto;}.btn table td{background-color: #ffffff; border-radius: 5px; text-align: center;}.btn a{background-color: #ffffff; border: solid 1px #3498db; border-radius: 5px; box-sizing: border-box; color: #3498db; cursor: pointer; display: inline-block; font-size: 14px; font-weight: bold; margin: 0; padding: 12px 25px; text-decoration: none; text-transform: capitalize;}.btn-primary table td{background-color: #3498db;}.btn-primary a{background-color: #27ae60; border-color: #27ae60; color: #ffffff;}/*------------------------------------- OTHER STYLES THAT MIGHT BE USEFUL ------------------------------------- */ .last{margin-bottom: 0;}.first{margin-top: 0;}.align-center{text-align: center;}.align-right{text-align: right;}.align-left{text-align: left;}.clear{clear: both;}.mt0{margin-top: 0;}.mb0{margin-bottom: 0;}.preheader{color: transparent; display: none; height: 0; max-height: 0; max-width: 0; opacity: 0; overflow: hidden; mso-hide: all; visibility: hidden; width: 0;}.powered-by a{text-decoration: none;}hr{border: 0; border-bottom: 1px solid #f6f6f6; margin: 20px 0;}/* ------------------------------------- RESPONSIVE AND MOBILE FRIENDLY STYLES ------------------------------------- */ table[class=body] h1{font-size: 28px !important; margin-bottom: 10px !important;}table[class=body] p, table[class=body] ul, table[class=body] ol, table[class=body] td, table[class=body] span, table[class=body] a{font-size: 16px !important;}table[class=body] .wrapper, table[class=body] .article{padding: 10px !important;}table[class=body] .content{padding: 0 !important;}table[class=body] .container{padding: 0 !important; width: 100% !important;}table[class=body] .main{border-left-width: 0 !important; border-radius: 0 !important; border-right-width: 0 !important;}table[class=body] .btn table{width: 100% !important;}table[class=body] .btn a{width: 100% !important;}table[class=body] .img-responsive{height: auto !important; max-width: 100% !important; width: auto !important;}/* ------------------------------------- PRESERVE THESE STYLES IN THE HEAD ------------------------------------- */ @media all{.ExternalClass{width: 100%;}.ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div{line-height: 100%;}.apple-link a{color: inherit !important; font-family: inherit !important; font-size: inherit !important; font-weight: inherit !important; line-height: inherit !important; text-decoration: none !important;}.btn-primary table td:hover{background-color: #34495e !important;}.btn-primary a:hover{background-color: #27ae40 !important; border-color: #27ae50 !important;}}
            </style>
          </head>
          <body width="100%">
            <table role="presentation" border="0" cellpadding="0" cellspacing="0" class="body" width="100%">
              <tr>
                <td>&nbsp;</td>
                <td class="container">
                  <div class="content">
                        <tr>
                            <td width="100%" align="center">
                                <img src="http://'.$address.'style/img/icon.png" width="100px" height="100px" style="border-radius: 10px;">
                            </td>
                        </tr>
        
                      <!-- START MAIN CONTENT AREA -->
                      <tr>
                        <td class="wrapper">
                          <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                            <tr>
                              <td>
                                <p>Olá '.$nomeUsuario.'.</p>
                                '.$corpo.'
                                <p>Esse e-mail é automático, qualquer dúvida entre em contato com nosso suporte <a href="mailto:sacleilao24h@gmail.com">sacleilao24h@gmail.com</a>.</p>
                                <p>Obrigado por utilizar nosso sistema!.</p>
                              </td>
                            </tr>
                          </table>
                        </td>
                      </tr>
        
                    <!-- END MAIN CONTENT AREA -->
                    </table>
        
                    <!-- START FOOTER -->
                    <div class="footer">
                      <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                          <td class="content-block" colspan="2">
                            <span class="apple-link"><a href="http://'.$address.'" target="_blank">Leilão 24H</a></span>
                          </td>
                        </tr>
                        <tr>
                          <td class="content-block powered-by" colspan="2">
                            Powered by <a href="http://megamil.net">Megamil.net</a>.
                          </td>
                        </tr>
                        <tr align="left">
                          <td class="content-block powered-by" colspan="2" align="left">
                            <small>Em: '.date('d/m/Y H:i:s').'</small> <br>
                            <br>
                            <a href="http://'.$address.'privacidade">Política de privacidade</a> <br> 
                            <a href="http://'.$address.'">Termos de uso</a>
                          </td>
                        </tr>
        
                      </table>
                    </div>
                    <!-- END FOOTER -->
        
                  <!-- END CENTERED WHITE CONTAINER -->
                  </div>
                </td>
                <td>&nbsp;</td>
              </tr>
            </table>
          </body>
        </html>';

        // Detalhes do Email.
        $this->email->from('no-reply@leilao24h.com.br', 'Leilão 24H')
                    ->to($email)
                    ->subject($titulo)
                    ->message($html);

        // Enviar...
        if ($this->email->send()) {

            return true;

        } else {

            return false;

        }

    }

    ######################################################
    //Notificações PUSH
    ######################################################
    public function notificando() {

        $this->model_leiloes->start();

        $tokens = $this->model_leiloes->cron_Push();

        $devicesToken = array();

        $resultadoHTML = "";
        $mensagem = "";
        $titulo = "";

        $prontos            = 0; //Garante que passou por todos token
        $ultimo_leilao      = 0; //Faz com que não seja atualizado toda hora o mesmo leilão.
        $tipo_notificacao   = 0; //Possibilita um envio diferênte para as notificações

        while ($prontos < $tokens->num_rows()) {

            echo "Enviar para {$tokens->num_rows()} <br>";

            if($tipo_notificacao == 0){

                $tipo_notificacao = $tokens->row($prontos)->tipo;
                echo "Tipo atualizado para {$tipo_notificacao}";
                //$ultimo_leilao == $tokens->row($prontos)->id
                $mensagem   = $tokens->row($prontos)->descricao;
                $titulo     = $tokens->row($prontos)->titulo;

            } else if($tipo_notificacao != $tokens->row($prontos)->tipo) { //Garante caso seja uma nova notificação já envia as anteriores


                echo "Tipo atualizado para {$tipo_notificacao} <br>";

                $tipo_notificacao = $tokens->row($prontos)->tipo;
                $gcpm = new FCMPushMessage();
                $gcpm->setDevices($devicesToken);

                $response = $gcpm->send(array('mensagem' => $mensagem,'titulo' => $titulo,'id_leilao' => $ultimo_leilao));
                //Atualizar token ou remover, caso necessário.
                $resultadoHTML .= $this->atualizarBD($response, $devicesToken);

                $resultadoHTML .= "<br><hr><br>";

                $devicesToken = array(); //zera o array, e fica pronto para mais 999 tokens
                $mensagem   = $tokens->row($prontos)->descricao;
                $titulo     = $tokens->row($prontos)->titulo;

            }


            $devicesToken[] = $tokens->row($prontos)->token_usuario;
            $prontos += 1;

            if($ultimo_leilao != $tokens->row($prontos)->id && $tipo_notificacao < 4) { //4 é igual o 3, porém só para o leiloeiro.
                $ultimo_leilao = $tokens->row($prontos)->id;
                echo "Leilão {$ultimo_leilao} notificado <br>";
                $this->model_leiloes->notificado($ultimo_leilao);
            }

            if(count($devicesToken) == 999) { //Garante que não passará o limite de envio ou seja uma nova notificação
                $gcpm = new FCMPushMessage();
                $gcpm->setDevices($devicesToken);
                echo "Enviando 999 Pushs <br>";
                $response = $gcpm->send(array('mensagem' => $mensagem,'titulo' => $titulo,'id_leilao' => $ultimo_leilao));
                //Atualizar token ou remover, caso necessário.
                $resultadoHTML .= $this->atualizarBD($response, $devicesToken);

                $resultadoHTML .= "<br><hr><br>";

                $devicesToken = array(); //zera o array, e fica pronto para mais 999 tokens

            }

        }

        if(count($devicesToken) > 0) { //Garante que se não entrar no if do while, enviara os tokens existentes no array.
            $gcpm = new FCMPushMessage();
            $gcpm->setDevices($devicesToken);
            echo "Enviando ".count($devicesToken)." Pushs <br>";
            $response = $gcpm->send(array('mensagem' => $mensagem,'titulo' => $titulo,'id_leilao' => $ultimo_leilao));

            $resultadoHTML .= $this->atualizarBD($response, $devicesToken);

            $resultadoHTML .= "<br><hr><br>";

        }

        $this->model_leiloes->commit();
        //Conferir envio das notificações.
       // echo $resultadoHTML;
        //echo $tokens->row(0)->token;
        //$this->response(array('status' => STATUS_OK, 'resultado' => "Nova publicação enviada com sucesso! notificado a {$prontos} aparelhos. {$titulo} / {$mensagem}"),Self::HTTP_OK); //200

    }



    public function atualizarBD($response = null, $devicesToken = null) {
        // RESULT JSON
        $html = '';
        $resultJson = json_decode($response);
        foreach($resultJson as $key=>$value){
            if(is_array($value)){
                $html .= $key.'=>{<br/>';
                $i = 0;

                foreach($value as $k=>$v){
                    $html .= '&nbsp;&nbsp;&nbsp;&nbsp;{&nbsp;';
                    foreach($v as $kObj=>$vObj){
                        $html .= $kObj.'=>'.$vObj;

                        // UPDATE REG ID
                            if(strcasecmp($kObj, 'registration_id') == 0 && strlen( trim($vObj) ) > 0){

                                $novo = trim($vObj);

                                if($this->model_webservice->atualizarToken($novo,$devicesToken[$i])) {
                                $html .= " Token : {$devicesToken[$i]} Atualizado com sucesso para: {$novo}";
                                } else {
                                    $html .= " Falha na atualização.";
                                }

                            }
                        // DELETE REG ID
                            else if(strcasecmp($kObj, 'error') == 0 && strcasecmp($vObj, 'NotRegistered') == 0){

                                if($this->model_webservice->deletarToken($devicesToken[$i])) {
                                    $html .= " Token: {$devicesToken[$i]} Deletado com sucesso. NotRegistered";
                                } else {
                                    $html .= " Token: {$devicesToken[$i]} Falhou ao deletar. NotRegistered";
                                }

                            }
                            else if(strcasecmp($kObj, 'error') == 0 && strcasecmp($vObj, 'MismatchSenderId') == 0){

                                if($this->model_webservice->deletarToken($devicesToken[$i])) {
                                    $html .= " Token: {$devicesToken[$i]} Deletado com sucesso. MismatchSenderId";
                                } else {
                                    $html .= " Token: {$devicesToken[$i]} Falhou ao deletar. MismatchSenderId";
                                }

                            } else if(strcasecmp($kObj, 'error') == 0 && strcasecmp($vObj, 'InvalidRegistration') == 0){

                                if($this->model_webservice->deletarToken($devicesToken[$i])) {
                                    $html .= " Token: {$devicesToken[$i]} Deletado com sucesso. InvalidRegistration";
                                } else {
                                    $html .= " Token: {$devicesToken[$i]} Falhou ao deletar. InvalidRegistration";
                                }

                            } else {

                                $html .= " Token {$devicesToken[$i]} OK";

                            }

                        $html .= '<br />';
                    }
                    $html = rtrim($html, '<br />');
                    $html .= '&nbsp;}<br />';
                    $i++;
                }
                $html .= '}<br />';
            }
            else{
                $html .= $key.'=>'.$value.'<br />';
            }
        }

        return $html; // PRINT RESULT

    }

    public function adicionar_Token(){

        $token['token'] = $this->input->get('token');
        $token['fk_usuario'] = $this->input->get('fk_usuario');
        $token['aparelho'] = $this->input->get('aparelho');

        if(!$this->model_webservice->novo_Token($token)){

            $this->response(array('status' => STATUS_FALHA, 'resultado' => "Sucesso"),Self::HTTP_OK); //400

        } else {

            $this->response(array('status' => STATUS_OK, 'resultado' => "Sucesso"),Self::HTTP_OK); //400

        }


    }

} // FIM Controller_webservices

class FCMPushMessage {

    var $url = 'https://fcm.googleapis.com/fcm/send';
    //Chave do Firebase
    var $serverApiKey  = "AAAAkxW33YQ:APA91bHNtnCUSCPRwjvu7bEAwBMw1hRG3DoOLU_stTNkSOBLISiarcSEshaEhH_uEb4n19i7q-vzMjsAdZq-cbn7yYKlBTi9gXeUaMUY7VqMHcUsT5bpYXh1YOxBsP3FWDlibl4A-HB5";
    var $devices = array();

    function setDevices($deviceIds){

        if(is_array($deviceIds)){
            $this->devices = $deviceIds;
        } else {
            $this->devices = array($deviceIds);
        }

    }

    function send($data = null)
    {

        if(!is_array($this->devices) || count($this->devices) == 0){
            $this->error("Nenhum Aparelho informado");
        }

        if(strlen($this->serverApiKey) < 8){
            $this->error("API Key não informada");
        }

        $notificacao = array
        (
            "body"      => $data['mensagem'],
            "title"     => $data['titulo'],
            "id_leilao" => $data['id_leilao'],
            "badge"     => 1,
            'vibrate'   => 1,
            'sound'     => 1
        );

        $campos = array(
            'registration_ids'  => $this->devices,
            'content_available' => true,
            'notification'      => $notificacao,
            'priority'          => 'high',
            'data'              => $data
        );


        $headers = array(
            'Authorization: key=' . $this->serverApiKey,
            'Content-Type: application/json'
        );

        $ch = curl_init();

        curl_setopt( $ch, CURLOPT_URL, $this->url );

        curl_setopt( $ch, CURLOPT_POST, true );
        curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );

        curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode( $campos ) );

        curl_setopt( $ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false);

        $result = curl_exec($ch);

        curl_close($ch);

        return $result;
    }

    function error($msg){
        echo "Falha ao enviar:";
        echo "\t" . $msg;
        exit(1);
    }


    public function atualizar_leilao(){
      if($this->data($datas[1],true))
        $dados = array (
            'data_inicio'                   => $this->data($datas[0],true),
            'data_fim_previsto'             => $this->data($datas[1],true),
            'status_leilao'                 => 3
        );

        if ($this->form_validation->run()) {

            $this->model_leiloes->start();
            $this->model_leiloes->update($dados);
            $commit = $this->model_leiloes->commit();

            if ($commit['status']) {

            } else {

            }

        } else {
        }
    }


    public function leilao_status() {

        $this->model_leiloes->start();
        $this->model_leiloes->getStatus_leilao();
        $commit = $this->model_leiloes->commit();

        if ($commit['status']) {
            $this->response(array('status' => STATUS_OK, 'resultado' => "Sucesso"),Self::HTTP_OK); //200
        } else {
            $this->response(array('status' => STATUS_FALHA, 'resultado' => "error"),Self::HTTP_OK); //200
        }
    }

    public function atualizar_leilao_status_dias() {

        $this->model_leiloes->start();
        $this->model_leiloes->getStatus_leilao_dias();
        $commit = $this->model_leiloes->commit();

        if ($commit['status']) {
            $this->response(array('status' => STATUS_OK, 'resultado' => "Sucesso"),Self::HTTP_OK); //200
        } else {
            $this->response(array('status' => STATUS_FALHA, 'resultado' => "error"),Self::HTTP_OK); //200
        }
    }


}


/* End of file Controller_leilao.php */
/* Location: ./application/controllers/Controller_leilao.php */


?>
