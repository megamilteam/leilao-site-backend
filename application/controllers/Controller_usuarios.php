<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Controller_usuarios extends CI_Controller {

	function __construct() {

	    parent::__construct();
	    $this->load->model('model_usuarios');
        $this->load->model('model_configuracao');
        $this->load->model('model_ingressos');
        $this->load->model('model_leiloes');

    }

	public function criar_usuario(){

		$this->form_validation->set_rules('nome_usuario','Nome do Usuário','required');
		$this->form_validation->set_rules('email_usuario','E-mail do Usuário','required|is_unique[seg_usuarios.email_usuario]');
		$this->form_validation->set_rules('telefone_usuario','Telefone do Usuário','required');
		$this->form_validation->set_rules('login_usuario','Login do Usuário','required|is_unique[seg_usuarios.login_usuario]');
		$this->form_validation->set_rules('senha_usuario','Senha do Usuário','required');
		$this->form_validation->set_rules('ativo_usuario','Status do Usuário','required');
		$this->form_validation->set_rules('fk_grupo_usuario','Grupo do Usuário','required');
		$this->form_validation->set_rules('cep_usuario','CEP do Usuário','required');
		$this->form_validation->set_rules('logradouro_usuario','Logradouro do Usuário','required');
		$this->form_validation->set_rules('bairro_usuario','Bairro do Usuário','required');
		$this->form_validation->set_rules('cidade_usuario','Cidade do Usuário','required');
		$this->form_validation->set_rules('estado_usuario','Estado do Usuário','required');
		$this->form_validation->set_rules('numero_usuario','Número do Usuário','required');

		$dados = array (
					'usuario_criou_usuario' => $this->session->userdata('usuario'),
					'nome_usuario' 					=> $this->input->post('nome_usuario'),
					'email_usuario' 				=> $this->input->post('email_usuario'),
					'telefone_usuario'			=> $this->input->post('telefone_usuario'),
					'celular_usuario' 			=> $this->input->post('telefone_usuario'),
					'login_usuario' 				=> $this->input->post('login_usuario'),
					'senha_usuario' 				=> sha1($this->input->post('senha_usuario')),
					'ativo_usuario' 				=> $this->input->post('ativo_usuario'),
					'fk_grupo_usuario' 			=> $this->input->post('fk_grupo_usuario'),
					'cep_usuario' 					=> $this->input->post('cep_usuario'),
					'logradouro_usuario'		=> $this->input->post('logradouro_usuario'),
					'bairro_usuario' 				=> $this->input->post('bairro_usuario'),
					'cidade_usuario' 				=> $this->input->post('cidade_usuario'),
					'estado_usuario' 				=> $this->input->post('estado_usuario'),
					'numero_usuario' 				=> $this->input->post('numero_usuario'),
					'complemento_usuario' 	=> $this->input->post('complemento_usuario')
					
				);

		if ($this->form_validation->run()) {

			$this->model_usuarios->start();
			
			$id = $this->model_usuarios->create($dados);

			$commit = $this->model_usuarios->commit();

			if ($commit['status']) {
				$this->aviso('Registro Criado','Usuário "'.$this->input->post('nome_usuario').'" criado com sucesso!.','success',false);

				redirect('main/redirecionar/5/'.$id);
			} else {
				$this->aviso('Falha ao criar','Erro(s) ao inserir dados: "'.$commit['message'].'" <br> <a href="#" id="erro_feedback" cod="'.$commit['log_erro'].'">Clique Aqui Para Reportar</a>','error',true);
				$this->session->set_flashdata($dados);

				redirect('main/redirecionar/7');
			}


		} else {

			$this->aviso('Falha ao criar','Erro(s) no formulário: '.validation_errors(),'error',true);
			$this->session->set_flashdata($dados);

			redirect('main/redirecionar/7');

		}

	}

	public function editar_usuario(){
	
		$this->form_validation->set_rules('nome_usuario','Nome do usuario','required');

		if($this->input->post('email_usuario') != $this->input->post('email_inicial')){
			$this->form_validation->set_rules('email_usuario','E-mail do usuario','required|is_unique[seg_usuarios.email_usuario]');
		}

		$this->form_validation->set_rules('telefone_usuario','Telefone do usuario','required');

		if($this->input->post('login_usuario') != $this->input->post('login_inicial')){
			$this->form_validation->set_rules('login_usuario','Login do usuario','required|is_unique[seg_usuarios.login_usuario]');	
		}
		
		$this->form_validation->set_rules('ativo_usuario','Status do usuario','required');
		$this->form_validation->set_rules('fk_grupo_usuario','Grupo do usuario','required');
		$this->form_validation->set_rules('cep_usuario','CEP do Usuário','required');
		$this->form_validation->set_rules('logradouro_usuario','Logradouro do Usuário','required');
		$this->form_validation->set_rules('bairro_usuario','Bairro do Usuário','required');
		$this->form_validation->set_rules('cidade_usuario','Cidade do Usuário','required');
		$this->form_validation->set_rules('estado_usuario','Estado do Usuário','required');
		$this->form_validation->set_rules('numero_usuario','Número do Usuário','required');

		$dados = array (
					'id_usuario' 					=> $this->input->post('id_usuario'),
					'nome_usuario' 				=> $this->input->post('nome_usuario'),
					'email_usuario' 			=> $this->input->post('email_usuario'),
					'telefone_usuario' 		=> $this->input->post('telefone_usuario'),
					'login_usuario' 			=> $this->input->post('login_usuario'),
					'ativo_usuario' 			=> $this->input->post('ativo_usuario'),
					'fk_grupo_usuario'		=> $this->input->post('fk_grupo_usuario'),
					'cep_usuario' 				=> $this->input->post('cep_usuario'),
					'logradouro_usuario' 	=> $this->input->post('logradouro_usuario'),
					'bairro_usuario' 			=> $this->input->post('bairro_usuario'),
					'cidade_usuario' 			=> $this->input->post('cidade_usuario'),
					'estado_usuario' 			=> $this->input->post('estado_usuario'),
					'numero_usuario' 			=> $this->input->post('numero_usuario'),
					'complemento_usuario' => $this->input->post('complemento_usuario')
				);

		//Houve alteração de senha?
		if($this->input->post('senha_usuario') != ""){
			$dados['senha_usuario'] = sha1($this->input->post('senha_usuario'));
		} 

		if ($this->form_validation->run()) {
			
			$this->model_usuarios->start();
			$this->model_usuarios->update($dados);

			$commit = $this->model_usuarios->commit();

			if ($commit['status']) {
				$this->aviso('Registro Editado','Usuário "'.$this->input->post('nome_usuario').'" editado com sucesso!.','success',false);
			} else {
				$this->aviso('Falha ao editar','Erro(s) ao atualizar dados: "'.$commit['message'].'" <br> <a href="#" id="erro_feedback" cod="'.$commit['log_erro'].'">Clique Aqui Para Reportar</a>','error',true);
			}


			redirect('main/redirecionar/5/'.$this->input->post('id_usuario'));

		} else {

			$this->aviso('Falha ao editar','Erro(s) no formulário: '.validation_errors(),'error',true);

			redirect('main/redirecionar/5/'.$this->input->post('id_usuario'));

		}

	}

    public function editar_whatsapp(){

        $this->form_validation->set_rules('nome_usuario','Nome do usuario','required');

        if($this->input->post('email_usuario') != $this->input->post('email_inicial')){
            $this->form_validation->set_rules('email_usuario','E-mail do usuario','required|is_unique[seg_usuarios.email_usuario]');
        }

        $this->form_validation->set_rules('telefone_usuario','Telefone do usuario','required');

        if($this->input->post('login_usuario') != $this->input->post('login_inicial')){
            $this->form_validation->set_rules('login_usuario','Login do usuario','required|is_unique[seg_usuarios.login_usuario]');
        }
        $dados = array (
            'id' 					=> 1,
            'whatsapp' 			    => $this->input->post('whatsapp')
        );


          //return var_dump($dados);

            $this->model_configuracao->start();
            $this->model_configuracao->update($dados);

            $commit = $this->model_configuracao->commit();

       // return var_dump($commit);
            if ($commit['status']) {
                $this->aviso('Registro Editado','Usuário "'.$this->input->post('whatsapp').'" editado com sucesso!.','success',false);
            } else {
                $this->aviso('Falha ao editar','Erro(s) ao atualizar dados: "'.$commit['message'].'" <br> <a href="#" id="erro_feedback" cod="'.$commit['log_erro'].'">Clique Aqui Para Reportar</a>','error',true);
            }

            redirect('main/redirecionar/33');

    }

	public function editar_perfil(){

		$this->form_validation->set_rules('nome_usuario','Nome do usuario','required');

		if($this->input->post('email_usuario') != $this->input->post('email_inicial')){
			$this->form_validation->set_rules('email_usuario','E-mail do usuario','required|is_unique[seg_usuarios.email_usuario]');
		}

		$this->form_validation->set_rules('telefone_usuario','Telefone do usuario','required');

		if($this->input->post('login_usuario') != $this->input->post('login_inicial')){
			$this->form_validation->set_rules('login_usuario','Login do usuario','required|is_unique[seg_usuarios.login_usuario]');	
		}

		$this->form_validation->set_rules('cep_usuario','CEP do Usuário','required');
		$this->form_validation->set_rules('logradouro_usuario','Logradouro do Usuário','required');
		$this->form_validation->set_rules('bairro_usuario','Bairro do Usuário','required');
		$this->form_validation->set_rules('cidade_usuario','Cidade do Usuário','required');
		$this->form_validation->set_rules('estado_usuario','Estado do Usuário','required');
		$this->form_validation->set_rules('numero_usuario','Número do Usuário','required');

		$dados = array (
					'id_usuario' 					=> $this->input->post('id_usuario'),
					'nome_usuario' 				=> $this->input->post('nome_usuario'),
					'email_usuario' 			=> $this->input->post('email_usuario'),
					'telefone_usuario' 		=> $this->input->post('telefone_usuario'),
					'login_usuario' 			=> $this->input->post('login_usuario'),
					'cep_usuario' 				=> $this->input->post('cep_usuario'),
					'logradouro_usuario' 	=> $this->input->post('logradouro_usuario'),
					'bairro_usuario' 			=> $this->input->post('bairro_usuario'),
					'cidade_usuario' 			=> $this->input->post('cidade_usuario'),
					'estado_usuario' 			=> $this->input->post('estado_usuario'),
					'numero_usuario' 			=> $this->input->post('numero_usuario'),
					'complemento_usuario' => $this->input->post('complemento_usuario')
				);

		//Houve alteração de senha?
		if($this->input->post('senha_usuario') != ""){
			$dados['senha_usuario'] = sha1($this->input->post('senha_usuario'));
		} 

		if ($this->form_validation->run()) {
			
			$this->model_usuarios->start();
			$this->model_usuarios->update($dados);

			$commit = $this->model_usuarios->commit();

			if ($commit['status']) {
				$this->session->set_userdata("login",$this->input->post('login_usuario'));
				$this->session->set_userdata("nome",$this->input->post('nome_usuario'));
				$this->aviso('Registro Editado','Usuário "'.$this->input->post('nome_usuario').'" editado com sucesso!.','success',false);
			} else {
				$this->aviso('Falha ao editar','Erro(s) ao atualizar dados: "'.$commit['message'].'" <br> <a href="#" id="erro_feedback" cod="'.$commit['log_erro'].'">Clique Aqui Para Reportar</a>','error',true);
			}

			redirect('main/redirecionar/1');

		} else {

			$this->aviso('Falha ao editar','Erro(s) no formulário: '.validation_errors(),'error',true);
			redirect('main/redirecionar/1');

		}

	}

	public function esqueci_Senha() {

		$this->load->helper('string');
        $senha = random_string('numeric', 6);
		
		$this->model_usuarios->start();
		$usuario = $this->model_usuarios->senha_Email(sha1($senha),$this->input->post('login'));

		$email       = $usuario['email'];
		$nomeUsuario = $usuario['nome'];

		//Usuário inativo ou desativado
		if($email == ""){

			//echo 'Usuário está desabilitado ou não existe, entre em contato com o administrador!';
			$json = array(
				'resultado' => "Dados incorretos ou perfil inativo",
				'status' => 0
			);

		} else { //Senha enviada para o E-mail.

			$emailCorpo = '
				<p>Sua nova senha de acesso ao leilão 24H é:</p>
				<table role="presentation" border="0" cellpadding="0" cellspacing="0" class="btn btn-primary">
				<tbody>
					<tr>
					<td align="left">
						<h1>'.$senha.'</h1>
					</td>
					</tr>
				</tbody>
				</table>
				';
			
			if ($this->bodyEmail('Nova Senha',$emailCorpo,$email,$nomeUsuario)) {       
				$this->model_usuarios->commit();
				$json = array(
					'resultado' => "Nova Senha enviada para o E-mail: (".$email.")",
					'status' => 1
				);
			} else {
				$this->model_usuarios->rollback();
				$json = array(
					'resultado' => "Erro ao enviar senha. <br>".$this->email->print_debugger(),
					'status' => 0
				);
			}

		}

		header('Content-Type: application/json');
		echo json_encode($json);


	}

	public function filtro_ajax(){

		$this->load->model("model_relatorios");

		$parametros = array(); //Recebe os valores com nomes certos do filtro

		$parametros['tabela']           = $this->input->get("tabela");
		$parametros['filtro_campo']     = $this->input->get("filtro_campo");
		$parametros['filtro_ordenacao'] = $this->input->get("filtro_ordenacao");
		$parametros['filtro_ordem']     = $this->input->get("filtro_ordem");
		$parametros['filtro_limite']    = $this->input->get("filtro_limite");

		$this->model_relatorios->start();

		//Carregando os campos dinamicos que irei receber via get, que são todos do filtro.
		$campos = $this->model_relatorios->listarCampos($parametros['tabela'],$parametros['filtro_campo']);

		$filtro = array();
		foreach ($campos as $key => $campo) {
			if ($campo['descricao_campo'] != "") {
				$campo_valor = $this->input->get($campo['nome_campo']);
				if (isset($campo_valor) && $campo_valor != "") {
					$campo['valor'] = $campo_valor;
					$filtro[$campo['nome_campo']] = $campo;
				}
			} 
		}

		$resultados = $this->model_relatorios->filtroAjax($parametros,$filtro);

		if ($this->model_relatorios->commit()) {
			echo '<table class="table table-bordered table-hover" align="center">
				<thead align="center">';
				
					foreach ($campos as $chave => $campo) {
						if ($campo['selecionado']) {
							if ($campo['descricao_campo'] == "ID") 
								echo '<th style="width: 60px;" align="center" class="no-filter">Editar</th>';
							echo  "<th>{$campo['descricao_campo']}: </th>";
						}
					}

				echo '</thead>
				<tbody align="center">';	
					
					foreach ($resultados as $resultado) {

						echo "<tr>";
						foreach ($parametros['filtro_campo'] as $select_) {
							if ($select_ == "id")
								echo '<td><a href="'.base_url().'main/redirecionar/5/'.$resultado[$select_].'"><button class="btn btn-info"> <i class="glyphicon glyphicon-edit"> </i> Editar</button></a></td>';

							if ($select_ == "telefone_usuario") {
								echo "<td class=\"mascara_cel\">".$resultado[$select_]."</td>";
							} else {
								echo "<td>".$resultado[$select_]."</td>";
							}
						}
						echo "</tr>";

					}
					
				echo '</tbody>
			</table>';

		}

	}

	public function detalhes_usuario(){

		$id = $this->input->post('id');
		$usuario = $this->model_usuarios->detalhesUsuario($id);

		echo "
				
				<strong class='info-titulo'>ID: </strong>	{$usuario->id_usuario} <br>
				<strong class='info-titulo'>Ingressos: </strong>	{$usuario->ingressos} <br>
				<strong class='info-titulo'>Login: </strong>	{$usuario->login_usuario} <br>
				<strong class='info-titulo'>Nome: </strong>	{$usuario->nome_usuario} <br>
				<strong class='info-titulo'>RG: </strong>	{$usuario->rg_usuario} <br>
				<strong class='info-titulo'>CPF: </strong>	{$usuario->cpf_usuario} <br>
				<strong class='info-titulo'>E-mail: </strong>	{$usuario->email_usuario} <br>
				<strong class='info-titulo'>Sexo: </strong>	{$usuario->sexo} <br>
				<strong class='info-titulo'>Telefone: </strong>	{$usuario->telefone_usuario} <br>
				<strong class='info-titulo'>Celular: </strong>	{$usuario->celular_usuario} <br>
				<strong class='info-titulo'>CEP: </strong>	{$usuario->cep_usuario} <br>
				<strong class='info-titulo'>Logradouro: </strong>	{$usuario->logradouro_usuario} <br>
				<strong class='info-titulo'>Número: </strong>	{$usuario->numero_usuario} <br>
				<strong class='info-titulo'>Complemento: </strong>	{$usuario->complemento_usuario} <br>
				<strong class='info-titulo'>Bairro: </strong>	{$usuario->bairro_usuario} <br>
				<strong class='info-titulo'>Estado: </strong>	{$usuario->estado_usuario} <br>
				<strong class='info-titulo'>Cidade: </strong>	{$usuario->cidade_usuario} <br>
				<strong class='info-titulo'>Status: </strong>	{$usuario->ativo_usuario} <br>
				<strong class='info-titulo'>Ativado por SMS? </strong>	{$usuario->ativado_sms} <br>
				<strong class='info-titulo'>Veio como afiliado? </strong>	{$usuario->criado_por} <br>
				<strong class='info-titulo'>Conta criada em: </strong>	{$usuario->criacao_usuario} <br>
				<strong class='info-titulo'>Criado </strong>	{$usuario->face}<br><br>";
				

		if($usuario->ativo){
			echo "<button id=\"inativar_usuario\" type=\"button\" class=\"btn btn-danger btn-block\" data-dismiss=\"modal\">Inativar</button>";
		} else {
			echo "<button id=\"ativar_usuario\" type=\"button\" class=\"btn btn-success btn-block\" data-dismiss=\"modal\">Ativar</button>";
		}

	}

	public function bodyEmail($titulo,$corpo,$email,$nomeUsuario) {

        $address = $_SERVER['SERVER_NAME'].base_url();
        
        $html = '
        <!doctype html>
        <html>
          <head>
            <meta name="viewport" content="width=device-width" />
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
            <title>'.$titulo.'</title>
            <style>
              /* ------------------------------------- GLOBAL RESETS ------------------------------------- */ /*All the styling goes here*/ img{border: none; -ms-interpolation-mode: bicubic; max-width: 100%;}body{background-color: #f6f6f6; font-family: sans-serif; -webkit-font-smoothing: antialiased; font-size: 14px; line-height: 1.4; margin: 0; padding: 15; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;}table{border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;}table td{font-family: sans-serif; font-size: 14px; vertical-align: top;}/* ------------------------------------- BODY & CONTAINER ------------------------------------- */ .body{background-color: #f6f6f6; width: 100%;}/* Set a max-width, and make it display as block so it will automatically stretch to that width, but will also shrink down on a phone or something */ .container{display: block; margin: 0 auto !important; /* makes it centered */ max-width: 580px; padding: 10px; width: 580px;}/* This should also be a block element, so that it will fill 100% of the .container */ .content{box-sizing: border-box; display: block; margin: 0 auto; max-width: 580px; padding: 10px;}/* ------------------------------------- HEADER, FOOTER, MAIN ------------------------------------- */ .main{background: #ffffff; border-radius: 3px; width: 100%;}.wrapper{box-sizing: border-box; padding: 20px;}.content-block{padding-bottom: 10px; padding-top: 10px;}.footer{clear: both; margin-top: 10px; text-align: center; width: 100%;}.footer td, .footer p, .footer span, .footer a{color: #999999; font-size: 12px; text-align: center;}/* ------------------------------------- TYPOGRAPHY ------------------------------------- */ h1, h2, h3, h4{color: #000000; font-family: sans-serif; font-weight: 400; line-height: 1.4; margin: 0; margin-bottom: 30px;}h1{font-size: 35px; font-weight: 300; text-align: center; text-transform: capitalize;}p, ul, ol{font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; margin-bottom: 15px;}p li, ul li, ol li{list-style-position: inside; margin-left: 5px;}a{color: #3498db; text-decoration: underline;}/* ------------------------------------- BUTTONS ------------------------------------- */ .btn{box-sizing: border-box; width: 100%;}.btn > tbody > tr > td{padding-bottom: 15px;}.btn table{width: auto;}.btn table td{background-color: #ffffff; border-radius: 5px; text-align: center;}.btn a{background-color: #ffffff; border: solid 1px #3498db; border-radius: 5px; box-sizing: border-box; color: #3498db; cursor: pointer; display: inline-block; font-size: 14px; font-weight: bold; margin: 0; padding: 12px 25px; text-decoration: none; text-transform: capitalize;}.btn-primary table td{background-color: #3498db;}.btn-primary a{background-color: #27ae60; border-color: #27ae60; color: #ffffff;}/*------------------------------------- OTHER STYLES THAT MIGHT BE USEFUL ------------------------------------- */ .last{margin-bottom: 0;}.first{margin-top: 0;}.align-center{text-align: center;}.align-right{text-align: right;}.align-left{text-align: left;}.clear{clear: both;}.mt0{margin-top: 0;}.mb0{margin-bottom: 0;}.preheader{color: transparent; display: none; height: 0; max-height: 0; max-width: 0; opacity: 0; overflow: hidden; mso-hide: all; visibility: hidden; width: 0;}.powered-by a{text-decoration: none;}hr{border: 0; border-bottom: 1px solid #f6f6f6; margin: 20px 0;}/* ------------------------------------- RESPONSIVE AND MOBILE FRIENDLY STYLES ------------------------------------- */ table[class=body] h1{font-size: 28px !important; margin-bottom: 10px !important;}table[class=body] p, table[class=body] ul, table[class=body] ol, table[class=body] td, table[class=body] span, table[class=body] a{font-size: 16px !important;}table[class=body] .wrapper, table[class=body] .article{padding: 10px !important;}table[class=body] .content{padding: 0 !important;}table[class=body] .container{padding: 0 !important; width: 100% !important;}table[class=body] .main{border-left-width: 0 !important; border-radius: 0 !important; border-right-width: 0 !important;}table[class=body] .btn table{width: 100% !important;}table[class=body] .btn a{width: 100% !important;}table[class=body] .img-responsive{height: auto !important; max-width: 100% !important; width: auto !important;}/* ------------------------------------- PRESERVE THESE STYLES IN THE HEAD ------------------------------------- */ @media all{.ExternalClass{width: 100%;}.ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div{line-height: 100%;}.apple-link a{color: inherit !important; font-family: inherit !important; font-size: inherit !important; font-weight: inherit !important; line-height: inherit !important; text-decoration: none !important;}.btn-primary table td:hover{background-color: #34495e !important;}.btn-primary a:hover{background-color: #27ae40 !important; border-color: #27ae50 !important;}}
            </style>
          </head>
          <body width="100%">
            <table role="presentation" border="0" cellpadding="0" cellspacing="0" class="body" width="100%">
              <tr>
                <td>&nbsp;</td>
                <td class="container">
                  <div class="content">
                        <tr>
                            <td width="100%" align="center">
                                <img src="http://'.$address.'style/img/icon.png" width="100px" height="100px" style="border-radius: 10px;">
                            </td>
                        </tr>
        
                      <!-- START MAIN CONTENT AREA -->
                      <tr>
                        <td class="wrapper">
                          <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                            <tr>
                              <td>
                                <p>Olá '.$nomeUsuario.'.</p>
                                '.$corpo.'
                                <p>Esse e-mail é automático, qualquer dúvida entre em contato com nosso suporte <a href="mailto:sacleilao24h@gmail.com">sacleilao24h@gmail.com</a>.</p>
                                <p>Obrigado por utilizar nosso sistema!.</p>
                              </td>
                            </tr>
                          </table>
                        </td>
                      </tr>
        
                    <!-- END MAIN CONTENT AREA -->
                    </table>
        
                    <!-- START FOOTER -->
                    <div class="footer">
                      <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                          <td class="content-block" colspan="2">
                            <span class="apple-link"><a href="http://'.$address.'" target="_blank">Leilão 24H</a></span>
                          </td>
                        </tr>
                        <tr>
                          <td class="content-block powered-by" colspan="2">
                            Powered by <a href="http://megamil.net">Megamil.net</a>.
                          </td>
                        </tr>
                        <tr align="left">
                          <td class="content-block powered-by" colspan="2" align="left">
                            <small>Solicitado pelo IP: '.$_SERVER['REMOTE_ADDR'].'</small> <br>
                            <small>Sistema / Navegador: '.$_SERVER['HTTP_USER_AGENT'].'</small> <br>
                            <small>Em: '.date('d/m/Y H:i:s').'</small> <br>
                            <br>
                            <a href="http://'.$address.'privacidade">Política de privacidade</a> <br> 
                            <a href="http://'.$address.'">Termos de uso</a>
                          </td>
                        </tr>
        
                      </table>
                    </div>
                    <!-- END FOOTER -->
        
                  <!-- END CENTERED WHITE CONTAINER -->
                  </div>
                </td>
                <td>&nbsp;</td>
              </tr>
            </table>
          </body>
        </html>';

        // Detalhes do Email. 
		$this->email->from('no-reply@leilao24h.com.br', 'Leilão 24H')
					->to($email)
					->subject($titulo)
					->message($html);

        // Enviar... 
        if ($this->email->send()) {

            return true;

        } else {
			log_message('error', 'Erro e-mail '.$this->email->print_debugger());
            return false;

        }
       
    }

	public function status_usuario(){

		$id = $this->input->post('id');
		$ativar = array('ativo_usuario' => $this->input->post('ativar'));
		$this->model_usuarios->ativarUsuario($ativar,$id);

	}

	public function aviso($titulo,$aviso,$tipo,$fixo){

		//Toast apresenta erro quando existe uma quebra de linha, que ocorre com o validation_errors().
			$aviso_ = str_replace('
', '', $aviso);

		$aviso = str_replace('\'', '"', $aviso_);

		$this->session->set_flashdata('titulo_alerta',$titulo);
		$this->session->set_flashdata('mensagem_alerta',$aviso);
		$this->session->set_flashdata('tipo_alerta',$tipo);
		$this->session->set_flashdata('mensagem_fixa',$fixo);

	}


    public function comprar_recompensas() {

        $usuarios = $this->input->post('fk_usuario_destino');
        $quantidade_ingresso = $this->input->post('quantidade_ingresso');


       // return  var_dump($usuarios);

        if ($usuarios) {

            foreach ($usuarios as $usuario) {

                $id_usuario = $usuario;
                $id_recompensa = 17; // id do banco de dados - pacote ingresso;
                $tipo_recompensa = 1;

                if ($tipo_recompensa == RECOMPENSA_PACOTE) {

                    $pacote = $this->model_ingressos->getPacote($id_recompensa);
                    //Validando Saldo
                    if ($this->model_leiloes->getCreditos($id_usuario) >= $pacote['valor_pacote']) {

                        $cad_ingressos = array(
                            'fk_usuario' => $id_usuario,
                            'quantidade' => $quantidade_ingresso,
                            'gratis' => $pacote['gratis']
                        );

                        $compra_ingresso = array(
                            'valor' => $pacote['valor_pacote'],
                            'fk_pacote' => $pacote['id_pacote_ingresso'],
                        );

                        $this->model_ingressos->start();

                        $id_ingresso = $this->model_ingressos->insertIngresso($cad_ingressos, $pacote['valor_pacote']);

                        $compra_ingresso['fk_ingresso'] = $id_ingresso;

                        $id_comprar = $this->model_ingressos->insertCompra($compra_ingresso);

                        $update = array(
                            'id_ingresso' => $id_ingresso,
                            'ativo' => true
                        );

                        $this->model_ingressos->updateIngresso($update);

                        $commit = $this->model_ingressos->commit();

                        if ($commit['status']) {
                            $this->aviso('Recompensa','Recompensa adquirida com sucesso','success',true);
                        } else {
                            $this->aviso('Recompensa','Falha ao adquirir','error',true);
                        }

                        redirect('main/redirecionar/34');

                    } else {
                        $this->aviso('Recompensa','Pontos insuficientes','error',true);

                    }

                } else if ($tipo_recompensa == RECOMPENSA_PRODUTOS) {

                    $produto = $this->model_ingressos->getProduto($id_recompensa);
                    //Validando Saldo
                    if ($this->model_leiloes->getCreditos($id_usuario) >= $produto['produto_recompensa_custo']) {

                        $this->model_ingressos->start();

                        $this->model_ingressos->debitarCredito($produto['produto_recompensa_custo'], $autenticacao->id_usuario);

                        $dados = array('fk_produto' => $produto['id_produto'], 'fk_usuario' => $autenticacao->id_usuario);
                        $this->model_ingressos->inserirRecompensa($dados);

                        $commit = $this->model_ingressos->commit();

                        if ($commit['status']) {
                            $this->aviso('Recompensa','Recompensa adquirida com sucesso','success',true);
                        } else {
                            $this->aviso('Recompensa','Falha ao adquirir','error',true);
                        }

                    redirect('main/redirecionar/34');

                    } else {
                        $this->aviso('Recompensa','Pontos insuficientes','error',true);

                    }
                }
            }
        }
    }


}