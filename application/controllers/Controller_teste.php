<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Controller_teste extends CI_Controller {

	public function __construct() {
		parent::__construct();
		
		$this->load->model('model_teste');
	}

	public function index() {
		
		$this->load->helper('string');
		$i = 0;
		while ($i <= 100) {
			echo $this->model_teste->insertTeste(array('nome' => random_string('alpha',7)));
			$i++;
		}
	}

	public function busca() {

		$qtd_pg = $this->input->get('qtd_pg');
		$pagina = $this->input->get('pagina');

		if (is_null($qtd_pg) || $qtd_pg == '' || $qtd_pg == 0) {
			$qtd_pg = 0;
		}

		if (is_null($pagina) || $pagina == '' || $pagina == 0) {
			$pagina = 1;
		}

		$dados = array(
			'sort' => $this->input->get('campo'),
			'order' => $this->input->get('ordem'),
			'limit' => $qtd_pg,
			'offset' => (($qtd_pg * $pagina) - $qtd_pg),
			'pagina' => $pagina
		);
		
		$busca = $this->model_teste->getTeste($dados);

		header("Content-type:application/json");
		print_r(json_encode($busca));
	}

	public function cripto() {

		$param = $this->input->post('pass');

		echo $this->setEncryption($param);
    }

    public function setEncryption($param) {
        $cripto = hash('whirlpool',md5(sha1($param)));

        return $cripto;

    }

}

/* End of file Controller_teste.php */
/* Location: ./application/controllers/Controller_teste.php */