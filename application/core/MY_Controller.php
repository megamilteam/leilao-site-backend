<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_controller {

	public function aviso($titulo,$aviso,$tipo,$fixo){

		//Toast apresenta erro quando existe uma quebra de linha, que ocorre com o validation_errors().
		$aviso_ = str_replace("\n", "", $aviso);

		$aviso = str_replace('\'', '"', $aviso_);

		$this->session->set_flashdata('titulo_alerta',$titulo);
		$this->session->set_flashdata('mensagem_alerta',$aviso);
		$this->session->set_flashdata('tipo_alerta',$tipo);
		$this->session->set_flashdata('mensagem_fixa',$fixo);

	}

	/**
	* <b>Tranforma Data:</b> Transforma uma data no formato DD/MM/YY em uma data no formato TIMESTAMP!
	* @return STRING = $Data = Data no formato timestamp, (d/m/Y) ou (d/m/Y H:i:s)!
	*/
	public function dateFormat($data) {
			$format = explode(' ', $data);
			$data = str_replace('/', '-', $format[0]);
			$data = explode('-', $data);
			$data = str_replace('?', '', $data);
			if (empty($format[1])):
					$format[1] = ' 23:59:59';
			endif;
			$data = $data[2] . '-' . $data[1] . '-' . $data[0] . $format[1];
			return $data;
	}

}

/* End of file My_controller.php */
/* Location: ./application/core/My_controller.php */
