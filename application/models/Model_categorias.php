<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_categorias extends MY_Model {

	public function view_categorias($where) {

		$categorias = $this->arvore_categorias();

		return $this->removeNullSub($categorias);
	}

	public function create($categoria) {
		return $this->db->insert('cad_categorias',$categoria);
	}

	public function listaCategorias($id_categoria = null,$todos = false) {

		$this->db->select("id_categoria, concat('categoria_',id_categoria,'.png') as icone, nome_categoria, ativa, fk_categoria_pai");
		if(!$todos)
			$this->db->where('fk_categoria_pai', $id_categoria);
		$this->db->where('ativa', true);
		$this->db->order_by('nome_categoria', 'asc');
		$categorias = $this->db->get('cad_categorias')->result_array();

		$categorias = $this->removeNullSub($categorias);

		return $categorias;
	}
}

/* End of file Model_categorias.php */
/* Location: ./application/models/Model_categorias.php */
