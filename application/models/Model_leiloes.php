<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_leiloes extends MY_Model {

	public function insert($valores) {
		$this->db->insert('cad_leilao', $valores);
		return $this->db->insert_id();
	}

	public function update($valores) {
		$this->db->where('id_leilao', $valores['id_leilao']);
		return $this->db->update('cad_leilao', $valores);
	}


	public function view_lista_leiloes() {

		$where['limit'] = 2000;

		 $this->db->select("id_leilao,round(valor_minimo,2) as valor_minimo,round(lance_minimo,2) as lance_minimo,ingressos,date_format(data_inicio,'%d/%m/%Y às  %H:%i:%s') as data_inicio,date_format(data_fim_previsto,'%d/%m/%Y às  %H:%i:%s') as data_fim_previsto,status,nome_produto,fk_usuario_arrematou,nome_usuario_arrematou,lance_formatado,nome_usuario_leiloando,nome_categoria, (select count(*) from sala_leilao where fk_leilao = id_leilao) as usuarios");
		 		//$this->db->where('data_inicio >= ','  DATE_FORMAT(NOW() - INTERVAL 3 MONTH');

		 $this->db->order_by('id_leilao', 'desc');

		$leiloes = $this->db->get('view_leilao', $where['limit'])->result();

		return 	array('leiloes' => $leiloes);

	}

	public function listarRecompensas($where = null) {

		$this->db->select('id_produto, concat(left(nome_produto,200),\'...\') as nome_produto, concat(left(descricao_produto,300),\'...\') as descricao_produto, nome_categoria, produto_usado, produto_recompensa_custo');
		$this->db->join('cad_categorias', 'cad_produtos.fk_categoria = cad_categorias.id_categoria', 'left');
		$this->db->where('produto_recompensa', 1);
		$this->db->order_by('nome_categoria', 'asc');
		$this->db->order_by('nome_produto', 'asc');
		$produtos = $this->db->get('cad_produtos', $where['limit'], $where['offset'])->result_array();

		$this->db->where(array('gratis' => 1));
		$pacotes = $this->db->get('pacote_ingresso', $where['limit'], $where['offset'])->result();

		return array(
			'pacotes' => $pacotes,
			'produtos' => $produtos
		);

	}

	public function listarMinhasRecompensas($id_usuario) {

		$this->db->select('id_produto, nome_produto, descricao_produto, nome_categoria, produto_usado, produto_recompensa_custo');
		$this->db->join('cad_categorias', 		 'cad_produtos.fk_categoria = cad_categorias.id_categoria', 'left');
		$this->db->join('cad_compra_recompensa as ccr', 'ccr.fk_produto = id_produto', 'inner');
		$this->db->where('produto_recompensa', 	 1);
		$this->db->where('ccr.fk_usuario', 			 $id_usuario);
		$this->db->order_by('nome_categoria', 	'asc');
		$this->db->order_by('nome_produto', 	'asc');
		return $this->db->get('cad_produtos')->result_array();

	}

	public function view_lista_leilao_pendente($where = null) {
		if ($where == null) {
			$where[0] = 10;
			$where[1] = 0;
		} else if (isset($where[0]) && !isset($where[1])) {
			$where[1] = 1;
			$where[1] = ($where[0] * $where[1]) - $where[0];
		} else {
			$where[1] = ($where[0] * $where[1]) - $where[0];
		}

		$this->db->where('status_leilao', 1);
		// $leiloes = $this->db->get('view_leilao', $where[0], $where[1])->result();
		$leiloes = $this->db->get('view_leilao')->result();

		$status = $this->db->get('status_leilao')->result();

		return array('leiloes' => $leiloes, 'status' => $status);
	}

	public function view_lista_leilao_ativo($where = null) {

		if ($where == null) {
			$where[0] = 10;
			$where[1] = 0;
		} else if (isset($where[0]) && !isset($where[1])) {
			$where[1] = 1;
			$where[1] = ($where[0] * $where[1]) - $where[0];
		} else {
			$where[1] = ($where[0] * $where[1]) - $where[0];
		}

		$this->db->where('status_leilao', 2);
		$leiloes = $this->db->get('view_leilao', $where[0], $where[1])->result();

		$status = $this->db->get('status_leilao')->result();

		return array('leiloes' => $leiloes, 'status' => $status);
	}

	public function view_lista_leilao_finalizado($where = null) {

		if ($where == null) {
			$where[0] = 10;
			$where[1] = 0;
		} else if (isset($where[0]) && !isset($where[1])) {
			$where[1] = 1;
			$where[1] = ($where[0] * $where[1]) - $where[0];
		} else {
			$where[1] = ($where[0] * $where[1]) - $where[0];
		}

		$this->db->where('status_leilao', 4);
		$leiloes = $this->db->get('view_leilao', $where[0], $where[1])->result();

		$status = $this->db->get('status_leilao')->result();

		return array('leiloes' => $leiloes, 'status' => $status);
	}

	public function getLeiloes($valores) {

		// $this->db->select('id_leilao, id_produto, nome_produto, descricao_produto, valor_minimo, lance_minimo, ingressos, data_inicio , data_fim_previsto, ifnull(cad_favoritos.fk_usuario, 0) favorito, nome_usuario,avaliacao');

		// if(isset($valores['id_leilao'])){
		// 	$this->db->where('id_leilao', $valores['id_leilao']);
		// }

		// $this->db->where('status_leilao', 2);
		// $this->db->where('view_leilao.fk_usuario <>', $valores['id_usuario']);
		// $this->db->join('cad_favoritos', 'view_leilao.id_leilao = cad_favoritos.fk_leilao', 'left');
		// //$this->db->order_by('data_inicio', 'desc');
		// $leiloes = $this->db->get('view_leilao', $valores['limit'], $valores['offset']);

		// // echo $this->db->last_query();
		// // die();

		// return $this->removeNullSub($leiloes->result_array());

		$this->db->select(" id_leilao,
						    id_produto,
						    nome_produto,
						    descricao_produto,
						    valor_inicial_lance as valor_minimo,
						    lance_minimo,
						    ingressos,
						    data_inicio,
						    produto_usado,
						    data_fim_previsto,
						    nome_usuario,
						    estado_usuario,
                            ifnull((SELECT count(*) from cad_favoritos where id_leilao = fk_leilao and id_usuario = cad_favoritos.fk_usuario),0) as favorito,
						    ifnull((SELECT avg(avaliacao) from cad_avaliacoes where fk_leiloeiro = fk_usuario),0) as avaliacao");

		$this->db->join('cad_produtos', 'cad_produtos.id_produto 	= cad_leilao.fk_produto', 	'inner');
		$this->db->join('seg_usuarios', 'seg_usuarios.id_usuario 	= cad_produtos.fk_usuario', 'inner');

		if(isset($valores['id_leilao'])){
			$this->db->where('id_leilao', $valores['id_leilao']);
	   	} else {
			$this->db->where('status_leilao', 2);
			$this->db->where('data_fim_previsto >= ', 'current_timestamp', false);
	   	}

		//$this->db->where('cad_produtos.fk_usuario <>', $valores['id_usuario']);
		$this->db->order_by('data_inicio', 'desc');
		$leiloes = $this->db->get('cad_leilao', $valores['limit'], $valores['offset']);

		// echo $this->db->last_query();
		// die();

		return $this->removeNullSub($leiloes->result_array());

	}

    public function getLeiloesArrematados($id_usuario) {

        $this->db->select(" concat('R$ ',format(max(`cadl`.`valor`) ,2,'de_DE')) as valor,
                            `cp`.`id_produto`,
							`cp`.`nome_produto`,
							 max(`cadl`.`valor`) as value,
							`cp`.`fk_usuario` as id_usuario,
                            `cl`.`id_leilao`, 
                            `cp`.`descricao_produto`,
                            `cl`.`lance_minimo`,
                            `su`.`cidade_usuario` as cidade_usuario_arrematou,
                            `cl`.`fk_usuario_arrematou` AS id_usuario_arrematou,
                            `su`.`cep_usuario` as cep_usuario_arrematou,
                             (select nome_usuario from seg_usuarios where id_usuario = `cp`.`fk_usuario`) as nome_usuario_produto,
                             (select telefone_usuario from seg_usuarios where id_usuario = `cp`.`fk_usuario`) as telefone_usuario_produto,
                             (select email_usuario from seg_usuarios where id_usuario = `cp`.`fk_usuario`) as email_usuario_produto,
                             (select cidade_usuario from seg_usuarios where id_usuario = `cp`.`fk_usuario`) as cidade_usuario_produto,
                             (select bairro_usuario from seg_usuarios where id_usuario = `cp`.`fk_usuario`) as bairro_usuario_produto,
                             (select nome_usuario from seg_usuarios where id_usuario = `cl`.`fk_usuario_arrematou`) as nome_usuario_arrematou,
                             (select nome_item_grupo from cad_item_grupo where id_item_grupo = su.estado_usuario) as estado_usuario_arrematou");

        $this->db->join('seg_usuarios su', 'cl.fk_usuario_arrematou = su.id_usuario', 'inner');

        $this->db->join('cad_produtos cp'   ,'cl.fk_produto = cp.id_produto'            ,'inner');
        $this->db->join('sala_leilao sl'    ,'cl.id_leilao = sl.fk_leilao'              ,'inner');
        $this->db->join('cad_lances cadl '  ,'cadl.fk_sala_leilao = sl.id_sala_leilao'  ,'inner');

        $this->db->where('cl.status_leilao', 4);
        $this->db->where('cl.finalizado', 1);
        $this->db->where('cl.fk_usuario_arrematou', $id_usuario);
        $this->db->order_by('cl.fk_usuario_arrematou', 'desc');
        $this->db->group_by('cl.id_leilao');

        $leiloesArrematados = $this->db->get('cad_leilao cl');

        $produtos = $this->db->query("SELECT count(*) as quantidade_produtos from cad_produtos where fk_usuario = {$id_usuario}")->row();
        $leiloes = $this->db->query("SELECT count(*) as quantidade_leiloes from cad_leilao where status_leilao = 4 and fk_usuario_arrematou = {$id_usuario}")->row();


        foreach($leiloesArrematados->result() as $key => $leilao){

                $endereco = $this->db->query("SELECT * from cad_endereco_arrematado where fk_leilao = {$leilao->id_leilao}")->row();
                $leiloesArrematados->result()[$key]->endereco = $endereco == null ?new StdClass: $endereco;

        }


        /*ifnull((SELECT count(*) from cad_produtos  where fk_usuario = ".$id_usuario."),0) as quantidade_produtos,
                            ifnull((SELECT count(*) from cad_leilao cl where cl.status_leilao = 4  and  cl.fk_usuario_arrematou = ".$id_usuario."),0) as quantidade_leiloes
  */
        $qtda_produtos = $leiloes->quantidade_leiloes;
 //
      //  echo $this->db->last_query();
        //print_r($leiloes->quantidade_leiloes);
    //  die();
        $leiloesArrematados->quantidade_leiloes =  $leiloes;

        return $this->removeNullSub(["quantidades"=>['quantidade_produtos'=>$produtos->quantidade_produtos,'quantidade_leiloes'=>$leiloes->quantidade_leiloes],'leiloes'=>$leiloesArrematados->result_array()]);
    }

	public function meusLeiloes($valores) {

		$this->db->select('id_leilao, id_produto, nome_produto, descricao_produto, round(valor_minimo,2) as valor_minimo, round(lance_minimo,2) as lance_minimo, ingressos, data_inicio, data_fim_previsto, status_leilao.status, status_leilao,status_produto,produto_usado');
		$this->db->where('view_leilao.fk_usuario', $valores['id_usuario']);
		$this->db->where('status_produto', 1);
        $this->db->join('status_leilao', 'view_leilao.status_leilao = status_leilao.id_status_leilao', 'left');
		$this->db->order_by('data_inicio', 'desc');
		$leiloes = $this->db->get('view_leilao', $valores['limit'], $valores['offset'])->result_array();
		return $this->removeNullSub($leiloes);
	}



		######################################################
		//Deletar Lance
		######################################################
		public function deletarLance($valores){

			$valores = $this->limpa_array($valores);

			$tabela = "seg_usuarios";
			$id = 'id_usuario';

			$this->gerarHistorico($id,$tabela,$valores,$valores[$id]);
			$this->db->where(array($id => $valores[$id]));
			$this->db->update($tabela,array('ativo_usuario' => 0));

			return $this->verificarErros($this->db->error(),'Model_webservice / deletarUsuario');

		}

		public function deletarLeilao($fk_produto, $id_leilao) {

			$this->db->where(array('id_leilao' => $id_leilao,'fk_produto' => $fk_produto));

			if ($this->db->get('cad_leilao')->num_rows() > 0) { // verifica se o usuário já tem o leilão
				$this->db->where(array('id_leilao' => $id_leilao,'fk_produto' => $fk_produto));
				$this->db->delete('cad_leilao'); // se tiver deleta
				return 'Leilão removido com sucesso';

			} else {
				//$this->db->insert('cad_favoritos', array('fk_usuario' => $id_usuario, 'fk_leilao' => $id_leilao)); // se não tiver insere
				return "Não existe nenhum Leilão para excluir";
			}
		}

	// public function ultimosLances($id_leilao) {
	// 	return $this->db->query("select valor, nome_usuario from cad_lances  limit 5")->result_array();
	// }

	public function consultaSala($valores) {
		$this->db->where($valores);
		return $this->db->get('sala_leilao');
	}

	public function favorito($id_usuario, $id_leilao) {

		$this->db->where(array('fk_usuario' => $id_usuario, 'fk_leilao' => $id_leilao));

		if ($this->db->get('cad_favoritos')->num_rows() > 0) { // verifica se o usuário já tem o leilão como favorito

			$this->db->where(array('fk_usuario' => $id_usuario, 'fk_leilao' => $id_leilao));
			$this->db->delete('cad_favoritos'); // se tiver deleta
			return 'Leilão removido dos favoritos';

		} else {

			$this->db->insert('cad_favoritos', array('fk_usuario' => $id_usuario, 'fk_leilao' => $id_leilao)); // se não tiver insere
			return 'Leilão adicionado aos favoritos';
		}
	}

	public function listarFavoritos($valores) {

		$this->db->select('id_leilao, id_produto, nome_usuario, nome_produto, descricao_produto, valor_inicial_lance as valor_minimo, lance_minimo, ingressos, data_inicio, data_fim_previsto, ifnull(cad_favoritos.fk_usuario, 0) favorito, ifnull((SELECT avg(avaliacao) from cad_avaliacoes where fk_leiloeiro = view_leilao.fk_usuario),0) as avaliacao, produto_usado');
		$this->db->where('status_leilao', 2);
		$this->db->where('cad_favoritos.fk_usuario', $valores['id_usuario']);
		$this->db->join('view_leilao', 'cad_favoritos.fk_leilao = view_leilao.id_leilao', 'left');
		$this->db->order_by('data_inicio', 'desc');
		$favoritos = $this->db->get('cad_favoritos', $valores['limit'], $valores['offset'])->result_array();
		return $this->removeNullSub($favoritos);
	}

	public function listarArrematados($valores) {

		$this->db->select('id_leilao, id_produto, nome_produto, descricao_produto, valor_minimo, lance_minimo, ingressos, data_inicio, data_fim_previsto, ifnull((select avaliacao from cad_avaliacoes ca where ca.fk_leilao = id_leilao), 0) avaliado,produto_usado, lance_formatado as ultimo_lance');
		$this->db->where('status_leilao', 4);
		$this->db->where('fk_usuario_arrematou', $valores['id_usuario']);
		$this->db->order_by('data_inicio', 'desc');
		$arrematados = $this->db->get('view_leilao', $valores['limit'], $valores['offset'])->result_array();
		return $this->removeNullSub($arrematados);
	}

	public function listarMeusArrematados($valores){
		$this->db->select('id_leilao, id_produto, nome_produto, descricao_produto, valor_minimo, lance_minimo, ingressos, data_inicio, data_fim_previsto, ifnull((select avaliacao from cad_avaliacoes ca where ca.fk_leilao = id_leilao), 0) avaliado,produto_usado, lance_formatado as ultimo_lance, nome_usuario_arrematou,
			(select email_usuario from seg_usuarios where id_usuario = fk_usuario_arrematou) as email_usuario_arrematou,
			(select telefone_usuario from seg_usuarios where id_usuario = fk_usuario_arrematou) as telefone_usuario_arrematou');
		$this->db->where('status_leilao', 4);
		$this->db->where('fk_usuario', $valores['id_usuario']);
		$this->db->order_by('data_inicio', 'desc');
		$arrematados = $this->db->get('view_leilao', $valores['limit'], $valores['offset'])->result_array();
		return $this->removeNullSub($arrematados);
	}

	public function listarAfiliados($valores) {

		$this->db->select("nome_usuario, date_format(criacao_usuario,'%d/%m/%Y às %H:%i:%s') as criado_em");
		$this->db->where('usuario_criou_usuario', $valores['id_usuario']);
		$this->db->order_by('criacao_usuario', 'desc');
		$arrematados = $this->db->get('seg_usuarios', $valores['limit'], $valores['offset'])->result_array();
		return $this->removeNullSub($arrematados);
	}

	//Não são os ingressos,
	public function getCreditos($id){
		$creditos = $this->db->query('select ifnull(creditos,0) as creditos from cad_creditos where fk_usuario = '.$id);
		if (isset($creditos) && !is_null($creditos) && $creditos->num_rows() == 1) {
			return $creditos->row()->creditos;
		} else {
			return 0;
		}
	}

	public function getLeiloesCategorias($valores) {

		$arvore = $this->arvore_categorias($valores['id_categoria']);

		$this->recursiva($arvore);


		$this->db->select(" id_leilao,
						    id_produto,
						    nome_produto,
						    descricao_produto,
						    valor_inicial_lance as valor_minimo,
						    lance_minimo,
						    ingressos,
						    data_inicio,
						    produto_usado,
						    data_fim_previsto,
						    nome_usuario,
						    estado_usuario,
                            CONCAT (cidade_usuario,' - ',ifnull((SELECT nome_item_grupo from cad_item_grupo where id_item_grupo = estado_usuario),''))as cidade_usuario,
                            ifnull((SELECT count(*) from cad_favoritos where id_leilao = fk_leilao and id_usuario = cad_favoritos.fk_usuario),0) as favorito,
						    ifnull((SELECT avg(avaliacao) from cad_avaliacoes where fk_leiloeiro = fk_usuario),0) as avaliacao");

		if(isset($valores['id_leilao'])){
		 	$this->db->where('id_leilao', $valores['id_leilao']);
		}

		$this->db->join('cad_produtos', 'cad_produtos.id_produto 	    = cad_leilao.fk_produto', 	'inner');
		$this->db->join('seg_usuarios', 'seg_usuarios.id_usuario 	    = cad_produtos.fk_usuario', 'inner');
       // $this->db->join('seg_usuarios', 'seg_usuarios.estado_usuario 	= cad_item_grupo.id_item_grupo', 'left');

        //$this->db->where('cad_produtos.fk_usuario <>', $valores['id_usuario']);
		$this->db->where('status_leilao', 2);
		$this->categorias[] = $valores['id_categoria']; // garante que a categoria recebida está na lista
		if($this->categorias != "" && $this->categorias != null && $this->categorias){
			$this->db->where_in('fk_categoria', $this->categorias);
		}

		$this->db->where('data_fim_previsto >= ', 'current_timestamp', false);

		$this->db->order_by('data_inicio', 'desc');
		$leiloes = $this->db->get('cad_leilao', $valores['limit'], $valores['offset'])->result_array();

		//echo $this->db->last_query();
		//die();

		return $this->removeNullSub($leiloes);
	}

	public function recursiva($array) {
		foreach ($array as $key => $value) {
			$this->categorias[] = $value['id_categoria'];
			$this->recursiva($value['subcategorias']);
		}
	}

	public function buscarLeiloes($valores) {

		$this->db->select(" id_leilao,
						    id_produto,
						    nome_produto,
						    descricao_produto,
						    valor_inicial_lance as valor_minimo,
						    lance_minimo,
						    ingressos,
						    data_inicio,
						    produto_usado,
						    data_fim_previsto,
						    nome_usuario,
						    estado_usuario,
						    cidade_usuario,
                            ifnull((SELECT count(*) from cad_favoritos where id_leilao = fk_leilao and id_usuario = cad_favoritos.fk_usuario),0) as favorito,
						    ifnull((SELECT avg(avaliacao) from cad_avaliacoes where fk_leiloeiro = fk_usuario),0) as avaliacao");

		if(isset($valores['id_leilao'])){
		 	$this->db->where('id_leilao', $valores['id_leilao']);
		}


		if(isset($valores['pago'])){
		     $pago = $valores['pago'];
		     $gratuito = $valores['gratuito'];

			if($pago == 1 && $gratuito == 1){
				 $this->db->where('ingressos >= 0');
		     }else if($pago == 1 && $gratuito != 1){
		     	$this->db->where('ingressos > 0');
		     }else if($pago != 1 && $gratuito == 1){
		     	$this->db->where('ingressos = 0');
		     }else{
		     	$this->db->where('ingressos >= 0');
		     }
		 }

		$this->db->join('cad_produtos', 'cad_produtos.id_produto 	= cad_leilao.fk_produto', 	'inner');
		$this->db->join('seg_usuarios', 'seg_usuarios.id_usuario 	= cad_produtos.fk_usuario', 'inner');
		$this->db->where('status_leilao', 2);
		$this->db->where('data_fim_previsto >=', 'current_timestamp', false); //unquoted
		//$this->db->where('cad_produtos.fk_usuario <>', $valores['id_usuario']);
		$this->db->where("(lower(nome_produto) LIKE lower('%{$valores['busca']}%') ESCAPE '!'
						OR  lower(descricao_produto) LIKE lower('%{$valores['busca']}%') ESCAPE '!' OR  lower(cidade_usuario) LIKE lower('%{$valores['busca']}%') ESCAPE '!')");
		$this->db->order_by('data_inicio', 'desc');
		$leiloes = $this->db->get('cad_leilao', $valores['limit'], $valores['offset'])->result_array();

		return $this->removeNullSub($leiloes);
	}

	public function historicoLances($valores){

		$this->db->select("	id_leilao,
						    nome_produto,
						    format(valor,2,'de_DE') as valor,
						    date_format(data_lance,'%d/%m/%Y às %H:%i:%s') as data");

		$this->db->where('id_usuario', $valores['id_usuario']);
		$this->db->like('nome_produto', $valores['nome_produto'], 'BOTH');


		$this->db->join('sala_leilao sl', 	'id_sala_leilao = fk_sala_leilao', 	'inner');
		$this->db->join('cad_leilao', 		'id_leilao = fk_leilao', 			'inner');
		$this->db->join('cad_produtos', 	'id_produto = fk_produto', 			'inner');
		$this->db->join('seg_usuarios', 	'id_usuario = sl.fk_usuario', 		'inner');

		$this->db->order_by('data_lance', 'desc');
		$this->db->order_by('valor', 'desc');
		$lances = $this->db->get('cad_lances', $valores['limit'], $valores['offset'])->result_array();

		return $this->removeNullSub($lances);

	}

	public function getArrematados() {

		/*No HAVING a sub-query traz o valor sem formatação*/
		$pendentes = $this->db->query("SELECT
			id_leilao, nome_produto, descricao_produto, id_produto,valor_minimo,
			format((select max(valor)
							from cad_lances
								inner join sala_leilao on fk_sala_leilao = id_sala_leilao
								where fk_leilao = id_leilao),2,'de_DE') as valor
				FROM cad_leilao cl
					inner join cad_produtos on id_produto = cl.fk_produto
					WHERE status_leilao > 1
						and status_leilao <> 4
						and data_fim_previsto < CURRENT_TIMESTAMP
						and fk_usuario_arrematou is null
						HAVING valor is not null
							and (select max(valor)
							from cad_lances
								inner join sala_leilao on fk_sala_leilao = id_sala_leilao
								where fk_leilao = id_leilao) >= cl.valor_minimo")->result();

		$arrematados = array();

		foreach($pendentes as $pendente){

			$usuario = $this->db->query("select id_usuario, email_usuario, nome_usuario
											from cad_lances
												inner join sala_leilao on fk_sala_leilao = id_sala_leilao
												inner join seg_usuarios on id_usuario = fk_usuario
													where fk_leilao = {$pendente->id_leilao}
														order by valor desc
														limit 1 ");

			log_message('debug',$this->db->last_query());

			if($usuario->num_rows() == 1){

				array_push($arrematados,array(
					'id_leilao' => $pendente->id_leilao,
					'nome_produto' => $pendente->nome_produto,
					'descricao_produto' => $pendente->descricao_produto,
					'id_produto' => $pendente->id_produto,
					'valor' => $pendente->valor,
					'id_usuario' => $usuario->row()->id_usuario,
					'email_usuario' => $usuario->row()->email_usuario,
					'nome_usuario' => $usuario->row()->nome_usuario
				));

				$this->db->query("update cad_leilao
									set status_leilao = 4,
									fk_usuario_arrematou = {$usuario->row()->id_usuario}
									where
										id_leilao = {$pendente->id_leilao}");

				log_message('debug',$this->db->last_query());

			}
		}

		return $arrematados;

	}

	######################################################
		//Editar Leilao
		######################################################
		public function editarLeilao($valores){
			$valores = $this->limpa_array($valores);
			$tabela = "cad_leilao";
			$id = 'id_leilao';
			$this->db->where(array($id => $valores[$id]));
			$this->db->update($tabela,$valores);
			return $this->verificarErros($this->db->error(),'Model_leiloes / editarLeilao');
		}

	public function cron_Push(){

		return $this->db->get("view_notificar_leiloes");

	}

	public function acetarRejeitar($aceitou,$leilao){
		return $this->db->query("update cad_leilao set status_leilao = {$aceitou} where id_leilao = {$leilao}");
	}

	public function view_novo_leiloes(){
		return $this->db->get_where('cad_produtos',array('fk_usuario' => $this->session->userdata('usuario')))->result();
	}

    public function view_dias(){
return "oi";
	}

	public function view_editar_leilao($where = null){

		$leilao = $this->db->select("*,round(lance_minimo,2) as lance_minimo,round(valor_minimo,2) as valor_minimo,round(valor_inicial_lance,2) as valor_inicial_lance , concat(date_format(data_inicio,'%d/%m/%Y %H:%i'),' até ',date_format(data_fim_previsto,'%d/%m/%Y %H:%i')) as periodo, altura,largura,comprimento,peso ")->get_where('cad_leilao', array('id_leilao' => $where[0]))->row();

		if (isset($leilao)) {
			foreach ($leilao as $key => $value) {
				$this->session->set_flashdata("{$key}_edicao",$value);
			}
		}

		return $this->db->get('cad_produtos')->result();
	}

	public function validandoPagamento($id_leilao){
		$this->db->query("update cad_leilao set finalizado = true where id_leilao = {$id_leilao}");
	}

	######################################################
	//Avaliar
	######################################################
	public function avaliar($dados){

		return $this->db->query("insert into cad_avaliacoes (fk_cliente,fk_leiloeiro,avaliacao,fk_leilao) values
									({$dados['id_usuario']},(select fk_usuario from cad_leilao inner join cad_produtos on id_produto = fk_produto where id_leilao = {$dados['id_leilao']}),{$dados['avaliacao']},{$dados['id_leilao']})");
	}

	public function notificado($id_leilao) {

		return $this->db->query("update cad_leilao set notificado = (ifnull(notificado,0)+1) where id_leilao = {$id_leilao}");

	}


    public function status_leilao(){

        return $this->db->query("UPDATE cad_leilao
                                SET    status_leilao = 4
                                WHERE `data_fim_previsto` <=  NOW() AND status_leilao = 2 AND `ingressos` = 0");
    }

    public function status_leilao_dias(){

      $leiloes = $this->db->query("SELECT id_leilao, fk_produto
                          FROM cad_leilao
                          WHERE data_fim_previsto BETWEEN DATE_SUB( NOW(), INTERVAL 30 DAY)
                          AND  NOW()
                          AND status_leilao = 2
                          AND `ingressos` > 0
                          AND `fk_usuario_arrematou` IS NULL;")->result();

      if(is_array($leiloes)){
          foreach ($leiloes as $leilao) {


              $sala = $this->db->query("SELECT *
                                           FROM sala_leilao
                                           WHERE `fk_leilao` =".$leilao->id_leilao)->result();

              $usuario = $this->db->query("SELECT *
                                           FROM seg_usuarios
                                           WHERE `id_usuario` =". $sala[0]->fk_usuario)->result();


              $address = $_SERVER['SERVER_NAME'].base_url();

              $produto = $this->db->query("SELECT *
                                            FROM cad_produtos
                                           WHERE `id_produto` =".$leilao->fk_produto)->result();
                  $titulo = "Atenção Leilao adiado";
                  $mensagem = "Esse leilão \"".$produto[0]->nome_produto."\" não atingiu o número mínimo de usuários. Foi adiado por mais 30 dias";

                  $curl = curl_init();
            // var_dump($mensagem) or die();
                  curl_setopt_array($curl,
                      array(
                          CURLOPT_URL => "http://{$address}controller_notificacoes/notificando",
                          CURLOPT_RETURNTRANSFER => true,
                          CURLOPT_ENCODING => "",
                          CURLOPT_MAXREDIRS => 10,
                          CURLOPT_TIMEOUT => 30,
                          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                          CURLOPT_CUSTOMREQUEST => "POST",
                          CURLOPT_POSTFIELDS => "usuarios={$usuario[0]->id_usuario}&titulo={$titulo}&mensagem={$mensagem}",
                          CURLOPT_HTTPHEADER => array(
                              "Content-Type: application/x-www-form-urlencoded",
                              "cache-control: no-cache"
                          ),
                      ));

                  $response = curl_exec($curl);
                  $err = curl_error($curl);

                  curl_close($curl);

                  //$link = 'http://'.$address.'controller_pagamentos/newPreferenceProduct/'.$usuario['id_usuario'].'/'.$leiloes.'"';

                  $emailCorpo = '
            <p>'.$titulo.'</p>
            <p>'.$mensagem.'</p>
            <table role="presentation" border="0" cellpadding="0" cellspacing="0" class="btn btn-primary">
              <tbody>
                <tr>
                  <td align="left">
                    <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                      <tbody>
                        <tr>
                         <!--<td> <a href="'."link".'" target="_blank">Pagar</a> </td>-->
                        </tr>
                      </tbody>
                    </table>
                  </td>
                </tr>
              </tbody>
            </table>';

                  $this->bodyEmail('Leilão '.$produto[0]->nome_produto.' Adiado',$emailCorpo,$usuario[0]->email_usuario,$usuario[0]->nome_usuario);
              }
          }
        if(is_array($leiloes)) {
            return $this->db->query("UPDATE cad_leilao
                                    SET    data_fim_previsto = DATE_ADD(data_fim_previsto, INTERVAL 30 DAY)
                                    WHERE data_fim_previsto BETWEEN DATE_SUB( NOW(), INTERVAL 30 DAY) AND  NOW()   AND status_leilao = 2 AND `ingressos` > 0  AND `fk_usuario_arrematou` IS NULL;");
        }

        return "ok";
    }

    public function bodyEmail($titulo,$corpo,$email,$nomeUsuario) {

        $address = $_SERVER['SERVER_NAME'].base_url();

        $html = '
        <!doctype html>
        <html>
          <head>
            <meta name="viewport" content="width=device-width" />
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
            <title>'.$titulo.'</title>
            <style>
              /* ------------------------------------- GLOBAL RESETS ------------------------------------- */ /*All the styling goes here*/ img{border: none; -ms-interpolation-mode: bicubic; max-width: 100%;}body{background-color: #f6f6f6; font-family: sans-serif; -webkit-font-smoothing: antialiased; font-size: 14px; line-height: 1.4; margin: 0; padding: 15; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;}table{border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;}table td{font-family: sans-serif; font-size: 14px; vertical-align: top;}/* ------------------------------------- BODY & CONTAINER ------------------------------------- */ .body{background-color: #f6f6f6; width: 100%;}/* Set a max-width, and make it display as block so it will automatically stretch to that width, but will also shrink down on a phone or something */ .container{display: block; margin: 0 auto !important; /* makes it centered */ max-width: 580px; padding: 10px; width: 580px;}/* This should also be a block element, so that it will fill 100% of the .container */ .content{box-sizing: border-box; display: block; margin: 0 auto; max-width: 580px; padding: 10px;}/* ------------------------------------- HEADER, FOOTER, MAIN ------------------------------------- */ .main{background: #ffffff; border-radius: 3px; width: 100%;}.wrapper{box-sizing: border-box; padding: 20px;}.content-block{padding-bottom: 10px; padding-top: 10px;}.footer{clear: both; margin-top: 10px; text-align: center; width: 100%;}.footer td, .footer p, .footer span, .footer a{color: #999999; font-size: 12px; text-align: center;}/* ------------------------------------- TYPOGRAPHY ------------------------------------- */ h1, h2, h3, h4{color: #000000; font-family: sans-serif; font-weight: 400; line-height: 1.4; margin: 0; margin-bottom: 30px;}h1{font-size: 35px; font-weight: 300; text-align: center; text-transform: capitalize;}p, ul, ol{font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; margin-bottom: 15px;}p li, ul li, ol li{list-style-position: inside; margin-left: 5px;}a{color: #3498db; text-decoration: underline;}/* ------------------------------------- BUTTONS ------------------------------------- */ .btn{box-sizing: border-box; width: 100%;}.btn > tbody > tr > td{padding-bottom: 15px;}.btn table{width: auto;}.btn table td{background-color: #ffffff; border-radius: 5px; text-align: center;}.btn a{background-color: #ffffff; border: solid 1px #3498db; border-radius: 5px; box-sizing: border-box; color: #3498db; cursor: pointer; display: inline-block; font-size: 14px; font-weight: bold; margin: 0; padding: 12px 25px; text-decoration: none; text-transform: capitalize;}.btn-primary table td{background-color: #3498db;}.btn-primary a{background-color: #27ae60; border-color: #27ae60; color: #ffffff;}/*------------------------------------- OTHER STYLES THAT MIGHT BE USEFUL ------------------------------------- */ .last{margin-bottom: 0;}.first{margin-top: 0;}.align-center{text-align: center;}.align-right{text-align: right;}.align-left{text-align: left;}.clear{clear: both;}.mt0{margin-top: 0;}.mb0{margin-bottom: 0;}.preheader{color: transparent; display: none; height: 0; max-height: 0; max-width: 0; opacity: 0; overflow: hidden; mso-hide: all; visibility: hidden; width: 0;}.powered-by a{text-decoration: none;}hr{border: 0; border-bottom: 1px solid #f6f6f6; margin: 20px 0;}/* ------------------------------------- RESPONSIVE AND MOBILE FRIENDLY STYLES ------------------------------------- */ table[class=body] h1{font-size: 28px !important; margin-bottom: 10px !important;}table[class=body] p, table[class=body] ul, table[class=body] ol, table[class=body] td, table[class=body] span, table[class=body] a{font-size: 16px !important;}table[class=body] .wrapper, table[class=body] .article{padding: 10px !important;}table[class=body] .content{padding: 0 !important;}table[class=body] .container{padding: 0 !important; width: 100% !important;}table[class=body] .main{border-left-width: 0 !important; border-radius: 0 !important; border-right-width: 0 !important;}table[class=body] .btn table{width: 100% !important;}table[class=body] .btn a{width: 100% !important;}table[class=body] .img-responsive{height: auto !important; max-width: 100% !important; width: auto !important;}/* ------------------------------------- PRESERVE THESE STYLES IN THE HEAD ------------------------------------- */ @media all{.ExternalClass{width: 100%;}.ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div{line-height: 100%;}.apple-link a{color: inherit !important; font-family: inherit !important; font-size: inherit !important; font-weight: inherit !important; line-height: inherit !important; text-decoration: none !important;}.btn-primary table td:hover{background-color: #34495e !important;}.btn-primary a:hover{background-color: #27ae40 !important; border-color: #27ae50 !important;}}
            </style>
          </head>
          <body width="100%">
            <table role="presentation" border="0" cellpadding="0" cellspacing="0" class="body" width="100%">
              <tr>
                <td>&nbsp;</td>
                <td class="container">
                  <div class="content">
                        <tr>
                            <td width="100%" align="center">
                                <img src="http://'.$address.'style/img/icon.png" width="100px" height="100px" style="border-radius: 10px;">
                            </td>
                        </tr>
        
                      <!-- START MAIN CONTENT AREA -->
                      <tr>
                        <td class="wrapper">
                          <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                            <tr>
                              <td>
                                <p>Olá '.$nomeUsuario.'.</p>
                                '.$corpo.'
                                <p>Esse e-mail é automático, qualquer dúvida entre em contato com nosso suporte <a href="mailto:sacleilao24h@gmail.com">sacleilao24h@gmail.com</a>.</p>
                                <p>Obrigado por utilizar nosso sistema!.</p>
                              </td>
                            </tr>
                          </table>
                        </td>
                      </tr>
        
                    <!-- END MAIN CONTENT AREA -->
                    </table>
        
                    <!-- START FOOTER -->
                    <div class="footer">
                      <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                          <td class="content-block" colspan="2">
                            <span class="apple-link"><a href="http://'.$address.'" target="_blank">Leilão 24H</a></span>
                          </td>
                        </tr>
                        <tr>
                          <td class="content-block powered-by" colspan="2">
                            Powered by <a href="http://megamil.net">Megamil.net</a>.
                          </td>
                        </tr>
                        <tr align="left">
                          <td class="content-block powered-by" colspan="2" align="left">
                            <small>Solicitado pelo IP: '.$_SERVER['REMOTE_ADDR'].'</small> <br>
                            <small>Sistema / Navegador: '.$_SERVER['HTTP_USER_AGENT'].'</small> <br>
                            <small>Em: '.date('d/m/Y H:i:s').'</small> <br>
                            <br>
                            <a href="http://'.$address.'privacidade">Política de privacidade</a> <br> 
                            <a href="http://'.$address.'">Termos de uso</a>
                          </td>
                        </tr>
        
                      </table>
                    </div>
                    <!-- END FOOTER -->
        
                  <!-- END CENTERED WHITE CONTAINER -->
                  </div>
                </td>
                <td>&nbsp;</td>
              </tr>
            </table>
          </body>
        </html>';

        // Detalhes do Email.
        $this->email->from('no-reply@leilao24h.com.br', 'Leilão 24H')
            ->to($email)
            ->subject($titulo)
            ->message($html);

        // Enviar...
        if ($this->email->send()) {

            return true;

        } else {
            log_message('error', 'Erro e-mail '.$this->email->print_debugger());
            return false;

        }

    }
}

/* End of file Model_leiloes.php */
/* Location: ./application/models/Model_leiloes.php */
