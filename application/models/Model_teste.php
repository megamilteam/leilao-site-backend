<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_teste extends MY_Model {

	public function insertTeste($valores = null) {
		$this->db->insert('teste', $valores);
		return $this->db->insert_id();
	}

	public function getTeste($valores = null) {
		$this->db->order_by($valores['sort'], $valores['order']);
		$this->db->limit($valores['limit'], $valores['offset']);
		$resultado = $this->db->get('teste');

		$this->db->where('id >', 90);
		$this->db->select('count(id) as total');
		$total = $this->db->get('teste')->row()->total;

		if ($resultado->num_rows() > 0) {
			return array(
					'total' => $total,
					'pagina_atual' => $valores['pagina'],
					'qtd_por_pagina' => $valores['limit'] == 0 ? $total : $valores['limit'],
					'resultado' => $resultado->result()
				);
		} else {
			return false;
		}
	}

}

/* End of file Model_teste.php */
/* Location: ./application/models/Model_teste.php */