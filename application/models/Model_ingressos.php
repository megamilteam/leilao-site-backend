<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_ingressos extends MY_Model {

	public function listarPacotes($valores) {
		
		$this->db->select('id_pacote_ingresso, descricao_pacote, quantidade_ingresso, round(valor_pacote, 2) valor_pacote');
		$this->db->where('gratis', 0);
		$this->db->where('status', 1);
		return $this->db->get('pacote_ingresso', $valores['limit'], $valores['offset'])->result_array();
	}

	public function getPacote($id_pacote) {
		
		$this->db->where('id_pacote_ingresso', $id_pacote);
		return $this->db->get('pacote_ingresso')->row_array();
	}

	public function getProduto($id_produto) {
		
		$this->db->where('id_produto', $id_produto);
		return $this->db->get('cad_produtos')->row_array();
	}
	
	public function debitarCredito($valor,$usuario){
		$this->db->query("update cad_creditos set creditos = (creditos-{$valor}) where fk_usuario = {$usuario}");
	}

	public function inserirRecompensa($dados){
		$this->db->insert('cad_compra_recompensa', $dados);
		return $this->db->insert_id();
	}

	public function insertIngresso($valores,$valor = null) {

		if (!$valores['gratis']) {
			
			$prioridade = $this->verifica_prioridade($valores['fk_usuario']);

			unset($valores['gratis']);

			$valores['prioridade'] = $prioridade['ultimo'] + 1;

			$this->db->query("update cad_creditos set creditos = (creditos+{$valores['quantidade']}) where fk_usuario = {$valores['fk_usuario']}");

		} else {

			$prioridade = $this->verifica_prioridade($valores['fk_usuario'], true);

			unset($valores['gratis']);

			$this->db->query("update cad_creditos set creditos = (creditos-{$valor}) where fk_usuario = {$valores['fk_usuario']}");

			$valores['prioridade'] = $prioridade['ultimo'] - 1;
		}

		$this->db->insert('cad_ingressos', $valores);

		return $this->db->insert_id();
	}

	public function insertCompra($valores) {

		$this->db->insert('compra_ingresso', $valores);
		return $this->db->insert_id();
	}

	function verifica_prioridade($id_usuario, $gratis = false) {

		$where = array(
			'ativo' => true,
			'gratis' => $gratis,
			'fk_usuario' => $id_usuario
		);

		$this->db->select('min(prioridade) primeiro, max(prioridade) ultimo');
		$this->db->where($where);
		// $this->db->order_by('prioridade', 'asc');
		return $this->db->get('view_ingresso')->row_array();
	}

	public function updateIngresso($valores) {
		$this->db->where('id_ingresso', $valores['id_ingresso']);
		return $this->db->update('cad_ingressos', $valores);
	}

	public function contaIngressos($id_usuario) {

		$where = array(
			'ativo' => true,
			'quantidade >' => 0,
			'fk_usuario' => $id_usuario
		);

		$this->db->select('count(*) as linhas,ifnull(sum(quantidade),0) quantidade');
		$this->db->where($where);
		return $this->db->get('view_ingresso')->row()->quantidade;

	}

	public function cobrarIngresso($id_usuario, $id_leilao) {

		$total = $this->contaIngressos($id_usuario); // busca total de ingressos do usuário

		$this->db->where('id_leilao', $id_leilao);
		$leilao = $this->db->get('cad_leilao')->row(); // busca quantidade de ingressos para entrar na sala

		$a_cobrar = $leilao->ingressos;

		if($a_cobrar == 0) //Caso de leilões gratuítos.
			return true;

		if ($total < $a_cobrar) { // verifica se total de ingressos é menor que a quantidade de ingressos para entrar na sala
			
			return false;

		} else { //se a quantidade for suficiente

			$ingressos = $this->buscaIngressos($id_usuario);

			foreach ($ingressos as $key => $ingresso) { //percorre os ingressos de acordo a prioridade
 				
 				if ($ingresso->quantidade < $a_cobrar) { //se não for suficiente
					
					$a_cobrar = $a_cobrar - $ingresso->quantidade; // abate do total a ser cobrado
					$this->updateIngresso(array('id_ingresso' => $ingresso->id_ingresso, 'quantidade' => 0 )); //zera

				} else { // se for suficiente abate do saldo

					$this->updateIngresso(array('id_ingresso' => $ingresso->id_ingresso, 'quantidade' => $ingresso->quantidade - $a_cobrar));
					return true;
				}
			}

		}
	}

	function buscaIngressos($id_usuario) {

		$where = array(
			'ativo' => true,
			'quantidade >' => 0,
			'fk_usuario' => $id_usuario
		);

		$this->db->select('id_ingresso, quantidade, prioridade');
		$this->db->where($where);
		$this->db->order_by('gratis', 'desc');
		$this->db->order_by('prioridade', 'asc');
		return $this->db->get('view_ingresso')->result();
	}

	public function entrarSala($id_usuario, $id_leilao) {
		
		$this->db->insert('sala_leilao', array('fk_leilao' => $id_leilao, 'fk_usuario' => $id_usuario));
	
		return $this->db->query("SELECT  
									(count(*) = (select quantidade_minima_usuarios from cad_leilao where id_leilao = fk_leilao)) as atingido,
									tempo_iniciar,
									nome_produto
								    from sala_leilao 
								    inner join cad_leilao on id_leilao = fk_leilao
								    inner join cad_produtos on id_produto = fk_produto
								    	where fk_leilao = {$id_leilao}
								    		group by tempo_iniciar, nome_produto")->row();

	}

	public function novoTempoFim($id_leilao){
		$this->db->query("UPDATE cad_leilao 
							set data_fim_previsto = date_add(current_timestamp,interval (tempo_iniciar*60) second)
						        where id_leilao = {$id_leilao}");
	}

	public function listarUsuarios($fk_leilao){

		$usuarios = $this->db->query("SELECT fk_usuario from sala_leilao where fk_leilao = {$fk_leilao}")->result();
		$retornar = "";
		foreach ($usuarios as $key => $usuario) {
			if($key > 0) {
				$retornar = "{$retornar},{$usuario->fk_usuario}";
			} else {
				$retornar = "{$usuario->fk_usuario}";
			}
		}
		return $retornar;

	}

}

/* End of file Model_ingressos.php */
/* Location: ./application/models/Model_ingressos.php */