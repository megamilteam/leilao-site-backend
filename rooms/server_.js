const ambiente  = require('./config/ambiente.js').getAmbiente();
const hostname = ambiente['ambienteLiteral'];
console.log("Server.js usando: "+hostname);

const app 		= require('express')();
const http 		= require('http').createServer(app);
const io		= require('socket.io')(http);
const port   	= 9800;
const request 	= require("request");
const connectionMiddleware = require('./config/connection-middleware');
const bodyparser = require('body-parser');

const EventEmitter = require('events');
const myEmitter = new EventEmitter();
myEmitter.setMaxListeners(0);

var users = 0;

var pool = require('./config/pool-factory.js');
app.use(bodyparser.json());

app.use(bodyparser.urlencoded({ extended: false }));

// ativando nosso middleware
app.use(connectionMiddleware(pool));
app.get('/leilao', (req, res) => {
	res.sendFile(__dirname+'/client.html');
});

function sleep(milliseconds) {
	var start = new Date().getTime();
	for (var i = 0; i < 1e7; i++) {
		if ((new Date().getTime() - start) > milliseconds) {
			break;
		}
	}
}
// middleware de tratamento de erro
app.use((err, req, res, next) => {
	console.error(err.stack);
	res.status(500).json({ error: err.toString() });
});

io.on('connect',(socket) => {

	var userName = false;
	var userID = false;
	var room = false;
	var valor_minimo = false;
	var valor_atual = false;
	var lance_minimo = false;
	var data_inicio = false;
	var data_fim_previsto = false;
	var fim_leilao = false;
	var inicio_leilao = false;
	var data_fim_efetivo = false;
	var sala_leilao = false;
	var token = false;
	var por_usuarios = false;
	var cronometro = false;

	// var availableRooms = [];
	// var rooms = io.sockets.adapter.rooms;
	// if (rooms) {
	//     for (var room in rooms) {
	//         if (!rooms[room].hasOwnProperty(room)) {
	//             availableRooms.push(room);
	//             //console.log(quando()+room);
	//         }
	//     }
	// }

	//Avisa sobre o fim do leilão
	function bidEnd(){

		console.log(quando()+"bidEnd");

		pool.getConnection((err, connection)=>{

			if (err) console.log(quando()+err);

			var detalhes = "select format(valor,2,'de_DE') as lance_formatado,valor, nome_usuario, id_usuario from cad_lances inner join sala_leilao on fk_sala_leilao = id_sala_leilao inner join seg_usuarios on id_usuario = fk_usuario where fk_leilao = "+room+" order by valor desc limit 1"; //buscaR dados do leilão e da sala
			console.log(quando()+detalhes);

			connection.query(detalhes, (err, data)=>{ // carrega variaveis com dados vindos da busca

				if (err) console.log(quando()+err.sqlMessage)
				else {
				//	console.log(quando()+data);

					if(data.length > 0){

						var arrematado = data[0].nome_usuario+' deu o lance de R$ '+data[0].lance_formatado+" e arrematou o leilão!";

						io.sockets.in(room).emit('newBid', arrematado);	//informa o lance vencedor

						vencedor(data[0].id_usuario,room,data[0].valor);

						if(data[0].id_usuario == userID){

							socket.emit('winner', "Parabéns você arrematou o produto",data[0].id_usuario,room);	//informa os últimos 5 lances
							console.log(quando()+'id = '+data[0].id_usuario+' socket id = '+userID, +' leilao id = '+room);
						} else {
							socket.emit('winner', arrematado);	//informa os últimos 5 lances
						}
					}

				}
			})
				.on('end', function() {

					//socket.disconnect();
					console.log(quando()+"bidEnd - end");
					connection.release();

				});

		});

	}

	function vencedor(id_usuario,leilao,valor) {

		console.log("vencedor");

		pool.getConnection((err, connection)=>{

			if (err) console.log(quando()+err);

			connection.beginTransaction((err)=>{

				var query = "UPDATE cad_leilao SET status_leilao = 4, data_fim_efetivo = now(), fk_usuario_arrematou = ? WHERE id_leilao = ? and status_leilao <> 4 and valor_minimo <= ?";
				console.log(query);

				connection.query(query, [id_usuario,leilao,valor], (err, result)=>{}) // query

				connection.commit(function(err){
					if (err){
						connection.rollback();
						connection.release();
						console.log("Vencedor não armazenado");
					} else {
						console.log("Vencedor armazenado");
						connection.release();
					}
				}) // commit
			}) // beginTransaction
		}); // getConnection

	}

	socket.on('bidEnd', ()=>{

		pool.getConnection((err, connection)=>{

			if (err) console.log(quando()+err);

			var detalhes = "select format(valor,2,'de_DE') as lance_formatado,valor, nome_usuario, id_usuario from cad_lances inner join sala_leilao on fk_sala_leilao = id_sala_leilao inner join seg_usuarios on id_usuario = fk_usuario where fk_leilao = "+room+" order by valor desc limit 1"; //buscaR dados do leilão e da sala
			console.log(quando()+"detalhes: "+detalhes);

			connection.query(detalhes, (err, data)=>{ // carrega variaveis com dados vindos da busca

				if (err) console.log(quando()+err.sqlMessage)
				else {
					//console.log(quando()+data);

					if(data.length > 0){

						var arrematado = data[0].nome_usuario+' deu o lance de R$ '+data[0].lance_formatado+" e arrematou o leilão!";

						io.sockets.in(room).emit('newBid', arrematado);	//informa o lance vencedor

						vencedor(data[0].id_usuario,room,data[0].valor);

						if(data[0].id_usuario == userID){
							fim_leilao = 0;

							//socket.emit('winner', "Parabéns você arrematou o produto");	//informa os últimos 5 lances
							//console.log(quando()+'id = '+data[0].id_usuario+' socket id = '+userID);
							socket.emit('winner', "Parabéns você arrematou o produto",data[0].id_usuario,room);	//informa os últimos 5 lances
							console.log(quando()+'id = '+data[0].id_usuario+' socket id = '+userID, +' leilao id = '+room);
							console.log(quando()+"winner Parabéns você arrematou o produto");	//informa os últimos 5 lances
							bidEnd();
						} else {
							socket.emit('winner', arrematado);	//informa os últimos 5 lances
						}


					}

				}
			})
				.on('end', function() {
					//socket.disconnect();
					console.log(quando()+"bidEnd - end");
					connection.release();
				});

		});

	});

	function ultimosLances() {

		console.log(quando()+"últimos lances");

		pool.getConnection((err, connection)=>{

			var calcLance = "select format(valor,2,'de_DE') as lance_formatado, nome_usuario from cad_lances inner join sala_leilao on fk_sala_leilao = id_sala_leilao inner join seg_usuarios on id_usuario = fk_usuario where fk_leilao = "+room+" order by valor desc limit 5"; //busca dados do leilão e da sala

			console.log(quando()+calcLance);

			connection.query(calcLance, (err, data)=>{ // carrega variaveis com dados vindos da busca

				if (err) console.log(quando()+err.sqlMessage)
				else {
					//console.log(quando()+data);

					if(data.length > 0){

						for (var i = data.length - 1; i >= 0; i--) {

							socket.emit('initRoom', data[i].nome_usuario+' deu o lance de R$ '+data[i].lance_formatado);	//informa os últimos 5 lances
							console.log(quando()+data[i].nome_usuario+' deu o lance de R$ '+data[i].lance_formatado);
						}

					}

					console.log(quando()+"últimos lances enviados");

				}
			})
				.on('end', function() {

					// se tentar conectar antes do horario de inicio desconecta cliente
					// se não vieram dados na busca desconecta cliente

					console.log(quando()+">>>>>>>"+!room +" || "+ inicio_leilao +"  > 0 || "+fim_leilao+" <= 0 ");
					if (!room || inicio_leilao > 0 || fim_leilao <= 0) {

							console.log(quando()+"Deveria chamar o bidEND");
							bidEnd();
					}

					console.log(quando()+"últimos lances END");
					connection.release();
				});

		});

	}

	function startRoom(){

		console.log(quando()+users+" usuários na room "+room+ " Usuário: "+ userName+' entrou no leilão')

		socket.join(room);
		io.sockets.in(room).emit('newUser', 	userName+' entrou no leilão');	//informa para todos usuários que usuário entrou
		//console.log(quando()+userName+' entrou no leilão, valor atual: '+valor_atual);
		socket.emit('currentBid', 		valor_atual);	//informa para o novo usuário o lance mínimo atual
		socket.emit('dateStart', 		inicio_leilao);	//informa para o novo usuário o tempo que falta para inicio do leilão

		console.log(quando()+"inicio_leilao:"+ inicio_leilao);
		console.log(quando()+"cronometro:"+cronometro);


		if(!por_usuarios){
			io.sockets.in(room).emit('dateEnd', 		fim_leilao); 	//informa para o novo usuário o tempo que falta para o fim do leilão
		} else {
			socket.emit('cronometro', 	cronometro); 	//informa o tempo para ficar congelado, caso seja sala por usuários
			socket.emit('por_usuarios',	por_usuarios); 	//informa se a sala é por número de usuários
		}

		console.log(quando()+"startRoom END");

	}

	socket.on('joinRoom', (token_acesso,id_leilao)=>{

		console.log(quando()+'joinRoom');

		if(id_leilao != null && id_leilao != ""
			&& token_acesso != null && token_acesso != ""){

			var creditos ="select ifnull(sum(quantidade),0) quantidade from view_ingresso where  fk_usuario = "+userID;

			var query = `
			SELECT id_sala_leilao, id_usuario, nome_usuario, id_leilao, valor_inicial_lance as valor_minimo,
				format(valor_atual,2,'de_DE') valor_atual, lance_minimo, data_inicio, data_fim_previsto, token_acesso, 
				timestampdiff(SECOND, current_timestamp, data_inicio) as inicio, fim, 
				(quantidade_minima_usuarios > 0 and quantidade_minima_usuarios > (select count(*) 
			FROM sala_leilao 
			WHERE fk_leilao = id_leilao) AND (timestampdiff(SECOND, current_timestamp, data_fim_previsto) > (tempo_iniciar*60))) as por_usuarios, 
				(tempo_iniciar*60) as cronometro 
			FROM view_usuario_leilao
			WHERE token_acesso = \"`+token_acesso+`\" AND id_leilao = `+id_leilao +" LIMIT 1" ; //busca dados do leilão e da sala

			console.log(quando()+query);

			pool.getConnection(async(err, connection)=>{
				console.log(quando()+' passou do getConnection no joinRoom');
				//sleep(2000);

				if (err) console.log(quando()+err);

				console.log(quando()+' passou do if do getConnection no joinRoom');

				await connection.query(query,  (err, data)=>{ // carrega variaveis com dados vindos da busca

					console.log(quando()+' passou query getConnection no joinRoom');

					if (err){ console.log(quando()+err.sqlMessage)}
					else {
						console.log(quando()+' passou if query getConnection no joinRoom');
						console.log(quando()+JSON.stringify(data[0]));

						if(data.length > 0){

							users++;
							sala_leilao 	  = data[0].id_sala_leilao;
							userName 		  = data[0].nome_usuario;
							room 			  = data[0].id_leilao;
							valor_minimo 	  = data[0].valor_minimo;
							valor_atual		  = data[0].valor_atual;
							lance_minimo 	  = data[0].lance_minimo;
							data_inicio 	  = data[0].data_inicio;
							data_fim_previsto = data[0].data_fim_previsto;
							userID			  = data[0].id_usuario;

							inicio_leilao	  = data[0].inicio;
							fim_leilao		  = data[0].fim;
							por_usuarios	  = data[0].por_usuarios;
							cronometro		  = data[0].cronometro;

							token = token_acesso;
						}
					}
				})
					.on('end', function() {
						connection.release();
						if(userName){
							startRoom();
							ultimosLances();
							console.log(quando()+'token recebido: '+token_acesso+" usuário "+userName+' id_leilao: '+id_leilao);
						} else {
							console.log(quando()+token_acesso+" Não irá conectar");
							startRoom();
						}
					})
			});

		}

	});


	socket.on('bid', ()=>{

		console.log(quando()+'bid');

		var lance;
		var lance_formatado;

		pool.getConnection((err, connection)=>{

			if (err) console.log(quando()+err);

			connection.beginTransaction((err)=>{

				var calcLance = "select ifnull(format((max(valor) + "+lance_minimo+"),2,'de_DE'),format(("+valor_minimo+" + "+lance_minimo+"),2,'de_DE')) as lance_formatado, ifnull((max(valor) + "+lance_minimo+"),("+valor_minimo+" + "+lance_minimo+")) as lance, timestampdiff(SECOND, CURRENT_TIMESTAMP, data_fim_previsto) as fim from cad_lances inner join sala_leilao on fk_sala_leilao = id_sala_leilao inner join cad_leilao on id_leilao = fk_leilao where fk_leilao = "+room+" limit 1"; //busca dados do leilão e da sala

				//console.log(quando()+calcLance);

				connection.query(calcLance, (err, data)=>{ // carrega variaveis com dados vindos da busca

					if (err) console.log(quando()+err.sqlMessage)
					else {
						console.log(quando()+"retorno lance: "+JSON.stringify(data[0]));

						if(data.length > 0){

							lance 			= data[0].lance;
							lance_formatado = data[0].lance_formatado;
							fim_leilao		= data[0].fim;

						} else {
							lance 			= valor_atual;
							lance_formatado = valor_atual;
						}
					}
				})
					.on('end', function() {

						var sqlNotificar = "select sl.fk_usuario, nome_produto, valor from cad_lances inner join sala_leilao sl on fk_sala_leilao = id_sala_leilao inner join cad_leilao cl on id_leilao = fk_leilao inner join cad_produtos on id_produto = fk_produto where sl.fk_leilao = "+room+" order by valor desc limit 1";

						console.log(sqlNotificar);

						connection.query(sqlNotificar, (err, data)=>{ // carrega variaveis com dados vindos da busca

							if (err) console.log(quando()+err.sqlMessage)
							else {

								if(data.length > 0){
									if(data[0].fk_usuario != userID){

										var id 		= data[0].fk_usuario;
										var produto = data[0].nome_produto;

										var titulo 		= "Lance coberto";
										var mensagem 	= "Seu lance no produto "+produto+" foi coberto!";

										console.log('CHAMANDO: http://'+hostname+'controller_notificacoes/notificando?usuarios='+id+'&titulo='+titulo+'&mensagem='+mensagem+'&id_leilao='+room);
										request.get('http://'+hostname+'controller_notificacoes/notificando?usuarios='+id+'&titulo='+titulo+'&mensagem='+mensagem+'&id_leilao='+room);

									}
								}

							}

						})
							.on('end', function() {

								var query = 'INSERT INTO cad_lances (valor, data_lance, fk_sala_leilao) VALUES (?, current_timestamp, ?)';

								connection.query(query, [lance, sala_leilao], (err, result)=>{

									if (err) {
										console.log(quando()+err.sqlMessage);
										console.log(quando()+err.sql);
										connection.rollback();

									} else {
										//console.log(quando()+'INSERT LANCE OK');
									}
								})

							});


					});


				// var query = 'UPDATE cad_leilao SET valor_atual = ? WHERE id_leilao = ?';

				// connection.query(query, [lance, room], (err, result)=>{

				// 	if (err) {
				// 		//console.log(quando()+err.sqlMessage);
				// 		//console.log(quando()+err.sql);
				// 		connection.rollback();

				// 	} else {
				// 		//console.log(quando()+'UPDATE OK');
				// 	}
				// })

				var query = 'SELECT valor_atual, data_fim_previsto, fim FROM view_usuario_leilao WHERE token_acesso = '+token; //busca dados do leilão e da sala

				connection.query(query, (err, data)=>{ // carrega variaveis com dados vindos da busca

					if (err) {

						//console.log(quando()+err.sqlMessage);
						//console.log(quando()+err.sql);
						connection.rollback();

					} else {
						console.log(quando()+'SELECT OK');

						valor_atual		  = data[0].valor_atual;
						data_fim_previsto = data[0].data_fim_previsto;
						fim_leilao		  = data[0].fim;

					}
				})

					.on('end', function() {

						if (fim_leilao <= 50 && fim_leilao > -7) {

							var query = "UPDATE cad_leilao SET data_fim_previsto = ADDTIME(CURRENT_TIMESTAMP, '00:00:50') WHERE id_leilao = ?";
							fim_leilao = 50;

							//console.log(quando()+"-----------"+query+" Sala "+room+" usuario "+userName);

							connection.query(query, [room], (err, result)=>{

								if (err) {
									//console.log(quando()+err.sqlMessage);
									//console.log(quando()+err.sql);
									connection.rollback();

								} else {
									console.log(quando()+"Aumentar 50 Segundos");
									io.sockets.in(room).emit('dateEnd', 	fim_leilao);
									//console.log(quando()+'########## UPDATE 2 OK'+fim_leilao);
								}

							})

						} else {
							console.log(quando()+'Tempo restante '+fim_leilao);
						}
					})

				connection.commit(function(err){
					if (err){
						connection.rollback();
						connection.release();
					} else {


						var sqlNotificar = `SELECT id_usuario as fk_usuario, nome_produto FROM db_leilao.seg_usuarios su 
												INNER JOIN db_leilao.sala_leilao sl ON fk_usuario = id_usuario  
												INNER JOIN db_leilao.cad_leilao cl ON id_leilao = fk_leilao
												INNER JOIN  db_leilao.cad_produtos ON id_produto = fk_produto  
												WHERE sl.fk_leilao =` +room;

						//	console.log(sqlNotificar);


						//	var lance_dado = userName+' deu o lance '+produto+' de R$ '+lance_formatado;

						var lance_dado = userName+' deu o lance de R$ '+lance_formatado;


						connection.query(sqlNotificar, (err, data)=>{ // carrega variaveis com dados vindos da busca

							if (err)
								console.log(quando()+err.sqlMessage)
							else {
								if(data.length > 0){
									for(var i = 0; i < data.length;i++){
										if(data[i].fk_usuario != userID){

											var id 	   = data[i].fk_usuario;
											var produto = data[i].nome_produto;

											var titulo 		= produto;
											var mensagem 	= lance_dado;//"Seu lance no produto "+produto+" foi coberto!";

											console.log('CHAMANDO: http://'+hostname+'controller_notificacoes/notificando?usuarios='+id+'&titulo='+titulo+'&mensagem='+mensagem+'&id_leilao='+room);
											request.get('http://'+hostname+'controller_notificacoes/notificando?usuarios='+id+'&titulo='+titulo+'&mensagem='+mensagem+'&id_leilao='+room);
										}
									}
								}
							}
						})

						console.log(quando()+lance_dado)
						io.sockets.in(room).emit('currentBid', 	lance_formatado);	//informa para todos usuários novo lance minimo
						io.sockets.in(room).emit('newBid', 	  	lance_dado);	//informa o último lance para todos da sala
						connection.release();
					}
				})
			})

			//connection.release();
		})

	});

	socket.on('disconnect', function(){
		ultimosLances();
		socket.disconnect();
		if(userName){
			console.log(quando()+userName+' saiu');
			users--;
			if(users == 0) {
				console.log(quando()+'removeAllListeners 0 usuários');
				socket.removeAllListeners();
			}
			console.log(quando()+users+" usuários na room "+room)
		} else {
			//console.log('=== Usuário false ===');
		}

	});

	//Possíveis erros
	socket.on('Connect', function() {
		//socket.disconnect();
		console.log(quando()+userName+" − When the client successfully connects.");
	});

	socket.on('Connecting', function() {
		//socket.disconnect();
		socket.removeAllListeners('Connecting');
		console.log(quando()+userName+" − When the client is in the process of connecting.");
	});

	socket.on('Disconnect', function() {
		//socket.disconnect();
		socket.removeAllListeners('Disconnect');
		console.log(quando()+userName+" − When the client is disconnected.");
	});

	socket.on('Connect_failed', function() {
		//socket.disconnect();
		socket.removeAllListeners('Connect_failed');
		console.log(quando()+userName+" − When the connection to the server fails.");
	});

	socket.on('Error', function() {
		//socket.disconnect();
		socket.removeAllListeners('Error');
		console.log(quando()+userName+" − An error event is sent from the server.");
	});

	socket.on('Message', function() {
		//socket.disconnect();
		socket.removeAllListeners('Message');
		console.log(quando()+userName+" − When the server sends a message using the send function.");
	});

	socket.on('Reconnect', function() {
		//socket.disconnect();
		socket.removeAllListeners('Reconnect');
		console.log(quando()+userName+" − When reconnection to the server is successful.");
	});

	socket.on('Reconnecting', function() {
		//socket.disconnect();
		socket.removeAllListeners('Reconnecting');
		console.log(quando()+userName+" − When the client is in the process of connecting.");
	});

	socket.on('Reconnect_failed', function() {
		//socket.disconnect();
		socket.removeAllListeners('Reconnect_failed');
		console.log(quando()+userName+" − When the reconnection attempt fails.");
	});

	function quando(){
		// Obtém a data/hora atual
		var data = new Date();

		// Guarda cada pedaço em uma variável
		var dia     = data.getDate();           // 1-31
		var dia_sem = data.getDay();            // 0-6 (zero=domingo)
		var mes     = data.getMonth();          // 0-11 (zero=janeiro)
		var ano2    = data.getYear();           // 2 dígitos
		var ano4    = data.getFullYear();       // 4 dígitos
		var hora    = data.getHours();          // 0-23
		var min     = data.getMinutes();        // 0-59
		var seg     = data.getSeconds();        // 0-59
		var mseg    = data.getMilliseconds();   // 0-999
		var tz      = data.getTimezoneOffset(); // em minutos

		// Formata a data e a hora (note o mês + 1)
		var str_data = dia + '/' + (mes+1) + '/' + ano4;
		var str_hora = hora + ':' + min + ':' + seg;

		return str_data + ' às ' + str_hora +" ";
	}


}); // io.on('connection') - FIM

http.listen(port, function(){
	console.log('Listening on port '+port)
});
