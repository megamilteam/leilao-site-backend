const ambiente  = require('./ambiente.js').getAmbiente();
const mysql     = require('mysql');

if(ambiente['ambienteCode'] == 1){
    console.log("producao")
    host      = 'localhost';
    user      = 'leilao24h';
    password  = 'l31l4024h#';
    database  = 'db_leilao';
} else if (ambiente['ambienteCode'] == 2) {
    console.log("homologacao")
    host      = 'localhost';
    user      = 'root';
    password  = 'projetos#2018&mysql';
    database  = 'db_leilao';
} else if (ambiente['ambienteCode'] == 3) {
    console.log("local")
    host      = '192.168.10.10';
    user      = 'homestead';
    password  = 'secret';
    database  = 'db_leilao';
} else {
    console.log("Falhou ao encontrar um ambiente: ");
    return;
}

const pool = mysql.createPool({
    //'connectionLimit' : 5,
    //connectTimeout: 120000,
   // debug:true,
    'host'          : host,
    'user'          : user,
    'password'      : password,
    'database'      : database,
    insecureAuth    : false,
    queueLimit : 0, // unlimited queueing
    connectionLimit : 0, // unlimited connections
    waitForConnections: true

});

console.log('pool => criado');

pool.on('release', () => console.log('POOL => Release Chamado!'));

process.on('SIGINT', () =>
    pool.end(err => {
        if(err) return console.log(err);
        console.log('POOL => Encerrado');
        process.exit(0);
    })
);

module.exports = pool;

