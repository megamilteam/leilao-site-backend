const ambiente  = require('../config/ambiente.js').getAmbiente();
const hostname = ambiente['ambienteLiteral'];
console.log("cron_pagamentos.js usando: "+hostname);

const CronJob 	= require('cron').CronJob
const request 	= require("request");

/*

	Seconds: 0-59
	Minutes: 0-59
	Hours: 0-23
	Day of Month: 1-31
	Months: 0-11 (Jan-Dec)
	Day of Week: 0-6 (Sun-Sat)

*/

const job = new CronJob('0 0 * * * *', () => {

	escrever();

}, null, true, 'America/Sao_Paulo');

job.start();

var fs = require('fs');

function escrever(){
	// Obtém a data/hora atual
	var data = new Date();

	// Guarda cada pedaço em uma variável
	var dia     = data.getDate();           // 1-31
	var dia_sem = data.getDay();            // 0-6 (zero=domingo)
	var mes     = data.getMonth();          // 0-11 (zero=janeiro)
	var ano2    = data.getYear();           // 2 dígitos
	var ano4    = data.getFullYear();       // 4 dígitos
	var hora    = data.getHours();          // 0-23
	var min     = data.getMinutes();        // 0-59
	var seg     = data.getSeconds();        // 0-59
	var mseg    = data.getMilliseconds();   // 0-999
	var tz      = data.getTimezoneOffset(); // em minutos

	// Formata a data e a hora (note o mês + 1)
	var str_data = dia + '/' + (mes+1) + '/' + ano4;
	var str_hora = hora + ':' + min + ':' + seg;

	fs.appendFile("file.txt", 'Chamado em ' + str_data + ' às ' + str_hora +"\n", function(erro) {

	    if(erro) {
	        throw erro;
	    }

	    //Consultando o mercado pago de hora em hora
	    console.log('Chamando em ' + str_data + ' às ' + str_hora +"\n"+' URL: http://'+hostname+'controller_pagamentos/forcarMultiploEnvio')

		request.get('http://'+hostname+'Controller_leilao/arrematados');

	});
}

